#include "stdafx.h"
#include "RaceWrap.h"

struct Race
{
	uint32_t formID = 0;
};

std::map<uint32_t, Race *> createdRaces;

struct RaceWrap::Impl
{
	Race *race = nullptr;
};

RaceWrap::RaceWrap(uint32_t formID) noexcept : pImpl(new Impl)
{
	try {
		pImpl->race = createdRaces.at(formID);
	}
	catch (...) {
		pImpl->race = new Race;
		pImpl->race->formID = formID;
		createdRaces.insert({ formID, pImpl->race });
	}
}

RaceWrap::~RaceWrap() noexcept
{
}

bool RaceWrap::operator==(const RaceWrap &rhs) const noexcept
{
	return this->pImpl->race == rhs.pImpl->race;
}

uint32_t RaceWrap::GetID() const noexcept
{
	return pImpl->race->formID;
}

void RaceWrap::Register(lua_State *L)
{
	luabridge::getGlobalNamespace(L)
		.beginClass<RaceWrap>("Race")
		.addConstructor<void(*)(uint32_t)>()
		.addFunction("__eq", &RaceWrap::operator==)
		.addFunction("GetID", &RaceWrap::GetID)
		.endClass();
}