﻿#include "stdafx.h"
#include "Player.h"
#include "Object.h"
#include "DataSearch.h"
#include "IDAssigner.h"
#include "PacketTypes.h"
#include <RakNetTypes.h>

namespace StackOverflow
{
	using namespace std;

	// https://stackoverflow.com/questions/5607589/right-way-to-split-an-stdstring-into-a-vectorstring
	vector<string> split(string str, string token) {
		vector<string>result;
		while (str.size()) {
			const auto index = str.find(token);
			if (index != string::npos) {
				result.push_back(str.substr(0, index));
				str = str.substr(index + token.size());
				if (str.size() == 0)result.push_back(str);
			}
			else {
				result.push_back(str);
				str = "";
			}
		}
		return result;
	}
}

inline void WriteEsp(RakNet::BitStream &bsOut) noexcept
{
	try {
		const auto pluginsStr = g.cfg->GetValue<std::string>("Gameplay.plugins", "");
		auto plugins = StackOverflow::split(pluginsStr, ",");
		log(Log::Debug, "Loading .esp files");
		bsOut.Write((uint64_t)plugins.size());
		for (auto plugin : plugins)
		{
#ifndef _WIN32
			plugin = current_directory() + "/" + plugin;
#endif
			auto input = std::ifstream(plugin, std::ios::binary);
			const auto buffer = std::vector<char>((
				std::istreambuf_iterator<char>(input)),
				(std::istreambuf_iterator<char>()));
			bsOut.Write((uint64_t)buffer.size());
			for (auto ch : buffer)
			{
				bsOut.Write(ch);
			}
			log(Log::Debug, "Loading ", plugin.data(), "(", buffer.size(), " bytes)");
		}
		log(Log::Debug, "Loaded ", plugins.size(), " files");
	}
	catch (const std::exception &e) {
		log(Log::Error, "WriteEsp() ", e.what());
	}
}

inline bool IsMessageOk(const std::string &msg)
{
	bool res = true;
	for (char ch : msg)
	{
		static const char templ[] = "!@$%^&*()_+-=/~`';,.[]{} :\"<>|?";
		static std::set<char> set = { std::begin(templ), std::end(templ) };
		res = res
			&& ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9') || set.count(ch) != 0);
	}
	return res;
}

void TestIsMessageOk()
{
	if (IsMessageOk("абв") != false)
	{
		log(Log::Fatal, "Test failed - IsMessageOk() - 1");
		std::exit(-1);
	}
	if (IsMessageOk("asdasd123<>123!@ ! ! ||000()()()((---* k a k") != true)
	{
		log(Log::Fatal, "Test failed - IsMessageOk() - 2");
		std::exit(-1);
	}
}

struct Networking::Impl
{
	RakNet::RakPeerInterface *peer;
	std::array<bool, MAX_PLAYERS> allowUpdateMovement;
};

Networking::Networking(RakNet::RakPeerInterface *peer) noexcept : pImpl(new Impl)
{
	TestIsMessageOk();
	pImpl->peer = peer;
}

Networking::~Networking() noexcept
{
	delete pImpl;
}

int32_t Networking::RunMainLoop() noexcept
{
	g.gamemode->OnServerInit();

	RakNet::Packet *packet;
	while (1)
	{
		for (auto &v : pImpl->allowUpdateMovement)
			v = true;

		std::this_thread::sleep_for(1ms);

		const auto c = clock();

		g.timers->Tick();

		for (packet = pImpl->peer->Receive(); packet; pImpl->peer->DeallocatePacket(packet), packet = pImpl->peer->Receive())
		{
			auto addr = packet->systemAddress.ToString();
			switch (packet->data[0])
			{
			case ID_NEW_INCOMING_CONNECTION:
				log(Log::Info, "A connection is incoming from ", addr);
				break;
			case ID_DISCONNECTION_NOTIFICATION:
			case ID_CONNECTION_LOST:
				this->DisconnectPlayer(packet->guid);
				break;
			case ID_CONNECTED_PING:
			case ID_UNCONNECTED_PING:
				break;
			default:
				this->ProcessPacket(packet);
				break;
			}
		}

		const auto ms = (clock() - c) * (1000.0 / CLOCKS_PER_SEC);

		static bool printDel = g.cfg->GetValue<std::string>("General.printDelay", "false") == "true" || (std::getenv("PRINTDELAY") && std::getenv("PRINTDELAY") == (std::string)"TRUE");
		if (printDel)
		{
			static clock_t lastPrint = 0;
			if (clock() - lastPrint > 500)
			{
				const auto online = g.plManager->GetPlayers().size();
				log(Log::Debug, "Single iteration finished in ", ms, "ms (", online, " players)");
				lastPrint = clock();
			}
		}
	}

	g.gamemode->OnServerExit();

	return 0;
}

void Networking::ProcessPacket(const RakNet::Packet *packet) noexcept
{
	auto player = g.plManager->GetPlayer(packet->guid);

	RakNet::BitStream bsIn(&packet->data[1], packet->length, false);

	if (player == nullptr)
	{
		switch (packet->data[0])
		{
		case ID_HANDSHAKE:
		{
			if (false)
			{
				log(Log::Warning, "Connection attempt failed (Incorrect packet)");
				CloseConnection(packet->guid, 0);
				break;
			}

			enum {
				maxName = 24,
				maxPassword = 32
			};
			char name[maxName];
			char password[maxPassword];
			for (size_t i = 0; i != maxName; ++i)
				bsIn.Read(name[i]);
			bsIn.IgnoreBytes(sizeof(char));
			bsIn.Read(password, maxPassword);
			name[maxName - 1] = 0;
			password[maxPassword - 1] = 0;

			auto realPassword = g.cfg->GetValue<std::string>("General.password", "");
			if (realPassword != "" && password != realPassword)
			{
				log(Log::Warning, "Connection attempt failed (Wrong password)");
				CloseConnection(packet->guid, ID_WRONG_PASS);
				break;
			}

			const auto nameSize = std::string(name).size();
			bool invalidName = false;
			if (nameSize < 2)
				invalidName = true;
//#ifdef CHECK_NICKNAME
			if (!invalidName)
				for (auto it = &name[0]; it != &name[nameSize]; ++it)
				{
					if ((*it >= L'A' && *it <= L'Z')
						|| (*it >= L'a' && *it <= L'z')
						|| (*it >= L'0' && *it <= L'9')
						|| *it == L'_'
						|| *it == L'-'
						|| *it == L'`'
						|| *it == L"'"[0])
						continue;
					invalidName = true;
					break;
				}
//#endif
			if (invalidName)
			{
				log(Log::Warning, "Connection attempt failed (Incorrect name)");
				CloseConnection(packet->guid, ID_NAME_INVALID);
				break;
			}

			auto &players = g.plManager->GetPlayers();
			bool nameUnique = true;
			for (auto it = players.begin(); it != players.end(); ++it)
			{
				auto player = it->second.get();
				if (player != nullptr && player->IsNPC() == false)
					if (player->GetName() == (name))
					{
						nameUnique = false;
						break;
					}
			}
			if (!nameUnique)
			{
				log(Log::Warning, "Connection attempt failed (Name is already used)");
				CloseConnection(packet->guid, ID_NAME_ALREADY_USED);
				break;
			}

			player = g.plManager->NewPlayer(packet->guid, (name));
			auto nameStr = (player->GetName());
			log(Log::Info, "Connected ", nameStr.data(), "[", player->GetID(), "]");

#if VETUKH_TEST
			if (player->GetID() >= VETUKH_LIMIT)
			{
				g.plManager->DeletePlayer(player->GetGUID());
				return;
			}
#endif

			RakNet::BitStream bsOut;
			bsOut.Write(ID_WELCOME);
			bsOut.Write(player->GetID());
			::WriteEsp(bsOut);
			auto size = pImpl->peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
			pImpl->peer->SetTimeoutTime(7000, packet->systemAddress);

			player->StreamIn(player);
			g.gamemode->OnPlayerConnect(player->GetID());
			break;
		}
		}
	}
	else
	{
		auto isDebug = std::getenv("ISDEBUG");

		if (isDebug != nullptr && std::string(isDebug) == "TRUE")
		{
			if (packet->data[0] != ID_UPDATE_MOVEMENT)
			{
				std::ofstream of("_" + player->GetName() + ".packets.txt", std::ios::app);
				of << (int32_t)packet->data[0] << " ";
			}
		}

		switch (packet->data[0])
		{
		case ID_UPDATE_MOVEMENT:
		{
			const auto playerid = player->GetID();
			if (/*pImpl->allowUpdateMovement[playerid] &&*/ packet->length == 43 && !player->IsLoading() && !player->IsWaitingForNewLook())
			{
				pImpl->allowUpdateMovement[playerid] = false;

				Packet::MovementData receivedData;
				Packet::Deserialize(bsIn, receivedData);
				uint16_t movOwnerID;
				bsIn.Read(movOwnerID);
				uint32_t locationID;
				bsIn.Read(locationID);

				for (size_t i = 0; i < g.gamemode->GetOverloadMult(); ++i)
				{
					auto movOwner = g.plManager->GetPlayer(movOwnerID);
					if (movOwner == nullptr)
						break;

					const bool isSpawned = movOwner->IsSpawned();

					if (movOwner != player && movOwner->GetHostGUIDRaw() != player->GetGUID())
					{
						log(Log::Warning, "NPC", movOwner->GetID(), ": Bad NPC movement data from player ", player->GetName().data(), "[", player->GetID(), "]");
						//if (movOwner->GetNumResets() < 0) //PVS-547
						//	movOwner->Reset();
						//else
						//	log(Log::Warning, "Unable to reset");
						break;
					}

					g.gamemode->OnPlayerUpdate(movOwner->GetID());

					{
						movOwner->InjectMovement(receivedData, true);
						movOwner->InjectCastConcentration(receivedData);
						movOwner->RunActiveEffects();
						if (locationID != ~0)
							movOwner->SetLocation(locationID);

						if (!isSpawned && player->IsSpawned())
						{
							g.gamemode->OnPlayerSpawn(movOwner->GetID());
							movOwner->UpdateStreamedPlayersList();
						}
					}
				}
			}
			break;
		}

		case ID_UPDATE_CELL:
			break;

		case ID_UPDATE_WORLDSPACE:
			break;

		case ID_MESSAGE:
		{
			using T = uint16_t;
			if (packet->length > sizeof(RakNet::MessageID) + sizeof(T))
			{
				std::string input;
				T characters;
				bsIn.Read(characters);
				//if (packet->length == sizeof(RakNet::MessageID) + sizeof(T) + sizeof(char) * characters)
				{
					for (size_t i = 0; i != characters; ++i)
					{
						char ch;
						bsIn.Read(ch);
						input += ch;
					}
					if (IsMessageOk(input))
					{
						g.gamemode->OnPlayerChatInput(player->GetID(), (input));
					}
				}
			}
			break;
		}
		case ID_LEARN_EFFECT:
		{
			uint32_t itemTypeID;
			uint32_t effectIdx;
			bool learn;
			bsIn.Read(itemTypeID);
			bsIn.Read(effectIdx);
			bsIn.Read(learn);

			if (effectIdx >= 4)
				break;

			if (learn != true)
				break;

			auto itemType = IDAssigner<ItemTypeWrap>::GetObj(itemTypeID);
			if (itemType != nonstd::nullopt)
			{
				const bool success = (g.gamemode->OnPlayerLearnEffect(player->GetID(), itemType->GetIdentifier(), effectIdx + 1));
				if (success)
					player->LearnItemTypeEffect(*itemType, effectIdx, learn);
				else
					player->LearnItemTypeEffect(*itemType, effectIdx, !learn);
			}
			break;
		}
		case ID_LOADING_START:
			player->SetLoading(true);
			break;
		case ID_LOADING_STOP:
			player->SetLoading(false);
			break;
		case ID_PAUSE_START:
			player->SetPaused(true);
			break;
		case ID_PAUSE_STOP:
			player->SetPaused(false);
		case ID_UPDATE_LOOK:
		{
			Packet::LookData look;
			Packet::Deserialize(bsIn, look);
			if (player->IsWaitingForNewLook())
				if (look.raceID >= 0x00013740 && look.raceID <= 0x00013749)
				{
					player->SetWaitingForNewLook(false);
					player->SetLook(look);
					g.gamemode->OnPlayerCharacterCreated(player->GetID());
				}
			break;
		}
		case ID_ACTIVATE:
		{
			const auto plMovement = player->GetLastInjectedMovement();
			if (plMovement == nullptr)
				return;

			const auto actiReach = g.cfg->GetValue("Gameplay.activateReach", 256);

			uint32_t id;
			bsIn.Read(id);

			// Activate actor
			if (id == 0)
			{
				uint16_t playerID;
				bsIn.Read(playerID);
				auto target = g.plManager->GetPlayer(playerID);
				if (target == nullptr)
					return;
				auto targetMovement = target->GetLastInjectedMovement();
				if (targetMovement == nullptr)
					return;
				const auto distance = ((plMovement->pos) - (targetMovement->pos)).Length();
				if (distance > actiReach)
					return;
				if (player->IsStreamedIn(target) == false)
					return;
				if (player == target)
					return log(Log::Warning, "Attempt to activate self");
				g.gamemode->OnPlayerActivatePlayer(player->GetID(), target->GetID());
				return;
			}

			uint8_t isOpen = 0;
			bsIn.Read(isOpen);
			isOpen = !!isOpen;

			uint16_t sourceId = ~0;
			bsIn.Read(sourceId);

			auto source = g.plManager->GetPlayer(sourceId);
			if (source == nullptr)
				break;
			if (source != player && source->GetHostGUID() != player->GetGUID())
				break;
			const auto sourceMovement = source->GetLastInjectedMovement();
			if (sourceMovement == nullptr)
				return;

			auto object = g.objManager->LookupObject(id);
			if (object != nullptr)
			{
				auto noZ = [](auto point) {
					point.z = 0;
					return point;
				};
				const auto distance = (noZ(sourceMovement->pos) - noZ(object->GetPos())).Length();

				const bool normalDistance = (distance < 256.0 && object->GetType() == Object::Type::TeleportDoor)
					|| distance < actiReach;

				if (!normalDistance)
				{
					if (object->GetType() == Object::Type::Item)
						source->StreamOut(object);
				}

				if (normalDistance
					&& source->IsStreamedIn(object)
					&& !source->IsDying())
				{
					object->Activate(source, true, isOpen);
				}
			}
			break;
		}
		case ID_LOCKPICK:
		{
			if (player->IsDying())
				break;
			const auto objID = player->GetCurrentFurniture();
			auto object = g.objManager->LookupObject(objID);
			if (object != nullptr)
			{
				if (object->GetLockLevel() != 0 && object->GetLockLevel() != 255)
				{
					object->SetLockLevel(0);
					player->SetCurrentFurniture(0);
					if (object->GetType() == Object::Type::Door)
						object->SetOpen(true);
				}
			}
			break;
		}
		case ID_DROPITEM:
		{
			if (player->IsDying()) // Remove this check to allow dropping equipped weapons on death
				break;
			uint32_t itemTypeID;
			uint32_t count;
			bsIn.Read(itemTypeID);
			bsIn.Read(count);
			auto itemType = IDAssigner<ItemTypeWrap>::GetObj(itemTypeID);
			if (count > 0 && itemType)
			{
				auto droppedItem = player->DropItem(*itemType, count);
				if (droppedItem != nullptr)
				{
					//if (droppedItem != nullptr)
					//	player->HostStartAttempt(droppedItem);
					const bool success = g.gamemode->OnPlayerDropObject(player->GetID(), droppedItem->GetID());
					if (!success)
					{
						g.objManager->DeleteObject(droppedItem->GetID());
						player->AddItem(*itemType, count);
					}
				}
			}
			break;
		}
		case ID_USEITEM:
		{
			if (player->IsDying())
				break;
			uint32_t itemTypeID;
			bsIn.Read(itemTypeID);
			auto itemType = IDAssigner<ItemTypeWrap>::GetObj(itemTypeID);
			if (itemType)
				player->UseItem(*itemType);
			break;
		}
		case ID_CRAFT_INGREDIENT:
		{
			if (player->IsDying())
				break;
			uint32_t itemTypeID;
			bsIn.Read(itemTypeID);
			uint32_t c;
			bsIn.Read(c);
			auto itemType = IDAssigner<ItemTypeWrap>::GetObj(itemTypeID);
			if (itemType)
				player->AddCraftIngredient(*itemType, c);
			break;
		}
		case ID_CRAFT_FINISH_ALCHEMY:
		{
			if (player->IsDying())
				break;
			uint64_t isPoison;
			bsIn.Read(isPoison);
			player->CraftPotion((bool)isPoison);
			break;
		}
		case ID_CRAFT_FINISH:
		{
			if (player->IsDying())
				break;
			uint32_t itemTypeID;
			uint32_t count;
			bsIn.Read(itemTypeID);
			bsIn.Read(count);
			auto itemType = IDAssigner<ItemTypeWrap>::GetObj(itemTypeID);
			if (itemType)
			{
				//log(Log::Debug, "Craft ", itemType->GetIdentifier().data(), " ", count);
				player->CraftItem(*itemType);
			}
			break;
		}
		case ID_ENCHANTING_ITEM:
		{
			auto furn = g.objManager->LookupObject(player->GetCurrentFurniture());
			if (furn != nullptr && furn->IsEnchantingWorkbench())
			{
				uint32_t itemTypeID, enchID;
				bsIn.Read(itemTypeID);
				bsIn.Read(enchID);

				auto item = IDAssigner<ItemTypeWrap>::GetObj(itemTypeID);
				if (!item)
					return log(Log::Warning, "Ench - Bad item");
				if (item->GetClass() != "Weapon" && item->GetClass() != "Armor")
					return log(Log::Warning, "Ench - Item class must be Weapon or Armor");
				if (player->GetItemCount(*item) == 0)
					return log(Log::Warning, "Ench - GetItemCount() returned 0");

				auto ench = IDAssigner<SpellWrap>::GetObj(enchID);
				if (!ench)
					return log(Log::Warning, "Ench - Bad enchantment");
				if (player->HasSpell(*ench) == false)
					return log(Log::Warning, "Ench - HasMagic() returned false");
				if (ench->IsEnchantment() == false)
					return log(Log::Warning, "Ench - Magic class must be Enchantment");

				auto soulGem = player->GetLastEnchSoulGem();
				if (!soulGem)
					return log(Log::Warning, "Ench - No soul gem");;
				if (soulGem->GetClass() != "SoulGem")
					return log(Log::Warning, "Ench - Soul gem class must be SoulGem");
				if (soulGem->GetSoulSize() == 0)
					return log(Log::Warning, "Ench - Empty soul gem");

				player->EnchantItemUnsafe(*item, *ench, *soulGem);
			}
			else
				log(Log::Warning, "Attempt to enchant item with bad workbench (", player->GetName().data(), "[", player->GetID(), "])");
			break;
		}
		case ID_CONTAINER_CHANGED:
		{
			if (player->IsDying())
				break;
			uint32_t itemTypeID;
			uint32_t change;
			bsIn.Read(itemTypeID);
			bsIn.Read(change);
			uint32_t isAdd;
			bsIn.Read(isAdd);
			uint32_t containerID;
			bsIn.Read(containerID);

			auto itemType = IDAssigner<ItemTypeWrap>::GetObj(itemTypeID);
			auto cont = g.objManager->LookupObject(containerID);
			if (itemType && cont)
				player->ChangeContainer(*itemType, change, isAdd != 0, cont);
		}
		case ID_HOST_START:
		{
			if (player->IsDying())
				break;
			uint32_t id;
			bsIn.Read(id);

			auto obj = g.objManager->LookupObject(id);
			if (obj != nullptr)
				player->HostStartAttempt(obj);
			break;
		}
		case ID_HOSTED_OBJECT_MOVEMENT:
		{
			uint32_t id;
			NiPoint3 pos, rot;
			bool grabbing;
			bsIn.Read(id);
			bsIn.Read(pos.x);
			bsIn.Read(pos.y);
			bsIn.Read(pos.z);
			bsIn.Read(rot.x);
			bsIn.Read(rot.y);
			bsIn.Read(rot.z);
			bsIn.Read(grabbing);

			auto obj = g.objManager->LookupObject(id);
			if (obj != nullptr)
				obj->InjectMovementBy(player, pos, rot, grabbing);

			break;
		}
		case ID_ANIMATION_EVENT_HIT:
		{
			uint32_t hitAnimID;
			uint16_t source;
			bsIn.Read(hitAnimID);
			bsIn.Read(source);
			auto pl = g.plManager->GetPlayer(source);
			if (pl == nullptr)
				break;
			if (pl == player || pl->GetHostGUID() == player->GetGUID())
			{
				if (pl->IsDying())
					break;
				pl->InjectHitAnim(hitAnimID);
			}
			break;
		}
		case ID_HIT_OBJECT:
		{
			if (player->IsDying())
				break;
			uint32_t objectID;
			uint32_t weaponID;
			bool isPowerAttack;
			uint32_t spellID;
			bsIn.Read(objectID);
			bsIn.Read(weaponID);
			bsIn.Read(isPowerAttack);
			bsIn.Read(spellID);

			auto myMov = player->GetLastInjectedMovement();
			if (myMov == nullptr)
				break;
			const auto myPos = myMov->pos;

			auto weap = IDAssigner<ItemTypeWrap>::GetObj(weaponID);
			if (weap != nonstd::nullopt)
			{
				const bool isWeapEquipped = player->IsEquipped(*weap);
				if (!isWeapEquipped)
					weap = nonstd::nullopt;
			}

			auto spell = IDAssigner<SpellWrap>::GetObj(spellID);
			if (spell != nonstd::nullopt)
			{
				const bool isSpellEquipped = player->IsEquipped(*spell);
				if (!isSpellEquipped)
					spell = nonstd::nullopt;
			}

			const auto ammo = player->GetEquippedAmmo();

			const bool isShot = myMov->attackState >= 8 && myMov->attackState <= 13
				&& weap != nonstd::nullopt && (weap->GetSubclass() == "Bow" || weap->GetSubclass() == "Crossbow")
				&& ammo != nonstd::nullopt;

			const auto object = g.objManager->LookupObject(objectID);
			const auto objPos = object ? object->GetPos() : NiPoint3{0, 0, 0};
			if (!object || !player->IsStreamedIn(object))
				break;
			if (!spell && !isShot && (myPos - objPos).Length() > g.cfg->GetValue("Gameplay.activateReach", 256))
				break;

			g.gamemode->OnPlayerHitObject(player->GetID(), objectID,
				weap ? weap->GetIdentifier() : "",
				(ammo && isShot) ? ammo->itemType.GetIdentifier() : "",
				spell ? spell->GetIdentifier() : "");
		}
		case ID_HIT_PLAYER:
		{
			uint16_t playerID;
			uint32_t weaponID;
			bool isPowerAttack;
			uint32_t spellID;
			uint16_t sourceID;
			bsIn.Read(playerID);
			bsIn.Read(weaponID);
			bsIn.Read(isPowerAttack);
			bsIn.Read(spellID);
			bsIn.Read(sourceID);

			Player *source = g.plManager->GetPlayer(sourceID);
			if (source == nullptr)
				break;

			if (source->IsDying())
				break;

			Player *target = g.plManager->GetPlayer(playerID);
			if (target == nullptr)
				break;

			if (source->GetHostGUID() != player->GetGUID() && source != player)
			{
				//log(Log::Warning, "Bad NPC ", source->GetID(), " hit data from player ", player->GetName().data(), "[", player->GetID(), "]");
			}

			auto weap = IDAssigner<ItemTypeWrap>::GetObj(weaponID);
			if (weap != nonstd::nullopt)
			{
				const bool isWeapEquipped = source->IsEquipped(*weap);
				if (!isWeapEquipped)
					weap = nonstd::nullopt;
			}

			auto spell = IDAssigner<SpellWrap>::GetObj(spellID);
			if (spell != nonstd::nullopt)
			{
				const bool isSpellEquipped = source->IsEquipped(*spell);
				if (!isSpellEquipped)
					spell = nonstd::nullopt;
			}

			source->InjectAttack(target, weap, spell, isPowerAttack);

			break;
		}
		case ID_DIALOG_RESPONSE:
		{
			using T = uint16_t;
			if (packet->length > sizeof(RakNet::MessageID) + sizeof(T))
			{
				std::string input;
				T characters;
				bsIn.Read(characters);

				if (packet->length == sizeof(int32_t) + sizeof(RakNet::MessageID) + sizeof(T) + sizeof(char) * characters)
				{
					for (size_t i = 0; i != characters; ++i)
					{
						char ch;
						bsIn.Read(ch);
						input += ch;
					}
					//if (IsMessageOk(input)) // it's ok to input to be cyrrilic text. for example: dialog style list
					{
						int32_t listItem;
						bsIn.Read(listItem);
						const auto dialogID = player->GetCurrentDialogID();
						player->ReleaseDialog();
						g.gamemode->OnPlayerDialogResponse(player->GetID(), dialogID, (input), listItem);
					}
				}
			}
			break;
		}
		case ID_AV_CHANGED:
		{
			if (player->IsDying())
				break;
			if (player->IsSpawned() == false)
				break;
			while (1)
			{
				uint8_t avID;
				float newPercentage;
				bsIn.Read(avID);
				const bool read = bsIn.Read(newPercentage);
				if (!read)
					break;
				player->InjectAVChange(avID, newPercentage);
			}
			break;
		}
		case ID_EQUIP_ITEM:
		case ID_UNEQUIP_ITEM:
		{
			if (player->IsDying())
				break;
			uint32_t itemID;
			int32_t handID;
			bsIn.Read(itemID);
			bsIn.Read(handID);
			auto itemType = IDAssigner<ItemTypeWrap>::GetObj(itemID);
			if (itemType != nonstd::nullopt)
			{
				player->InjectEquipUnequip({ *itemType }, handID,
					packet->data[0] == ID_EQUIP_ITEM);
			}
			break;
		}
		case ID_EQUIP_SPELL:
		case ID_UNEQUIP_SPELL:
		{
			if (player->IsDying())
				break;
			uint32_t spellID;
			int32_t handID;
			bsIn.Read(spellID);
			bsIn.Read(handID);
			auto spell = IDAssigner<SpellWrap>::GetObj(spellID);
			if (spell != nonstd::nullopt)
			{
				player->InjectEquipUnequip(*spell, handID,
					packet->data[0] == ID_EQUIP_SPELL);
			}
			break;
		}
		case ID_BOW_SHOT:
		{
			if (player->IsDying())
				break;
			uint32_t power;
			bsIn.Read(power);
			player->InjectBowShot(power);
		}
		case ID_SPELL_RELEASE:
		{
			if (player->IsDying())
				break;
			int32_t handID;
			bsIn.Read(handID);
			player->InjectSpellRelease(handID);
		}
		case ID_DATASEARCH_RESULT:
		{
			if (player->IsDataSearchStarted())
			{
				static auto dataSearch = new DataSearch;
				dataSearch->HandleResult(bsIn, player);
			}
			break;
		}
		case ID_FORGET_PLAYER:
		{
			// Deprecated
			break;
		}
		case ID_POISON_ATTACK:
		{
			player->InjectPoisonApply();
			break;
		}
		case ID_LEVEL_UP:
		{
			uint8_t avID;
			bsIn.Read(avID);
			player->InjectLevelUp(avID);
			break;
		}
		case ID_LEARN_PERK:
		{
			uint32_t perkID;
			bsIn.Read(perkID);
			auto avd = player->GetAVData("perkpoints");
			if (avd.base >= 1.0f)
			{
				avd.base -= 1.0f;
				player->SetAVData("perkpoints", avd);
				g.gamemode->OnPlayerLearnPerk(player->GetID(), perkID);
			}
			break;
		}
		case ID_LEGENDARY_SKILL:
		{
			break;
		}
		default:
			break;
		}
	}
}

void Networking::CloseConnection(RakNet::RakNetGUID guid, RakNet::MessageID reasonPacketID) noexcept
{
	if (reasonPacketID != 0)
	{
		RakNet::BitStream bsOut;
		bsOut.Write(reasonPacketID);
		pImpl->peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, guid, false);
	}
	pImpl->peer->CloseConnection(guid, true);
}

void Networking::DisconnectPlayer(RakNet::RakNetGUID guid) noexcept
{
	auto player = g.plManager->GetPlayer(guid);
	if (player == nullptr)
		return;

	log(Log::Info, "Disconnected ", player->GetName(), "[", player->GetID(), "]");
	g.gamemode->OnPlayerDisconnect(player->GetID());
	g.plManager->DeletePlayer(guid);
}

RakNet::RakPeerInterface *Networking::GetPeer() const noexcept
{
	return pImpl->peer;
}

void Networking::SendToPlayer(Player *player, RakNet::BitStream *data, PacketReliability reliability, PacketPriority priority) noexcept
{
	if (player == nullptr)
		return;
	if (player->IsNPC())
		return;
	if (player->reliability != PacketReliability::UNRELIABLE && reliability == PacketReliability::UNRELIABLE)
		reliability = PacketReliability::RELIABLE;
	auto guid = player->GetGUID();
	if (guid != RakNet::UNASSIGNED_RAKNET_GUID)
		pImpl->peer->Send(data, priority, reliability, 0, guid, false);
}
