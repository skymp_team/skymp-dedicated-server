#include "stdafx.h"
#include "DataSearch.h"

#include "ItemTypeWrap.h"
#include "NavMeshWrap.h"
#include "ObjectWrap.h"
#include "PlayerWrap.h"
#include "RaceWrap.h"
#include "IDAssigner.h"

struct DataSearch::Impl
{
	std::map<uint32_t, ItemTypeWrap *> created;
	std::set<std::string> posUsed;
	std::set<uint32_t> createdNpc;
};

DataSearch::DataSearch() : pImpl(new Impl)
{
}

void DataSearch::HandleResult(RakNet::BitStream &bsIn, Player *player) noexcept
{
	if (player == nullptr)
		return;

	Opcode opcode;
	bsIn.Read(opcode);


	std::vector<luabridge::LuaRef> dataSearchResults;

	const static std::vector<std::string> opcodeNames = {
		"NavMesh",
		"TPDs",
		"Cont",
		"Door",
		"Item",
		"Actor",
		"Acti"
	};
	if ((size_t)opcode >= opcodeNames.size())
		return;
	const auto dataSearchType = opcodeNames[(size_t)opcode];
	if (player->IsDSOpcodeEnabled(opcode))
	{
		switch (opcode)
		{
		case Opcode::NavMesh:
		{
			uint32_t formID = 0;
			bsIn.Read(formID);

			uint32_t numVerts = 0;
			std::vector<NiPoint3> verts;
			bsIn.Read(numVerts);
			for (uint32_t i = 0; i != numVerts; ++i)
			{
				NiPoint3 vert;
				bsIn.Read(vert.x);
				bsIn.Read(vert.y);
				bsIn.Read(vert.z);
				verts.push_back(vert);
			}

			using VertID = uint16_t;
			using Triangle = std::array<VertID, 3>;
			uint32_t numTriangles = 0;
			bsIn.Read(numTriangles);
			std::vector<Triangle> triangles;
			for (uint32_t i = 0; i != numTriangles; ++i)
			{
				Triangle t;
				bsIn.Read(t[0]);
				bsIn.Read(t[1]);
				bsIn.Read(t[2]);
				triangles.push_back(t);
			}

			// TODO: Read External Connections

			if (NavMeshWrap::LookupByID(formID).isNil())
			{
				auto navRef = NavMeshWrap::Create(formID);
				if (navRef.isNil() == false)
				{
					auto nav = navRef.cast<NavMeshWrap>();
					nav.SetNumVertices(numVerts);
					for (uint32_t i = 0; i != verts.size(); ++i)
						nav.SetNthVerticePos(i + 1, verts[i].x, verts[i].y, verts[i].z);
					nav.SetNumTriangles(numTriangles);
					for (uint32_t i = 0; i != triangles.size(); ++i)
						nav.SetNthTriangleVertices(i + 1, triangles[i][0], triangles[i][1], triangles[i][2]);

					std::stringstream ss;
					ss << (player->GetName()) << '[' << player->GetID() << ']';
					ss << " is loading NavMesh " << std::hex << formID;
					ss << " (" << std::to_string(numVerts) << " VRTS, " << std::to_string(numTriangles) << " TRI)";
					log(Log::Debug, ss.str().data());

					if (nav.GetNumTriangles() != numTriangles)
						log(Log::Warning, "Unable to assign NavMesh triangles");

					if (nav.GetNumVertices() != numVerts)
						log(Log::Warning, "Unable to assign NavMesh vertices");

					nav.SetLocation(player->GetLocation());
					//g.pathing->Add(nav);
					//g.pathing->Remove(nav);
					dataSearchResults.emplace_back(g.gamemode->GetState());
					dataSearchResults.back() = navRef;
				}
			}
			break;
		}
		case Opcode::TPDs:
		{
			std::vector<ObjectWrap> doors;
			std::vector<luabridge::LuaRef> doorRefs;
			uint32_t locID;
			for (int32_t i = 0; i != 2; ++i)
			{
				uint32_t refID, baseID;
				NiPoint3 p, a;
				bsIn.Read(refID);
				bsIn.Read(baseID);
				bsIn.Read(locID);
				bsIn.Read(p.x);
				bsIn.Read(p.y);
				bsIn.Read(p.z);
				bsIn.Read(a.x);
				bsIn.Read(a.y);
				bsIn.Read(a.z);
				luabridge::LuaRef locRef(g.gamemode->GetState());
				locRef = LocationWrap(locID);
				auto existingObject = g.objManager->LookupObject(refID);
				if (existingObject != nullptr)
				{
					try {
						std::stringstream ss;
						ss << "Object " << std::hex << refID << " (" << ObjectWrap(refID).GetType() << ") already exist and will be registered as Teleport Door";
						existingObject->SetType(Object::Type::Static);
						log(Log::Warning, ss.str());
					}
					catch (...) {
						log(Log::Error, "DataSearch.HandleResult() bad cast");
					}
				}
				auto doorRef = existingObject != nullptr ? ObjectWrap::LookupByID(existingObject->GetID()) : ObjectWrap::Create(refID, baseID, locRef, p.x, p.y, p.z);
				if (!doorRef.isNil())
				{
					auto door = doorRef.cast<ObjectWrap>();
					door.SetAngle(a.x, a.y, a.z);
					doors.push_back(door);
					doorRefs.push_back(doorRef);
				}
				else
				{
					std::stringstream ss;
					ss << "Unable to find or create object { " << std::hex << refID << ", " << baseID << ", " << locID << ", " << p.x << ", " << p.y << ", " << p.z << " }";
					log(Log::Debug, ss.str().data());
				}
			}
			if (doors.size() == 2)
			{
				doors[0].RegisterAsTeleportDoor(doorRefs[1]);
				doors[1].RegisterAsTeleportDoor(doorRefs[0]);
				player->SetLocation(locID);

				std::stringstream ss;
				ss << (player->GetName()) << '[' << player->GetID() << ']';
				ss << " is loading Teleport Doors";
				log(Log::Debug, ss.str().data());

				dataSearchResults.emplace_back(g.gamemode->GetState());
				dataSearchResults.back() = doors[0];
				dataSearchResults.emplace_back(g.gamemode->GetState());
				dataSearchResults.back() = doors[1];
			}
			else
			{
				std::stringstream ss;
				ss << (player->GetName()) << '[' << player->GetID() << ']';
				ss << " can't load Teleport Doors (" << doors.size() << " doors loaded, " << 2 << " expected)";
				log(Log::Warning, ss.str().data());
			}
			break;
		}
		case Opcode::Cont:
		case Opcode::Door:
		case Opcode::Acti:
		{
			uint32_t refID, baseID; uint32_t locID;
			NiPoint3 p, a;
			bsIn.Read(refID);
			bsIn.Read(baseID);
			bsIn.Read(locID);
			bsIn.Read(p.x);
			bsIn.Read(p.y);
			bsIn.Read(p.z);
			bsIn.Read(a.x);
			bsIn.Read(a.y);
			bsIn.Read(a.z);
			luabridge::LuaRef locRef(g.gamemode->GetState());
			locRef = LocationWrap(locID);
			if (ObjectWrap::LookupByID(refID).isNil() == false)
				break;
			auto cRef = ObjectWrap::Create(refID, baseID, locRef, p.x, p.y, p.z);
			if (!cRef.isNil())
			{
				auto c = cRef.cast<ObjectWrap>();
				if (opcode == Opcode::Cont)
					c.RegisterAsContainer();
				if (opcode == Opcode::Door)
					c.RegisterAsDoor();
				if (opcode == Opcode::Acti)
				{
					switch (c.GetBaseID())
					{
					case 0x000CAE0B:
					case 0x000bad0c:
					case 0x000D5501:
					case 0x0006B691:
						c.RegisterAsFurniture();
					default:
						c.RegisterAsActivator();
					}
				}
				std::stringstream ss;
				ss << (player->GetName()) << '[' << player->GetID() << ']';
				ss << " is loading Object";
				log(Log::Debug, ss.str().data());

				dataSearchResults.emplace_back(g.gamemode->GetState());
				dataSearchResults.back() = cRef;
			}
			else
			{
			}
			break;
		}
		case Opcode::Item:
		{
			uint32_t refID, baseID; uint32_t locID;
			uint32_t val, damage;
			uint8_t cl, subCl;
			NiPoint3 p, a;
			bsIn.Read(refID);
			bsIn.Read(baseID);
			bsIn.Read(locID);
			bsIn.Read(p.x);
			bsIn.Read(p.y);
			bsIn.Read(p.z);
			bsIn.Read(a.x);
			bsIn.Read(a.y);
			bsIn.Read(a.z);
			bsIn.Read(val);
			bsIn.Read(damage);
			bsIn.Read(cl);
			bsIn.Read(subCl);

			auto toStr = [=](NiPoint3 pp) {
				return std::to_string(refID);
			};

			if (pImpl->posUsed.find(toStr(p)) == pImpl->posUsed.end())
			{
				pImpl->posUsed.insert(toStr(p));

				if (pImpl->created.find(baseID) == pImpl->created.end())
				{
					luabridge::LuaRef damageRef(g.gamemode->GetState());
					damageRef = damage;

					luabridge::LuaRef skillRef(g.gamemode->GetState());
					skillRef = (std::string)"OneHanded";
					auto i = ItemTypeWrap::Create(std::to_string(baseID), ItemTypeWrap::GetClassAndSubclassByID(cl, subCl), baseID, (float)(rand() % 10) + 1, val, damageRef, skillRef);
					if (i.isNil() == false)
					{
						pImpl->created[baseID] = new ItemTypeWrap(i.cast<ItemTypeWrap>());
					}
				}
				luabridge::LuaRef locRef(g.gamemode->GetState());
				locRef = LocationWrap(locID);
				auto cRef = ObjectWrap::CreateItem(ItemTypeWrap::LookupByIdentifier(std::to_string(baseID)), 1, locRef, p.x, p.y, p.z);
				if (cRef.isNil() == false)
				{
					auto objWrap = cRef.cast<ObjectWrap>();
					objWrap.SetAngle(a.x, a.y, a.z);
					dataSearchResults.emplace_back(g.gamemode->GetState());
					dataSearchResults.back() = objWrap;
				}

				std::stringstream ss;
				ss << (player->GetName()) << '[' << player->GetID() << ']';
				ss << " is loading Item";
				log(Log::Debug, ss.str().data());
			}
		}
		case Opcode::Actor:
		{
			uint32_t refID, baseID; uint32_t locID;
			NiPoint3 p, a;
			uint32_t raceID;
			bsIn.Read(refID);

			if (pImpl->createdNpc.insert(refID).second == false)
				break;

			bsIn.Read(baseID);
			bsIn.Read(locID);
			bsIn.Read(p.x);
			bsIn.Read(p.y);
			bsIn.Read(p.z);
			bsIn.Read(a.x);
			bsIn.Read(a.y);
			bsIn.Read(a.z);
			bsIn.Read(raceID);
			luabridge::LuaRef locRef(g.gamemode->GetState()), baseRef(g.gamemode->GetState()), raceRef(g.gamemode->GetState());
			locRef = LocationWrap(locID);
			baseRef = baseID;
			raceRef = RaceWrap(raceID);
			auto npcRef = PlayerWrap::CreateNPC(baseRef);
			if (npcRef.isNil())
			{
				log(Log::Warning, "DataSearch failed loading Actor");
				break;
			}
			auto npc = npcRef.cast<PlayerWrap>();
			auto rawNpc = g.plManager->GetPlayer(npc.GetID());
			if (rawNpc != nullptr)
				rawNpc->SetRefID(refID);
			npc.SetRace(raceRef);
			npc.SetSpawnPoint(locRef, p.x, p.y, p.z, a.z);
			npc.Spawn();
			//npc.SetVirtualWorld(player->GetVirtualWorld());

			dataSearchResults.emplace_back(g.gamemode->GetState());
			dataSearchResults.back() = npcRef;
			break;
		}
		}
	}

	for (auto &dsRes : dataSearchResults)
	{
		g.gamemode->OnPlayerDataSearchResult(player->GetID(), dataSearchType, dsRes);
	}
}