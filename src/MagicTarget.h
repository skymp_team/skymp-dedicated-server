#pragma once
#include "EffectWrap.h"

class Player;

namespace Components
{
	class MagicTarget
	{
	public:
		void ApplyEffect(const EffectWrap &effect, float magnitude, float duration, uint16_t casterID) noexcept {
			this->CollectGarbadge();

			if (duration == 0)
				duration = 1;

			const auto endMoment = std::chrono::system_clock::now() + uint32_t(duration * 1000) * 1ms;

			auto it = effects.find(effect);
			if (it == effects.end())
			{
				it = effects.insert({ effect, {casterID, magnitude, endMoment} }).first;
			}
			else
			{
				it->second.casterID = casterID;
				it->second.endMoment = endMoment;
				it->second.magnitude = magnitude;
			}
		}

		void ApplySpell(const SpellWrap &spell, uint16_t casterID) noexcept {
			for (uint32_t n = 1; n <= spell.GetNumEffects(); ++n)
			{
				this->ApplyEffect(
					spell.GetNthEffect(n),
					spell.GetNthEffectMagnitude(n),
					spell.GetNthEffectDuration(n),
					casterID
				);
			}
		}

		size_t GetNumActiveEffects() const noexcept {
			this->CollectGarbadge();
			return this->effects.size();
		}

		const auto &GetActiveEffects() const noexcept {
			this->CollectGarbadge();
			return effects;
		}

		void DispellEffect(EffectWrap effect) noexcept {
			this->effects.erase(effect);
		}

	private:
		void CollectGarbadge() const noexcept {
			if (effects.empty())
				return;
			const auto effectsCopy = effects;
			const auto now = std::chrono::system_clock::now();
			for (auto &pair : effectsCopy)
			{
				if (pair.second.endMoment < now)
					effects.erase(pair.first);
			}
		}

	protected:
		struct ActiveEffectData
		{
			uint16_t casterID;
			float magnitude;
			std::chrono::time_point<std::chrono::system_clock> endMoment;

			bool operator==(const ActiveEffectData &rhs) const {
				return this->casterID == rhs.casterID && this->magnitude == rhs.magnitude && this->endMoment == rhs.endMoment;
			}
		};

		mutable std::map<EffectWrap, ActiveEffectData> effects;

	};
}