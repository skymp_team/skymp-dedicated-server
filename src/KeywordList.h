#pragma once
#include "IDAssigner.h"

namespace Components
{
	class Keyword
	{
	public:
		Keyword() : Keyword("")
		{
		}

		Keyword(std::string str) {
			this->str = str;
			if (this->str == "")
				this->str = "SkympInvalidKeyword";
			IDAssigner<Keyword>::GetID(*this); // generate id
		}

		Keyword(IDAssigner<Keyword>::id_t id) {
			auto kwrd = IDAssigner<Keyword>::GetObj(id);
			if (kwrd != nonstd::nullopt)
				*this = *kwrd;
		}

		auto GetID() const {
			return IDAssigner<Keyword>::GetID(*this);
		}

		std::string GetString() const {
			return this->str;
		}

		bool operator<(const Keyword &rhs) const {
			return this->str < rhs.str;
		}

		friend bool operator==(const Keyword &lhs, const Keyword &rhs) {
			return lhs.str == rhs.str;
		}

		friend bool operator!=(const Keyword &lhs, const Keyword &rhs) {
			return !(lhs == rhs);
		}

	private:
		std::string str;
	};

	class KeywordList
	{
	public:
		KeywordList() {
			this->keywords.clear();
		}

		KeywordList(std::list<Keyword> ks) {
			this->keywords = { ks.begin(), ks.end() };
		}

		KeywordList(std::set<Keyword> ks) {
			this->keywords = std::move(ks);
		}

		const auto &GetKeywords() const {
			return this->keywords;
		}

		uint32_t GetNumKeywords() const {
			return (uint32_t)this->GetKeywords().size();
		}

		void AddKeyword(Keyword k) {
			this->keywords.insert(k);
		}

		void RemoveKeyword(Keyword k) {
			this->keywords.erase(k);
		}

		bool HasKeyword(Keyword k) {
			return this->keywords.find(k) != this->keywords.end();
		}

	private:
		std::set<Keyword> keywords;
	};
}