#include "stdafx.h"
#include "LocationWrap.h"

#include "Location.h"

std::map<uint32_t, Location *> createdLocations;

struct LocationWrap::Impl
{
	Location *location = nullptr;
};

LocationWrap::LocationWrap(uint32_t formID) noexcept : pImpl(new Impl)
{
	try {
		pImpl->location = createdLocations.at(formID);
	}
	catch (...) {
		pImpl->location = new Location(formID);
		createdLocations.insert({ formID, pImpl->location });
	}
}

LocationWrap::~LocationWrap() noexcept
{
}

bool LocationWrap::operator==(const LocationWrap &rhs) const noexcept
{
	return pImpl->location == rhs.pImpl->location;
}

uint32_t LocationWrap::GetID() const noexcept
{
	if (pImpl->location == nullptr)
	{
		log(Log::Warning, "Location should not be null");
		return 0;
	}
	return pImpl->location->GetFormID();
}

void LocationWrap::Register(lua_State *L)
{
	luabridge::getGlobalNamespace(L)
		.beginClass<LocationWrap>("Location")
		.addConstructor<void(*)(uint32_t)>()
		.addFunction("__eq", &LocationWrap::operator==)
		.addFunction("GetID", &LocationWrap::GetID)
		.endClass();
}