#pragma once
#include "Player.h"

namespace Components
{
	class Hostable
	{
	public:
		template<class Host>
		void SetHost(Host *host) noexcept {
			this->hostGuid = host ? host->GetGUID() : RakNet::UNASSIGNED_RAKNET_GUID;
		}

		void SetHost(std::nullptr_t host) noexcept {
			this->hostGuid = RakNet::UNASSIGNED_RAKNET_GUID;
		}

		RakNet::RakNetGUID GetHostGUID() const noexcept {
			if (!GUIDExist(this->hostGuid))
			{
				return RakNet::UNASSIGNED_RAKNET_GUID;
			}
			return this->GetHostGUIDRaw();
		}

		const RakNet::RakNetGUID &GetHostGUIDRaw() const noexcept {
			return this->hostGuid;
		}

	private:

		static bool GUIDExist(RakNet::RakNetGUID guid);

		RakNet::RakNetGUID hostGuid;
	};
}