#include "stdafx.h"
#include "PlayerWrap.h"

#include "LocationWrap.h"
#include "RaceWrap.h"
#include "ObjectWrap.h"

#include "TintMask.h"

struct SpawnPointData
{
	uint64_t ownerUniqueID;
	NiPoint3 pos;
	uint16_t angleZ;
	uint32_t locationID;
};
std::map<uint16_t, SpawnPointData> spawnPointData;

std::map<RakNet::RakNetGUID, uint32_t> displayedGold;

std::map<RakNet::RakNetGUID, nonstd::optional<PlayerWrap>> horseRiders;

namespace HelperFuncs
{
	inline uint32_t ColorToNumber(const Packet::LookData::Color &c)
	{
		std::stringstream ss;
		ss << "0x";
		ss << std::hex << std::setfill('0') << std::setw(2) << (uint64_t)c.r;
		ss << std::hex << std::setfill('0') << std::setw(2) << (uint64_t)c.g;
		ss << std::hex << std::setfill('0') << std::setw(2) << (uint64_t)c.b;
		ss << std::hex << std::setfill('0') << std::setw(2) << (uint64_t)c.a;
		uint32_t result;
		ss >> result;
		return result;
	}

	inline Packet::LookData::Color NumberToColor(const uint32_t &n)
	{
		Packet::LookData::Color result;
		result.r = n >> 24 & 255;
		result.g = n >> 16 & 255;
		result.b = n >> 8 & 255; 
		result.a = n >> 0 & 255;
		return result;
	}

	inline Packet::LookData::TintMask CreateTintmask(const std::string &texturePath, const std::string &type, uint32_t color)
	{
		Packet::LookData::TintMask result;

		const auto textureID = TintMask::GetTintMaskTextureID(texturePath.data());
		if (textureID == 0)
			throw std::runtime_error("Invalid texture");
		result.tintMaskTextureID = textureID;

		const auto typeID = TintMask::GetTypeID(type);
		if (typeID == 255)
			throw std::runtime_error("Invalid type");
		result.tintType = typeID;

		const auto c = NumberToColor(color);
		result.color = c;
		result.color.a = 0;
		result.alpha = c.a / 256.0f;

		return result;
	}
}

struct PlayerWrap::Impl
{
	uint16_t id = 0;
	uint64_t uniqueID = 0;
	RakNet::RakNetGUID guid = RakNet::UNASSIGNED_RAKNET_GUID;
	std::string name;
};

PlayerWrap::PlayerWrap(uint16_t id) noexcept : pImpl(new Impl)
{
	auto player = g.plManager->GetPlayer(id);
	if (player)
	{
		pImpl->guid = player->GetGUID();
		pImpl->name = (player->GetName());
		pImpl->id = id;
		pImpl->uniqueID = player->uniqueID;
	}
}

PlayerWrap::~PlayerWrap() noexcept
{
}

bool PlayerWrap::operator==(const PlayerWrap &rhs) const noexcept
{
	return pImpl->guid == rhs.pImpl->guid;
}

bool PlayerWrap::IsConnected() const noexcept
{
	return this->FindPlayer() != nullptr;
}

bool PlayerWrap::IsSpawned() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
			return player->IsSpawned();
	}
	return false;
}

bool PlayerWrap::IsPaused() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
			return player->IsPaused();
	}
	return false;
}

uint16_t PlayerWrap::GetID() const noexcept
{
	if (this->IsConnected())
		return this->FindPlayer()->GetID();
	else
		return ~0;
}

std::string PlayerWrap::GetName() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		pImpl->name = (player->GetName());
	}
	return pImpl->name;
}

luabridge::LuaRef PlayerWrap::GetLocation() const noexcept
{
	auto L = g.gamemode->GetState();
	luabridge::LuaRef result(L);
	result = luabridge::Nil();
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto plLocation = player->GetLocation();
			if (plLocation != 0)
				result = LocationWrap(plLocation);
		}
	}
	return result;
}

NiPoint3 PlayerWrap::GetPos() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return movement->pos;
		}
	}
	return { 0,0,0 };
}

float PlayerWrap::GetX() const noexcept
{
	return this->GetPos().x;
}

float PlayerWrap::GetY() const noexcept
{
	return this->GetPos().y;
}
float PlayerWrap::GetZ() const noexcept
{
	return this->GetPos().z;
}

float PlayerWrap::GetAngleZ() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return (float)movement->angleZ;
		}
	}
	return 0.0f;
}

bool PlayerWrap::IsStanding() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return movement->runMode == Packet::MovementData::RunMode::Standing;
		}
	}
	return false;
}

bool PlayerWrap::IsWalking() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return movement->runMode == Packet::MovementData::RunMode::Walking;
		}
	}
	return false;
}

bool PlayerWrap::IsRunning() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return movement->runMode == Packet::MovementData::RunMode::Running;
		}
	}
	return false;
}

bool PlayerWrap::IsJumping() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return movement->jumpStage == Packet::MovementData::JumpStage::Jumping;
		}
	}
	return false;
}

bool PlayerWrap::IsFalling() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return movement->jumpStage == Packet::MovementData::JumpStage::Falling;
		}
	}
	return false;
}

bool PlayerWrap::IsInJumpState() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return movement->isInJumpState;
		}
	}
	return false;
}

bool PlayerWrap::IsSprinting() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return movement->isSprinting;
		}
	}
	return false;
}

bool PlayerWrap::IsSneaking() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return movement->isSneaking;
		}
	}
	return false;
}

bool PlayerWrap::IsWeaponDrawn() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return movement->isWeapDrawn;
		}
	}
	return false;
}

bool PlayerWrap::IsBlocking() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return movement->isBlocking;
		}
	}
	return false;
}

bool PlayerWrap::IsSwimming() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return movement->isSwimming;
		}
	}
	return false;
}

bool PlayerWrap::IsFirstPerson() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto movement = player->GetLastInjectedMovement();
			if (movement != nullptr)
				return movement->isFirstPerson;
		}
	}
	return false;
}

uint32_t PlayerWrap::GetVirtualWorld() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
			return player->GetVirtualWorld();
	}
	return 0;
}

luabridge::LuaRef PlayerWrap::GetRace() const noexcept
{
	auto L = g.gamemode->GetState();
	luabridge::LuaRef result(L);
	result = luabridge::Nil();
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (player != nullptr)
		{
			auto plRace = player->GetLook().raceID;
			if (plRace != 0)
				result = RaceWrap(plRace);
			else
				result = RaceWrap(0x00013746);
		}
	}
	return result;
}

bool PlayerWrap::IsFemale() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		return player->GetLook().isFemale;
	}
	return false;
}

float PlayerWrap::GetWeight() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		return player->GetLook().weight;
	}
	return 0.0f;
}

uint32_t PlayerWrap::GetSkinColor() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto color = player->GetLook().skinColor;
		color.a = 0xFF;
		return HelperFuncs::ColorToNumber(color);
	}
	return 0;
}

uint32_t PlayerWrap::GetHairColor() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto color = player->GetLook().hairColor;
		color.a = 0xFF;
		return HelperFuncs::ColorToNumber(color);
	}
	return 0;
}

uint32_t PlayerWrap::GetHeadpartCount() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		return (uint32_t)player->GetLook().headpartIDs.size();
	}
	return 0;
}

uint32_t PlayerWrap::GetNthHeadpartID(uint32_t indexStartsIn1) const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto i = indexStartsIn1 - 1;
		if (i < player->GetLook().headpartIDs.size())
			return player->GetLook().headpartIDs[i];
	}
	return 0;
}

uint32_t PlayerWrap::GetTintmaskCount() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		return (uint32_t)player->GetLook().tintmasks.size();
	}
	return 0;
}

uint8_t PlayerWrap::GetNthTintmaskTexture(uint32_t indexStartsIn1) const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto i = indexStartsIn1 - 1;
		if (i < player->GetLook().tintmasks.size())
			return /*TintMask::GetTintMaskTexturePath*/(player->GetLook().tintmasks[i].tintMaskTextureID);
	}
	return /*""*/0;
}

uint8_t PlayerWrap::GetNthTintmaskType(uint32_t indexStartsIn1) const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto i = indexStartsIn1 - 1;
		if (i < player->GetLook().tintmasks.size())
			return /*TintMask::GetTintTypeName*/(player->GetLook().tintmasks[i].tintType);
	}
	return /*""*/0;
}

uint32_t PlayerWrap::GetNthTintmaskColor(uint32_t indexStartsIn1) const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto i = indexStartsIn1 - 1;
		if (i < player->GetLook().tintmasks.size())
		{
			auto color = player->GetLook().tintmasks[i].color;
			color.a = uint8_t(player->GetLook().tintmasks[i].alpha * 256.0);
			return HelperFuncs::ColorToNumber(color);
		}
	}
	return 0;
}

float PlayerWrap::GetNthTintmaskAlpha(uint32_t indexStartsIn1) const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto i = indexStartsIn1 - 1;
		if (i < player->GetLook().tintmasks.size())
		{
			return player->GetLook().tintmasks[i].alpha;
		}
	}
	return 0;
}

uint32_t PlayerWrap::GetFaceOptionCount() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		return (uint32_t)player->GetLook().options.size();
	}
	return 0;
}

float PlayerWrap::GetNthFaceOption(uint32_t indexStartsIn1) const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto i = indexStartsIn1 - 1;
		if (i < player->GetLook().options.size())
			return player->GetLook().options[i];
	}
	return 0;
}

uint32_t PlayerWrap::GetFacePresetCount() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		return (uint32_t)player->GetLook().presets.size();
	}
	return 0;
}

uint32_t PlayerWrap::GetNthFacePreset(uint32_t indexStartsIn1) const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto i = indexStartsIn1 - 1;
		if (i < player->GetLook().presets.size())
			return player->GetLook().presets[i];
	}
	return 0;
}

uint32_t PlayerWrap::GetHeadTextureSetID() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		return player->GetLook().headTextureSetID;
	}
	return 0;
}

uint32_t PlayerWrap::GetItemCount(luabridge::LuaRef itemType) const noexcept
{
	try {
		if (this->IsConnected())
		{
			auto player = this->FindPlayer();
			auto type = itemType.cast<ItemTypeWrap>();
			return player->GetItemCount(type);
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "GetItemCount() ", e.what());
	}
	return 0;
}

uint32_t PlayerWrap::GetNumInventorySlots() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		return (uint32_t)player->GetItems().size();
	}
	return 0;
}

float PlayerWrap::GetTotalWeight() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		return player->GetTotalWeight();
	}
	return 0;
}

luabridge::LuaRef PlayerWrap::GetItemTypeInSlot(uint32_t slotID) const noexcept
{
	slotID--; // in lua starts at 1
	auto L = g.gamemode->GetState();
	luabridge::LuaRef result(L);
	result = luabridge::Nil();
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		const auto slot = player->GetInventorySlot(slotID);
		if (slot != nonstd::nullopt)
			result = slot->first.itemType;
	}
	return result;
}

uint32_t PlayerWrap::GetItemCountInSlot(uint32_t slotID) const noexcept
{
	slotID--; // in lua starts at 1
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		const auto slot = player->GetInventorySlot(slotID);
		if (slot != nonstd::nullopt)
			return slot->second;
	}
	return 0;
}

bool PlayerWrap::IsEquipped(luabridge::LuaRef itemType) const noexcept
{
	try {
		auto itemType_ = itemType.cast<ItemTypeWrap>();
		if (this->IsConnected())
		{
			auto player = this->FindPlayer();
			return player->IsEquipped(itemType_);
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "IsEquipped() ", e.what());
	}
	return false;
}

luabridge::LuaRef PlayerWrap::GetEquippedWeapon(luabridge::LuaRef handID) const noexcept
{
	auto L = g.gamemode->GetState();
	luabridge::LuaRef result(L);
	result = luabridge::Nil();
	try {
		if (handID.isNumber())
		{
			const auto hand = (int32_t)handID.cast<float>();
			if (hand >= 0 && hand <= 1 && this->IsConnected())
			{
				auto player = this->FindPlayer();
				auto item = player->GetEquippedWeapon(hand);
				if (item != nonstd::nullopt)
					result = item->itemType;
			}
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "GetEquippedWeapon() ", e.what());
	}
	return result;
}

bool PlayerWrap::IsMenuOpen(std::string utf8MenuName) noexcept
{
	utf8MenuName.erase(std::remove_if(utf8MenuName.begin(), utf8MenuName.end(), isspace), utf8MenuName.end());
	std::transform(utf8MenuName.begin(), utf8MenuName.end(), utf8MenuName.begin(), ::tolower);
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (utf8MenuName == "racesexmenu")
		{
			return player->IsWaitingForNewLook();
		}
	}
	return false;
}

bool PlayerWrap::IsDialogOpen(luabridge::LuaRef dialogID) noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (!dialogID.isNumber() || dialogID == -1)
			return player->GetCurrentDialogID() != ~0;
		else
			return dialogID == player->GetCurrentDialogID();
	}
	return false;
}

float PlayerWrap::GetBaseAV(const std::string &avName) const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		return player->GetAVData(avName).base;
	}
	return 0.0f;
}

float PlayerWrap::GetCurrentAV(const std::string &avName) const noexcept
{
	if (this->IsConnected() && avName[0] != '_' && avName[0] != '-')
	{
		auto player = this->FindPlayer();
		return player->GetAVData(avName).GetCurrent();
	}
	return 0.0f;
}

float PlayerWrap::GetAVPercentage(const std::string &avName) const noexcept
{
	if (this->IsConnected() && avName[0] != '_' && avName[0] != '-')
	{
		auto player = this->FindPlayer();
		return player->GetAVData(avName).percentage;
	}
	return 1.0f;
}

float PlayerWrap::GetAVMaximum(const std::string &avName) const noexcept
{
	if (this->IsConnected() && avName[0] != '_' && avName[0] != '-')
	{
		auto player = this->FindPlayer();
		return player->GetAVData(avName).GetAfterBuffs();
	}
	return 0.0f;
}

float PlayerWrap::GetSkillExperience(const std::string &skillName) const noexcept
{
	auto player = this->FindPlayer();
	if (player != nullptr)
	{
		auto realAvName = "-" + skillName + "Exp";
		auto avData = player->GetAVData(realAvName);
		return avData.GetAfterBuffs();
	}
	return 0;
}

bool PlayerWrap::HasMagic(luabridge::LuaRef spell) const noexcept
{
	try {
		if (this->IsConnected() && (spell.isLightUserdata() || spell.isUserdata()))
		{
			auto player = this->FindPlayer();
			return player->HasSpell(spell);
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "HasMagic() ", e.what());
	}
	return false;
}

uint32_t PlayerWrap::GetNumMagic() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		return player->GetNumSpells();
	}
	return 0;
}

luabridge::LuaRef PlayerWrap::GetNthMagic(uint32_t n) const noexcept
{
	luabridge::LuaRef res(g.gamemode->GetState());
	res = luabridge::Nil();

	if (this->IsConnected())
	{
		auto spellList = dynamic_cast<Player::SpellList *>(this->FindPlayer());
		if (spellList != nullptr && n <= spellList->GetNumSpells())
		{
			auto spells = spellList->GetSpells();
			auto it = spells.begin();

			--n;
			for (uint32_t i = 0; i < n; ++i)
				++it;

			res = *it;
		}
	}

	return res;
}

luabridge::LuaRef PlayerWrap::GetEquippedMagic(luabridge::LuaRef handID) const noexcept
{
	auto L = g.gamemode->GetState();
	luabridge::LuaRef result(L);
	result = luabridge::Nil();
	try {
		if (handID.isNumber())
		{
			const auto hand = (int32_t)handID.cast<float>();
			if (hand >= 0 && hand <= 1 && this->IsConnected())
			{
				auto player = this->FindPlayer();
				auto sp = player->GetEquippedSpell(hand);
				if (sp != nonstd::nullopt)
					result = *sp;
			}
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "GetEquippedMagic() ", e.what());
	}
	return result;
}

int32_t PlayerWrap::GetSoulSize() const noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		return (int32_t)player->GetSoulSize();
	}
	return 0;
}

luabridge::LuaRef PlayerWrap::GetCurrentFurniture() const noexcept
{
	auto L = g.gamemode->GetState();
	luabridge::LuaRef result(L);
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto obj = g.objManager->LookupObject(player->GetCurrentFurniture());
		if (obj != nullptr)
			result = ObjectWrap(obj->GetID());
	}
	return result;
}

bool PlayerWrap::IsNPC() const noexcept
{
	auto player = this->FindPlayer();
	return player != nullptr && player->IsNPC();
}

bool PlayerWrap::IsWerewolf() const noexcept
{
	auto player = this->FindPlayer();
	return player != nullptr && player->IsWerewolf();
}

std::string PlayerWrap::GetIP() const noexcept
{
	return g.networking->GetPeer()->GetSystemAddressFromGuid(pImpl->guid).ToString(false);
}

bool PlayerWrap::HasPerk(luabridge::LuaRef perk) const noexcept
{
	log(Log::Warning, "HasPerk() not implemented");
	return false;
}

uint32_t PlayerWrap::GetRefID() const noexcept
{
	auto player = this->FindPlayer();
	return player ? player->GetRefID() : 0;
}

uint32_t PlayerWrap::GetBaseID() const noexcept
{
	auto player = this->FindPlayer();
	return player ? player->GetBaseNPC() : 0;
}

bool PlayerWrap::SetName(const std::string &utf8name)
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (this->IsNPC() || PlayerWrap::LookupByName(utf8name).isNil())
		{
			player->SetName((utf8name));
			return true;
		}
	}
	return false;
}

void PlayerWrap::SetSpawnPoint(luabridge::LuaRef location, float x, float y, float z, float angleZ) noexcept
{
	try {
		if (this->IsConnected())
		{
			// Prevent server crash
			if (location.isNil() == false)
				if (location.isUserdata() || location.isLightUserdata())
				{
					spawnPointData[this->GetID()] = { pImpl->uniqueID, NiPoint3(x, y, z), (uint16_t)angleZ, location.cast<LocationWrap>().GetID() };
				}
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "SetSpawnPoint() ", e.what());
	}
}

bool PlayerWrap::Spawn() noexcept
{
	if (this->IsConnected())
	{
		try {
			auto data = spawnPointData.at(this->GetID());
			if (data.ownerUniqueID == pImpl->uniqueID)
			{
				auto player = this->FindPlayer();
				if (player != nullptr && !player->IsLoading())
				{
					player->MoveTo(data.locationID, data.pos, data.angleZ);
					player->SetLocation(data.locationID);
					return true;
				}
			}
		}
		catch (...) {
		}
	}
	return false;
}

void PlayerWrap::SendChatMessage(const std::string &message) noexcept
{
	if (this->IsConnected())
	{
		auto message_ = (message);
		if (message_ == " ")
			message_ = "   ";
		this->FindPlayer()->SendClientMessage(message_, false);
	}
}

void PlayerWrap::SetChatBubble(const std::string &message, uint32_t ms, luabridge::LuaRef optionalShowSelf) noexcept
{
	if (this->IsConnected())
	{
		this->FindPlayer()->SetChatBubble(message, ms, optionalShowSelf.isNil() == false && optionalShowSelf.cast<bool>());
	}
}

bool PlayerWrap::ShowDialog(uint32_t dialogID, const std::string &style, const std::string &title, const std::string &text, luabridge::LuaRef defaultIndexRef) noexcept
{
	if (dialogID == ~0)
		return false;
	static auto strToDialogStyle = [](std::string str) {
		std::transform(str.begin(), str.end(), str.begin(), ::tolower);
		if (str == "list")
			return Player::DialogStyle::List;
		else if (str == "input")
			return Player::DialogStyle::Input;
		else
			return Player::DialogStyle::Message;
	};

	if (this->IsConnected())
	{
		int32_t defaultIndex = 0;
		try {
			defaultIndex = defaultIndexRef.cast<int32_t>();
		}
		catch (...) {
		}
		auto pl = this->FindPlayer();
		if (pl->GetCurrentDialogID() == ~0)
		{
			pl->ShowDialog(dialogID, title, strToDialogStyle(style), text, defaultIndex);
		}
		return true;
	}
	return false;
}

void PlayerWrap::HideDialog() noexcept
{
	if (this->IsConnected())
	{
		auto pl = this->FindPlayer();
		pl->ShowDialog(~0, "", Player::DialogStyle::Message, "", 0);
	}
}

bool PlayerWrap::SetPos(float x, float y, float z) noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (this->IsSpawned() && player->IsDying() == false)
		{
			player->MoveTo(nonstd::nullopt, NiPoint3{ x,y,z }, nonstd::nullopt);
			return true;
		}
	}
	return false;
}

bool PlayerWrap::SetAngleZ(float degrees) noexcept
{
	while (degrees > 360)
		degrees -= 360;
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		if (this->IsSpawned() && player->IsDying() == false)
		{
			player->MoveTo(nonstd::nullopt, nonstd::nullopt, (uint16_t)degrees);
			return true;
		}
	}
	return false;
}

void PlayerWrap::SetVirtualWorld(uint32_t world) noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		player->SetVirtualWorld(world);
	}
}

void PlayerWrap::Kick() noexcept
{
	if (this->FindPlayer() != nullptr)
	{
		const auto wrap = *this;
		g.timers->SetTimer(std::chrono::system_clock::now() + 1ms, [=] {
			if (wrap.FindPlayer() != nullptr)
			{
				g.networking->CloseConnection(wrap.FindPlayer()->GetGUID(), ID_SERVER_CLOSED_THE_CONNECTION);
				g.networking->DisconnectPlayer(wrap.FindPlayer()->GetGUID());
			}
		});
	}
}

bool PlayerWrap::ShowMenu(std::string utf8MenuName) noexcept
{
	utf8MenuName.erase(std::remove_if(utf8MenuName.begin(), utf8MenuName.end(), isspace), utf8MenuName.end());
	std::transform(utf8MenuName.begin(), utf8MenuName.end(), utf8MenuName.begin(), ::tolower);
	if (this->IsConnected() && this->IsSpawned() && !this->IsPaused())
	{
		auto player = this->FindPlayer();
		if (utf8MenuName == "racesexmenu")
		{
			player->ShowRaceMenu();
			return true;
		}
	}
	return false;
}

void PlayerWrap::SetRace(luabridge::LuaRef newRace_) noexcept
{
	try {
		if (this->IsConnected())
		{
			const auto newRace = newRace_.cast<RaceWrap>();
			const auto player = this->FindPlayer();
			auto look = player->GetLook();
			look.raceID = newRace.GetID();
			player->SetLook(look);
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "SetRace() ", e.what());
	}
}

void PlayerWrap::SetFemale(bool isFemale) noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto look = player->GetLook();
		look.isFemale = isFemale;
		player->SetLook(look);
	}
}

void PlayerWrap::SetWeight(float weight) noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto look = player->GetLook();
		look.weight = (uint8_t)weight;
		if (look.weight > 100)
			look.weight = 100;
		player->SetLook(look);
	}
}

void PlayerWrap::SetSkinColor(uint32_t color) noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto look = player->GetLook();
		look.skinColor = HelperFuncs::NumberToColor(color);
		player->SetLook(look);
	}
}

void PlayerWrap::SetHairColor(uint32_t color) noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto look = player->GetLook();
		look.hairColor = HelperFuncs::NumberToColor(color);
		player->SetLook(look);
	}
}

void PlayerWrap::SetHeadpartIDs(luabridge::LuaRef list) noexcept
{
	try {
		if (this->IsConnected())
		{
			auto player = this->FindPlayer();
			auto look = player->GetLook();
			std::vector<uint32_t> headpartIDs;
			int32_t idx = 1;
			while (true)
			{
				luabridge::LuaRef val = list[idx++];
				if (val == luabridge::Nil())
					break;
				try {
					headpartIDs.push_back(val.cast<uint32_t>());
				}
				catch (...) {
				}
			}
			look.headpartIDs = headpartIDs;
			player->SetLook(look);
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "SetHeadpartIDs() ", e.what());
	}
}

void PlayerWrap::RemoveAllTintmasks() noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto look = player->GetLook();
		look.tintmasks = {};
		player->SetLook(look);
	}
}

void PlayerWrap::SetNthTintmask(uint32_t indexStartsIn1, uint8_t texturePath, uint8_t type, uint32_t color, float alpha)
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto look = player->GetLook();
		auto i = indexStartsIn1 - 1;
		if (i < look.tintmasks.size())
		{
			try {
				look.tintmasks[i] = /*HelperFuncs::CreateTintmask(texturePath, type, color);*/ Packet::LookData::TintMask();
				look.tintmasks[i].tintMaskTextureID = texturePath;
				look.tintmasks[i].tintType = type;
				auto c = HelperFuncs::NumberToColor(color);
				look.tintmasks[i].color = c;
				look.tintmasks[i].alpha = alpha;
				player->SetLook(look);
			}
			catch (...) {
			}
		}
	}
}

void PlayerWrap::AddTintmask(uint8_t texturePath, uint8_t type, uint32_t color, float alpha)
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto look = player->GetLook();
		try {
			auto tm = Packet::LookData::TintMask();
			tm.tintMaskTextureID = texturePath;
			tm.tintType = type;
			auto c = HelperFuncs::NumberToColor(color);
			tm.color = c;
			tm.alpha = alpha;
			player->SetLook(look);
			look.tintmasks.push_back(tm);
			player->SetLook(look);
		}
		catch (...) {
		}
	}
}

void PlayerWrap::SetFaceOptions(luabridge::LuaRef list) noexcept
{
	try {
		if (this->IsConnected())
		{
			auto player = this->FindPlayer();
			auto look = player->GetLook();
			std::array<float, 19> options;
			int32_t idx = 1;
			while (true)
			{
				luabridge::LuaRef val = list[idx];
				if (val == luabridge::Nil())
					break;
				try {
					if (idx - 1 < (int64_t)options.size())
						options[idx - 1] = val.cast<float>();
				}
				catch (...) {
				}
				idx++;
			}
			look.options = options;
			player->SetLook(look);
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "SetFaceOptions() ", e.what());
	}
}

void PlayerWrap::SetFacePresets(luabridge::LuaRef list) noexcept
{
	try {
		if (this->IsConnected())
		{
			auto player = this->FindPlayer();
			auto look = player->GetLook();
			std::array<uint32_t, 4> presets;
			int32_t idx = 1;
			while (true)
			{
				luabridge::LuaRef val = list[idx];
				if (val == luabridge::Nil())
					break;
				try {
					if (idx - 1 < (int64_t)presets.size())
						presets[idx - 1] = val.cast<uint32_t>();
				}
				catch (...) {
				}
				idx++;
			}
			look.presets = presets;
			player->SetLook(look);
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "SetFacePresets() ", e.what());
	}
}

void PlayerWrap::SetHeadTextureSetID(uint32_t formID) noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto look = player->GetLook();
		look.headTextureSetID = formID;
		player->SetLook(look);
	}
}

void PlayerWrap::AddItem(luabridge::LuaRef itemType, int32_t count) noexcept
{
	try {
		if (this->IsConnected() && (itemType.isUserdata() || itemType.isLightUserdata()))
		{
			if (count > 0)
			{
				auto player = this->FindPlayer();
				player->AddItem(itemType.cast<ItemTypeWrap>(), count);
			}
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "AddItem() ", e.what());
	}
}

bool PlayerWrap::RemoveItem(luabridge::LuaRef itemType, uint32_t count) noexcept
{
	try {
		if (this->IsConnected())
		{
			auto player = this->FindPlayer();
			return player->RemoveItem(itemType.cast<ItemTypeWrap>(), count);
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "RemoveItem() ", e.what());
	}
	return false;
}

auto setEquipped = [](const auto player, const auto &equipable, luabridge::LuaRef &handID, bool equipped) {
	{
		int32_t hand = -1;
		if (handID.isNumber())
			hand = (int32_t)((float)handID);
		if (equipped)
			player->Equip(equipable, hand);
		else
			player->Unequip(equipable, hand);
	}
};

void PlayerWrap::EquipItem(luabridge::LuaRef itemTypeRef, luabridge::LuaRef handID) noexcept
{
	try {
		if (this->IsConnected())
		{
			if (itemTypeRef.isLightUserdata() || itemTypeRef.isUserdata())
			{
				auto itemType = itemTypeRef.cast<ItemTypeWrap>();
				auto item = Components::Container::Item{ itemType };
				return setEquipped(this->FindPlayer(), item, handID, true);
			}
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "UnequipItem() ", e.what());
	}
}

void PlayerWrap::UnequipItem(luabridge::LuaRef itemTypeRef, luabridge::LuaRef handID) noexcept
{
	try {
		if (this->IsConnected())
		{
			if (itemTypeRef.isLightUserdata() || itemTypeRef.isUserdata())
			{
				auto itemType = itemTypeRef.cast<ItemTypeWrap>();
				auto item = Components::Container::Item{ itemType };
				return setEquipped(this->FindPlayer(), item, handID, false);
			}
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "UnequipItem() ", e.what());
	}
}

void PlayerWrap::RemoveAllItems() noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		player->RemoveAllItems();
	}
}

void PlayerWrap::MuteInventoryNotifications(bool mute) noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		player->SetSilent(mute);
	}
}

void PlayerWrap::SetWeather(uint32_t weatherTypeOrID) noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		player->SetWeather(weatherTypeOrID);
	}
}

void PlayerWrap::SetGlobal(uint32_t globalID, float value) noexcept
{
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		player->SetGlovalVariable(globalID, value);
	}
}

void PlayerWrap::SetBaseAV(const std::string &avName, float value) noexcept
{
	if (avName.empty() || avName[0] == '_' || avName[0] == '-')
		return;
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto avData = player->GetAVData(avName);
		avData.base = value;
		player->SetAVData(avName, avData);
	}
}

void PlayerWrap::ModBaseAV(const std::string &avName, float value) noexcept
{
	if (avName.empty() || avName[0] == '_' || avName[0] == '-')
		return;
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto avData = player->GetAVData(avName);
		avData.modifier += value;
		player->SetAVData(avName, avData);
	}
}

void PlayerWrap::SetCurrentAV(const std::string &avName, float value) noexcept
{
	if (avName.empty() || avName[0] == '_' || avName[0] == '-')
		return;
	if (this->IsConnected())
	{
		auto player = this->FindPlayer();
		auto avData = player->GetAVData(avName);
		avData.percentage = value / (avData.GetAfterBuffs());
		if (avData.percentage < 0)
			avData.percentage = 0;
		if (avData.percentage > 1)
			avData.percentage = 1;
		player->SetAVData(avName, avData);
	}
}

void PlayerWrap::SetGameSettingFloat(const std::string &name, float value) noexcept
{
	[&](std::string name) {
		name = "_" + name;
		if (this->IsConnected())
		{
			auto player = this->FindPlayer();
			auto avData = player->GetAVData(name);
			if (avData.base == player->GetInvalidAVBase())
				return;
			avData.base = value;
			player->SetAVData(name, avData);
		}
	}(name);
}

void PlayerWrap::SetSkillExperience(const std::string &skillName, float numPercents) noexcept
{
	auto player = this->FindPlayer();
	if (player != nullptr)
	{
		auto realAvName = "-" + skillName + "Exp";
		auto avData = player->GetAVData(realAvName);
		avData.base = (uint16_t)numPercents;
		avData.modifier = 0;
		player->SetAVData(realAvName, avData);
	}
}

void PlayerWrap::IncrementSkill(const std::string &skillName) noexcept
{
	auto pl = this->FindPlayer();
	if (pl != nullptr)
	{
		std::ostringstream ss;
		ss << "IncrementSkill(" << skillName << ")";
		pl->SendCommand(Player::CommandType::SkyMP, ss.str()); 

		auto skill = (uint32_t)this->GetBaseAV(skillName);
		skill++;
		this->SetBaseAV(skillName, (float)skill);

		this->SetSkillExperience(skillName, 0);

		auto xp = this->GetBaseAV("experience");
		xp += this->GetBaseAV(skillName);
		this->SetBaseAV("experience", xp);
	}
}

void PlayerWrap::TriggerScreenBlood(uint32_t val) noexcept
{
	if (this->IsConnected())
	{
		auto pl = this->FindPlayer();
		std::ostringstream ss;
		ss << "Game.TriggerScreenBlood(" << val << ")";
		pl->SendCommand(Player::CommandType::CDScript, ss.str());
	}
}

void PlayerWrap::SetDisplayGold(uint32_t count) noexcept
{
	if (this->IsConnected())
	{
		auto pl = this->FindPlayer();
		if (displayedGold[pl->GetGUID()] != count)
		{
			std::ostringstream ss;
			ss << "SetDisplayGold(" << std::to_string(count) << ")";
			pl->SendCommand(Player::CommandType::SkyMP, ss.str());
			displayedGold[pl->GetGUID()] = count;
		}
	}
}

void PlayerWrap::AddMagic(luabridge::LuaRef spell) noexcept
{
	try {
		if (this->IsConnected())
		{
			auto pl = this->FindPlayer();
			pl->AddSpell(spell);
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "AddMagic() ", e.what());
	}
}

void PlayerWrap::RemoveMagic(luabridge::LuaRef spell) noexcept
{
	try {
		if (this->IsConnected())
		{
			auto pl = this->FindPlayer();
			pl->RemoveSpell(spell);
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "RemoveMagic() ", e.what());
	}
}

void PlayerWrap::EquipMagic(luabridge::LuaRef magic, luabridge::LuaRef handID) noexcept
{
	try {
		if (this->IsConnected())
		{
			const SpellWrap spell = magic;
			setEquipped(this->FindPlayer(), spell, handID, true);
			return;
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "EquipMagic() ", e.what());
	}
}

void PlayerWrap::UnequipMagic(luabridge::LuaRef magic, luabridge::LuaRef handID) noexcept
{
	try {
		if (this->IsConnected())
		{
			const SpellWrap spell = magic;
			setEquipped(this->FindPlayer(), spell, handID, false);
			return;
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "UnequipMagic() ", e.what());
	}
}

void PlayerWrap::RemoveAllMagic() noexcept
{
	if (this->IsConnected())
	{
		auto pl = this->FindPlayer();
		pl->RemoveAllSpells();
	}
}

void PlayerWrap::MakeHost() noexcept
{
	return this->StartDataSearch();
}

void PlayerWrap::StartDataSearch() noexcept
{
	if (this->IsConnected())
	{
		auto pl = this->FindPlayer();
		pl->StartDataSearch();
	}
}

void PlayerWrap::EnableDataSearchOpcode(std::string opcodeStr, bool enable) noexcept
{
	if (this->IsConnected())
	{
		auto pl = this->FindPlayer();

		std::transform(opcodeStr.begin(), opcodeStr.end(), opcodeStr.begin(), ::tolower);

		DataSearch::Opcode opcode;
		if (opcodeStr == "navmesh")
			opcode = DataSearch::Opcode::NavMesh;
		else if (opcodeStr == "tpds")
			opcode = DataSearch::Opcode::TPDs;
		else if (opcodeStr == "cont")
			opcode = DataSearch::Opcode::Cont;
		else if (opcodeStr == "door")
			opcode = DataSearch::Opcode::Door;
		else if (opcodeStr == "item")
			opcode = DataSearch::Opcode::Item;
		else if (opcodeStr == "actor")
			opcode = DataSearch::Opcode::Actor;
		else if (opcodeStr == "acti")
			opcode = DataSearch::Opcode::Acti;
		else
			return;

		pl->SetDSOpcodeEnabled(opcode, enable);
	}
}

void PlayerWrap::SendAnimationEvent(const std::string &animEventName, luabridge::LuaRef noSendSelf) noexcept
{
	for (int32_t i = 0; i < 3; ++i)
	{
		if (this->IsConnected())
		{
			auto pl = this->FindPlayer();
			auto aeID = pl->RegisterAnimation(animEventName);

			if (aeID != ~0)
				pl->InjectAnim(aeID, noSendSelf.isNil() || !noSendSelf.cast<bool>());
		}
	}
}

void PlayerWrap::SetSoulSize(int32_t soulSize) noexcept
{
	if (this->IsConnected())
	{
		auto pl = this->FindPlayer();
		if (soulSize > (int32_t)SoulSize::BACK)
			soulSize = (int32_t)SoulSize::BACK;
		if (soulSize < (int32_t)SoulSize::FRONT)
			soulSize = (int32_t)SoulSize::FRONT;
		pl->SetSoulSize((SoulSize)soulSize);
	}
}

void PlayerWrap::TrapSoul(luabridge::LuaRef targetPl) noexcept
{
	try {
		if (this->IsConnected())
		{
			auto pl = this->FindPlayer();
			if (targetPl.isLightUserdata() || targetPl.isUserdata())
			{
				auto targetWrap = targetPl.cast<PlayerWrap>();
				if (targetWrap.IsConnected())
				{
					auto target = g.plManager->GetPlayer(targetWrap.pImpl->guid);
					pl->TrapSoul(target);
				}
			}
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "TrapSoul() ", e.what());
	}
}

void PlayerWrap::EnableMovementSync(bool e) noexcept
{
	if (this->IsConnected())
	{
		auto pl = this->FindPlayer();
		pl->EnableMovementSync(e);
	}
}

void PlayerWrap::UpdateRecipeInfo(luabridge::LuaRef recipe) noexcept
{
	try {
		if (this->IsConnected())
		{
			auto pl = this->FindPlayer();
			pl->UpdateRecipe(recipe.cast<RecipeWrap>());
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "UpdateRecipeInfo() ", e.what());
	}
}

void PlayerWrap::ApplyMagicEffect(luabridge::LuaRef ef, float mag, float dur, float area) noexcept
{
	try {
		if (this->IsConnected())
		{
			auto effect = ef.cast<EffectWrap>();
			auto pl = this->FindPlayer();
			pl->ApplyEffect(effect, mag, dur, ~0);
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "ApplyMagicEffect() ", e.what());
	}
}

void PlayerWrap::SetCombatTarget(luabridge::LuaRef pl) noexcept
{
	try {
		if (this->IsConnected() == false)
			return;
		auto player = this->FindPlayer();

		if (pl.isLightUserdata() || pl.isUserdata())
		{
			auto targetWrap = pl.cast<PlayerWrap>();
			auto target = g.plManager->GetPlayer(targetWrap.GetID());
			if (player != nullptr && target != nullptr)
				player->SetAggressive(true);
			else
				player->SetAggressive(false);
			log(Log::Warning, "SetCombatTarget is deprecated, use SetAggressive");
		}
		else
		{
			player->SetAggressive(false);
		}
	}
	catch (const std::exception &e)
	{
		log(Log::Error, "SetCombatTarget() ", e.what());
	}
}

void PlayerWrap::SetWerewolf(bool isWerewolf) noexcept
{
	auto player = this->FindPlayer();
	if (player != nullptr)
		player->SetWerewolf(isWerewolf);
}

void PlayerWrap::Mount(luabridge::LuaRef targetHorse) noexcept
{
	try {
		auto pl = this->FindPlayer();
		if (pl == nullptr)
			return;
		auto horseWrap = targetHorse.cast<PlayerWrap>();
		const auto horseID = horseWrap.GetID();
		auto horse = g.plManager->GetPlayer(horseID);
		if (!horse)
			return;
		if (horse->IsNPC() == false)
			return;
		auto &horseRider = horseRiders[horse->GetGUID()];
		if (horseRider.has_value() == false || horseRider->IsConnected() == false || (horseRider->GetPos() - horseWrap.GetPos()).Length() > 256)
		{
			horseRider = PlayerWrap(this->GetID());
			std::ostringstream ss;
			ss << "Mount(" << std::to_string(horseID) << ")";
			pl->SendCommand(Player::CommandType::SkyMP, ss.str());
			horse->SetForegroundHost(pl);
		}
	}
	catch (const std::exception &e) {
		log(Log::Error, "Mount() ", e.what());
	}

}

void PlayerWrap::Dismount() noexcept
{
	auto pl = this->FindPlayer();
	if (pl == nullptr)
		return;
	pl->SendCommand(Player::CommandType::SkyMP, "Dismount()"); 

	RakNet::RakNetGUID horseGUID = RakNet::UNASSIGNED_RAKNET_GUID;
	for (auto pair : horseRiders)
	{
		if (pair.second.has_value() && pair.second->GetID() == this->GetID())
		{
			horseGUID = pair.first;
			break;
		}
	}
	if (horseGUID != RakNet::UNASSIGNED_RAKNET_GUID)
	{
		horseRiders.erase(horseGUID);
		auto horse = g.plManager->GetPlayer(horseGUID);
		if (horse != nullptr)
		{
			horse->SetForegroundHost(nullptr);
		}
	}
}

luabridge::LuaRef PlayerWrap::GetRider() noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();
	auto this_ = g.plManager->GetPlayer(this->GetID());
	if (this_ != nullptr && this_->IsNPC())
	{
		const auto &riderWrapOpt = horseRiders[this_->GetGUID()];
		if (riderWrapOpt.has_value())
		{
			if (riderWrapOpt->IsConnected())
			{
				result = *riderWrapOpt;
			}
		}
	}
	return result;
}

void PlayerWrap::ClearChat() noexcept
{
	if (this->IsConnected())
		this->FindPlayer()->SendClientMessage(" ", false);
}

void PlayerWrap::AddPerk(luabridge::LuaRef p) noexcept
{
	if (p.isUserdata() || p.isLightUserdata())
	{
		try {
			if (this->IsConnected())
			{
				auto pl = this->FindPlayer();
				auto perk = p.cast<PerkWrap>();
				std::ostringstream ss;
				ss << "AddPerk(" << std::to_string(perk.GetID()) << "," << (uint32_t)perk.GetSkillLevel() << ")";
				pl->SendCommand(Player::CommandType::SkyMP, ss.str());
			}
		}
		catch (const std::exception &e)
		{
			log(Log::Error, "AddPerk() ", e.what());
		}
	}
}

void PlayerWrap::RemovePerk(luabridge::LuaRef p) noexcept
{
	if (p.isUserdata() || p.isLightUserdata())
	{
		try {
			if (this->IsConnected())
			{
				auto pl = this->FindPlayer();
				auto perk = p.cast<PerkWrap>();
				std::ostringstream ss;
				ss << "RemovePerk(" << std::to_string(perk.GetID()) << ")";
				pl->SendCommand(Player::CommandType::SkyMP, ss.str());
			}
		}
		catch (const std::exception &e)
		{
			log(Log::Error, "RemovePerk() ", e.what());
		}
	}
}

void PlayerWrap::ExecuteCommand(std::string target, std::string cmdText) noexcept
{
	std::transform(target.begin(), target.end(), target.begin(), ::tolower);
	auto pl = this->FindPlayer();
	if (pl != nullptr)
	{
		auto type = Player::CommandType::Console;
		if (target == "console")
			type = Player::CommandType::Console;
		else if (target == "skymp")
			type = Player::CommandType::SkyMP;
		else if (target == "cdscript")
			type = Player::CommandType::CDScript;
		pl->SendCommand(type, cmdText);
	}
}

void PlayerWrap::SetEffectLearned(luabridge::LuaRef itemType, uint32_t effectIdx, bool learned) noexcept
{
	effectIdx--;
	try {
		if (itemType.isUserdata() || itemType.isLightUserdata())
		{
			auto pl = this->FindPlayer();
			if (pl != nullptr)
			{
				pl->LearnItemTypeEffect(itemType.cast<ItemTypeWrap>(), effectIdx, learned);
			}
		}
	}
	catch (const std::exception &e) {
		log(Log::Error, "SetEffectLearned() ", e.what());
	}
}

void PlayerWrap::SetControlEnabled(std::string controlName, bool enable) noexcept
{
	auto pl = this->FindPlayer();
	if (pl == nullptr)
		return;
	std::ostringstream ss;
	ss << "SetControlEnabled(" << controlName << "," << (enable ? 1 : 0) << ")";
	pl->SendCommand(Player::CommandType::SkyMP, ss.str());
}

luabridge::LuaRef PlayerWrap::GetHost() noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();
	auto pl = this->FindPlayer();
	if (pl == nullptr)
		return result;
	auto host = g.plManager->GetPlayer(pl->GetHostGUIDRaw());
	if (host)
	{
		result = PlayerWrap(host->GetID());
	}
	return result;
}

void PlayerWrap::DebugUpdateChunk() noexcept
{
	auto pl = this->FindPlayer();
	if (pl == nullptr)
		return;
	pl->UpdateStreamedPlayersList();
}

void PlayerWrap::SetAggressive(bool v) noexcept
{
	auto pl = this->FindPlayer();
	if (pl != nullptr)
		pl->SetAggressive(v);
}

uint16_t PlayerWrap::GetInvalidPlayerID() noexcept
{
	return ~0;
}

luabridge::LuaRef PlayerWrap::LookupByID(uint16_t id) noexcept
{
	auto playerWrap = PlayerWrap(id);
	auto L = g.gamemode->GetState();
	luabridge::LuaRef result(L);
	if (playerWrap.GetID() != PlayerWrap::GetInvalidPlayerID())
		result = playerWrap;
	else
		result = luabridge::Nil();
	return result;
}

luabridge::LuaRef PlayerWrap::LookupByName(const std::string &name) noexcept
{
	auto L = g.gamemode->GetState();
	luabridge::LuaRef result(L);
	result = luabridge::Nil();
	auto &players = g.plManager->GetPlayers();
	for (auto it = players.begin(); it != players.end(); ++it)
	{
		auto player = it->second;
		if (player && player->GetName() == name && player->IsNPC() == false)
		{
			result = PlayerWrap(player->GetID());
			break;
		}
	}
	return result;
}

luabridge::LuaRef PlayerWrap::CreateNPC(luabridge::LuaRef baseRef) noexcept
{
	auto L = g.gamemode->GetState();
	luabridge::LuaRef result(L);
	result = luabridge::Nil();

	auto npc = g.plManager->NewNPC();
	if (npc != nullptr)
	{
		npc->SetName("");
		result = PlayerWrap(npc->GetID());
		
		if (baseRef.isNumber())
		{
			try {
				const uint32_t base = baseRef.cast<uint32_t>();
				npc->SetBaseNPC(base);
			}
			catch (const std::exception &e) {
				log(Log::Error, e.what());
			}
		}
	}

	return result;
}

void PlayerWrap::Register(lua_State *L)
{
	luabridge::getGlobalNamespace(L)
		.beginClass<PlayerWrap>("Player")
		//.addConstructor<void(*)(uint16_t)>()
		.addFunction("__eq", &PlayerWrap::operator==)
		.addFunction("IsConnected", &PlayerWrap::IsConnected)
		.addFunction("IsSpawned", &PlayerWrap::IsSpawned)
		.addFunction("IsPaused", &PlayerWrap::IsPaused)
		.addFunction("GetID", &PlayerWrap::GetID)
		.addFunction("GetName", &PlayerWrap::GetName)
		.addFunction("GetLocation", &PlayerWrap::GetLocation)
		.addFunction("GetX", &PlayerWrap::GetX)
		.addFunction("GetY", &PlayerWrap::GetY)
		.addFunction("GetZ", &PlayerWrap::GetZ)
		.addFunction("GetAngleZ", &PlayerWrap::GetAngleZ)
		.addFunction("IsStanding", &PlayerWrap::IsStanding)
		.addFunction("IsWalking", &PlayerWrap::IsWalking)
		.addFunction("IsRunning", &PlayerWrap::IsRunning)
		.addFunction("IsJumping", &PlayerWrap::IsJumping)
		.addFunction("IsFalling", &PlayerWrap::IsFalling)
		.addFunction("IsInJumpState", &PlayerWrap::IsInJumpState)
		.addFunction("IsSprinting", &PlayerWrap::IsSprinting)
		.addFunction("IsSneaking", &PlayerWrap::IsSneaking)
		.addFunction("IsWeaponDrawn", &PlayerWrap::IsWeaponDrawn)
		.addFunction("IsBlocking", &PlayerWrap::IsBlocking)
		.addFunction("IsSwimming", &PlayerWrap::IsSwimming)
		.addFunction("IsFirstPerson", &PlayerWrap::IsFirstPerson)
		.addFunction("GetVirtualWorld", &PlayerWrap::GetVirtualWorld)
		.addFunction("GetRace", &PlayerWrap::GetRace)
		.addFunction("IsFemale", &PlayerWrap::IsFemale)
		.addFunction("GetWeight", &PlayerWrap::GetWeight)
		.addFunction("GetSkinColor", &PlayerWrap::GetSkinColor)
		.addFunction("GetHairColor", &PlayerWrap::GetHairColor)
		.addFunction("GetHeadpartCount", &PlayerWrap::GetHeadpartCount)
		.addFunction("GetNthHeadpartID", &PlayerWrap::GetNthHeadpartID)
		.addFunction("GetTintmaskCount", &PlayerWrap::GetTintmaskCount)
		.addFunction("GetNthTintmaskTexture", &PlayerWrap::GetNthTintmaskTexture)
		.addFunction("GetNthTintmaskType", &PlayerWrap::GetNthTintmaskType)
		.addFunction("GetNthTintmaskColor", &PlayerWrap::GetNthTintmaskColor)
		.addFunction("GetNthTintmaskAlpha", &PlayerWrap::GetNthTintmaskAlpha)
		.addFunction("GetFaceOptionCount", &PlayerWrap::GetFaceOptionCount)
		.addFunction("GetNthFaceOption", &PlayerWrap::GetNthFaceOption)
		.addFunction("GetFacePresetCount", &PlayerWrap::GetFacePresetCount)
		.addFunction("GetNthFacePreset", &PlayerWrap::GetNthFacePreset)
		.addFunction("GetHeadTextureSetID", &PlayerWrap::GetHeadTextureSetID)
		.addFunction("GetItemCount", &PlayerWrap::GetItemCount)
		.addFunction("GetNumInventorySlots", &PlayerWrap::GetNumInventorySlots)
		.addFunction("GetTotalWeight", &PlayerWrap::GetTotalWeight)
		.addFunction("GetItemTypeInSlot", &PlayerWrap::GetItemTypeInSlot)
		.addFunction("GetItemCountInSlot", &PlayerWrap::GetItemCountInSlot)
		.addFunction("GetEquippedWeapon", &PlayerWrap::GetEquippedWeapon)
		.addFunction("IsEquipped", &PlayerWrap::IsEquipped)
		.addFunction("IsMenuOpen", &PlayerWrap::IsMenuOpen)
		.addFunction("IsDialogOpen", &PlayerWrap::IsDialogOpen)
		.addFunction("GetBaseAV", &PlayerWrap::GetBaseAV)
		.addFunction("GetCurrentAV", &PlayerWrap::GetCurrentAV)
		.addFunction("GetAVPercentage", &PlayerWrap::GetAVPercentage)
		.addFunction("GetAVMaximum", &PlayerWrap::GetAVMaximum)
		.addFunction("GetSkillExperience", &PlayerWrap::GetSkillExperience)
		.addFunction("HasMagic", &PlayerWrap::HasMagic)
		.addFunction("GetNumMagic", &PlayerWrap::GetNumMagic)
		.addFunction("GetNthMagic", &PlayerWrap::GetNthMagic)
		.addFunction("GetEquippedMagic", &PlayerWrap::GetEquippedMagic)
		.addFunction("GetSoulSize", &PlayerWrap::GetSoulSize)
		.addFunction("GetCurrentFurniture", &PlayerWrap::GetCurrentFurniture)
		.addFunction("IsNPC", &PlayerWrap::IsNPC)
		.addFunction("IsWerewolf", &PlayerWrap::IsWerewolf)
		.addFunction("GetIP", &PlayerWrap::GetIP) 
		.addFunction("HasPerk", &PlayerWrap::HasPerk)
		.addFunction("GetRefID", &PlayerWrap::GetRefID)
		.addFunction("GetBaseID", &PlayerWrap::GetBaseID)
		.addFunction("SetName", &PlayerWrap::SetName)
		.addFunction("SetSpawnPoint", &PlayerWrap::SetSpawnPoint)
		.addFunction("Spawn", &PlayerWrap::Spawn)
		.addFunction("SendChatMessage", &PlayerWrap::SendChatMessage)
		.addFunction("SetChatBubble", &PlayerWrap::SetChatBubble)
		.addFunction("ShowDialog", &PlayerWrap::ShowDialog)
		.addFunction("HideDialog", &PlayerWrap::HideDialog)
		.addFunction("SetPos", &PlayerWrap::SetPos)
		.addFunction("SetAngleZ", &PlayerWrap::SetAngleZ)
		.addFunction("SetVirtualWorld", &PlayerWrap::SetVirtualWorld)
		.addFunction("Kick", &PlayerWrap::Kick)
		.addFunction("ShowMenu", &PlayerWrap::ShowMenu)
		.addFunction("SetRace", &PlayerWrap::SetRace)
		.addFunction("SetFemale", &PlayerWrap::SetFemale)
		.addFunction("SetWeight", &PlayerWrap::SetWeight)
		.addFunction("SetSkinColor", &PlayerWrap::SetSkinColor)
		.addFunction("SetHairColor", &PlayerWrap::SetHairColor)
		.addFunction("SetHeadpartIDs", &PlayerWrap::SetHeadpartIDs)
		.addFunction("RemoveAllTintmasks", &PlayerWrap::RemoveAllTintmasks)
		.addFunction("SetNthTintmask", &PlayerWrap::SetNthTintmask)
		.addFunction("AddTintmask", &PlayerWrap::AddTintmask)
		.addFunction("SetFaceOptions", &PlayerWrap::SetFaceOptions)
		.addFunction("SetFacePresets", &PlayerWrap::SetFacePresets)
		.addFunction("SetHeadTextureSetID", &PlayerWrap::SetHeadTextureSetID)
		.addFunction("AddItem", &PlayerWrap::AddItem)
		.addFunction("RemoveItem", &PlayerWrap::RemoveItem)
		.addFunction("EquipItem", &PlayerWrap::EquipItem)
		.addFunction("UnequipItem", &PlayerWrap::UnequipItem)
		.addFunction("RemoveAllItems", &PlayerWrap::RemoveAllItems)
		.addFunction("MuteInventoryNotifications", &PlayerWrap::MuteInventoryNotifications)
		.addFunction("SetWeather", &PlayerWrap::SetWeather)
		.addFunction("SetGlobal", &PlayerWrap::SetGlobal)
		.addFunction("SetBaseAV", &PlayerWrap::SetBaseAV)
		.addFunction("ModBaseAV", &PlayerWrap::ModBaseAV)
		.addFunction("SetCurrentAV", &PlayerWrap::SetCurrentAV)
		.addFunction("SetGameSettingFloat", &PlayerWrap::SetGameSettingFloat)
		.addFunction("SetSkillExperience", &PlayerWrap::SetSkillExperience)
		.addFunction("IncrementSkill", &PlayerWrap::IncrementSkill) // Deprecated. Use ShowSkillIncreaseNotification()
		.addFunction("ShowSkillIncreaseNotification", &PlayerWrap::IncrementSkill)
		.addFunction("TriggerScreenBlood", &PlayerWrap::TriggerScreenBlood)
		.addFunction("SetDisplayGold", &PlayerWrap::SetDisplayGold)
		.addFunction("AddMagic", &PlayerWrap::AddMagic)
		.addFunction("RemoveMagic", &PlayerWrap::RemoveMagic)
		.addFunction("EquipMagic", &PlayerWrap::EquipMagic)
		.addFunction("UnequipMagic", &PlayerWrap::UnequipMagic)
		.addFunction("RemoveAllMagic", &PlayerWrap::RemoveAllMagic)
		.addFunction("MakeHost", &PlayerWrap::MakeHost)
		.addFunction("StartDataSearch", &PlayerWrap::StartDataSearch)
		.addFunction("EnableDataSearchOpcode", &PlayerWrap::EnableDataSearchOpcode)
		.addFunction("SendAnimationEvent", &PlayerWrap::SendAnimationEvent)
		.addFunction("SetSoulSize", &PlayerWrap::SetSoulSize)
		.addFunction("DebugTrapSoul", &PlayerWrap::TrapSoul)
		.addFunction("EnableMovementSync", &PlayerWrap::EnableMovementSync)
		.addFunction("UpdateRecipeInfo", &PlayerWrap::UpdateRecipeInfo)
		.addFunction("ApplyMagicEffect", &PlayerWrap::ApplyMagicEffect)
		.addFunction("SetCombatTarget", &PlayerWrap::SetCombatTarget)
		.addFunction("SetWerewolf", &PlayerWrap::SetWerewolf)
		.addFunction("Mount", &PlayerWrap::Mount)
		.addFunction("Dismount", &PlayerWrap::Dismount)
		.addFunction("GetRider", &PlayerWrap::GetRider)
		.addFunction("ClearChat", &PlayerWrap::ClearChat)
		.addFunction("AddPerk", &PlayerWrap::AddPerk)
		.addFunction("RemovePerk", &PlayerWrap::RemovePerk)
		.addFunction("ExecuteCommand", &PlayerWrap::ExecuteCommand)
		.addFunction("SetEffectLearned", &PlayerWrap::SetEffectLearned)
		.addFunction("SetControlEnabled", &PlayerWrap::SetControlEnabled)
		.addFunction("GetHost", &PlayerWrap::GetHost)
		.addFunction("DebugUpdateChunk", &PlayerWrap::DebugUpdateChunk)
		.addFunction("SetAggressive", &PlayerWrap::SetAggressive)
		.addStaticProperty("InvalidID", &PlayerWrap::GetInvalidPlayerID)
		.addStaticFunction("LookupByID", &PlayerWrap::LookupByID)
		.addStaticFunction("LookupByName", &PlayerWrap::LookupByName)
		.addStaticFunction("CreateNPC", &PlayerWrap::CreateNPC)
		.endClass();
}

Player *PlayerWrap::FindPlayer() const noexcept
{
	auto pl = g.plManager->GetPlayer(pImpl->id);
	if (pl && pl->uniqueID != pImpl->uniqueID)
		pl = nullptr;
	return pl;
}