#pragma once
#include <functional>
#include <chrono>

class Timers : noncopyable
{
public:
	Timers() noexcept;
	void SetTimer(std::chrono::time_point<std::chrono::system_clock> endMoment, std::function<void(void)> func) noexcept; 
	void Tick() noexcept;

private:
	void CollectGarbage() noexcept;
	struct Impl;
	Impl *const pImpl;
};