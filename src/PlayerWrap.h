#pragma once

class LocationWrap;
class RaceWrap;

class PlayerWrap
{
public:
	explicit PlayerWrap(uint16_t id) noexcept;
	~PlayerWrap() noexcept;

	bool operator==(const PlayerWrap &rhs) const noexcept;
	bool IsConnected() const noexcept;
	bool IsSpawned() const noexcept;
	bool IsPaused() const noexcept;
	uint16_t GetID() const noexcept;
	std::string GetName() const noexcept;
	luabridge::LuaRef GetLocation() const noexcept;
	NiPoint3 GetPos() const noexcept;
	float GetX() const noexcept;
	float GetY() const noexcept;
	float GetZ() const noexcept;
	float GetAngleZ() const noexcept;
	bool IsStanding() const noexcept;
	bool IsWalking() const noexcept;
	bool IsRunning() const noexcept;
	bool IsJumping() const noexcept;
	bool IsFalling() const noexcept;
	bool IsInJumpState() const noexcept; // 1.0.17
	bool IsSprinting() const noexcept;
	bool IsSneaking() const noexcept;
	bool IsWeaponDrawn() const noexcept;
	bool IsBlocking() const noexcept;
	bool IsSwimming() const noexcept;
	bool IsFirstPerson() const noexcept; // 1.0.34
	uint32_t GetVirtualWorld() const noexcept;
	luabridge::LuaRef GetRace() const noexcept;
	bool IsFemale() const noexcept;
	float GetWeight() const noexcept;
	uint32_t GetSkinColor() const noexcept;
	uint32_t GetHairColor() const noexcept;
	uint32_t GetHeadpartCount() const noexcept;
	uint32_t GetNthHeadpartID(uint32_t indexStartsIn1) const noexcept;
	uint32_t GetTintmaskCount() const noexcept;
	uint8_t GetNthTintmaskTexture(uint32_t indexStartsIn1) const noexcept;
	uint8_t GetNthTintmaskType(uint32_t indexStartsIn1) const noexcept;
	uint32_t GetNthTintmaskColor(uint32_t indexStartsIn1) const noexcept;
	float GetNthTintmaskAlpha(uint32_t indexStartsIn1) const noexcept;
	uint32_t GetFaceOptionCount() const noexcept;
	float GetNthFaceOption(uint32_t indexStartsIn1) const noexcept;
	uint32_t GetFacePresetCount() const noexcept;
	uint32_t GetNthFacePreset(uint32_t indexStartsIn1) const noexcept;
	uint32_t GetHeadTextureSetID() const noexcept;
	uint32_t GetItemCount(luabridge::LuaRef itemType) const noexcept;
	uint32_t GetNumInventorySlots() const noexcept;
	float GetTotalWeight() const noexcept;
	luabridge::LuaRef GetItemTypeInSlot(uint32_t slot) const noexcept;
	uint32_t GetItemCountInSlot(uint32_t slot) const noexcept;
	bool IsEquipped(luabridge::LuaRef itemType) const noexcept;
	luabridge::LuaRef GetEquippedWeapon(luabridge::LuaRef handID) const noexcept;
	bool IsMenuOpen(std::string menuName) noexcept;
	bool IsDialogOpen(luabridge::LuaRef dialogID) noexcept;
	float GetBaseAV(const std::string &avName) const noexcept;
	float GetCurrentAV(const std::string &avName) const noexcept;
	float GetAVPercentage(const std::string &avName) const noexcept;
	float GetAVMaximum(const std::string &avName) const noexcept;
	float GetSkillExperience(const std::string &skillName) const noexcept; // 1.0
	bool HasMagic(luabridge::LuaRef spell) const noexcept;
	uint32_t GetNumMagic() const noexcept;
	luabridge::LuaRef GetNthMagic(uint32_t n) const noexcept;
	luabridge::LuaRef GetEquippedMagic(luabridge::LuaRef handID) const noexcept;
	int32_t GetSoulSize() const noexcept;
	luabridge::LuaRef GetCurrentFurniture() const noexcept;
	bool IsNPC() const noexcept; // 0.14
	bool IsWerewolf() const noexcept; // 0.15
	std::string GetIP() const noexcept; //0.15.3
	bool HasPerk(luabridge::LuaRef perk) const noexcept; // 1.0
	uint32_t GetRefID() const noexcept; // 1.0.4
	uint32_t GetBaseID() const noexcept; // 1.0.4

	bool SetName(const std::string &utf8Name);
	void SetSpawnPoint(luabridge::LuaRef location, float x, float y, float z, float angleZ) noexcept;
	bool Spawn() noexcept;
	void SendChatMessage(const std::string &message) noexcept;
	void SetChatBubble(const std::string &message, uint32_t ms, luabridge::LuaRef optionalShowSelf) noexcept;
	bool ShowDialog(uint32_t dialogID, const std::string &style, const std::string &title, const std::string &text, luabridge::LuaRef defaultIndex) noexcept;
	void HideDialog() noexcept;
	bool SetPos(float x, float y, float z) noexcept;
	bool SetAngleZ(float degrees) noexcept;
	void SetVirtualWorld(uint32_t world) noexcept;
	void Kick() noexcept;
	bool ShowMenu(std::string menuName) noexcept;
	void SetRace(luabridge::LuaRef newRace) noexcept;
	void SetFemale(bool isFemale) noexcept;
	void SetWeight(float weight) noexcept;
	void SetSkinColor(uint32_t color) noexcept;
	void SetHairColor(uint32_t color) noexcept;
	void SetHeadpartIDs(luabridge::LuaRef list) noexcept;
	void RemoveAllTintmasks() noexcept;
	void SetNthTintmask(uint32_t indexStartsIn1, uint8_t texturePath, uint8_t type, uint32_t color, float alpha);
	void AddTintmask(uint8_t texturePath, uint8_t type, uint32_t color, float alpha);
	void SetFaceOptions(luabridge::LuaRef list) noexcept;
	void SetFacePresets(luabridge::LuaRef list) noexcept;
	void SetHeadTextureSetID(uint32_t formID) noexcept;
	void AddItem(luabridge::LuaRef itemType, int32_t count) noexcept;
	bool RemoveItem(luabridge::LuaRef itemType, uint32_t count) noexcept;
	void EquipItem(luabridge::LuaRef itemType, luabridge::LuaRef optionalHandID) noexcept;
	void UnequipItem(luabridge::LuaRef itemType, luabridge::LuaRef optionalHandID) noexcept;
	void RemoveAllItems() noexcept;
	void MuteInventoryNotifications(bool mute) noexcept;
	void SetWeather(uint32_t weatherTypeOrID) noexcept;
	void SetGlobal(uint32_t globalID, float value) noexcept;
	void SetBaseAV(const std::string &avName, float value) noexcept;
	void ModBaseAV(const std::string &avName, float value) noexcept;
	void SetCurrentAV(const std::string &avName, float value) noexcept;
	void SetGameSettingFloat(const std::string &name, float value) noexcept;
	void SetSkillExperience(const std::string &skillName, float numPercents) noexcept; // 1.0
	void IncrementSkill(const std::string &skillName) noexcept; // 1.0
	void TriggerScreenBlood(uint32_t value) noexcept;
	void SetDisplayGold(uint32_t count) noexcept;
	void AddMagic(luabridge::LuaRef spell) noexcept;
	void RemoveMagic(luabridge::LuaRef spell) noexcept;
	void EquipMagic(luabridge::LuaRef spell, luabridge::LuaRef optionalHandID) noexcept;
	void UnequipMagic(luabridge::LuaRef spell, luabridge::LuaRef optionalHandID) noexcept;
	void RemoveAllMagic() noexcept;
	void MakeHost() noexcept; // Deprecated
	void StartDataSearch() noexcept; // 0.14.19
	void EnableDataSearchOpcode(std::string opcode, bool enable) noexcept; // 0.14.19
	void SendAnimationEvent(const std::string &animEventName, luabridge::LuaRef noSendSelf) noexcept; // 0.11
	void SetSoulSize(int32_t size) noexcept; // 0.11
	void TrapSoul(luabridge::LuaRef targetPl) noexcept; // 0.11
	void EnableMovementSync(bool e) noexcept; // 0.11
	void UpdateRecipeInfo(luabridge::LuaRef recipe) noexcept; // 0.11
	void ApplyMagicEffect(luabridge::LuaRef effect, float magnitude, float duration, float area) noexcept; // 0.12
	void SetCombatTarget(luabridge::LuaRef target) noexcept; // 0.14.16 [Deprecated in 1.0.36]
	void SetWerewolf(bool werewolf) noexcept; // 0.15
	void Mount(luabridge::LuaRef targetHorse) noexcept; // 0.15
	void Dismount() noexcept; // 0.15
	luabridge::LuaRef GetRider() noexcept; // 0.15
	void ClearChat() noexcept; // 0.15.3
	void AddPerk(luabridge::LuaRef perk) noexcept; // 1.0
	void RemovePerk(luabridge::LuaRef perk) noexcept; // 1.0
	void ExecuteCommand(std::string target, std::string cmdtext) noexcept; // 1.0
	void SetEffectLearned(luabridge::LuaRef itemType, uint32_t effectIdx, bool learned) noexcept; // 1.0
	void SetControlEnabled(std::string controlName, bool enable) noexcept; // 1.0.12
	luabridge::LuaRef GetHost() noexcept; // 1.0.15
	void DebugUpdateChunk() noexcept; // 1.0.19
	void SetAggressive(bool aggressive) noexcept; //1.0.36

	static luabridge::LuaRef LookupByID(uint16_t id) noexcept;
	static luabridge::LuaRef LookupByName(const std::string &utfName) noexcept;
	static luabridge::LuaRef CreateNPC(luabridge::LuaRef optionalBaseFormID) noexcept;

	static uint16_t GetInvalidPlayerID() noexcept;

	static void Register(lua_State *state);

private:
	Player *FindPlayer() const noexcept;

	struct Impl;
	std::shared_ptr<Impl> pImpl;
};