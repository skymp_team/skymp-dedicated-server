#include "stdafx.h"
#include "EffectWrap.h"
#include "IDAssigner.h"

class EffectArchetype
{
public:
	enum
	{
		ValueMod = 0,
		Script,
		Dispel,
		CureDisease,
		Absorb,
		DualValueMod,
		Calm,
		Demoralize,
		Frenzy,
		Disarm,
		CommandSummoned,
		Invisibility,
		Light,
		Lock = 15,
		Open,
		BoundWeapon,
		SummonCreature,
		DetectLife,
		Telekinesis,
		Paralysis,
		Reanimate,
		SoulTrap,
		TurnUndead,
		Guide,
		WerewolfFeed,
		CureParalysis,
		CureAddiction,
		CurePoison,
		Concussion,
		ValueAndParts,
		AccumulateMagnitude,
		Stagger,
		PeakValueMod,
		Cloak,
		Werewolf,
		SlowTime,
		Rally,
		EnhanceWeapon,
		SpawnHazard,
		Etherealize,
		Banish,
		Disguise = 44,
		GrabActor,
		VampireLord,
	};

	EffectArchetype() noexcept {
		Init();
	}

	EffectArchetype(uint8_t classID) noexcept {
		Init();
		this->typeID = classID;
	}

	EffectArchetype(const std::string &className) noexcept {
		Init();
		try {
			this->typeID = this->idByName.at(className);
		}
		catch (...) {
			this->typeID = ValueMod;
			log(Log::Error, "Unknown effect archetype ", className.data());
		};
	}

	uint8_t GetID() const noexcept {
		return this->typeID;
	}

	std::string GetName() const noexcept {
		try {
			return nameByID.at(this->typeID);
		}
		catch (...) {
			return EffectArchetype(ValueMod).GetName();
		}
	}

private:
	void Init() {
		this->idByName = {
			{"ValueMod", ValueMod},
			{"Script", Script },
			{"Dispel", Dispel },
			{"CureDisease", CureDisease },
			{"Absorb", Absorb },
			{"DualValueMod", DualValueMod },
			{"Calm", Calm },
			{"Demoralize", Demoralize },
			{"Frenzy", Frenzy },
			{"Disarm", Disarm },
			{"CommandSummoned", CommandSummoned },
			{"Invisibility", Invisibility },
			{"Light", Light },
			{"Lock", Lock },
			{"Open", Open },
			{"BoundWeapon", BoundWeapon },
			{"SummonCreature", SummonCreature },
			{"DetectLife", DetectLife },
			{"Telekinesis", Telekinesis },
			{"Paralysis", Paralysis },
			{"Reanimate", Reanimate },
			{"SoulTrap", SoulTrap },
			{"TurnUndead", TurnUndead },
			{"Guide", Guide },
			{"WerewolfFeed", WerewolfFeed },
			{"CureParalysis", CureParalysis },
			{"CureAddiction", CureAddiction },
			{"CurePoison", CurePoison },
			{"Concussion", Concussion },
			{"ValueAndParts", ValueAndParts },
			{"AccumulateMagnitude", AccumulateMagnitude },
			{"Stagger", Stagger },
			{"PeakValueMod", PeakValueMod },
			{"Cloak", Cloak },
			{"Werewolf", Werewolf },
			{"SlowTime", SlowTime },
			{"Rally", Rally },
			{"EnhanceWeapon", EnhanceWeapon },
			{"SpawnHazard", SpawnHazard },
			{"Etherealize", Etherealize },
			{"Banish", Banish },
			{"Disguise", Disguise },
			{"GrabActor", GrabActor },
			{"VampireLord", VampireLord }
		};
		this->nameByID.clear();
		for (auto pair : this->idByName)
			this->nameByID.insert({ pair.second, pair.first });
	}

	uint8_t typeID = ValueMod;

	std::map<std::string, uint8_t> idByName;
	std::map<uint8_t, std::string> nameByID;
};

enum class CastingType : uint8_t
{
	ConstantEffect = 0,
	FireAndForget = 1,
	Concentration = 2,
};

enum class Delivery : uint8_t
{
	Self = 0,
	Contact = 1,
	Aimed = 2,
	TargetActor = 3,
	TargetLocation = 4,
};

struct Effect
{
	std::string identifier;
	EffectArchetype archetype;
	CastingType castingType = CastingType::ConstantEffect;
	Delivery delivery = Delivery::Self;
	uint32_t existingEffectID = 0;
	std::string av1, av2;
};

std::map<std::string, Effect *> createdEffects;

struct EffectWrap::Impl
{
	Effect *effect = nullptr;
};

EffectWrap::EffectWrap(std::string name) : pImpl(new Impl)
{
	try {
		pImpl->effect = createdEffects.at(name);
		IDAssigner<EffectWrap>::GetID(*this); // generate id
		return;
	}
	catch (...) {
	}
	throw std::runtime_error("unable to create wrap for Effect " + name);
}

EffectWrap::~EffectWrap()
{
}

bool EffectWrap::operator==(const EffectWrap &rhs) const noexcept {
	return this->pImpl->effect == rhs.pImpl->effect;
}

std::string EffectWrap::GetIdentifier() const noexcept {
	return pImpl->effect->identifier;
}

std::string EffectWrap::GetArchetype() const noexcept {
	return pImpl->effect->archetype.GetName();
}

uint8_t EffectWrap::GetArchetypeID() const noexcept {
	return pImpl->effect->archetype.GetID();
}

uint8_t EffectWrap::GetCastingTypeID() const noexcept {
	return (uint8_t)pImpl->effect->castingType;
}

uint8_t EffectWrap::GetDeliveryID() const noexcept {
	return (uint8_t)pImpl->effect->delivery;
}

void  EffectWrap::SetActorValues(std::string av1, std::string av2) noexcept {
	pImpl->effect->av1 = av1;
	pImpl->effect->av2 = av2;
}

std::string EffectWrap::GetCastingType() const noexcept
{
	switch (pImpl->effect->castingType)
	{
	case CastingType::ConstantEffect:
		return "ConstantEffect";
	case CastingType::FireAndForget:
		return "FireAndForget";
	case CastingType::Concentration:
		return "Concentration";
	default:
		return "";
	}
}

std::string EffectWrap::GetDelivery() const noexcept
{
	switch (pImpl->effect->delivery)
	{
	case Delivery::Self:
		return "Self";
	case Delivery::Contact:
		return "Contact";
	case Delivery::Aimed:
		return "Aimed";
	case Delivery::TargetActor:
		return "TargetActor";
	case Delivery::TargetLocation:
		return "TargetLocation";
	default:
		return "";
	}
}

uint32_t EffectWrap::GetExistingGameFormID() const noexcept
{
	return pImpl->effect->existingEffectID;
}

std::string EffectWrap::GetActorValue(uint32_t n) const noexcept
{
	if (n == 1)
		return pImpl->effect->av1;
	if (n == 2)
		return pImpl->effect->av2;
	return "";
}

luabridge::LuaRef EffectWrap::Create(std::string ident, std::string archetype, uint32_t existingEffectID, std::string castingTypeStr, std::string deliveryTypeStr) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	if (ident.empty() || !(LookupByIdentifier(ident) == luabridge::Nil()))
		return result;

	if (EffectArchetype(archetype).GetName() != archetype)
		return result;

	CastingType castingType;
	if (castingTypeStr == "ConstantEffect")
		castingType = CastingType::ConstantEffect;
	else if (castingTypeStr == "FireAndForget")
		castingType = CastingType::FireAndForget;
	else if (castingTypeStr == "Concentration")
		castingType = CastingType::Concentration;
	else
		return result;

	Delivery delivery;
	if (deliveryTypeStr == "Self")
		delivery = Delivery::Self;
	else if (deliveryTypeStr == "Contact")
		delivery = Delivery::Contact;
	else if (deliveryTypeStr == "Aimed")
		delivery = Delivery::Aimed;
	else if (deliveryTypeStr == "TargetActor")
		delivery = Delivery::TargetActor;
	else if (deliveryTypeStr == "TargetLocation")
		delivery = Delivery::TargetLocation;
	else
		return result;

	auto effect = createdEffects.insert({ ident, new Effect }).first->second;
	effect->archetype = EffectArchetype(archetype);
	effect->castingType = castingType;
	effect->delivery = delivery;
	effect->identifier = ident;
	effect->existingEffectID = existingEffectID;

	nonstd::optional<EffectWrap> wrap;
	try {
		wrap = EffectWrap(ident);
	}
	catch (...) {
	}
	if (wrap != nonstd::nullopt)
		result = *wrap;

	return result;
}

luabridge::LuaRef EffectWrap::LookupByIdentifier(std::string name) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());

	try {
		result = EffectWrap(name);
	}
	catch (...) {
		result = luabridge::Nil();
	}

	return result;
}

void EffectWrap::Register(lua_State *L)
{
	luabridge::getGlobalNamespace(L)
		.beginClass<EffectWrap>("Effect")
		.addFunction("__eq", &EffectWrap::operator==)
		.addFunction("GetIdentifier", &EffectWrap::GetIdentifier)
		.addFunction("GetCastingType", &EffectWrap::GetCastingType)
		.addFunction("GetDelivery", &EffectWrap::GetDelivery)
		.addFunction("GetBaseID", &EffectWrap::GetBaseID)
		.addFunction("GetActorValue", &EffectWrap::GetActorValue)
		.addFunction("SetActorValues", &EffectWrap::SetActorValues)
		.addStaticFunction("Create", &EffectWrap::Create)
		.addStaticFunction("LookupByIdentifier", &EffectWrap::LookupByIdentifier)
		.endClass();
}

EffectWrap::StepResult EffectWrap::Step(const EffectWrap &effect, uint16_t playerID, float mag, double thisStepDurMs, uint16_t casterID)
{
	auto player = g.plManager->GetPlayer(playerID);
	if (player == nullptr)
		return StepResult::Default;

	auto caster = g.plManager->GetPlayer(casterID);

	switch (effect.GetArchetypeID())
	{
	case EffectArchetype::ValueMod:
	case EffectArchetype::DualValueMod:
	{
		std::vector<std::string> avs;
		avs.push_back(effect.GetActorValue(1));
		if (effect.GetArchetypeID() == EffectArchetype::DualValueMod)
			avs.push_back(effect.GetActorValue(2));

		for (auto av : avs)
		{
			if (!av.empty())
			{
				auto dat = player->GetAVData(av);
				if (mag < 0)
					dat.DamageCurrent(mag / 1000 * (float)thisStepDurMs);
				else
					dat.RestoreCurrent(mag / 1000 * (float)thisStepDurMs);
				player->SetAVData(av, dat, caster);
			}
		}
		break;
	}
	case EffectArchetype::SoulTrap:
	{
		if (player->IsDying())
		{
			player->DispellEffect(effect);
			if (caster != nullptr)
			{
				caster->TrapSoul(player);
				return StepResult::ActiveEffectsChanged;
			}
		}
		break;
	}
	default:
		break;
	}

	return StepResult::Default;
}