#pragma once
#include "PerkWrap.h"

namespace Components
{
	class PerkList
	{
	public:
		using Perks = std::set<PerkWrap>;

		void AddPerk(PerkWrap s) noexcept {
			this->perks.insert(s);
		}

		void RemovePerk(PerkWrap s) noexcept {
			this->perks.erase(s);
		}

		void RemoveAllPerks() noexcept {
			this->perks = {};
		}

		bool HasPerk(PerkWrap s) const noexcept {
			return this->perks.find(s) != this->perks.end();
		}

		auto GetPerks() const noexcept {
			return this->perks;
		}

		uint32_t GetNumPerks() const noexcept {
			return uint32_t(this->GetPerks().size());
		}

		friend bool operator==(const PerkList &lhs, const PerkList &rhs) {
			return lhs.GetPerks() == rhs.GetPerks();
		}

		friend bool operator!=(const PerkList &lhs, const PerkList &rhs) {
			return !(lhs == rhs);
		}

	private:
		Perks perks;
	};
}