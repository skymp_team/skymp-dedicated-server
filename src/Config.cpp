#include "stdafx.h"
#include <INIReader.h>

struct Config::Impl
{
	std::unique_ptr<INIReader> reader;
};

Config::Config(const std::string &fileName) noexcept : pImpl(new Impl), fileName(fileName)
{
	try {
		pImpl->reader = std::make_unique<INIReader>(fileName.data());
		if (pImpl->reader->ParseError() < 0)
			throw std::logic_error("can't load ini file " + fileName);
	}
	catch (std::exception &e) {
		std::printf("%s\n", e.what());
	}
}

Config::~Config()
{
	delete pImpl;
}

std::string Config::GetValueImpl(const std::string &variableName, const std::string &defaultValue) const
{
	std::string result = defaultValue;
	try {
		auto delPos = variableName.find('.');
		std::string section, name;
		if (delPos != std::string::npos)
		{
			section = { variableName.begin(), variableName.begin() + delPos };
			name = { variableName.begin() + delPos + 1, variableName.end() };
		}
		result = pImpl->reader.get()->Get(section, name, defaultValue);
	}
	catch (...) {
	}
	return result;
}