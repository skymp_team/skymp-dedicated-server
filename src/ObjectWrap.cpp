#include "stdafx.h"
#include "ObjectWrap.h"
#include "Object.h"

#include "PlayerWrap.h"

struct ObjectWrap::Impl
{
	uint32_t id;
};

ObjectWrap::ObjectWrap(uint32_t id) noexcept : pImpl(new Impl)
{
	pImpl->id = id;
}

uint32_t ObjectWrap::GetID() const noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		return object->GetID();
	return 0;
}

uint32_t ObjectWrap::GetBaseID() const noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		return object->GetBaseFormID();
	return 0;
}

bool ObjectWrap::IsDeleted() const noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	return(object == nullptr);
}

bool ObjectWrap::operator==(const ObjectWrap &rhs) const noexcept
{
	return this->GetID() == rhs.GetID();
}

float ObjectWrap::GetX() const noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		return object->GetPos().x;
	return 0.0f;
}

float ObjectWrap::GetY() const noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		return object->GetPos().y;
	return 0.0f;
}

float ObjectWrap::GetZ() const noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		return object->GetPos().z;
	return 0.0f;
}

float ObjectWrap::GetAngleX() const noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		return object->GetRot().x;
	return 0.0f;
}

float ObjectWrap::GetAngleY() const noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		return object->GetRot().y;
	return 0.0f;
}

float ObjectWrap::GetAngleZ() const noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		return object->GetRot().z;
	return 0.0f;
}

luabridge::LuaRef ObjectWrap::GetLocation() const noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		result = LocationWrap(object->GetLocation());
	return result;
}

luabridge::LuaRef ObjectWrap::GetVirtualWorld() const noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr && object->IsNative() == false)
		result = object->GetVirtualWorld();
	return result;
}

std::string ObjectWrap::GetName() const noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		return (object->GetName());
	return "";
}

bool ObjectWrap::IsDisabled() const noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		return object->IsDisabled();
	return false;
}

bool ObjectWrap::IsDestroyed() const noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		return object->IsDestroyed();
	return false;
}

std::string ObjectWrap::GetType() const noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
	{
		switch (object->GetType())
		{
		case Object::Type::Static:
			return "Static";
		case Object::Type::Door:
			return "Door";
		case Object::Type::TeleportDoor:
			return "TeleportDoor";
		case Object::Type::Activator:
			return "Activator";
		case Object::Type::Container:
			return "Container";
		case Object::Type::Furniture:
			return "Furniture";
		case Object::Type::Item:
			return "Item";
		default:
			log(Log::Warning, "Unknown object type ", (int32_t)object->GetType());
			return "";
		}
	}
	return "";
}

uint32_t ObjectWrap::GetItemCount(luabridge::LuaRef itemType) const noexcept
{
	if ((itemType.isLightUserdata() || itemType.isUserdata()))
	{
		auto obj = g.objManager->LookupObject(pImpl->id);
		if (obj != nullptr)
		{
			auto type = itemType.cast<ItemTypeWrap>();
			return obj->GetItemCount(type);
		}
	}
	return 0;
}

uint32_t ObjectWrap::GetNumInventorySlots() const noexcept
{
	auto obj = g.objManager->LookupObject(pImpl->id);
	if (obj != nullptr)
		return (uint32_t)obj->GetItems().size();
	return 0;
}

float ObjectWrap::GetTotalWeight() const noexcept
{
	auto obj = g.objManager->LookupObject(pImpl->id);
	if (obj != nullptr)
		return obj->GetTotalWeight();
	return 0;
}

luabridge::LuaRef ObjectWrap::GetItemTypeInSlot(uint32_t slotID) const noexcept
{
	slotID--; // in lua starts at 1
	auto L = g.gamemode->GetState();
	luabridge::LuaRef result(L);
	result = luabridge::Nil();
	auto obj = g.objManager->LookupObject(pImpl->id);
	if (obj != nullptr)
	{
		const auto slot = obj->GetInventorySlot(slotID);
		if (slot != nonstd::nullopt)
			result = slot->first.itemType;
	}
	return result;
}

uint32_t ObjectWrap::GetItemCountInSlot(uint32_t slotID) const noexcept
{
	slotID--; // in lua starts at 1
	auto obj = g.objManager->LookupObject(pImpl->id);
	if (obj != nullptr)
	{
		const auto slot = obj->GetInventorySlot(slotID);
		if (slot != nonstd::nullopt)
			return slot->second;
	}
	return 0;
}

uint8_t ObjectWrap::GetLockLevel() const noexcept
{
	auto obj = g.objManager->LookupObject(pImpl->id);
	if (obj != nullptr)
	{
		return obj->GetLockLevel();
	}
	return 0;
}

bool ObjectWrap::IsHarvested() const noexcept
{
	auto obj = g.objManager->LookupObject(pImpl->id);
	if (obj != nullptr)
	{
		return obj->IsHarvested();
	}
	return false;
}

bool ObjectWrap::IsOpen() const noexcept
{
	auto obj = g.objManager->LookupObject(pImpl->id);
	if (obj != nullptr)
	{
		return obj->IsOpen();
	}
	return false;
}

luabridge::LuaRef ObjectWrap::GetTeleportTarget() const noexcept
{
	luabridge::LuaRef res(g.gamemode->GetState());
	res = luabridge::Nil();
	auto obj = g.objManager->LookupObject(pImpl->id);
	if (obj != nullptr)
	{
		return ObjectWrap::LookupByID(obj->GetTeleportTarget());
	}
	return res;
}

void ObjectWrap::SetPos(float x, float y, float z) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		object->SetPos({ x,y,z });
}

void ObjectWrap::SetAngle(float x, float y, float z) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		object->SetRot({ x,y,z });
}

bool ObjectWrap::SetLocation(luabridge::LuaRef location) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	try {
		if (object != nullptr && !location.isNil())
			return object->SetLocation(location.cast<LocationWrap>().GetID());
	}
	catch (...) {
	}
	return false;
}

bool ObjectWrap::SetVirtualWorld(uint32_t world) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr && object->IsNative() == false)
	{
		object->SetVirtualWorld(world);
		return true;
	}
	return false;
}

bool ObjectWrap::Delete() noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
	{
		//if (object->IsNative() == false)
		{
			g.objManager->DeleteObject(this->GetID());
			return true;
		}
	}
	return false;
}

void ObjectWrap::SetName(const std::string &utf8Name) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
	{
		auto newName = (utf8Name);
		if (newName.size() > 64)
			newName.resize(64);;
		object->SetName(newName);
	}
}

void ObjectWrap::SetDisabled(bool disabled) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		object->SetDisabled(disabled);
}

void ObjectWrap::SetDestroyed(bool d) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		object->SetDestroyed(d);
}

void ObjectWrap::SetDestroyedForPlayer(luabridge::LuaRef player, bool d) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr && (player.isLightUserdata() || player.isUserdata()))
	{
		auto pl = g.plManager->GetPlayer(player.cast<PlayerWrap>().GetID());
		if (pl != nullptr)
			object->SetDestroyedForPlayer(d, pl);
	}
}

void ObjectWrap::RegisterAsDoor() noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr && object->GetType() == Object::Type::Static)
	{
		object->SetOpen(false);
		object->SetType(Object::Type::Door);
	}
}

bool ObjectWrap::RegisterAsTeleportDoor(luabridge::LuaRef target_) noexcept
{
	if (target_.isUserdata() || target_.isLightUserdata())
	{
		auto target = target_.cast<ObjectWrap>();
		auto object = g.objManager->LookupObject(pImpl->id);
		if (object != nullptr && target.IsDeleted() == false && object->GetType() == Object::Type::Static)
		{
			auto object2 = g.objManager->LookupObject(target.GetID());
			if (object2 != nullptr && object2->IsNative())
			{
				object->SetOpen(false);
				object->SetTeleportTarget(target.GetID());
				object->SetType(Object::Type::TeleportDoor);
				return true;
			}
		}
	}
	return false;
}

void ObjectWrap::RegisterAsActivator() noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr && object->GetType() == Object::Type::Static)
	{
		object->SetType(Object::Type::Activator);
	}
}

void ObjectWrap::RegisterAsFurniture() noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr && object->GetType() == Object::Type::Static)
	{
		object->SetType(Object::Type::Furniture);
	}
}

void ObjectWrap::RegisterAsContainer() noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr && object->GetType() == Object::Type::Static)
	{
		object->SetType(Object::Type::Container);
	}
}

void ObjectWrap::AddItem(luabridge::LuaRef itemType, uint32_t count) noexcept
{
	if (itemType.isUserdata() || itemType.isLightUserdata())
	{
		auto object = g.objManager->LookupObject(pImpl->id);
		if (object != nullptr)
		{
			const auto itemType_ = itemType.cast<ItemTypeWrap>();
			if (object->GetType() == Object::Type::Container)
			{
				object->AddItem(itemType_, count);
			}
			else if (object->GetType() == Object::Type::Item)
			{
				if (object->GetItems().find({ itemType_ }) != object->GetItems().end())
					object->AddItem(itemType_, count);
			}
		}
	}
}

bool ObjectWrap::RemoveItem(luabridge::LuaRef itemType, uint32_t count) noexcept
{
	if (itemType.isUserdata() || itemType.isLightUserdata())
	{
		auto object = g.objManager->LookupObject(pImpl->id);
		if (object != nullptr)
		{
			if (object->GetType() == Object::Type::Container
				|| object->GetType() == Object::Type::Item)
				return object->RemoveItem(itemType.cast<ItemTypeWrap>(), count);
		}
	}
	return false;
}

void ObjectWrap::RemoveAllItems() noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		object->RemoveAllItems();
}

void ObjectWrap::SetLockLevel(uint8_t ll) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		object->SetLockLevel(ll);
}

void ObjectWrap::AddKeyword(const std::string &keyword) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		object->AddKeyword(keyword);
}

void ObjectWrap::RemoveKeyword(const std::string &keyword) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		object->RemoveKeyword(keyword);
}

void ObjectWrap::SetHarvested(bool harvested) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		object->SetHarvested(harvested);
}

void ObjectWrap::SetOpen(bool op) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
		object->SetOpen(op);
}

void ObjectWrap::ResetFor(luabridge::LuaRef player) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
	{
		if (player.isUserdata() || player.isLightUserdata())
		{
			try {
				auto playerWrap = player.cast<PlayerWrap>();
				auto pl = g.plManager->GetPlayer(playerWrap.GetID());
				if (pl != nullptr)
				{
					const bool streamIn = pl->IsStreamedIn(object);
					pl->StreamOut(object);
					if (streamIn)
						pl->StreamIn(object);
				}
			}
			catch (const std::exception &e) {
				log(Log::Error, "ResetFor() ", e.what());
			}
		}
	}
}

void ObjectWrap::Activate(luabridge::LuaRef player) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
	{
		auto playerWrap = player.cast<PlayerWrap>();
		auto pl = g.plManager->GetPlayer(playerWrap.GetID());
		if (pl != nullptr)
		{
			object->Activate(pl, true, true);
		}
	}
}

void ObjectWrap::SetHostable(bool hostable) noexcept
{
	auto object = g.objManager->LookupObject(pImpl->id);
	if (object != nullptr)
	{
		object->SetHostable(hostable);
	}
}

luabridge::LuaRef ObjectWrap::Create(uint32_t existingReferenceID, uint32_t baseFormID, luabridge::LuaRef location, float x, float y, float z) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	auto existingObject = g.objManager->LookupObject(existingReferenceID);

	if (existingObject == nullptr)
	{
		if (location.isLightUserdata() || location.isUserdata())
		{
			auto resultObject = g.objManager->NewObject(existingReferenceID, baseFormID, location.cast<LocationWrap>().GetID(), { x,y,z }, { 0,0,0 });
			result = ObjectWrap(resultObject->GetID());
		}
		else
		{
			log(Log::Warning, "Object.Create() bad location");
		}
	}
	else
	{
		std::stringstream ss;
		ss << "Object.Create() object " << std::hex << existingReferenceID << " already exist";
		log(Log::Warning, ss.str());
	}
	return result;
}

luabridge::LuaRef ObjectWrap::CreateItem(luabridge::LuaRef itemType, uint32_t count, luabridge::LuaRef location, float x, float y, float z) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	try {
		if (count > 0)
		{
			const auto itemType_ = itemType.cast<ItemTypeWrap>();
			const auto obj = g.objManager->NewObject(0, itemType_.GetExistingGameFormID(), location.cast<LocationWrap>().GetID(), { x,y,z }, { 0,0,0 });
			obj->SetVirtualWorld(0);
			obj->SetType(Object::Type::Item);
			obj->AddItem(itemType_, count);
			result = ObjectWrap(obj->GetID());
		}
	}
	catch (const std::exception &e) {
		log(Log::Error, "CreateItem() ", e.what());
	}
	return result;
}

luabridge::LuaRef ObjectWrap::LookupByID(uint32_t objectID) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	if (g.objManager->LookupObject(objectID) != nullptr)
		result = ObjectWrap(objectID);
	return result;
}

void ObjectWrap::Register(lua_State *L)
{
	luabridge::getGlobalNamespace(L)
		.beginClass<ObjectWrap>("Object")
		//.addConstructor<void(*)(uint16_t)>()
		.addFunction("__eq", &ObjectWrap::operator==)
		.addFunction("GetID", &ObjectWrap::GetID)
		.addFunction("GetBaseID", &ObjectWrap::GetBaseID)
		.addFunction("IsDeleted", &ObjectWrap::IsDeleted)
		.addFunction("GetX", &ObjectWrap::GetX)
		.addFunction("GetY", &ObjectWrap::GetY)
		.addFunction("GetZ", &ObjectWrap::GetZ)
		.addFunction("GetAngleX", &ObjectWrap::GetAngleX)
		.addFunction("GetAngleY", &ObjectWrap::GetAngleY)
		.addFunction("GetAngleZ", &ObjectWrap::GetAngleZ)
		.addFunction("GetLocation", &ObjectWrap::GetLocation)
		.addFunction("GetVirtualWorld", &ObjectWrap::GetVirtualWorld)
		.addFunction("GetName", &ObjectWrap::GetName)
		.addFunction("IsDisabled", &ObjectWrap::IsDisabled)
		.addFunction("IsDestroyed", &ObjectWrap::IsDestroyed)
		.addFunction("GetType", &ObjectWrap::GetType)
		.addFunction("GetItemCount", &ObjectWrap::GetItemCount)
		.addFunction("GetNumInventorySlots", &ObjectWrap::GetNumInventorySlots)
		.addFunction("GetTotalWeight", &ObjectWrap::GetTotalWeight)
		.addFunction("GetItemTypeInSlot", &ObjectWrap::GetItemTypeInSlot)
		.addFunction("GetItemCountInSlot", &ObjectWrap::GetItemCountInSlot)
		.addFunction("GetLockLevel", &ObjectWrap::GetLockLevel)
		.addFunction("IsHarvested", &ObjectWrap::IsHarvested)
		.addFunction("IsOpen", &ObjectWrap::IsOpen)
		.addFunction("GetTeleportTarget", &ObjectWrap::GetTeleportTarget)
		.addFunction("SetPos", &ObjectWrap::SetPos)
		.addFunction("SetAngle", &ObjectWrap::SetAngle)
		.addFunction("SetLocation", &ObjectWrap::SetLocation)
		.addFunction("SetVirtualWorld", &ObjectWrap::SetVirtualWorld)
		.addFunction("Delete", &ObjectWrap::Delete)
		.addFunction("SetName", &ObjectWrap::SetName)
		.addFunction("SetDisabled", &ObjectWrap::SetDisabled)
		.addFunction("SetDestroyed", &ObjectWrap::SetDestroyed)
		.addFunction("SetDestroyedForPlayer", &ObjectWrap::SetDestroyedForPlayer)
		.addFunction("RegisterAsDoor", &ObjectWrap::RegisterAsDoor)
		.addFunction("RegisterAsTeleportDoor", &ObjectWrap::RegisterAsTeleportDoor)
		.addFunction("RegisterAsActivator", &ObjectWrap::RegisterAsActivator)
		.addFunction("RegisterAsFurniture", &ObjectWrap::RegisterAsFurniture)
		.addFunction("RegisterAsContainer", &ObjectWrap::RegisterAsContainer)
		.addFunction("AddItem", &ObjectWrap::AddItem)
		.addFunction("RemoveItem", &ObjectWrap::RemoveItem)
		.addFunction("RemoveAllItems", &ObjectWrap::RemoveAllItems)
		.addFunction("SetLockLevel", &ObjectWrap::SetLockLevel)
		.addFunction("AddKeyword", &ObjectWrap::AddKeyword)
		.addFunction("RemoveKeyword", &ObjectWrap::RemoveKeyword)
		.addFunction("SetHarvested", &ObjectWrap::SetHarvested)
		.addFunction("SetOpen", &ObjectWrap::IsOpen)
		.addFunction("ResetFor", &ObjectWrap::ResetFor)
		.addFunction("Activate", &ObjectWrap::Activate)
		.addFunction("SetHostable", &ObjectWrap::SetHostable)
		.addStaticFunction("Create", &Create)
		.addStaticFunction("CreateItem", &CreateItem)
		.addStaticFunction("LookupByID", &LookupByID)
		.endClass();
}