#pragma once
#include "NavMeshWrap.h"

class Pathing : noncopyable
{
public:
	Pathing() noexcept;
	~Pathing() noexcept;
	void Add(const NavMeshWrap &navMesh) noexcept;
	void Remove(const NavMeshWrap &navMesh) noexcept;

	using TRIID = size_t;
	using VERTID = size_t;

private:
	struct Impl;
	Impl *const pImpl;

	void RecalcGraph() noexcept;
	bool AreNeighbors(TRIID triangle1, TRIID triangle2) const noexcept;
	std::vector<NiPoint3> PathTo(NiPoint3 src, NiPoint3 end, int32_t srcVertID = -1, std::vector<NiPoint3> basePath = {}, std::vector<NiPoint3> painted = std::vector<NiPoint3>()) const noexcept;
	bool IsInTriangle(NiPoint3 src, TRIID tri) const noexcept;
	void TestPathTo() noexcept;
};