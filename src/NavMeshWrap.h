#pragma once

struct NavMesh
{
	using VertID = uint16_t;
	using Triangle = std::array<VertID, 3>;

	std::vector<NiPoint3> verts;
	std::vector<Triangle> triangles;
	uint32_t formID = 0;
	uint32_t locationID = 60;
};

class NavMeshWrap
{
public:
	explicit NavMeshWrap(uint32_t formID);
	~NavMeshWrap() noexcept;

	bool operator==(const NavMeshWrap &rhs) const noexcept;
	bool operator!=(const NavMeshWrap &rhs) const noexcept {
		return !(*this == rhs);
	}

	void SetNumVertices(uint32_t num) noexcept;
	uint32_t GetNumVertices() const noexcept;
	void SetNumTriangles(uint32_t num) noexcept;
	uint32_t GetNumTriangles() const noexcept;
	void SetNthVerticePos(uint32_t n, float x, float y, float z) noexcept;
	float GetNthVerticeX(uint32_t n) const noexcept;
	float GetNthVerticeY(uint32_t n) const noexcept;
	float GetNthVerticeZ(uint32_t n) const noexcept;
	void SetNthTriangleVertices(uint32_t n, uint16_t vert1, uint16_t vert2, uint16_t vert3) const noexcept;
	uint16_t GetNthTriangleNthVertice(uint32_t n, uint32_t nVert) const noexcept;
	void SetLocation(luabridge::LuaRef location) noexcept;
	luabridge::LuaRef GetLocation() const noexcept;

	static luabridge::LuaRef Create(uint32_t formID) noexcept;
	static luabridge::LuaRef LookupByID(uint32_t formID) noexcept;

	// Non-API:
	const NavMesh &GetData() const noexcept;
	void SetLocation(uint32_t locID) noexcept;
	uint32_t GetID() const noexcept;

	static void Register(lua_State *state);

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;

	friend bool operator <(const NavMeshWrap &lhs, const NavMeshWrap &rhs) {
		return lhs.GetID() < rhs.GetID();
	}
};