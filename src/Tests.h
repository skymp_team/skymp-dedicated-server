#pragma once

class Tests : noncopyable
{
public:
	void RunAll() noexcept;

private:

	void Test_Locale() noexcept;

	void Test_Player_Create() noexcept;
	void Test_Player_ShowDialog() noexcept;
	void Test_Player_SetName() noexcept;
	void Test_Player_SetLocation() noexcept;
	void Test_Player_SetVirtualWorld() noexcept;
	void Test_Player_MoveTo() noexcept;
	void Test_Player_SetPaused() noexcept;
	void Test_Player_DamageHealth() noexcept;
	void Test_Player_SetAVData() noexcept;
};