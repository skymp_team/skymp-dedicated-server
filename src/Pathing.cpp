#include "stdafx.h"
#include "Pathing.h"

#include <tuple>

using TRIID = Pathing::TRIID;
using VERTID = Pathing::VERTID;

inline bool operator <(const NiPoint3 &lhs, const NiPoint3 &rhs) {
	if (lhs.x < rhs.x)
		return true;
	if (lhs.x == rhs.x && lhs.y < rhs.y)
		return true;
	if (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z < rhs.z)
		return true;
	return false;
}

struct Pathing::Impl
{
	std::set<NavMeshWrap> navMesh;

	struct Vert
	{
		NiPoint3 pos;
		uint32_t locID;

		bool operator==(const Vert &rhs) {
			return rhs.pos == this->pos && rhs.locID == this->locID;
		}
	};
	std::vector<Vert> vertices;

	using Triangle = std::array<size_t, 3>;
	std::vector<Triangle> triangles;
};

Pathing::Pathing() noexcept : pImpl(new Impl)
{
}

Pathing::~Pathing() noexcept
{
	delete pImpl;
}

void Pathing::Add(const NavMeshWrap &navMesh) noexcept
{
	if(pImpl->navMesh.insert(navMesh).second == true)
		this->RecalcGraph();
	log(Log::Debug, "New NavMesh added (", navMesh.GetNumTriangles(), " TRI ", navMesh.GetNumVertices()," VERTS)");
	log(Log::Debug, "(total: ", pImpl->triangles.size(), " TRI, ", pImpl->vertices.size(), " VERTS)");
}

void Pathing::Remove(const NavMeshWrap &navMesh) noexcept
{
	if (pImpl->navMesh.erase(navMesh) > 0)
		this->RecalcGraph();
}

void Pathing::RecalcGraph() noexcept
{
	pImpl->triangles.clear();
	pImpl->vertices.clear();

	auto navMeshCopy = pImpl->navMesh;
	for (auto nav : navMeshCopy)
	{
		if (nav.GetNumTriangles() < 2)
		{
			log(Log::Warning, "Invalid NavMesh: skipping");
			pImpl->navMesh.erase(nav);
		}
	}

	for (auto nav : pImpl->navMesh)
	{
		auto data = nav.GetData();
		std::map<uint16_t, size_t> vertIDTranslate;
		for (uint16_t i = 0; i != data.verts.size(); ++i)
		{
			Impl::Vert newVert;
			newVert.pos = data.verts[i];
			newVert.locID = data.locationID;
			pImpl->vertices.push_back(newVert);
			vertIDTranslate[i] = pImpl->vertices.size() - 1;
		}

		for (size_t n = 0; n != data.triangles.size(); ++n)
		{
			Impl::Triangle trNew;
			for (int32_t v = 0; v != 3; ++v)
			{
				auto vertID = data.triangles[n][v];
				trNew[v] = vertIDTranslate[vertID];
			}
			pImpl->triangles.push_back(trNew);
		}
	}
}

bool Pathing::AreNeighbors(TRIID triangle1, TRIID triangle2) const noexcept
{
	std::set<VERTID> set;
	for (int32_t v = 0; v != 3; ++v)
	{
		set.insert(pImpl->triangles[triangle1][v]);
		set.insert(pImpl->triangles[triangle2][v]);
	}
	return set.size() == 3 + 1;
}

#ifdef max
#undef max
#endif

bool Pathing::IsInTriangle(NiPoint3 src, TRIID tri) const noexcept
{
	auto &triRef = pImpl->triangles[tri];
	const NiPoint3 center = { (pImpl->vertices[triRef[0]].pos.x + pImpl->vertices[triRef[1]].pos.x + pImpl->vertices[triRef[2]].pos.x) / 3.f,
		(pImpl->vertices[triRef[0]].pos.y + pImpl->vertices[triRef[1]].pos.y + pImpl->vertices[triRef[2]].pos.y) / 3.f,
		(pImpl->vertices[triRef[0]].pos.z + pImpl->vertices[triRef[1]].pos.z + pImpl->vertices[triRef[2]].pos.z) / 3.f };

	float notRadius = (pImpl->vertices[triRef[0]].pos - pImpl->vertices[triRef[1]].pos).Length();
	notRadius = std::max(notRadius, (pImpl->vertices[triRef[1]].pos - pImpl->vertices[triRef[2]].pos).Length());
	notRadius = std::max(notRadius, (pImpl->vertices[triRef[0]].pos - pImpl->vertices[triRef[2]].pos).Length());

	if ((src - center).Length() < notRadius / 1.1)
		return true;
	else
		return false;
}

std::vector<NiPoint3> Pathing::PathTo(NiPoint3 src, NiPoint3 end, int32_t srcVertID, std::vector<NiPoint3> basePath, std::vector<NiPoint3> painted) const noexcept
{
	auto triangles = pImpl->triangles;
	auto vertices = pImpl->vertices;

	basePath.push_back(src);
	painted.push_back(src);

	std::set<VERTID> neighbors;

	if (srcVertID == -1)
	{
		for (int32_t tri = 0; tri != triangles.size(); ++tri)
		{
			if (this->IsInTriangle(src, tri))
			{
				neighbors.insert(triangles[tri][0]);
				neighbors.insert(triangles[tri][1]);
				neighbors.insert(triangles[tri][2]);
			}
		}
	}
	else
	{
		for (int32_t tri = 0; tri != triangles.size(); ++tri)
		{
			for (int32_t v = 0; v != 3; ++v)
			{
				if (triangles[tri][v] == srcVertID)
				{
					neighbors.insert(triangles[tri][0]);
					neighbors.insert(triangles[tri][1]);
					neighbors.insert(triangles[tri][2]);
					neighbors.erase(srcVertID);
					break;
				}
			}
		}
	}

	if (srcVertID != -1)
		for (int32_t tri = 0; tri != triangles.size(); ++tri)
		{
			if (IsInTriangle(end, tri))
			{
				for (int32_t v = 0; v != 3; ++v)
				{
					if (triangles[tri][v] == srcVertID)
					{
						basePath.push_back(end);
						return basePath;
					}
				}
			}
		}

	float minF = std::numeric_limits<float>::infinity();
	VERTID minNei = (VERTID)-1;

	for (auto nei : neighbors)
	{
		auto neiPos = vertices[nei].pos;
		auto g = (neiPos - src).Length();
		auto h = (neiPos - end).Length();
		auto f = g + h;
		if (minF > f && std::find(painted.begin(), painted.end(), neiPos) == painted.end())
		{
			minF = f;
			minNei = nei;
		}
	}

	if (minNei == -1)
	{
		log(Log::Warning, "Invalid NEI");
		return {};
	}

	return this->PathTo(vertices[minNei].pos, end, minNei, basePath, painted);
}

void Pathing::TestPathTo() noexcept
{
	if (pImpl->triangles.size() == 0)
		return;

	srand((uint32_t)time(nullptr));
	TRIID a = rand() % pImpl->triangles.size();
	TRIID b = 0;
	do {
		b = rand() % pImpl->triangles.size();
	} while (a == b);

	if (pImpl->vertices.empty())
	{
		log(Log::Error, "Path finder broken");
		return;
	}

	auto path = this->PathTo(pImpl->vertices[pImpl->triangles[a][0]].pos, pImpl->vertices[pImpl->triangles[b][0]].pos);

	auto A = pImpl->vertices[pImpl->triangles[a][0]].pos,
		B = pImpl->vertices[pImpl->triangles[b][0]].pos;
	log(Log::Debug, "Tested pathing from ", A.x, " ", A.y, " ", A.z, " ", " to", B.x, " ", B.y, " ", B.z, " ");

	std::stringstream pathStrS;
	for (auto pathEl : path)
		pathStrS << "{" << (int64_t)pathEl.x << ";" << (int64_t)pathEl.y << ";" << (int64_t)pathEl.z << "} - ";

	auto pathStr = pathStrS.str();
	if (pathStr.size() > 2)
	{
		for (int32_t i = 0; i != 2; ++i)
			pathStr.erase(pathStr.end() - 1);
	}

	log(Log::Debug, "Path is ", pathStr.data());
}