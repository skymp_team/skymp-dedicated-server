#include "stdafx.h"
#include "StringUtil.h"

std::wstring StringUtil::Utf8ToWchar(const std::string &src) noexcept
{
	wchar_t ws[maxSize];
	std::mbstowcs(ws, src.data(), maxSize);
	return ws;
}

std::string StringUtil::WcharToUtf8(const std::wstring &src) noexcept
{
	char s[maxSize];
	std::wcstombs(s, src.data(), maxSize);
	return s;
}