#pragma once

class DataSearch
{
public:
	DataSearch();
	void HandleResult(RakNet::BitStream &bsIn, Player *source) noexcept;

	enum class Opcode : uint8_t {
		NavMesh = 0,
		TPDs = 1,
		Cont = 2,
		Door = 3,
		Item = 4,
		Actor = 5,
		Acti = 6 // 1.0
	};

private:
	struct Impl;
	std::unique_ptr<Impl> pImpl;
};