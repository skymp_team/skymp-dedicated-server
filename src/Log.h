#pragma once

class Log : noncopyable
{
public:
	enum LogLevel {
		Debug,
		Info,
		Warning,
		Error,
		Fatal,
		None = 255
	};

	static std::string get_log_prefix(Log::LogLevel ll);
	static std::ofstream &get_log_ofstream();
};

enum {
	INVALID_LOG_LEVEL = 228228
};

template<class T, class ... Ts>
void log(Log::LogLevel ll, T a, Ts... as)
{
	auto &of = Log::get_log_ofstream();
	std::string result;
	if ((int)ll != INVALID_LOG_LEVEL)
		result = Log::get_log_prefix(ll);
	std::cout << result << a;
	of << result << a;
	log((Log::LogLevel)INVALID_LOG_LEVEL, as ...);
}

template<class T>
void log(Log::LogLevel ll, T a)
{
	auto &of = Log::get_log_ofstream();
	std::string result;
	if ((int)ll != INVALID_LOG_LEVEL)
		result = Log::get_log_prefix(ll);
	std::cout << result << a << std::endl;
	of << result << a << std::endl;
}

