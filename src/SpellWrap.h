#pragma once

class SpellWrap
{
public:
	explicit SpellWrap(std::string identifier);
	~SpellWrap() noexcept;

	bool operator==(const SpellWrap &rhs) const noexcept;
	bool operator!=(const SpellWrap &rhs) const noexcept {
		return !(*this == rhs);
	}
	std::string GetIdentifier() const noexcept;
	std::string GetCastingType() const noexcept;
	std::string GetDelivery() const noexcept;
	luabridge::LuaRef GetNthEffect(uint32_t n) const noexcept;
	std::string GetNthEffectIdentifier(uint32_t n) const noexcept;
	float GetNthEffectMagnitude(uint32_t n) const noexcept;
	float GetNthEffectDuration(uint32_t n) const noexcept;
	float GetNthEffectArea(uint32_t n) const noexcept;
	uint32_t GetNumEffects() const noexcept;
	uint32_t GetExistingGameFormID() const noexcept;
	uint32_t GetBaseID() const noexcept { return this->GetExistingGameFormID(); }; // 1.0.12
	float GetCost() const noexcept;
	bool IsEnchantment() const noexcept;

	void AddEffect(luabridge::LuaRef effect, float magnitude, float durationSec, float area) noexcept;

	static luabridge::LuaRef Create(std::string magicType, std::string identifier, uint32_t existingSpellID, float cost) noexcept;
	static luabridge::LuaRef LookupByIdentifier(std::string identifier) noexcept;
	static luabridge::LuaRef Clone(luabridge::LuaRef otherSpell, luabridge::LuaRef optionalNewSpellIdent, bool copyEffects) noexcept;

	static void Register(lua_State *state);

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;

	friend bool operator <(const SpellWrap &lhs, const SpellWrap &rhs) {
		return lhs.GetIdentifier() < rhs.GetIdentifier();
	}
};

struct SpellInfo
{
	struct EffectItemInfo
	{
		std::string effectIdentifier;
		float magnitude;
		float duration;
		float area;

		friend bool operator==(const EffectItemInfo &lhs, const EffectItemInfo &rhs) noexcept {
			return lhs.effectIdentifier == rhs.effectIdentifier && lhs.magnitude == rhs.magnitude && lhs.duration == rhs.duration && lhs.area == rhs.area;
		}
	};
	std::vector<EffectItemInfo> effects;
	uint32_t existingGameFormID = 0;

	friend bool operator==(const SpellInfo &lhs, const SpellInfo &rhs) noexcept {
		return lhs.effects == rhs.effects && lhs.existingGameFormID == rhs.existingGameFormID;
	}

	friend bool operator!=(const SpellInfo &lhs, const SpellInfo &rhs) noexcept {
		return !(lhs == rhs);
	}

	static SpellInfo Get(const SpellWrap &spell)
	{
		SpellInfo result;

		for (uint32_t i = 1; i <= spell.GetNumEffects(); ++i)
		{
			result.existingGameFormID = spell.GetExistingGameFormID();
			result.effects.push_back({ 
				spell.GetNthEffectIdentifier(i), 
				spell.GetNthEffectMagnitude(i), 
				spell.GetNthEffectDuration(i), 
				spell.GetNthEffectArea(i)
			});
		}

		return result;
	}
};