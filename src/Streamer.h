#pragma once

class Streamer
{
public:
	static Streamer &GetSingleton() noexcept;

	void Connect(uint16_t playerID, bool isTargetOnly) noexcept;
	void Disconnect(uint16_t playerID) noexcept;
	void SetPos(uint16_t playerID, NiPoint3 pos) noexcept;
	void SetVirtualWorld(uint16_t playerID, uint32_t virtualWorld) noexcept;
	void SetLocation(uint16_t playerID, uint32_t location) noexcept;

	void ConnectObject(const FastID &fid, uint32_t objectID) noexcept;
	void DisconnectObject(const FastID &fid) noexcept;
	void SetObjectPos(const FastID &fid, NiPoint3 pos) noexcept;
	void SetObjectVirtualWorld(const FastID &fid, uint32_t vw) noexcept;
	void SetObjectLocation(const FastID &fid, uint32_t loc) noexcept;

	void Step() noexcept;

private:
	Streamer();
	struct Impl;
	Impl *const pImpl;
};