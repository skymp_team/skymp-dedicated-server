#include "stdafx.h"
#include "RecipeWrap.h"

struct Recipe : public Components::Container
{
	uint32_t id = 0;
	std::string keyword;
	nonstd::optional<ItemTypeWrap> produce;
	uint32_t produceCount = 0;
};

std::vector<Recipe *> createdRecipes;

struct RecipeWrap::Impl
{
	Recipe *r = nullptr;
};

RecipeWrap::RecipeWrap(uint32_t id) : pImpl(new Impl)
{
	try {
		pImpl->r = createdRecipes.at(id);
		return;
	}
	catch (...) {
	}
	throw std::runtime_error("unable to create wrap for Recipe " + std::to_string(id));
}

bool RecipeWrap::operator==(const RecipeWrap &rhs) const noexcept
{
	return pImpl->r == rhs.pImpl->r;
}

uint32_t RecipeWrap::GetID() const noexcept
{
	return pImpl->r->id;
}


std::string RecipeWrap::GetWorkbenchKeyword() const noexcept
{
	return pImpl->r->keyword;
}

ItemTypeWrap RecipeWrap::GetProduce() const noexcept
{
	if (pImpl->r->produce == nonstd::nullopt)
		log(Log::Fatal, "Invalid Recipe produce");
	return *pImpl->r->produce;
}

uint32_t RecipeWrap::GetProduceCount() const noexcept
{
	return pImpl->r->produceCount;
}

luabridge::LuaRef RecipeWrap::Create(std::string workbenchKeyword, luabridge::LuaRef produce, uint32_t produceCount) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	try {
		if (produce.isUserdata() || produce.isLightUserdata())
		{
			if (produceCount > 0 && workbenchKeyword != "")
			{
				auto newRec = new Recipe;
				newRec->id = (uint32_t)createdRecipes.size();
				newRec->produce = produce.cast<ItemTypeWrap>();
				newRec->produceCount = produceCount;
				newRec->keyword = workbenchKeyword;
				createdRecipes.push_back(newRec);
				result = RecipeWrap::LookupByID(newRec->id);
			}
		}
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}

	return result;
}

luabridge::LuaRef RecipeWrap::LookupByID(uint32_t id) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	try {
		auto recipePtr = createdRecipes.at(id);
		if (recipePtr == nullptr)
			throw std::logic_error("deleted recipe");
		result = RecipeWrap(recipePtr->id);
	}
	catch (...) {
		result = luabridge::Nil();
	}
	return result;
}

Components::Container RecipeWrap::GetContainer() const noexcept
{
	return *pImpl->r;
}
std::list<RecipeWrap> RecipeWrap::GetAllRecipes() noexcept
{
	static std::list<RecipeWrap> cache;

	if (cache.size() == createdRecipes.size())
		return cache;

	std::list<RecipeWrap> result; 
	for (auto &rec : createdRecipes)
		result.push_back(RecipeWrap(rec->id));
	cache = result;

	return result;
}

size_t RecipeWrap::GetNumRecipes() noexcept
{
	return createdRecipes.size();
}

void RecipeWrap::AddItem(luabridge::LuaRef itemType, uint32_t count) noexcept
{
	if (itemType.isUserdata() || itemType.isLightUserdata())
	{
		const auto itemType_ = itemType.cast<ItemTypeWrap>();
		pImpl->r->AddItem(itemType_, count);
	}
}

bool RecipeWrap::RemoveItem(luabridge::LuaRef itemType, uint32_t count) noexcept
{
	if (itemType.isUserdata() || itemType.isLightUserdata())
		return pImpl->r->RemoveItem(itemType.cast<ItemTypeWrap>(), count);
	return false;
}

void RecipeWrap::RemoveAllItems() noexcept
{
	pImpl->r->RemoveAllItems();
}

void RecipeWrap::Register(lua_State *L)
{
	luabridge::getGlobalNamespace(L)
		.beginClass<RecipeWrap>("Recipe")
		.addFunction("__eq", &RecipeWrap::operator==)
		.addFunction("GetID", &RecipeWrap::GetID)
		.addFunction("GetWorkbenchKeyword", &RecipeWrap::GetWorkbenchKeyword)
		.addFunction("GetProduce", &RecipeWrap::GetProduce)
		.addFunction("GetProduceCount", &RecipeWrap::GetProduceCount)
		.addFunction("AddItem", &RecipeWrap::AddItem)
		.addFunction("RemoveItem", &RecipeWrap::RemoveItem)
		.addFunction("RemoveAllItems", &RecipeWrap::RemoveAllItems)
		.addStaticFunction("Create", &RecipeWrap::Create)
		.addStaticFunction("LookupByID", &RecipeWrap::LookupByID)
		.endClass();
}