#pragma once

class RaceWrap
{
public:
	explicit RaceWrap(uint32_t formID) noexcept;
	~RaceWrap() noexcept;

	bool operator==(const  RaceWrap &rhs) const noexcept;
	uint32_t GetID() const noexcept;

	static void Register(lua_State *state);

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;
};