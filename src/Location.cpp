#include "stdafx.h"
#include "Location.h"

struct Location::Impl
{
	uint32_t formID;
};

Location::Location(uint32_t formID) noexcept : pImpl(new Impl)
{
	pImpl->formID = formID;
}

Location::~Location() noexcept
{
}

uint32_t Location::GetFormID() const noexcept
{
	return pImpl->formID;
}