#include "stdafx.h"
#include "SpellWrap.h"

#include "MagicBase.h"
#include "IDAssigner.h"

class Spell : public Components::MagicBase
{
public:
	std::string identifier;
	uint32_t existingSpellID = 0;
	float cost = 0;
	bool isEnch = false;
};

std::map<std::string, Spell *> createdSpells;

struct SpellWrap::Impl
{
	Spell *spell = nullptr;
};

SpellWrap::SpellWrap(std::string identifier) : pImpl(new Impl)
{
	try {
		pImpl->spell = createdSpells.at(identifier);
		IDAssigner<SpellWrap>::GetID(*this); // generate id
		return;
	}
	catch (...) {
	}
	throw std::runtime_error("unable to create wrap for Spell " + identifier);
}

SpellWrap::~SpellWrap() noexcept
{
}

bool SpellWrap::operator==(const SpellWrap &rhs) const noexcept
{
	return this->pImpl->spell == rhs.pImpl->spell;
}

std::string SpellWrap::GetIdentifier() const noexcept
{
	return pImpl->spell->identifier;
}

std::string SpellWrap::GetCastingType() const noexcept
{
	return pImpl->spell->IsEmpty() ? "" : pImpl->spell->GetEffects().front().effect.GetCastingType();
}

std::string SpellWrap::GetDelivery() const noexcept
{
	return pImpl->spell->IsEmpty() ? "" : pImpl->spell->GetEffects().front().effect.GetDelivery();
}

luabridge::LuaRef SpellWrap::GetNthEffect(uint32_t n) const noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	--n;
	if (n < pImpl->spell->GetNumEffects())
		result = (EffectWrap)pImpl->spell->GetEffects()[n].effect;

	return result;
}

std::string SpellWrap::GetNthEffectIdentifier(uint32_t n) const noexcept
{
	--n;
	if (n < pImpl->spell->GetNumEffects())
		return pImpl->spell->GetEffects()[n].effect.GetIdentifier();

	return "";
}

float SpellWrap::GetNthEffectMagnitude(uint32_t n) const noexcept
{
	--n;
	if (n < pImpl->spell->GetNumEffects())
		return pImpl->spell->GetEffects()[n].magnitude;
	return 0;
}

float SpellWrap::GetNthEffectDuration(uint32_t n) const noexcept
{
	--n;
	if (n < pImpl->spell->GetNumEffects())
		return pImpl->spell->GetEffects()[n].durationSec;
	return 0;
}

float SpellWrap::GetNthEffectArea(uint32_t n) const noexcept
{
	--n;
	if (n < pImpl->spell->GetNumEffects())
		return pImpl->spell->GetEffects()[n].area;
	return 0;
}

uint32_t SpellWrap::GetNumEffects() const noexcept
{
	return pImpl->spell->GetNumEffects();
}

uint32_t SpellWrap::GetExistingGameFormID() const noexcept
{
	return pImpl->spell->existingSpellID;
}

float SpellWrap::GetCost() const noexcept
{
	return pImpl->spell->cost;
}

bool SpellWrap::IsEnchantment() const noexcept
{
	return pImpl->spell->isEnch;
}

void SpellWrap::AddEffect(luabridge::LuaRef effectLuaRef, float magnitude, float durationSec, float area) noexcept
{
	if (effectLuaRef.isLightUserdata() || effectLuaRef.isUserdata())
	{
		const EffectWrap effect = effectLuaRef;
		pImpl->spell->AddEffect(effect, magnitude, durationSec, area);
	}
}

luabridge::LuaRef SpellWrap::Create(std::string magicType, std::string ident, uint32_t existingSpellID, float cost) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	bool isEnch;
	if (magicType == "Spell")
		isEnch = false;
	else if (magicType == "Enchantment")
		isEnch = true;
	else
	{
		log(Log::Warning, "Unknown magic type ", magicType.data());
		return result;
	}

	/*static std::set<uint32_t> created;
	if (created.find(existingSpellID) != created.end())
	{
		std::stringstream ss;
		ss << "Base ID " << std::hex << existingSpellID << " already in use";
		log(Log::Warning, ss.str().data());
		return result;
	}*/

	if (ident.empty()
		|| !(SpellWrap::LookupByIdentifier(ident) == luabridge::Nil())
		|| !(ItemTypeWrap::LookupByIdentifier(ident) == luabridge::Nil()))
	{
		log(Log::Warning,
			ident.empty() ? "Empty identifier" : "Entity already exist");
		return result;
	}

	auto spell = createdSpells.insert({ ident, new Spell }).first->second;
	spell->identifier = ident;
	spell->existingSpellID = existingSpellID;
	if (!(cost >= 0))
		cost = 0;
	spell->cost = cost;
	spell->isEnch = isEnch;

	nonstd::optional<SpellWrap> wrap;
	try {
		wrap = SpellWrap(ident);
	}
	catch (...) {
	}
	if (wrap != nonstd::nullopt)
	{
		result = *wrap;
		//created.insert(existingSpellID);
	}

	return result;
}

luabridge::LuaRef SpellWrap::LookupByIdentifier(std::string name) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());

	try {
		result = SpellWrap(name);
	}
	catch (...) {
		result = luabridge::Nil();
	}

	return result;
}

luabridge::LuaRef SpellWrap::Clone(luabridge::LuaRef srcRef, luabridge::LuaRef optionalNewItemTypeIdent, bool copyEffects) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	if (srcRef.isLightUserdata() == false && srcRef.isUserdata() == false)
	{
		log(Log::Warning, srcRef.isNil() ? "Clone() source must not be nil" : "Clone() unable to clone from non-Spell Lua reference");
		return result;
	}

	const SpellWrap src = srcRef;

	std::string newIdent;

	if (optionalNewItemTypeIdent.isString())
		newIdent = (optionalNewItemTypeIdent).cast<std::string>();

	if (newIdent == "")
	{
		do {
			static std::map<SpellWrap, uint32_t> numClones;
			newIdent = src.GetIdentifier() + std::to_string(numClones[src]);
			++numClones[src];
		} while (LookupByIdentifier(newIdent).isNil() == false);
	}

	if (LookupByIdentifier(newIdent).isNil() == false)
	{
		log(Log::Warning, "Clone() identifier already used");
		return result;
	}

	auto newItemType = new Spell(*src.pImpl->spell);
	newItemType->identifier = newIdent;
	createdSpells.insert({ newIdent, newItemType }).first->second;
	result = SpellWrap(newIdent);

	if (copyEffects == false)
	{
		newItemType->RemoveAllEffects();
	}

	return result;
}

void SpellWrap::Register(lua_State *L)
{
	luabridge::getGlobalNamespace(L)
		.beginClass<SpellWrap>("Magic")
		.addFunction("__eq", &SpellWrap::operator==)
		.addFunction("GetIdentifier", &SpellWrap::GetIdentifier)
		.addFunction("GetCastingType", &SpellWrap::GetCastingType)
		.addFunction("GetDelivery", &SpellWrap::GetDelivery)
		.addFunction("GetNthEffect", &SpellWrap::GetNthEffect)
		.addFunction("GetNthEffectMagnitude", &SpellWrap::GetNthEffectMagnitude)
		.addFunction("GetNthEffectDuration", &SpellWrap::GetNthEffectDuration)
		.addFunction("GetNthEffectArea", &SpellWrap::GetNthEffectArea)
		.addFunction("GetNumEffects", &SpellWrap::GetNumEffects)
		.addFunction("GetCost", &SpellWrap::GetCost)
		.addFunction("IsEnchantment", &SpellWrap::IsEnchantment)
		.addFunction("AddEffect", &SpellWrap::AddEffect)
		.addFunction("GetBaseID", &SpellWrap::GetBaseID)
		.addStaticFunction("Create", &SpellWrap::Create)
		.addStaticFunction("LookupByIdentifier", &SpellWrap::LookupByIdentifier)
		.addStaticFunction("Clone", &SpellWrap::Clone)
		.endClass();
}