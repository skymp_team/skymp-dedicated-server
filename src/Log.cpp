#include "stdafx.h"
#include <queue>

std::ofstream &Log::get_log_ofstream()
{
	static std::ofstream *ofstream = nullptr;
	if (ofstream == nullptr)
	{
		auto fileName = g.cfg->GetValue<std::string>("General.logFile", "");
		ofstream = new std::ofstream;
		ofstream->open(fileName, std::ios_base::app);
		(*ofstream) << std::endl;
	}
	return *ofstream;
}

std::string Log::get_log_prefix(Log::LogLevel ll)
{
	std::string result;
	tm *newtime;
	time_t aclock;
	time(&aclock);
	newtime = localtime(&aclock);
	result = (asctime(newtime));

	auto pos = result.find(':');
	result = '[' + std::string(result.begin() + pos - 2, result.begin() + pos + 6) + "] ";
	switch (ll)
	{
	case Log::LogLevel::Debug:
		result += "debug: ";
		break;
	case Log::LogLevel::Info:
		result += "";
		break;
	case Log::LogLevel::Warning:
		result += "warning: ";
		break;
	case Log::LogLevel::Error:
		result += "error: ";
		break;
	case Log::LogLevel::Fatal:
		result += "fatal: ";
		break;
	}
	return result;
}