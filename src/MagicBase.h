#pragma once
#include "EffectWrap.h"

namespace Components
{
	class MagicBase
	{
	public:
		struct EffectItem
		{
			EffectWrap effect;
			float magnitude;
			float durationSec;
			float area;
		};

		using Effects = std::vector<EffectItem>;

		MagicBase() noexcept
		{
		}

		const Effects &GetEffects() noexcept {
			return this->effects;
		}

		void AddEffect(const EffectItem &effectItem) noexcept {
			if(this->IsEmpty() 
				|| (effectItem.effect.GetDelivery() == this->GetEffects().front().effect.GetDelivery() 
					&& effectItem.effect.GetCastingType() == this->GetEffects().front().effect.GetCastingType()))
			this->effects.push_back(effectItem);
		}

		void AddEffect(EffectWrap effect, float magnitude, float durationSec, float area) noexcept {
			return this->AddEffect({ effect, magnitude, durationSec, area });
		}
		
		std::string GetCastingType() const noexcept {
			return this->effects.empty() ? "" : this->effects.front().effect.GetCastingType();
		}

		std::string GetDelivery() const noexcept {
			return this->effects.empty() ? "" : this->effects.front().effect.GetDelivery();
		}

		bool IsEmpty() const noexcept {
			return this->GetNumEffects() == 0;
		}

		uint32_t GetNumEffects() const noexcept {
			return (uint32_t)this->effects.size();
		}

		void RemoveAllEffects() noexcept {
			this->effects.clear();
		}

	private:
		Effects effects;
	};
}