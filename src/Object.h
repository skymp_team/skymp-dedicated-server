#pragma once
#include "ItemTypeWrap.h"
#include "Container.h"
#include "KeywordList.h"
#include "Hostable.h"

class Object;
class Player;

using Objects = std::map<uint32_t, std::shared_ptr<Object>>;

struct FastID
{
	size_t n = 0;

	friend bool operator==(const FastID &lhs, const FastID &rhs) {
		return lhs.n == rhs.n;
	}

	friend bool operator!=(const FastID &lhs, const FastID &rhs) {
		return !(lhs == rhs);
	}

	bool operator<(const FastID &rhs) const {
		return n < rhs.n;
	}
};

class ObjectManager : noncopyable
{
public:
	ObjectManager() noexcept;
	~ObjectManager() noexcept;

	Object *NewObject(uint32_t existingReferenceID, uint32_t baseFormID, uint32_t locationID, NiPoint3 pos, NiPoint3 rot) noexcept;
	void DeleteObject(uint32_t id) noexcept;
	Object *LookupObject(uint32_t id) const noexcept;
	Object *LookupObject(const FastID &fid) const noexcept;
	const Objects &GetObjects() const noexcept;

private:
	uint32_t GenerateID() const noexcept;

	struct Impl;
	Impl *const pImpl;
};

class Object : 
	public Components::Container, 
	public Components::Hostable,
	private noncopyable
{
	friend class ObjectManager;

public:

	enum class Type : uint8_t
	{
		Static =			0x00,
		Door =				0x01,
		TeleportDoor =		0x02,
		Activator =			0x03,
		Furniture =			0x04,
		Item =				0x05,
		Container =			0x06,
	};

	~Object() noexcept;

	uint32_t GetID() const noexcept;
	FastID GetFastID() const noexcept;
	bool IsNative() const noexcept;
	uint32_t GetBaseFormID() const noexcept;
	uint32_t GetLocation() const noexcept;
	NiPoint3 GetPos() const noexcept;
	NiPoint3 GetRot() const noexcept;
	uint32_t GetVirtualWorld() const noexcept;
	std::string GetName() const noexcept;
	Type GetType() const noexcept;
	bool IsOpen() const noexcept;
	uint32_t GetTeleportTarget() const noexcept;
	bool IsDisabled() const noexcept;
	bool IsDestroyed(Player *optionalPl = nullptr) const noexcept;
	uint8_t GetLockLevel() const noexcept;
	bool IsHarvested() const noexcept;
	bool IsHostable() const noexcept;
	bool IsSpawned() const noexcept { return true; }

	void SetPos(NiPoint3 pos) noexcept;
	void SetRot(NiPoint3 rot) noexcept;
	bool SetLocation(uint32_t locationID) noexcept;
	void SetVirtualWorld(uint32_t virtualWorld) noexcept;
	void SetName(const std::string &name) noexcept;
	void SetType(Type type) noexcept;
	void SetOpen(bool open) noexcept;
	void SetTeleportTarget(uint32_t targetID) noexcept;
	void SetDisabled(bool disabled) noexcept;
	void SetDestroyed(bool destroyed) noexcept;
	void SetDestroyedForPlayer(bool destroyed, const Player *player) noexcept;
	bool InjectMovementBy(Player *host, const NiPoint3 &pos, const NiPoint3 &rot, bool grabbing) noexcept;
	void SetLockLevel(uint8_t lockLevel) noexcept;
	void SetHarvested(bool harvested) noexcept;
	void Activate(Player *source, bool triggerCallback, bool isOpen) noexcept; // can delete object
	void SetHostable(bool hostable) noexcept;

	void SetStreamedFor(Player *target, bool isStreamedIn) noexcept;
	bool IsStreamedFor(Player *target) const noexcept;

	void StreamIn(void *) noexcept {};
	void StreamOut(void *) noexcept {};

	void AddKeyword(Components::Keyword k) noexcept;
	void RemoveKeyword(Components::Keyword k) noexcept;
	std::set<Components::Keyword> GetKeywords() const noexcept;
	uint32_t GetNumKeywords() const noexcept {
		return (uint32_t)this->GetKeywords().size();
	}
	bool HasKeyword(Components::Keyword k) const noexcept;

	bool IsAlchemyWorkbench() const noexcept {
		//Components::Keyword k("isAlchemy");
		//return this->HasKeyword(k);
		return true;
	}

	bool IsEnchantingWorkbench() const noexcept {
		//Components::Keyword k("isEnchanting");
		//return this->HasKeyword(k);
		return true;
	}

private:
	void Update() noexcept;
	Object(uint32_t id, const FastID &fid, bool isNative, uint32_t baseFormID, uint32_t locationID, NiPoint3 pos, NiPoint3 rot) noexcept;

	struct Impl;
	Impl *const pImpl;
};