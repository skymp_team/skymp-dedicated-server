#include "stdafx.h"
#include "Object.h"
#include "Streamer.h"

using namespace Components;

struct ObjectManager::Impl
{
	Objects objects;
	std::vector<Object *> objectByFid;
};

ObjectManager::ObjectManager() noexcept : pImpl(new Impl)
{
}

ObjectManager::~ObjectManager() noexcept
{
}

Object *ObjectManager::NewObject(uint32_t existingReferenceID, uint32_t baseFormID, uint32_t locationID, NiPoint3 pos, NiPoint3 rot) noexcept
{
	uint32_t id = existingReferenceID;
	bool isNative = (id != 0);
	if (!isNative)
		id = this->GenerateID();

	static FastID fid = { 0 };
	const std::shared_ptr<Object> object(new Object(id, fid, isNative, baseFormID, locationID, pos, rot));
	pImpl->objectByFid.push_back(object.get());
	++fid.n;

	pImpl->objects[id] = object;
	return object.get();
}

void ObjectManager::DeleteObject(uint32_t id) noexcept
{
	auto object = this->LookupObject(id);
	if (object == nullptr)
		return;

	const auto &players = g.plManager->GetPlayers();
	for (auto it = players.begin(); it != players.end(); ++it)
	{
		auto &player = it->second;
		if (player != nullptr)
			player->StreamOut(object);
	}

	pImpl->objectByFid[(size_t)object->GetFastID().n] = nullptr;
	pImpl->objects.erase(id);
}

Object *ObjectManager::LookupObject(uint32_t id) const noexcept
{
	try {
		return pImpl->objects.at(id).get();
	}
	catch (...) {
		return nullptr;
	}
}

Object *ObjectManager::LookupObject(const FastID &fid) const noexcept
{
	try {
		return pImpl->objectByFid.at(fid.n);
	}
	catch (...) {
		return nullptr;
	}
}

const Objects &ObjectManager::GetObjects() const noexcept
{
	return pImpl->objects;
}

uint32_t ObjectManager::GenerateID() const noexcept
{
	static uint32_t id = 0xFF000000;
	return id++;
}

struct Object::Impl
{
	uint32_t id = 0;
	FastID fid;
	bool isNative = false;
	uint32_t baseFormID = 0;
	uint32_t locationID = 0;
	NiPoint3 pos = { 0,0,0 };
	NiPoint3 rot = { 0,0,0 };
	uint32_t virtualWorld = 0;
	std::string name = "";
	Type type = Type::Static;
	struct {
		bool isOpen = false;
		uint32_t targetDoorID = 0;
		struct {
			nonstd::optional<ItemTypeWrap> type;
			uint32_t count = 0;
		} item;

	} typeSensitive;
	bool isDisabled = false;
	bool isDestroyed = false;
	std::map<RakNet::RakNetGUID, bool> isDestroyedForPlayers;
	std::set<uint16_t> streamedInPlayers;
	uint8_t lockLevel = 0;
	bool harvested = false;
	bool hostable = true;
};

Object::Object(uint32_t id, const FastID &fid, bool isNative, uint32_t baseFormID, uint32_t locationID, NiPoint3 pos, NiPoint3 rot) noexcept : pImpl(new Impl)
{
	pImpl->id = id;
	pImpl->fid = fid;
	pImpl->isNative = isNative;
	pImpl->baseFormID = baseFormID;
	pImpl->locationID = locationID; // do not change to 0
	pImpl->pos = { 0,0,0 };
	pImpl->rot = rot;

	Streamer::GetSingleton().ConnectObject(fid, id);

	this->SetPos(pos);
	this->SetLocation(locationID);
	this->SetVirtualWorld(0);
}

Object::~Object() noexcept
{
	Streamer::GetSingleton().DisconnectObject(this->GetFastID());
	delete pImpl;
}

uint32_t Object::GetID() const noexcept
{
	return pImpl->id;
}

FastID Object::GetFastID() const noexcept
{
	return pImpl->fid;
}

bool Object::IsNative() const noexcept
{
	return pImpl->isNative;
}

uint32_t Object::GetBaseFormID() const noexcept
{
	return pImpl->baseFormID;
}

uint32_t Object::GetLocation() const noexcept
{
	return pImpl->locationID;
}

NiPoint3 Object::GetPos() const noexcept
{
	return pImpl->pos;
}

NiPoint3 Object::GetRot() const noexcept
{
	return pImpl->rot;
}

uint32_t Object::GetVirtualWorld() const noexcept
{
	return pImpl->virtualWorld;
}

std::string Object::GetName() const noexcept
{
	return pImpl->name;
}

Object::Type Object::GetType() const noexcept
{
	return pImpl->type;
}

bool Object::IsOpen() const noexcept
{
	return pImpl->typeSensitive.isOpen;
}

uint32_t Object::GetTeleportTarget() const noexcept
{
	return pImpl->typeSensitive.targetDoorID;
}

bool Object::IsDisabled() const noexcept
{
	return pImpl->isDisabled;
}

bool Object::IsDestroyed(Player *optionalPl) const noexcept
{
	if (!optionalPl)
		return pImpl->isDestroyed;
	else
		return pImpl->isDestroyedForPlayers[optionalPl->GetGUID()];
}

uint8_t Object::GetLockLevel() const noexcept
{
	return pImpl->lockLevel;
}

bool Object::IsHarvested() const noexcept
{
	return pImpl->harvested;
}

bool Object::IsHostable() const noexcept
{
	return pImpl->hostable;
}

void Object::SetPos(NiPoint3 pos) noexcept
{
	pImpl->pos = pos;
	Streamer::GetSingleton().SetObjectPos(this->GetFastID(), pos);
	this->Update();
}

void Object::SetRot(NiPoint3 rot) noexcept
{
	pImpl->rot = rot;
	this->Update();
}

bool Object::SetLocation(uint32_t locationID) noexcept
{
	/*if (this->IsNative())
		return false;*/
	//if (pImpl->locationID != locationID) // DO NOT uncomment THIS!!!
	{
		pImpl->locationID = locationID;
		Streamer::GetSingleton().SetObjectLocation(this->GetFastID(), locationID);
		this->Update();
	}
	return true;
}

void Object::SetVirtualWorld(uint32_t virtualWorld) noexcept
{
	pImpl->virtualWorld = virtualWorld;
	Streamer::GetSingleton().SetObjectVirtualWorld(this->GetFastID(), virtualWorld);
	this->Update();
}

void Object::SetName(const std::string &name) noexcept
{
	pImpl->name = name;
	this->Update();
}

void Object::SetType(Type type) noexcept
{
	pImpl->type = type;
	this->Update();
}

void Object::SetOpen(bool open) noexcept
{
	pImpl->typeSensitive.isOpen = open;
	this->Update();
}

void Object::SetTeleportTarget(uint32_t target) noexcept
{
	pImpl->typeSensitive.targetDoorID = target;
	this->Update();
}

void Object::SetDisabled(bool disabled) noexcept
{
	pImpl->isDisabled = disabled;
	this->Update();
}

void Object::SetDestroyed(bool destroyed) noexcept
{
	pImpl->isDestroyed = destroyed;
	pImpl->isDestroyedForPlayers.clear();
	this->Update();
}

void Object::SetDestroyedForPlayer(bool destroyed, const Player *player) noexcept
{
	pImpl->isDestroyedForPlayers[player->GetGUID()] = destroyed;
	this->Update();
}

bool Object::InjectMovementBy(Player *player, const NiPoint3 &pos, const NiPoint3 &rot, bool grabbing) noexcept
{
	const auto id = this->GetID();

	auto guid = this->GetHostGUID();

	if (guid == player->GetGUID())
	{
		static std::map<uint32_t, bool> hasTimer;
		if (!hasTimer[id] && (pos - this->GetPos()).Length() <= 0.1 && !grabbing)
		{
			hasTimer[id] = true;
			g.timers->SetTimer(std::chrono::system_clock::now() + 1s, [id] {
				hasTimer.erase(id);
				auto obj = g.objManager->LookupObject(id);
				if (obj != nullptr)
					obj->SetHost(nullptr);
			});
		}

		this->SetPos(pos);
		this->SetRot(rot);
		return true;
	}
	return false;
}

void Object::SetLockLevel(uint8_t level) noexcept
{
	switch (this->GetType())
	{
	case Object::Type::Door:
	case Object::Type::Container:
	case Object::Type::TeleportDoor:
		if (level != 0 && level != 255 && this->GetType() == Object::Type::TeleportDoor)
			return log(Log::Warning, "Lockpicking is not implemented for object type TeleportDoor");
			
		pImpl->lockLevel = level;
		if (level > 0)
			this->SetOpen(false);
		break;
	default:
		break;
	}
	this->Update();
}

void Object::SetHarvested(bool harvested) noexcept
{
	if (this->GetType() == Object::Type::Activator)
	{
		pImpl->harvested = harvested;
		this->Update();
	}
}

void Object::Activate(Player *source, bool triggerCallback, bool isOpen) noexcept
{
	const auto sourceID = source->GetID();
	const auto object = this;
	const auto id = object->GetID();

	auto onActivate = [&] {
		if (triggerCallback == false)
			return true;
		return g.gamemode->OnPlayerActivateObject(source->GetID(), object->GetID());
	};

	switch (object->GetType())
	{
	case Object::Type::TeleportDoor: // Deprecated
	{
		bool result = onActivate();
		if (!result)
		{
			log(Log::Warning, "Blocking activation of teleport doors is not implemented. Use SetDestroyed.");
		}
		if (isOpen)
		{
			if (object->GetLockLevel() == 0)
			{
				source->UseTeleportDoor(object);

				g.timers->SetTimer(std::chrono::system_clock::now() + 1s, [id] {
					auto object = g.objManager->LookupObject(id);
					if (object != nullptr)
						object->SetOpen(false);
				});
			}
			else
			{
				source->SetCurrentFurniture(object->GetID());
			}
		}
		object->SetOpen(isOpen);
		break;
	}
	case Object::Type::Door:
		if (!onActivate())
			break;
		if (object->GetLockLevel() != 0)
		{
			source->SetCurrentFurniture(object->GetID());
			break;
		}
		object->SetOpen(isOpen);
		g.timers->SetTimer(std::chrono::system_clock::now() + 2s, [id, sourceID] {
			const auto object = g.objManager->LookupObject(id);
			const auto pl = g.plManager->GetPlayer(sourceID);
			const auto sec = pl ? pl->GetSecondsFromLastLoading() : 0.0;
			if (object != nullptr && pl != nullptr && sec < 5.0)
			{
				object->SetOpen(false);
			}
		});
		break;
	case Object::Type::Activator:
		if (!onActivate())
			break;
		break;
	case Object::Type::Container:
	case Object::Type::Furniture:
		if (object->GetType() == Object::Type::Furniture)
		{
			if (source->GetLastInjectedMovement() == nullptr ||
				source->GetLastInjectedMovement()->runMode != Packet::MovementData::RunMode::Standing) // prevent dropping the furniture
				break;
		}
		if (!onActivate())
			break;
		if (object->GetLockLevel() != 0)
		{
			source->SetCurrentFurniture(object->GetID());
			break;
		}
		object->SetOpen(isOpen);
		if (source->GetCurrentFurniture() != id)
		{
			source->SetCurrentFurniture(id);
		}
		else
		{
			source->SetCurrentFurniture(0);
		}
		break;
	case Object::Type::Item:
		if (!onActivate())
			break;
		object->RemoveAllItems(source);
		g.objManager->DeleteObject(id);
		break;
	default:
		break;
	}
}

void Object::SetHostable(bool v) noexcept
{
	pImpl->hostable = v;
	this->Update();
}

void Object::SetStreamedFor(Player *target, bool isStreamedIn) noexcept
{
	if (target != nullptr)
	{
		if (isStreamedIn)
		{
			pImpl->streamedInPlayers.insert(target->GetID());
			target->UpdateObject(this);
		}
		else
			pImpl->streamedInPlayers.erase(target->GetID());
	}
}

bool Object::IsStreamedFor(Player *target) const noexcept
{
	return target != nullptr && pImpl->streamedInPlayers.count(target->GetID()) != 0;
}

using BaseFormID = uint32_t;

std::map<BaseFormID, KeywordList> gKeywords;

void Object::AddKeyword(Keyword k) noexcept
{
	gKeywords[this->GetBaseFormID()].AddKeyword(k);
}

void Object::RemoveKeyword(Keyword k) noexcept
{
	gKeywords[this->GetBaseFormID()].RemoveKeyword(k);
}

std::set<Components::Keyword> Object::GetKeywords() const noexcept
{
	return gKeywords[this->GetBaseFormID()].GetKeywords();
}

bool Object::HasKeyword(Components::Keyword k) const noexcept
{
	return gKeywords[this->GetBaseFormID()].HasKeyword(k);
}

void Object::Update() noexcept
{
	std::vector<uint16_t> toErase;
	for (auto playerID : pImpl->streamedInPlayers)
	{
		auto pl = g.plManager->GetPlayer(playerID);
		if (pl == nullptr)
			toErase.push_back(playerID);
		else
			pl->UpdateObject(this);
	}
	for (auto playerID : toErase)
		pImpl->streamedInPlayers.erase(playerID);
}