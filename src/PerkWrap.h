#pragma once

class PerkWrap
{
public:
	explicit PerkWrap(uint32_t id);
	bool operator==(const PerkWrap &rhs) const noexcept;
	bool operator!=(const PerkWrap &rhs) const noexcept {
		return !(*this == rhs);
	}

	uint32_t GetID() const noexcept;
	std::string GetSkill() const noexcept;
	float GetSkillLevel() const noexcept;
	luabridge::LuaRef GetRequiredPerk() const noexcept;
	void SetPlayable(bool pl) noexcept;
	bool IsPlayable() const noexcept;

	static luabridge::LuaRef Create(uint32_t existingPerkID, std::string skillName, float requiredSkillLevel, luabridge::LuaRef optionalRequiredPerk) noexcept;
	static luabridge::LuaRef LookupByID(uint32_t id) noexcept;

	static void Register(lua_State *state);

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;

	friend bool operator <(const PerkWrap &lhs, const PerkWrap &rhs) {
		return lhs.GetID() < rhs.GetID();
	}
};