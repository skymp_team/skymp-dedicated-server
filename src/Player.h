#pragma once
#include "PacketTypes.h"

#include "Container.h"
#include "SpellList.h"
#include "MagicTarget.h"
#include "Hostable.h"
#include "DataSearch.h"

#include "ItemTypeWrap.h"
#include "EffectWrap.h"
#include "SpellWrap.h"
#include "Object.h"
#include "Text3D.h"
#include "RecipeWrap.h"
#include "PerkWrap.h"

class Player;
struct PlayerInfo;

class PlayerManager : noncopyable
{
public:
	using Players = std::map<RakNet::RakNetGUID, std::shared_ptr<Player>>;

	PlayerManager() noexcept;
	~PlayerManager() noexcept;

	Player *NewPlayer(RakNet::RakNetGUID guid, const std::string &nickname) noexcept;
	Player *NewNPC() noexcept;
	void DeletePlayer(RakNet::RakNetGUID guid) noexcept;
	Player *GetPlayer(RakNet::RakNetGUID guid) const noexcept;
	Player *GetPlayer(uint16_t id) const noexcept;
	const Players &GetPlayers() const noexcept;

private:
	struct Impl;
	Impl *const pImpl;
};

class Player : 
	public Components::Container,
	public Components::SpellList,
	public Components::MagicTarget,
	public Components::Hostable,
	private noncopyable
{
	friend class PlayerManager;
public:

	enum class DialogStyle : uint8_t
	{
		Message =		0x00,
		Input =			0x01,
		List =			0x02,
	};

	enum class CommandType : uint8_t
	{
		Console =		0x00,
		CDScript =		0x01,
		SkyMP =			0x02,
	};


	struct AVData
	{
		float base;
		float modifier;
		float percentage;

		float GetAfterBuffs() {
			return base + modifier;
		}

		float GetCurrent() {
			return percentage * GetAfterBuffs();
		}

		void SetCurrent(float newValue) {
			percentage = newValue / GetAfterBuffs();
			if (percentage < 0)
				percentage = 0;
			if (percentage > 1)
				percentage = 1;
		}

		void RestoreCurrent(float mod) {
			mod = abs(mod);
			SetCurrent(GetCurrent() + mod);
		}

		void DamageCurrent(float mod) {
			mod = -abs(mod);
			SetCurrent(GetCurrent() + mod);
		}
	};

	static float GetInvalidAVBase() noexcept {
		return std::numeric_limits<float>::infinity();
	}

public:
	PacketReliability reliability = PacketReliability::UNRELIABLE;
	const uint64_t uniqueID;

	virtual ~Player() noexcept;

	uint16_t GetID() const noexcept;
	RakNet::RakNetGUID GetGUID() const noexcept;

	void SetName(const std::string &name) noexcept;
	const std::string &GetName() const noexcept;

	void UpdateStreamedPlayersList() noexcept;

	void SetLoading(bool loading) noexcept;
	bool IsLoading() const noexcept;
	float GetSecondsFromLastLoading() const noexcept;

	void SetPaused(bool paused) noexcept;
	bool IsPaused() const noexcept;

	void SetLook(const Packet::LookData &look) noexcept;
	const Packet::LookData &GetLook() const noexcept;
	size_t GetNumLookChanges() const noexcept;

	bool IsSpawned() const noexcept;

	void SetMovement(const Packet::MovementData &movement) noexcept;
	Packet::MovementData GetMovement() const noexcept;

	void SetLocation(uint32_t location) noexcept;
	uint32_t GetLocation() const noexcept;

	void SetVirtualWorld(uint32_t world) noexcept;
	uint32_t GetVirtualWorld() const noexcept;

	void SetWaitingForNewLook(bool v) noexcept;
	bool IsWaitingForNewLook() const noexcept;

	void SetCurrentFurniture(uint32_t furniture) noexcept;
	uint32_t GetCurrentFurniture() const noexcept;

	void SetSoulSize(SoulSize s) noexcept;
	SoulSize GetSoulSize() const noexcept;

	void SetRefID(uint32_t refID) noexcept;
	uint32_t GetRefID() const noexcept;

	void PickUpObject(Object *target) noexcept;
	Object *DropItem(ItemTypeWrap itemType, uint32_t count) noexcept;
	void ChangeContainer(ItemTypeWrap itemType, uint32_t count, bool isAdd, Object *cont) noexcept;

	void TrapSoul(Player *target) noexcept;

	void UseItem(ItemTypeWrap itemType) noexcept;

	void AddCraftIngredient(ItemTypeWrap itemType, uint32_t coiunt) noexcept;
	nonstd::optional<Container::Item> CraftItem(const ItemTypeWrap &itemType) noexcept;
	nonstd::optional<Container::Item> CraftPotion(bool isPoison = false) noexcept;

	void EnchantItemUnsafe(ItemTypeWrap itemType, SpellWrap baseEnch, ItemTypeWrap soulGem) noexcept;
	nonstd::optional<ItemTypeWrap> GetLastEnchSoulGem() const noexcept;

	void InjectMovement(const Packet::MovementData &movement, bool broadcastMyMovement = true) noexcept;
	const Packet::MovementData *GetLastInjectedMovement() const noexcept;

	void InjectCastConcentration(const Packet::MovementData &movement) noexcept;

	void CastItem(ItemTypeWrap itemType, Player *reason) noexcept;

	void RunActiveEffects() noexcept;

	void InjectHitAnim(uint32_t hitAnimID) noexcept; // Legacy
	void InjectAnim(uint32_t animID, bool sendToSelf = false) noexcept;
	const uint32_t *GetLastInjectedAnim() const noexcept;
	size_t GetNumAnims() const noexcept;

	void ShowDialog(uint32_t dialogID, const std::string &title, DialogStyle style, const std::string &text, int32_t defaultIndex) noexcept;
	uint32_t GetCurrentDialogID() const noexcept;
	void ReleaseDialog() noexcept;

	void SetAVData(const std::string &avName, AVData data, const Player *reason = nullptr) noexcept;
	AVData GetAVData(const std::string &avName) const noexcept;
	size_t GetNumAVChanges() const noexcept;
	void InjectAVChange(uint8_t avID, float newPercentage) noexcept;

	void DamageHealth(float damage, Player *reason) noexcept {
		auto healthData = this->GetAVData("Health");
		healthData.DamageCurrent(damage);
		this->SetAVData("Health", healthData, reason);
	}

	bool IsDying() const noexcept;

	void EndDeath() noexcept;

	void LearnItemTypeEffect(ItemTypeWrap itemType, uint32_t effectIdx, bool learn) noexcept;

	bool IsDataSearchStarted() const noexcept;

	void InjectEquipUnequip(Container::Item item, int32_t handID, bool equip) noexcept;
	void Equip(Container::Item item, int32_t handID) noexcept;
	bool Unequip(Container::Item item, int32_t handID) noexcept;
	void InjectEquipUnequip(SpellWrap spell, int32_t handID, bool equip) noexcept;
	void Equip(SpellWrap, int32_t handID) noexcept;
	bool Unequip(SpellWrap, int32_t handID) noexcept;
	nonstd::optional<Container::Item> GetEquippedWeapon(int32_t handID) const noexcept;
	nonstd::optional<Container::Item> GetEquippedArmor(const std::string &subclass) const noexcept;
	Container GetEquippedArmor() const noexcept;
	nonstd::optional<Container::Item> GetEquippedAmmo() const noexcept;
	nonstd::optional<SpellWrap> GetEquippedSpell(int32_t handID) const noexcept;
	std::string GetEquippedObjectIdentifier(int32_t handID) const noexcept;
	bool IsEquipped(Container::Item) const noexcept;
	bool IsEquipped(ItemTypeWrap) const noexcept;
	bool IsEquipped(SpellWrap) const noexcept;
	bool IsEquipped(const std::string &equipableIdentifier) const noexcept;

	void StartDataSearch() noexcept;
	void SetDSOpcodeEnabled(DataSearch::Opcode opcode, bool enabled) noexcept;
	bool IsDSOpcodeEnabled(DataSearch::Opcode opcode) const noexcept;

	void InjectBowShot(uint32_t power) noexcept;

	void InjectSpellRelease(int32_t handID) noexcept;

	void InjectAttack(Player *target, nonstd::optional<ItemTypeWrap> weapon, nonstd::optional<SpellWrap> spell, bool isPowerHit) noexcept;

	void InjectPoisonApply() noexcept;

	void InjectLevelUp(uint8_t increasedAvID) noexcept;

	void SetSilent(bool silent) noexcept;
	void ShowRaceMenu() noexcept;
	void SendClientMessage(const std::string &message, bool isNotification) noexcept;
	void SetChatBubble(const std::string &message, uint32_t ms, bool showSelf) noexcept;
	void MoveTo(nonstd::optional<uint32_t> cellOrWorldSpace, nonstd::optional<NiPoint3> pos, nonstd::optional<uint16_t> angleZ) noexcept;
	void SetWeather(uint32_t weatherTypeOrID) noexcept;
	void SetGlovalVariable(uint32_t id, float value) noexcept;
	void SendCommand(CommandType cmdType, const std::string &cmdText) noexcept;

	void StreamIn(Object *target) noexcept;
	void StreamOut(Object *target) noexcept;
	void StreamIn(const Text3D *target) noexcept;
	void StreamOut(Text3D *target) noexcept;
	void StreamIn(Player *target) noexcept;
	void StreamOut(const Player *target) noexcept;
	void StreamOutAllObjects() noexcept;
	bool IsStreamedIn(const Object *target) const noexcept;
	bool IsStreamedIn(const Player *target) const noexcept;
	bool IsStreamedIn(const Text3D *target) const noexcept;

	bool HostStartAttempt(Object *target) noexcept;

	void UseTeleportDoor(const Object *object) noexcept;

	void EnableMovementSync(bool enable) noexcept;
	bool IsMovementSyncEnabled() const noexcept;

	void SetWerewolf(bool ww) noexcept;
	bool IsWerewolf() const noexcept;

	// NPC:
	bool IsNPC() const noexcept;
	void SetBaseNPC(uint32_t baseFormID) noexcept; // do not call in runtime
	uint32_t GetBaseNPC() const noexcept;
	void SetAggressive(bool aggressive) noexcept;
	Player *GetCombatTarget() const noexcept;
	void SetForegroundHost(Player *host) noexcept;
	void ClearCombatTarget() noexcept;

	// For Tests:
	static std::unique_ptr<Player> CreateDummy() noexcept;

	uint32_t RegisterAnimation(const std::string &aeName) noexcept;

	void UpdateRecipe(const RecipeWrap &r) noexcept {
		return this->SendRecipeInfo(r);
	}

	inline void UpdateObject(const Object *source) noexcept {
		if (source && this->IsStreamedIn(source))
		{
			this->SendObjectState(source);
			this->SendObjectName(source);
			this->SendObjectBehavior(source);
			this->SendObjectKeywords(source);
		}
	}

	static float GetStreamDistance() noexcept;

private:
	Player(RakNet::RakNetGUID guid, uint16_t id, const std::string &name, bool isNpc = false) noexcept;

	void UpdatePlayer(const Player *source) noexcept;

	PlayerInfo &GetPlayerInfo(uint16_t id) noexcept;
	void ClearPlayerInfo(uint16_t id) noexcept;

	void SendPlayerName(const Player *source, PlayerInfo &inf) noexcept;
	void SendPlayerFurniture(const Player *source, PlayerInfo &inf) noexcept;
	void SendPlayerPause(const Player *source, PlayerInfo &inf) noexcept;
	void SendPlayerLook(const Player *source, PlayerInfo &inf) noexcept;
	bool SendPlayerInventory(const Player *source, PlayerInfo &inf) noexcept;
	void SendPlayerAVs(const Player *source, PlayerInfo &inf, bool force = false) noexcept;
	void SendPlayerEquipment(const Player *source, PlayerInfo &inf) noexcept;
	bool SendPlayerSpellList(const Player *source, PlayerInfo &inf) noexcept;
	void SendPlayerMagicEquipment(const Player *source, PlayerInfo &inf) noexcept;
	void SendPlayerActiveEffects(const Player *source, PlayerInfo &inf) noexcept;
	void SendPlayerHost(const Player *source, PlayerInfo &inf) noexcept;

	void SendObjectState(const Object *source) noexcept;
	void SendObjectName(const Object *source) noexcept;
	void SendObjectBehavior(const Object *source) noexcept;
	void SendObjectKeywords(const Object *source) noexcept;
	void SendObjectRecipes() noexcept;

	void SendItemTypeInfo(const ItemTypeWrap &source) noexcept;
	void SendSpellInfo(const SpellWrap &spell) noexcept;
	void SendEffectInfo(const EffectWrap &effect) noexcept;
	bool RegisterKeyword(const Components::Keyword &keyword) noexcept;
	void SendRecipeInfo(const RecipeWrap &recipe) noexcept;

	void SendAnimationInfo(uint32_t aeID) noexcept;

	void UpdateItemTypes(const Container &itemTypesSource) noexcept;
	void UpdateSpells(const SpellList &spellsSource) noexcept;

	int32_t FindHandToEquipObject(const std::string &equipableIdentifier) const noexcept;

	void HosterUpdateFor(Player *target) noexcept;

	void ClearCraftIngredients() noexcept;

	struct Impl;
	Impl *const pImpl;

private: // Helper functions:
	void WriteNonPlayerContainer(RakNet::BitStream &bsOut, const Container &container) noexcept;
};