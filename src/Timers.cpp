#include "stdafx.h"

struct Timer
{
	std::chrono::time_point<std::chrono::system_clock> endMoment;
	std::function<void(void)> func;
};

struct Timers::Impl
{
	std::list<Timer> timers;
	std::chrono::time_point<std::chrono::system_clock> lastGarbageCollect;
	std::recursive_mutex m;
};

Timers::Timers() noexcept : pImpl(new Impl)
{
}

void Timers::SetTimer(std::chrono::time_point<std::chrono::system_clock> endMoment, std::function<void(void)> func) noexcept
{
	std::lock_guard<std::recursive_mutex> l(pImpl->m);

	pImpl->timers.push_back({ endMoment, func });
}

void Timers::Tick() noexcept
{
	std::lock_guard<std::recursive_mutex> l(pImpl->m);

	const auto now = std::chrono::system_clock::now();
	const auto end = pImpl->timers.end();
	for (auto it = pImpl->timers.begin(); it != end; ++it)
	{
		if (it->func != nullptr)
		{
			if (it->endMoment <= now)
			{
				it->func();
				it->func = nullptr;
			}
		}
	}

	//if (pImpl->lastGarbageCollect + 5s < now)
	{
		this->CollectGarbage();
		//pImpl->lastGarbageCollect = now;
	}
}

void Timers::CollectGarbage() noexcept
{
	std::lock_guard<std::recursive_mutex> l(pImpl->m);

	pImpl->timers.remove_if([](const Timer &value) {
		return value.func == nullptr;
	});
}