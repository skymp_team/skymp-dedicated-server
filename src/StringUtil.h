#pragma once

namespace StringUtil
{
	enum {
		maxSize = 4096
	};

	std::wstring Utf8ToWchar(const std::string &src) noexcept;
	std::string WcharToUtf8(const std::wstring &src) noexcept;
}