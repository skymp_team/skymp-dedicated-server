#include "stdafx.h"
#include "Hostable.h"

bool Components::Hostable::GUIDExist(RakNet::RakNetGUID guid)
{
	return g.plManager->GetPlayer(guid) != nullptr;
}