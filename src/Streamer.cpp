#include "stdafx.h"
#include "Streamer.h"

struct PlayerState
{
	NiPoint3 pos = { 0,0,0 };
	uint32_t virtualWorld = 0;
	uint32_t location = 0;
	bool isConnected = false;
};

struct ObjectState
{
	NiPoint3 pos = { 0,0,0 };
	uint32_t vw = 0;
	uint32_t loc = 0;
	bool isNative = false;
};

struct Chunk
{
	enum {
		CHUNK_SIZE = 1250,
	};

	int64_t x = 0;
	int64_t y = 0;

	Chunk()
	{
	}

	Chunk(const NiPoint3 &pos) {
		x = int64_t(pos.x / CHUNK_SIZE);
		y = int64_t(pos.y / CHUNK_SIZE);
	}

	Chunk(int64_t x, int64_t y) {
		this->x = x;
		this->y = y;
	}

	friend bool operator<(const Chunk &lhs, const Chunk &rhs) {
		return std::make_tuple(lhs.x, lhs.y) < std::make_tuple(rhs.x, rhs.y);
	}

	bool operator==(const Chunk &rhs) const {
		return x == rhs.x && y == rhs.y;
	}

	bool operator!=(const Chunk &rhs) const {
		return !(*this == rhs);
	}
};

struct Streamer::Impl
{
	// Player thread only
	std::array<PlayerState, MAX_PLAYERS> data;
	std::set<uint16_t> results[MAX_PLAYERS];
	std::set<uint16_t> sources, targets;

	// Object thread only
	std::array<PlayerState, MAX_PLAYERS> othrData;
	std::set<uint16_t> othrSources;
	std::vector<FastID> othrResults[MAX_PLAYERS];
	std::map<FastID, ObjectState> objData;
	std::map<uint32_t, std::set<FastID>> objectsInLocation;
	std::map<Chunk, std::set<FastID>> objectsInChunk;
	std::map<uint32_t, std::set<FastID>> objectsInVirtualWorld;

	// Synced
	std::vector<std::function<void()>> tasks, objTasks, tasksOut;

	std::recursive_mutex mutex;
};

Streamer::Streamer() : pImpl(new Impl)
{
	const auto streamDistance = Player::GetStreamDistance();

	// Player thread:
	std::thread([=] {
		while (true)
		{
			std::this_thread::sleep_for(1ms);
			clock_t time = clock();
			{
				std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
				for (auto task : pImpl->tasks)
					task();
				pImpl->tasks.clear();
			}
			for (auto source : pImpl->sources)
			{
				const auto &sourceData = pImpl->data[source];

				const auto resultsWas = pImpl->results[source];

				pImpl->results[source].clear();
				for (auto target : pImpl->targets)
				{
					const auto &targetData = pImpl->data[target];
					if (sourceData.location == targetData.location
						&& sourceData.virtualWorld == targetData.virtualWorld
						&& (sourceData.pos - targetData.pos).Length() < streamDistance)
					{
						pImpl->results[source].insert(target);
					}
				}


				const auto &resultsNow = pImpl->results[source];
				if (resultsWas != resultsNow)
				{
					std::vector<uint16_t> toStreamOut;
					std::set_difference(resultsWas.begin(), resultsWas.end(), resultsNow.begin(), resultsNow.end(), std::back_inserter(toStreamOut));

					std::vector<uint16_t> toStreamIn;
					std::set_difference(resultsNow.begin(), resultsNow.end(), resultsWas.begin(), resultsWas.end(), std::back_inserter(toStreamIn));

					std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
					pImpl->tasksOut.push_back([source, toStreamIn, toStreamOut] {
						auto pl = g.plManager->GetPlayer(source);
						if (pl != nullptr)
						{
							for (auto id : toStreamIn)
							{
								auto target = g.plManager->GetPlayer(id);
								if (target != nullptr)
								{
									pl->StreamIn(target);
									target->StreamIn(pl);
								}
							}
							for (auto id : toStreamOut)
							{
								auto target = g.plManager->GetPlayer(id);
								if (target != nullptr)
								{
									pl->StreamOut(target);
									target->StreamOut(pl);
								}
							}
						}
					});
				}
			}
		}
	}).detach();

	// Object thread
	std::thread([=] {
		while (true)
		{
			std::this_thread::sleep_for(1ms);
			clock_t time = clock();
			{
				std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
				for (auto task : pImpl->objTasks)
					task();
				pImpl->objTasks.clear();
			}
			for (auto source : pImpl->othrSources)
			{
				const auto &myPos = pImpl->othrData[source].pos;
				const auto myVw = pImpl->othrData[source].virtualWorld;
				const auto myLoc = pImpl->othrData[source].location;

				std::set<FastID> nei;

				const Chunk myChunk = myPos;
				for (auto x = myChunk.x - 1; x <= myChunk.x + 1; ++x)
				{
					for (auto y = myChunk.y - 1; y <= myChunk.y + 1; ++y)
					{
						const auto &chunkContent = pImpl->objectsInChunk[{x, y}];
						for (auto oid : chunkContent)
							nei.insert(oid);
					}
				}

				const auto &locationNei = pImpl->objectsInLocation[myLoc];
				const auto &virtualWorldNei = pImpl->objectsInVirtualWorld[myVw];

				std::vector<FastID> res1;
				std::set_intersection(nei.begin(), nei.end(), locationNei.begin(), locationNei.end(), std::back_inserter(res1));


				std::vector<FastID> res2;
				for (auto fid : res1)
				{
					static auto isNative = [this](const FastID &fid)->bool {
						try {
							return pImpl->objData.at(fid).isNative;
						}
						catch (...) {
							return false;
						}
					};

					if (isNative(fid) || virtualWorldNei.count(fid) != 0)
						res2.push_back(fid);
				}

				if (res2 != pImpl->othrResults[source])
				{
					const auto &resultsWas = pImpl->othrResults[source];
					const auto &resultsNow = res2;

					std::vector<FastID> toStreamOut;
					std::set_difference(resultsWas.begin(), resultsWas.end(), resultsNow.begin(), resultsNow.end(), std::back_inserter(toStreamOut));

					std::vector<FastID> toStreamIn;
					std::set_difference(resultsNow.begin(), resultsNow.end(), resultsWas.begin(), resultsWas.end(), std::back_inserter(toStreamIn));

					pImpl->othrResults[source] = res2;

					std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
					pImpl->tasksOut.push_back([source, toStreamIn, toStreamOut] {
						clock_t time = clock();
						auto pl = g.plManager->GetPlayer(source);
						if (pl != nullptr)
						{
							for (auto oid : toStreamIn)
							{
								auto target = g.objManager->LookupObject(oid);
								if (target != nullptr)
								{
									pl->StreamIn(target);
									target->StreamIn(pl);
								}
							}
							for (auto oid : toStreamOut)
							{
								auto target = g.objManager->LookupObject(oid);
								if (target != nullptr)
								{
									pl->StreamOut(target);
									target->StreamOut(pl);
								}
							}
						}
					});
				}
			}
		}
	}).detach();
}

Streamer &Streamer::GetSingleton() noexcept
{
	static Streamer s;
	return s;
}

void Streamer::Connect(uint16_t playerID, bool isTargetOnly) noexcept
{
	if (playerID >= MAX_PLAYERS)
		return;
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->tasks.push_back([=] {
		if (pImpl->data[playerID].isConnected == false)
		{
			pImpl->data[playerID].isConnected = true;
			if (isTargetOnly == false)
				pImpl->sources.insert(playerID);
			pImpl->targets.insert(playerID);
		}
	});
	pImpl->objTasks.push_back([=] {
		if (pImpl->othrData[playerID].isConnected == false)
		{
			pImpl->othrData[playerID].isConnected = true;
			if (isTargetOnly == false)
				pImpl->othrSources.insert(playerID);
		}
	});
}

void Streamer::Disconnect(uint16_t playerID) noexcept
{
	if (playerID >= MAX_PLAYERS)
		return;
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->tasks.push_back([=] {
		if (pImpl->data[playerID].isConnected == true)
		{
			pImpl->data[playerID].isConnected = false;
			pImpl->data[playerID].pos = {};
			pImpl->data[playerID].location = 0;
			pImpl->data[playerID].virtualWorld = 0;
			pImpl->sources.erase(playerID);
			pImpl->targets.erase(playerID);
		}
	});
	pImpl->objTasks.push_back([=] {
		if (pImpl->othrData[playerID].isConnected == true)
		{
			pImpl->othrData[playerID].isConnected = false;
			pImpl->othrData[playerID].pos = {};
			pImpl->othrData[playerID].location = 0;
			pImpl->othrData[playerID].virtualWorld = 0;
			pImpl->othrSources.erase(playerID);
		}
	});
}

void Streamer::SetPos(uint16_t playerID, NiPoint3 pos) noexcept
{
	if (playerID >= MAX_PLAYERS)
		return;
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->tasks.push_back([=] {
		pImpl->data[playerID].pos = pos;
	});
	pImpl->objTasks.push_back([=] {
		pImpl->othrData[playerID].pos = pos;
	});
}

void Streamer::SetVirtualWorld(uint16_t playerID, uint32_t vw) noexcept
{
	if (playerID >= MAX_PLAYERS)
		return;
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->tasks.push_back([=] {
		pImpl->data[playerID].virtualWorld = vw;
	});
	pImpl->objTasks.push_back([=] {
		pImpl->othrData[playerID].virtualWorld = vw;
	});
}

void Streamer::SetLocation(uint16_t playerID, uint32_t loc) noexcept
{
	if (playerID >= MAX_PLAYERS)
		return;
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->tasks.push_back([=] {
		pImpl->data[playerID].location = loc;
	});
	pImpl->objTasks.push_back([=] {
		pImpl->othrData[playerID].location = loc;
	});
}

void Streamer::ConnectObject(const FastID &fid, uint32_t objectID) noexcept
{
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->objTasks.push_back([=] {
		ObjectState state;
		state.isNative = objectID < 0xFF000000;
		pImpl->objData[fid] = state;
	});
}

void Streamer::DisconnectObject(const FastID &fid) noexcept
{
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->objTasks.push_back([=] {
		pImpl->objData.erase(fid);
	});
}

void Streamer::SetObjectPos(const FastID &fid, NiPoint3 pos) noexcept
{
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->objTasks.push_back([=] {
		try {
			auto &data = pImpl->objData.at(fid);

			pImpl->objectsInChunk[data.pos].erase(fid);
			data.pos = pos;
			pImpl->objectsInChunk[data.pos].insert(fid);
		}
		catch (...) {
			log(Log::Error, "Streamer::SetObjectPos object not found");
		}
	});
}

void Streamer::SetObjectVirtualWorld(const FastID &fid, uint32_t vw) noexcept
{
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->objTasks.push_back([=] {
		try {
			auto &data = pImpl->objData.at(fid);

			pImpl->objectsInVirtualWorld[data.vw].erase(fid);
			data.vw = vw;
			pImpl->objectsInVirtualWorld[data.vw].insert(fid);
		}
		catch (...) {
			log(Log::Error, "Streamer::SetObjectVirtualWorld object not found");
		}
	});
}

void Streamer::SetObjectLocation(const FastID &fid, uint32_t loc) noexcept
{
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->objTasks.push_back([=] {
		try {
			auto &data = pImpl->objData.at(fid);

			pImpl->objectsInLocation[data.loc].erase(fid);
			data.loc = loc;
			pImpl->objectsInLocation[data.loc].insert(fid);
		}
		catch (...) {
			log(Log::Error, "Streamer::SetObjectLocation object not found");
		}
	});
}

void Streamer::Step() noexcept
{
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	if (!pImpl->tasksOut.empty())
	{
		for (auto task : pImpl->tasksOut)
			task();
		pImpl->tasksOut.clear();
	}
}