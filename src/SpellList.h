#pragma once
#include "SpellWrap.h"

namespace Components
{
	class SpellList
	{
	public:
		using Spells = std::set<SpellWrap>;

		void AddSpell(SpellWrap s) noexcept {
			++numChanges;
			this->spells.insert(s);
		}

		void RemoveSpell(SpellWrap s) noexcept {
			++numChanges;
			this->spells.erase(s);
		}

		void RemoveAllSpells() noexcept {
			++numChanges;
			this->spells = {};
		}

		bool HasSpell(SpellWrap s) const noexcept {
			return this->spells.find(s) != this->spells.end();
		}

		const auto &GetSpells() const noexcept {
			return this->spells;
		}

		uint32_t GetNumSpells() const noexcept {
			return uint32_t(this->GetSpells().size());
		}

		friend bool operator==(const SpellList &lhs, const SpellList &rhs) {
			return lhs.GetSpells() == rhs.GetSpells();
		}

		friend bool operator!=(const SpellList &lhs, const SpellList &rhs) {
			return !(lhs == rhs);
		}

		size_t GetNumChanges() const noexcept {
			return this->numChanges;
		}

	private:
		size_t numChanges = 0;
		Spells spells;
	};
}