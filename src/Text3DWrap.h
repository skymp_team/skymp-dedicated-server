#pragma once

class Text3DWrap
{
public:
	explicit Text3DWrap(uint32_t id) noexcept;
	~Text3DWrap() noexcept;

	bool operator==(const Text3DWrap &rhs) const noexcept;
	bool operator!=(const Text3DWrap &rhs) const noexcept {
		return !(*this == rhs);
	}

	uint32_t GetID() const noexcept;
	std::string GetText() const noexcept;
	float GetX() const noexcept;
	float GetY() const noexcept;
	float GetZ() const noexcept;
	uint32_t GetVirtualWorld() const noexcept;
	luabridge::LuaRef GetLocation() const noexcept;
	bool IsDeleted() const noexcept;

	void SetText(std::string str) noexcept;
	void SetPos(float x, float y, float z) noexcept;
	void SetVirtualWorld(uint32_t w) noexcept;
	void SetLocation(luabridge::LuaRef loc) noexcept;
	void Destroy() noexcept;

	static luabridge::LuaRef Create(std::string text, luabridge::LuaRef location, float x, float y, float z, uint32_t virtualWorldID) noexcept;
	static luabridge::LuaRef LookupByID(uint32_t id) noexcept;

	static void Register(lua_State *state);

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;

	friend bool operator <(const Text3DWrap &lhs, const Text3DWrap &rhs) {
		return lhs.GetID() < rhs.GetID();
	}
};