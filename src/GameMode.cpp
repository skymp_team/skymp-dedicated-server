#include "stdafx.h"
#include "GameMode.h"

#include "PlayerWrap.h"
#include "LocationWrap.h"
#include "RaceWrap.h"
#include "ObjectWrap.h"
#include "ItemTypeWrap.h"
#include "EffectWrap.h"
#include "SpellWrap.h"
#include "NavMeshWrap.h"
#include "Text3DWrap.h"
#include "RecipeWrap.h"
#include "PerkWrap.h"

struct GameMode::Impl
{
	std::string file;
	lua_State* state = nullptr;
	uint32_t overloadMult = 1;
};

GameMode *pThis = nullptr;

GameMode::GameMode(const std::string &file) : pImpl(new Impl)
{
	if (pThis != nullptr)
		throw std::logic_error("unable to create more than one GameMode");
	pThis = this;

	std::string appPath = current_directory(); 

	setenv("LUA_PATH", (appPath + "/gamemodes/?.lua" + ";"
		+ appPath + "/gamemodes/?.t" + ";"
		+ appPath + "/lib/?.lua" + ";"
		+ appPath + "/lib/?.t").data(), true);

#ifdef _WIN32
	setenv("LUA_CPATH", (appPath + "/lib/?.dll").data(), true);
#else
	setenv("LUA_CPATH", (appPath + "/lib/?.so").data(), true);
#endif

	pImpl->file = file;
	pImpl->state = luaL_newstate();

	const auto L = pImpl->state;

	luaL_openlibs(L);

	lua_pcall(L, 0, 0, 0);

	luabridge::getGlobalNamespace(L)
		.addFunction("print", print)
		.addFunction("SendChatMessageToAll", SendChatMessageToAll)
		.addFunction("GetMaxPlayers", GetMaxPlayers)
		.addFunction("SetTimer", SetTimer)
		.addFunction("GetCurrentDirectory", GetCurrentDirectory)
		.addFunction("GetNthFileInDirectory", GetNthFileInDirectory)
		.addFunction("GetVersion", GetVersion)
		.addFunction("GetTickCount", GetTickCount)
		.addFunction("Terminate", Terminate)
		.addFunction("GetPointerSize", GetPointerSize)
		.addFunction("SetOverloadMult", SetOverloadMult)
		.addFunction("GetOverloadMult", GetOverloadMult)
		;

	const auto dofileSuccess = !luaL_dofile(L, pImpl->file.data());
	if (!dofileSuccess)
		throw std::logic_error(lua_tostring(L, -1));

	PlayerWrap::Register(L);
	LocationWrap::Register(L);
	RaceWrap::Register(L);
	ObjectWrap::Register(L);
	ItemTypeWrap::Register(L);
	EffectWrap::Register(L);
	SpellWrap::Register(L);
	NavMeshWrap::Register(L);
	Text3DWrap::Register(L);
	RecipeWrap::Register(L);
	PerkWrap::Register(L); // 1.0
}

GameMode::~GameMode() noexcept
{
	delete pImpl;
}

void GameMode::print(const std::string &text) noexcept
{
	log(Log::None, text.data());
}

void GameMode::SendChatMessageToAll(const std::string &message_) noexcept
{
	if (g.plManager == nullptr)
		return;
	try {
		auto message = message_;
		if (message == " ")
			message = "   ";
		auto &players = g.plManager->GetPlayers();
		std::for_each(players.begin(), players.end(), [&](auto pair) {
			auto player = pair.second;
			if (player != nullptr)
				player->SendClientMessage(message, false);
		});

	}
	catch (...) {
	}
}

uint16_t GameMode::GetMaxPlayers() noexcept
{
	return g.cfg->GetMaxPlayers();
}

void GameMode::SetTimer(uint32_t time, luabridge::LuaRef callback, luabridge::LuaRef argumentsTable) noexcept
{
	g.timers->SetTimer(std::chrono::system_clock::now() + time * 1ms, [=] {
		try {
			callback(argumentsTable);
		}
		catch (const std::exception &e) {
			log(Log::Error, e.what());
		}
	});
}

std::string GameMode::GetCurrentDirectory() noexcept
{
	static const auto dir = current_directory();
	return dir;
}

std::string GameMode::GetNthFileInDirectory(std::string dir, uint32_t n) noexcept
{
	std::vector<std::string> files;
	read_directory(dir, files);
	try {
		return files.at(n - 1);
	}
	catch (...) {
		return "";
	}
}

std::string GameMode::GetVersion() noexcept
{
	return g.version;
}

uint32_t GameMode::GetTickCount() noexcept
{
	return (uint32_t)((double)clock() / (double)CLOCKS_PER_SEC * 1000.0);
}

void GameMode::Terminate() noexcept
{
	std::terminate();
}

uint32_t GameMode::GetPointerSize() noexcept
{
	return (int32_t)sizeof(void *);
}

uint32_t GameMode::GetOverloadMult() noexcept
{
	return pThis->pImpl->overloadMult;
}

void GameMode::SetOverloadMult(uint32_t v) noexcept
{
	pThis->pImpl->overloadMult = v;
}

void GameMode::OnServerInit() noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnServerInit"].isNil())
		{
			log(Log::Error, "OnServerInit callback not found");
			return;
		}
		_G["OnServerInit"]();
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnServerExit() noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnServerExit"].isNil())
			return;
		_G["OnServerExit"]();
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerConnect(uint16_t id) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerConnect"].isNil())
			return;
		_G["OnPlayerConnect"](PlayerWrap(id));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerDisconnect(uint16_t id) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerDisconnect"].isNil())
			return;
		_G["OnPlayerDisconnect"](PlayerWrap(id));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerSpawn(uint16_t id) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerSpawn"].isNil())
			return;
		_G["OnPlayerSpawn"](PlayerWrap(id));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

bool GameMode::OnPlayerUpdate(uint16_t id) noexcept
{
	return true;
}

void GameMode::OnPlayerDying(uint16_t id, nonstd::optional<uint16_t> killerID) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerDying"].isNil())
			return;
		if (killerID != nonstd::nullopt)
			_G["OnPlayerDying"](PlayerWrap(id), PlayerWrap(*killerID));
		else
			_G["OnPlayerDying"](PlayerWrap(id));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerDeath(uint16_t id, nonstd::optional<uint16_t> killerID) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerDeath"].isNil())
			return;
		if (killerID != nonstd::nullopt)
			_G["OnPlayerDeath"](PlayerWrap(id), PlayerWrap(*killerID));
		else
			_G["OnPlayerDeath"](PlayerWrap(id));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerChatInput(uint16_t id, const std::string &text) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerChatInput"].isNil())
			return;
		_G["OnPlayerChatInput"](PlayerWrap(id), text);
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerDialogResponse(uint16_t id, uint32_t dialogID, const std::string &inputText, int32_t listItem) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerDialogResponse"].isNil())
			return;
		_G["OnPlayerDialogResponse"](PlayerWrap(id), dialogID, inputText, listItem);
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerCharacterCreated(uint16_t id) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerCharacterCreated"].isNil())
			return;
		_G["OnPlayerCharacterCreated"](PlayerWrap(id));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

bool GameMode::OnPlayerDropObject(uint16_t id, uint32_t objectID) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerDropObject"].isNil())
			return true;
		auto result = _G["OnPlayerDropObject"](PlayerWrap(id), ObjectWrap(objectID));
		return !result.isNil() && result.cast<bool>();
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
		return false;
	}
}

bool GameMode::OnPlayerActivateObject(uint16_t id, uint32_t objectID) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerActivateObject"].isNil())
			return true;
		auto result = _G["OnPlayerActivateObject"](PlayerWrap(id), ObjectWrap(objectID));
		return !result.isNil() && result.cast<bool>();
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
		return false;
	}
}

void GameMode::OnPlayerActivatePlayer(uint16_t id, uint16_t targetID) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerActivatePlayer"].isNil())
			return;
		_G["OnPlayerActivatePlayer"](PlayerWrap(id), PlayerWrap(targetID));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
		return;
	}
}

void GameMode::OnPlayerHitObject(uint16_t id, uint32_t objectID, std::string weaponIdentifier, std::string ammoIdentifier, std::string spellIdentifier) noexcept
{
	nonstd::optional<ItemTypeWrap> itemType;
	try {
		itemType = ItemTypeWrap(weaponIdentifier);
	}
	catch (...) {
	}

	nonstd::optional<SpellWrap> spell;
	try {
		spell = SpellWrap(spellIdentifier);
	}
	catch (...) {
	}

	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerHitObject"].isNil())
			return;
		if (itemType != nonstd::nullopt)
		{
			nonstd::optional<ItemTypeWrap> ammoItemType;
			try {
				ammoItemType = ItemTypeWrap(ammoIdentifier);
			}
			catch (...) {
			}
			if (ammoItemType != nonstd::nullopt)
				_G["OnPlayerHitObject"](PlayerWrap(id), ObjectWrap(objectID), *itemType, *ammoItemType);
			else
				_G["OnPlayerHitObject"](PlayerWrap(id), ObjectWrap(objectID), *itemType);
		}
		else if (spell != nonstd::nullopt)
			_G["OnPlayerHitObject"](PlayerWrap(id), ObjectWrap(objectID), luabridge::Nil(), luabridge::Nil(), *spell);
		else
			_G["OnPlayerHitObject"](PlayerWrap(id), ObjectWrap(objectID));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

bool GameMode::OnPlayerHitPlayer(uint16_t id, uint16_t targetID, std::string weaponIdentifier, std::string ammoIdentifier, std::string spellIdentifier) noexcept
{
	nonstd::optional<ItemTypeWrap> itemType;
	try {
		itemType = ItemTypeWrap(weaponIdentifier);
	}
	catch (...) {
	}

	nonstd::optional<SpellWrap> spell;
	try {
		spell = SpellWrap(spellIdentifier);
	}
	catch (...) {
	}

	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerHitPlayer"].isNil())
			return true;
		if (itemType != nonstd::nullopt)
		{
			nonstd::optional<ItemTypeWrap> ammoItemType;
			try {
				ammoItemType = ItemTypeWrap(ammoIdentifier);
			}
			catch (...) {
			}
			if (ammoItemType != nonstd::nullopt)
				return _G["OnPlayerHitPlayer"](PlayerWrap(id), PlayerWrap(targetID), *itemType, *ammoItemType).cast<bool>();
			else
				return _G["OnPlayerHitPlayer"](PlayerWrap(id), PlayerWrap(targetID), *itemType).cast<bool>();
		}
		else if (spell != nonstd::nullopt)
			return _G["OnPlayerHitPlayer"](PlayerWrap(id), PlayerWrap(targetID), luabridge::Nil(), luabridge::Nil(), *spell).cast<bool>();
		else 
			return _G["OnPlayerHitPlayer"](PlayerWrap(id), PlayerWrap(targetID)).cast<bool>();
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
	return true;
}

bool GameMode::OnPlayerLearnEffect(uint16_t id, std::string ingrIdent, uint32_t n) noexcept
{
	nonstd::optional<ItemTypeWrap> itemType;
	try {
		itemType = ItemTypeWrap(ingrIdent);
	}
	catch (...) {
	}

	try {
		if (itemType != nonstd::nullopt)
		{
			auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
			if (_G["OnPlayerLearnEffect"].isNil())
				return true;
			return _G["OnPlayerLearnEffect"](PlayerWrap(id), *itemType, n).cast<bool>();
		}
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
	return true;
}

void GameMode::OnPlayerDataSearchResult(uint16_t id, std::string type, luabridge::LuaRef res) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerDataSearchResult"].isNil() == false)
			_G["OnPlayerDataSearchResult"](PlayerWrap(id), type, res);
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerLearnPerk(uint16_t id, uint32_t perkID) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		auto perkWrap = PerkWrap(perkID);
		if (perkWrap.IsPlayable() == false)
			throw std::logic_error("attempt to learn non-playable perk");
		if (_G["OnPlayerLearnPerk"].isNil() == false)
			_G["OnPlayerLearnPerk"](PlayerWrap(id), perkWrap);
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerEatItem(uint16_t id, std::string itemTypeIdent) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerEatItem"].isNil() == false)
			_G["OnPlayerEatItem"](PlayerWrap(id), ItemTypeWrap(itemTypeIdent));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerUseItem(uint16_t id, std::string itemTypeIdent) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerUseItem"].isNil() == false)
			_G["OnPlayerUseItem"](PlayerWrap(id), ItemTypeWrap(itemTypeIdent));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerHostPlayer(uint16_t hosterID, uint16_t targetID) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerHostPlayer"].isNil() == false)
			_G["OnPlayerHostPlayer"](PlayerWrap(hosterID), PlayerWrap(targetID));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerJump(uint16_t id) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerJump"].isNil())
			return;
		_G["OnPlayerJump"](PlayerWrap(id));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerFall(uint16_t id) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerFall"].isNil())
			return;
		_G["OnPlayerFall"](PlayerWrap(id));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerOnTheGround(uint16_t id) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerOnTheGround"].isNil())
			return;
		_G["OnPlayerOnTheGround"](PlayerWrap(id));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerStreamInPlayer(uint16_t id, uint16_t otherId) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerStreamInPlayer"].isNil() == false)
			_G["OnPlayerStreamInPlayer"](PlayerWrap(id), PlayerWrap(otherId));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerStreamOutPlayer(uint16_t id, uint16_t otherId) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerStreamOutPlayer"].isNil() == false)
			_G["OnPlayerStreamOutPlayer"](PlayerWrap(id), PlayerWrap(otherId));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerStreamInObject(uint16_t id, uint32_t otherId) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerStreamInObject"].isNil() == false)
			_G["OnPlayerStreamInObject"](PlayerWrap(id), ObjectWrap(otherId));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerStreamOutObject(uint16_t id, uint32_t otherId) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerStreamOutObject"].isNil() == false)
			_G["OnPlayerStreamOutObject"](PlayerWrap(id), ObjectWrap(otherId));
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerBowShot(uint16_t id, float power) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerBowShot"].isNil() == false)
			_G["OnPlayerBowShot"](PlayerWrap(id), power);
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerChangeContainer(uint16_t id, uint32_t objectID, std::string itemTypeIdentifier, uint32_t count, bool isAdd) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerChangeContainer"].isNil() == false)
			_G["OnPlayerChangeContainer"](PlayerWrap(id), ObjectWrap(objectID), ItemTypeWrap(itemTypeIdentifier), count, isAdd);
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

void GameMode::OnPlayerCreateItem(uint16_t id, std::string itemTypeIdentifier, uint32_t count) noexcept
{
	try {
		auto _G = luabridge::getGlobal(this->pImpl->state, "_G");
		if (_G["OnPlayerCreateItem"].isNil() == false)
			_G["OnPlayerCreateItem"](PlayerWrap(id), ItemTypeWrap(itemTypeIdentifier), count);
	}
	catch (const std::exception &e) {
		log(Log::Error, e.what());
	}
}

lua_State *GameMode::GetState() const noexcept
{
	return pImpl->state;
}