#include "stdafx.h"
#include "Text3D.h"

struct Text3DManager::Impl
{
	Text3Ds text3Ds;
};

struct Text3D::Impl
{
	uint32_t id;
	std::string text;
	uint32_t locationID;
	NiPoint3 pos;
	uint32_t virtualWorldID;
};

Text3DManager::Text3DManager() noexcept : pImpl(new Impl) 
{
}

Text3DManager::~Text3DManager() noexcept
{
	delete pImpl;
}

Text3D *Text3DManager::NewText3D(std::string text, uint32_t locationID, NiPoint3 pos, uint32_t virtualWorldID) noexcept
{
	const std::shared_ptr<Text3D> text3D(new Text3D(text, locationID, pos, virtualWorldID));
	const auto id = this->GenerateID();
	pImpl->text3Ds[id] = text3D;
	text3D->pImpl->id = id;
	return text3D.get();
}

void Text3DManager::DeleteText3D(uint32_t id) noexcept
{
	auto text3D = this->LookupText3D(id);
	if (text3D == nullptr)
		return;

	const auto &players = g.plManager->GetPlayers();
	for (auto it = players.begin(); it != players.end(); ++it)
	{
		auto &player = it->second;
		if (player != nullptr)
			player->StreamOut(text3D);
	}

	pImpl->text3Ds.erase(id);
}

Text3D *Text3DManager::LookupText3D(uint32_t id) const noexcept
{
	try {
		return pImpl->text3Ds.at(id).get();
	}
	catch (...) {
		return nullptr;
	}
}

const Text3Ds &Text3DManager::GetText3Ds() const noexcept
{
	return pImpl->text3Ds;
}

uint32_t Text3DManager::GenerateID() const noexcept
{
	static uint32_t id = 0;
	return id++;
}

Text3D::Text3D(std::string text, uint32_t locationID, NiPoint3 pos, uint32_t virtualWorldID) noexcept : pImpl(new Impl)
{
	pImpl->text = text;
	pImpl->locationID = locationID;
	pImpl->pos = pos;
	pImpl->virtualWorldID = virtualWorldID;
	pImpl->id = 0;
}

Text3D::~Text3D()
{
	delete pImpl;
}

uint32_t Text3D::GetID() const noexcept
{
	return pImpl->id;
}

std::string Text3D::GetText() const noexcept
{
	return pImpl->text;
}

uint32_t Text3D::GetLocationID() const noexcept
{
	return pImpl->locationID;
}

NiPoint3 Text3D::GetPos() const noexcept
{
	return pImpl->pos;
}

uint32_t Text3D::GetVirtualWorld() const noexcept
{
	return pImpl->virtualWorldID;
}

void Text3D::SetText(std::string str) noexcept
{
	pImpl->text = str;
}

void Text3D::SetLocation(uint32_t locID) noexcept
{
	pImpl->locationID = locID;
}

void Text3D::SetPos(NiPoint3 pos) noexcept
{
	pImpl->pos = pos;
}

void Text3D::SetVirtualWorld(uint32_t w) noexcept
{
	pImpl->virtualWorldID = w;
}