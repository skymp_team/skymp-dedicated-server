#include "stdafx.h"

std::string GetFileNameByPath(const std::string &path)
{
	auto tmp = path;
	size_t pos = 0;
	for (size_t i = 0; i != tmp.size(); ++i)
	{
		if (tmp[i] == '\\')
			pos = i;
	}
	++pos;
	tmp = std::string(tmp.begin() + pos, tmp.end());
	return tmp;
}

#if defined _WIN32 || _WIN64
int setenv(const char *name, const char *value, int overwrite)
{
	int errcode = 0;
	if (!overwrite) {
		size_t envsize = 0;
		errcode = getenv_s(&envsize, NULL, 0, name);
		if (errcode || envsize) return errcode;
	}
	return _putenv_s(name, value);
}
#endif

#if defined _WIN32 || _WIN64
#include <Winbase.h>
std::string current_directory()
{
	char pBuf[512];
	int len = 512;
	int bytes = GetModuleFileNameA(NULL, pBuf, len);
	if (bytes)
	{
		pBuf[bytes - (sizeof "skymp_server.exe")] = 0;
		return pBuf;
	}
	else
		return "";
}
#else
std::string current_directory()
{
	char pBuf[512];
	int len = 512;
	char szTmp[32];
	sprintf(szTmp, "/proc/%d/exe", getpid());
	int bytes = readlink(szTmp, pBuf, len);
	if (len - 1 < bytes)
		bytes = len - 1;
	if (bytes >= 0)
	{
		pBuf[bytes - (sizeof "skymp_server")] = '\0';
		return pBuf;
	}
	return "";
}
#endif

#if defined _WIN32 || _WIN64
#include <windows.h>
void read_directory(const std::string& name, std::vector<std::string>& v)
{
	std::string pattern = name;
	pattern.append("\\*");
	WIN32_FIND_DATA data;
	HANDLE hFind;
	if ((hFind = FindFirstFileA(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
		do {
			v.push_back(data.cFileName);
		} while (FindNextFile(hFind, &data) != 0);
		FindClose(hFind);
	}
}
#else
#include <sys/types.h>
#include <dirent.h>
void read_directory(const std::string& name, std::vector<std::string>& v)
{
	DIR* dirp = opendir(name.c_str());
	struct dirent * dp;
	while ((dp = readdir(dirp)) != NULL) {
		v.push_back(dp->d_name);
	}
	closedir(dirp);
}
#endif