#pragma once

class Location : noncopyable
{
public:
	Location(uint32_t formID) noexcept;
	~Location() noexcept;

	uint32_t GetFormID() const noexcept;

private:
	struct Impl;
	Impl *const pImpl;
};