#pragma once

class EffectWrap
{
public:
	explicit EffectWrap(std::string identifier);
	~EffectWrap() noexcept;

	bool operator==(const EffectWrap &rhs) const noexcept;
	bool operator!=(const EffectWrap &rhs) const noexcept {
		return !(*this == rhs);
	}
	std::string GetIdentifier() const noexcept;
	std::string GetArchetype() const noexcept;
	std::string GetCastingType() const noexcept;
	std::string GetDelivery() const noexcept;
	uint8_t GetArchetypeID() const noexcept;
	uint8_t GetCastingTypeID() const noexcept;
	uint8_t GetDeliveryID() const noexcept;
	uint32_t GetExistingGameFormID() const noexcept;
	uint32_t GetBaseID() const noexcept { return this->GetExistingGameFormID(); }; // 1.0.14
	std::string GetActorValue(uint32_t n) const noexcept;

	void SetActorValues(std::string av1, std::string av2) noexcept;

	static luabridge::LuaRef Create(std::string identifier, std::string archetype, uint32_t existingEffectID, std::string castingType, std::string deliveryType) noexcept;
	static luabridge::LuaRef LookupByIdentifier(std::string identifier) noexcept;

	static void Register(lua_State *state);

	// Non-API Functions:

	enum class StepResult
	{
		Default,
		ActiveEffectsChanged
	};
	static StepResult Step(const EffectWrap &effect, uint16_t playerID, float magnitude, double thisStepDurationMs, uint16_t casterID);

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;

	friend bool operator <(const EffectWrap &lhs, const EffectWrap &rhs) {
		return lhs.GetIdentifier() < rhs.GetIdentifier();
	}
};

struct EffectInfo
{
	uint8_t archetype;
	uint8_t castingType;
	uint8_t delivery;
	uint32_t existingGameFormID;

	friend bool operator==(const EffectInfo &lhs, const EffectInfo &rhs) noexcept {
		return lhs.archetype == rhs.archetype && lhs.castingType == rhs.castingType && lhs.delivery == rhs.delivery && lhs.existingGameFormID == rhs.existingGameFormID;
	}

	friend bool operator!=(const EffectInfo &lhs, const EffectInfo &rhs) noexcept {
		return !(lhs == rhs);
	}

	static EffectInfo Get(const EffectWrap &effect)
	{
		return {
			effect.GetArchetypeID(),
			effect.GetCastingTypeID(),
			effect.GetDeliveryID(),
			effect.GetExistingGameFormID()
		};
	}
};