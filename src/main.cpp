#include "stdafx.h"

GodObject g;

GameMode *LoadGameMode()
{
	const auto gamemodeFile = "gamemodes/" + g.cfg->GetValue<std::string>("General.gamemode", "");
	std::ifstream f(gamemodeFile);
	if (!f.good())
	{
		log(Log::Fatal, "incorrect gamemode name");
		return nullptr;
	}

	log(Log::Info, "loading gamemode script ", gamemodeFile.data());
	try {
		return new GameMode(gamemodeFile);
	}
	catch (const std::exception &e) {
		printfUnix("<SkympUnix> %s\n", e.what());
		log(Log::Fatal, e.what());
		return nullptr;
	}
}

int32_t RunServer() noexcept
{
	g.version = "1.0.37";

	printfUnix("<SkympUnix> Constructing g.cfg\n");
	g.cfg = std::make_unique<ServerCfg>("server.cfg");

	try {
		printfUnix("<SkympUnix> Constructing g.log\n");
		const auto logFile = g.cfg->GetValue<std::string>("General.logFile", "");
		const auto logLevel = (Log::LogLevel)g.cfg->GetValue<int32_t>("General.logLevel", Log::Info);
		if (logLevel != Log::Info && (int32_t)logLevel != 0)
		{
			log(Log::Warning, "General.logLevel was removed in 0.18");
		}
	}
	catch (const std::exception &e) {
		printfUnix("<SkympUnix> Fatal error: %s\n", e.what());
		printfUnix("<SkympUnix> Unable to construct g.log");
		return -1;
	}
	log(Log::None, "SkyMP ", g.version.data(), " Server");
	try {
		std::locale::global(std::locale(g.cfg->GetValue<std::string>("General.locale", "")));
	}
	catch (const std::exception &) {
		printfUnix("<SkympUnix> Unable to set locale\n");
	}

	printfUnix("<SkympUnix> Constructing g.tests\n");
	g.tests = std::make_unique<Tests>();
	if (g.cfg->GetValue<std::string>("General.runTests", "false") == "true")
	{
		log(Log::Info, "running tests");
		g.tests->RunAll();
	}

	const auto maxPlayers = g.cfg->GetMaxPlayers();
	if (maxPlayers == 0)
	{
		log(Log::Fatal, "maxPlayers equals 0");
		return -1;
	}
	if (maxPlayers > MAX_PLAYERS)
	{
		log(Log::Fatal, "maxPlayers must not be greater than ", MAX_PLAYERS);
		return -1;
	}

	const auto password = g.cfg->GetValue<std::string>("General.password", "");
	if (password.size() > constant.maxServerPassword)
	{
		log(Log::Fatal, "the password is ", constant.maxServerPassword, " characters max");
		return -1;
	}

	auto peer = RakNet::RakPeerInterface::GetInstance();
	std::ostringstream s;
	s << g.version;
	peer->SetIncomingPassword(s.str().data(), (int32_t)s.str().size());

	const auto localAddress = g.cfg->GetValue<std::string>("General.localAddress", "");
	if (RakNet::NonNumericHostString(localAddress.data()))
	{
		log(Log::Fatal, "non-numeric local address");
		return -1;
	}

	const auto port = g.cfg->GetValue<uint16_t>("General.port", 0);
	if (port == 0)
	{
		log(Log::Fatal, "incorrect port");
		return -1;
	}

	printfUnix("<SkympUnix> Constructing g.pathing\n");
	g.pathing = std::make_unique<Pathing>();

	RakNet::SocketDescriptor socket(port, localAddress.data());

	try {
		switch (peer->Startup(maxPlayers, &socket, 1))
		{
		case RakNet::RAKNET_STARTED:
			break;
		case RakNet::RAKNET_ALREADY_STARTED:
			throw std::runtime_error("already started");
		case RakNet::INVALID_SOCKET_DESCRIPTORS:
			throw std::runtime_error("incorrect port or address");
		case RakNet::SOCKET_FAILED_TO_BIND:
		case RakNet::SOCKET_PORT_ALREADY_IN_USE:
		case RakNet::PORT_CANNOT_BE_ZERO:
			throw std::runtime_error("failed to bind port");
		default:
			throw std::runtime_error("can't start server");
		}

	}
	catch (const std::exception &e) {
		log(Log::Fatal, e.what());
		return -1;
	}

	peer->SetMaximumIncomingConnections(maxPlayers);

	printfUnix("<SkympUnix> Constructing g.plManager\n");
	g.plManager = std::make_unique<PlayerManager>();
	printfUnix("<SkympUnix> Constructing g.objManager\n");
	g.objManager = std::make_unique<ObjectManager>();
	printfUnix("<SkympUnix> Constructing g.timers\n");
	g.timers = std::make_unique<Timers>();
	printfUnix("<SkympUnix> Constructing g.networking\n");
	g.networking = std::make_unique<Networking>(peer);
	printfUnix("<SkympUnix> Constructing g.t3dManager\n");
	g.t3dManager = std::make_unique<Text3DManager>();

	printfUnix("<SkympUnix> Loading gamemode\n");
	GameMode *gm = LoadGameMode();
	if (gm == nullptr)
	{
		printfUnix("<SkympUnix> Failed\n");
		return -1;
	}
	g.gamemode.reset(gm);

	log(Log::Info, "started");

	printfUnix("<SkympUnix> All ok. Starting main loop\n");
	const auto code = g.networking->RunMainLoop();
	if (code == 0)
		log(Log::Info, "quitting peacefully");

	return code;
}

int main(int c, char *v[])
{
	//ProfilerStart("dump.txt");

	printfUnix("<SkympUnix> Preparing to start the server under Unix (%d bit)\n", (int32_t)sizeof(void *) * 8);
	try {
		std::locale::global(std::locale(""));
	}
	catch (const std::exception &e) {
		printfUnix("<SkympUnix> An exception was thrown. Don't worry. It's ok. Error text: %s\n", e.what());
	}
	const auto res = RunServer();

	return res;
}
