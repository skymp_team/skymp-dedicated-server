#pragma once
#include "ItemTypeWrap.h"

namespace Components
{
	class Container
	{
	public:
		struct Item
		{
			ItemTypeWrap itemType;
			// enchantment, poison, etc...

			class Comparator
			{
			public:
				bool operator() (const Item &lhs, const Item &rhs) const {
					return lhs.itemType < rhs.itemType;
				}
			};

			friend bool operator==(const Item &lhs, const Item &rhs) {
				return lhs.itemType.GetIdentifier() == rhs.itemType.GetIdentifier();
			}
		};

		using Items = std::map<Item, uint32_t, Item::Comparator>;

		Container GetInventoryDifference(const Container &rhs) const noexcept
		{
			Items result;
			for (auto it = this->items.begin(); it != this->items.end(); ++it)
			{
				Item item = it->first;
				const auto &count = it->second;

				uint32_t rhsCount = 0;
				try {
					rhsCount = rhs.items.at(item);
				}
				catch (...) {
				}
				if (count > rhsCount)
					result.insert({ item, count - rhsCount });
			}
			return Container(result);
		}

		Container() noexcept
		{
		}

		explicit Container(const Items &items) noexcept : items(items)
		{
		}

		virtual ~Container() = default;

		void AddItem(const ItemTypeWrap &itemType, uint32_t count) noexcept
		{
			this->numInventoryChanges++;
			if (count > 0)
				items[{itemType}] += count;
		}

		void AddItem(const Item &item, uint32_t count) noexcept
		{
			this->numInventoryChanges++;
			if (count > 0)
				items[item] += count;
		}

		void RemoveAllItems(Container *otherContainer = nullptr) noexcept
		{
			this->numInventoryChanges++;
			if (otherContainer)
			{
				for (auto it = this->items.begin(); it != this->items.end(); ++it)
					otherContainer->AddItem(it->first.itemType, it->second);
			}
			this->items.clear();
		}

		bool RemoveItem(const ItemTypeWrap &itemType, uint32_t count) noexcept
		{
			return this->RemoveItem(Item{ itemType }, count);
		}

		bool RemoveItem(const Item &item, uint32_t count) noexcept
		{
			this->numInventoryChanges++;
			if (count > 0)
			{
				uint32_t countWas;
				try {
					countWas = items.at(item);
				}
				catch (...) {
					countWas = 0;
				}

				if (countWas == count)
				{
					items.erase(item);
					return true;
				}
				else if (countWas > count)
				{
					items[item] -= count;
					return true;
				}
				return false;
			}
			return true;
		}

		uint32_t GetItemCount(ItemTypeWrap itemType) const noexcept
		{
			Item item{ itemType };
			return this->GetItemCount(item);
		}

		uint32_t GetItemCount(Item item) const noexcept
		{
			try {
				return items.at(item);
			}
			catch (...) {
				return 0;
			}
		}

		const Items &GetItems() const noexcept {
			return items;
		}

		uint32_t GetNumItems() const noexcept {
			uint32_t result = 0;
			for (auto it = this->items.begin(); it != this->items.end(); ++it)
				result += it->second;
			return result;
		}

		nonstd::optional<std::pair<const Components::Container::Item, uint32_t>> GetInventorySlot(uint32_t slotID) noexcept 
		{
			const auto size = items.size();
			if (slotID < size)
			{
				auto it = items.begin();
				size_t i = 0;
				for (; i != size; ++i, ++it)
				{
					if (i == slotID)
						return *it;
				}
			}
			return nonstd::nullopt;
		}

		float GetTotalWeight() const noexcept {
			float result = 0;
			for (auto it = this->items.begin(); it != this->items.end(); ++it)
				result += it->second * it->first.itemType.GetWeight();
			return result;
		}

		size_t GetNumInventoryChanges() const noexcept {
			return numInventoryChanges;
		}

	private:
		Items items;
		size_t numInventoryChanges = 0;
	};
}