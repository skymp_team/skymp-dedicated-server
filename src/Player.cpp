﻿#include "stdafx.h"
#include "Player.h"
#include "ItemTypeWrap.h"
#include "IDAssigner.h"
#include "Streamer.h"

#define EPSILON (0.01f)

using Container = Components::Container;

struct Chunk
{
	int64_t x;
	int64_t y;

	friend bool operator<(const Chunk &lhs, const Chunk &rhs) {
		return std::make_tuple(lhs.x, lhs.y) < std::make_tuple(rhs.x, rhs.y);
	}

	bool operator==(const Chunk &rhs) const {
		return x == rhs.x && y == rhs.y;
	}

	bool operator!=(const Chunk &rhs) const {
		return !(*this == rhs);
	}
};

struct PlayerManager::Impl
{
	Players players;
	std::shared_ptr<Player> playersBySlot[MAX_PLAYERS];
};

std::map<Chunk, std::set<uint16_t>> gPlayersByChunk;

PlayerManager::PlayerManager() noexcept : pImpl(new Impl)
{
}

PlayerManager::~PlayerManager() noexcept
{
	delete pImpl;
}

Player *PlayerManager::NewPlayer(RakNet::RakNetGUID guid, const std::string &name) noexcept
{
	uint16_t id = ~0;
	for (uint16_t i = 0; i < g.cfg->GetMaxPlayers(); i++)
		if (pImpl->playersBySlot[i] == nullptr)
		{
			id = i;
			break;
		}

	const std::shared_ptr<Player> newPlayer(new Player(guid, id, name));
	pImpl->players[guid] = newPlayer;
	pImpl->playersBySlot[id] = newPlayer;
	Streamer::GetSingleton().Connect(id, false);

	return newPlayer.get();
}

Player *PlayerManager::NewNPC() noexcept
{
	static uint32_t counter = 0;
	const std::string newName = "---NPC" + std::to_string(counter++);

	uint16_t id = ~0;
	for (uint16_t i = g.cfg->GetMaxPlayers(); i != (uint16_t)~0; i++)
		if (pImpl->playersBySlot[i] == nullptr)
		{
			id = i;
			break;
		}

	RakNet::RakNetGUID guid;
	guid.g = g.networking->GetPeer()->Get64BitUniqueRandomNumber();

	const std::shared_ptr<Player> newPlayer(new Player(guid, id, newName, true));
	pImpl->players[guid] = newPlayer;
	pImpl->playersBySlot[id] = newPlayer;
	Streamer::GetSingleton().Connect(id, true);

	return newPlayer.get();
}

void PlayerManager::DeletePlayer(RakNet::RakNetGUID guid) noexcept
{
	const auto player = pImpl->players[guid];
	if (player != nullptr)
	{
		player->StreamOutAllObjects();

		auto &players = this->GetPlayers();
		std::for_each(players.begin(), players.end(), [&](auto val) {
			auto &player2 = val.second;
			if (player2 != nullptr)
				player2->StreamOut(player.get());
		});

		const uint16_t id = player->GetID();
		pImpl->playersBySlot[id] = nullptr;
		pImpl->players.erase(guid);
		Streamer::GetSingleton().Disconnect(id);
	}
}

Player *PlayerManager::GetPlayer(RakNet::RakNetGUID guid) const noexcept
{
	try {
		return pImpl->players.at(guid).get();
	}
	catch (...) {
		return nullptr;
	}
}

Player *PlayerManager::GetPlayer(uint16_t id) const noexcept
{
	try {
		if (id >= MAX_PLAYERS)
			return nullptr;
		return pImpl->playersBySlot[id].get();
	}
	catch (...) {
		return nullptr;
	}
}

const PlayerManager::Players &PlayerManager::GetPlayers() const noexcept
{
	return pImpl->players;
}

class ActorValue
{
public:
	float base = 100.0f;
	float modifier = 0.0f;
	float percentage = 1.0f;

	friend bool operator==(ActorValue lhs, ActorValue rhs) noexcept {
		if (lhs.percentage == 0 || lhs.percentage == 1
			|| rhs.percentage == 0 || rhs.percentage == 1)
		{
			return lhs.base == rhs.base && lhs.modifier == rhs.modifier
				&& lhs.percentage == rhs.percentage;
		}
		return lhs.base == rhs.base && lhs.modifier == rhs.modifier
			&& std::abs(lhs.percentage - rhs.percentage) <= EPSILON; // (0.01) to fix restoring AVs (Stamina, Magicka, Health)
	}

	friend bool operator!=(ActorValue lhs, ActorValue rhs) noexcept {
		return !(lhs == rhs);
	}
};

class ActorValueArray
{
public:
	enum {
		InvalidAV,
		Health,
		Magicka,
		Stamina,
		HealRate,
		MagickaRate,
		StaminaRate,
		HealRateMult,
		MagickaRateMult,
		StaminaRateMult,
		_fSprintStaminaDrainMult,
		_fSprintStaminaWeightBase,
		_fSprintStaminaWeightMult,
		CarryWeight,
		UnarmedDamage,
		OneHanded,
		TwoHanded,
		Marksman,
		Block,
		Smithing,
		HeavyArmor,
		LightArmor,
		Pickpocket,
		Lockpicking,
		Sneak,
		Alchemy,
		Speechcraft,
		Alteration,
		Conjuration,
		Destruction,
		Illusion,
		Restoration,
		Enchanting,
		Level,
		PerkPoints,
		_fXPLevelUpBase,
		_fXPLevelUpMult,
		Experience,
		__OneHandedExp,
		__TwoHandedExp,
		__MarksmanExp,
		__BlockExp,
		__SmithingExp,
		__HeavyArmorExp,
		__LightArmorExp,
		__PickpocketExp,
		__LockpickingExp,
		__SneakExp,
		__AlchemyExp,
		__SpeechcraftExp,
		__AlterationExp,
		__ConjurationExp,
		__DestructionExp,
		__IllusionExp,
		__RestorationExp,
		__EnchantingExp,
		NUM_AVS,
	};

	ActorValueArray() noexcept {
		data[InvalidAV].base = Player::GetInvalidAVBase();
		data[HealRate].base = 1.0f;
		data[MagickaRate].base = 3.0f;
		data[StaminaRate].base = 10.0f;
		data[_fSprintStaminaDrainMult].base = 7.0f;
		data[_fSprintStaminaWeightBase].base = 1.0f;
		data[_fSprintStaminaWeightMult].base = 0.02f;
		data[UnarmedDamage].base = 4.0f;
		data[Level].base = 1.0f;
		data[PerkPoints].base = 0.0f;
		data[_fXPLevelUpBase].base = 75.0f;
		data[_fXPLevelUpMult].base = 25.0f;
		data[Experience].base = 0.0f;
		for (int32_t i = OneHanded; i <= Enchanting; ++i)
			data[i].base = 15;
		for (int32_t i = __OneHandedExp; i <= __EnchantingExp; ++i)
			data[i].base = 0;
	}
	
	static uint8_t GetAVID(std::string avName) noexcept {
		std::transform(avName.begin(), avName.end(), avName.begin(), ::tolower);
		for (uint8_t i = 0; i != NUM_AVS; ++i)
		{
			auto name = GetAVName(i);
			std::transform(name.begin(), name.end(), name.begin(), ::tolower);
			if (name == avName)
				return i;
		}
		return  InvalidAV;
	}

	static const std::string &GetAVName(uint8_t id) noexcept {
		static const std::vector<std::string> names = {
			"",
			"Health",
			"Magicka",
			"Stamina",
			"HealRate",
			"MagickaRate",
			"StaminaRate",
			"HealRateMult",
			"MagickaRateMult",
			"StaminaRateMult",
			"_fSprintStaminaDrainMult",
			"_fSprintStaminaWeightBase",
			"_fSprintStaminaWeightMult",
			"CarryWeight",
			"UnarmedDamage",
			"OneHanded",
			"TwoHanded",
			"Marksman",
			"Block",
			"Smithing",
			"HeavyArmor",
			"LightArmor",
			"Pickpocket",
			"Lockpicking",
			"Sneak",
			"Alchemy",
			"Speechcraft",
			"Alteration",
			"Conjuration",
			"Destruction",
			"Illusion",
			"Restoration",
			"Enchanting",
			"Level",
			"PerkPoints",
			"_fXPLevelUpBase",
			"_fXPLevelUpMult",
			"Experience",
			"-OneHandedExp",
			"-TwoHandedExp",
			"-MarksmanExp",
			"-BlockExp",
			"-SmithingExp",
			"-HeavyArmorExp",
			"-LightArmorExp",
			"-PickpocketExp",
			"-LockpickingExp",
			"-SneakExp",
			"-AlchemyExp",
			"-SpeechcraftExp",
			"-AlterationExp",
			"-ConjurationExp",
			"-DestructionExp",
			"-IllusionExp",
			"-RestorationExp",
			"-EnchantingExp"
		};
		if (id < NUM_AVS)
			return names[id];
		else
			return names[InvalidAV];
	}

	ActorValue &operator[](uint8_t avID) noexcept {
		return this->data[avID];
	}

	ActorValue &operator[](std::string avName) noexcept {
		uint8_t avID = GetAVID(avName);
		return this->data[avID];
	}

	friend bool operator==(const ActorValueArray &lhs, const ActorValueArray &rhs) noexcept {
		return lhs.data == rhs.data;
	}

	friend bool operator!=(const ActorValueArray &lhs, const ActorValueArray &rhs) noexcept {
		return !(lhs == rhs);
	}

private:
	std::array<ActorValue, NUM_AVS> data;
};

class Equipment 
{
public:
	using Armor = std::map<std::string, nonstd::optional<Container::Item>>; // key is subclass name ("Armor", "Helmet", etc)

	friend bool operator==(const Equipment &lhs, const Equipment &rhs) noexcept {
		return lhs.ammo == rhs.ammo && ToContainer(lhs.armor).GetItems() == ToContainer(rhs.armor).GetItems() && lhs.hands[0] == rhs.hands[0] && lhs.hands[1] == rhs.hands[1];
	}
	friend bool operator!=(const Equipment &lhs, const Equipment &rhs) noexcept {
		return !(lhs == rhs);
	}

	static Container ToContainer(const Armor &armor) {
		Container result;
		for (auto &pair : armor)
		{
			auto &item = pair.second;
			if (item != nonstd::nullopt)
				result.AddItem(*item, 1);
		}
		return result;
	}

	const nonstd::optional<Container::Item> &GetHand(int32_t handID) const noexcept {
		if (handID != 0 && handID != 1)
			handID = 0;
		return this->hands[handID];
	}

	const Armor &GetArmor() const noexcept {
		return this->armor;
	}

	const nonstd::optional<Container::Item> &GetAmmo() const noexcept {
		return this->ammo;
	}

	void SetArmor(std::string subclass, nonstd::optional<Container::Item> item) noexcept {
		++numChanges;
		this->armor[subclass] = item;
	}

	void SetHand(int32_t handID, nonstd::optional<Container::Item> item) noexcept {
		++numChanges;
		if (handID != 0 && handID != 1)
			handID = 0;
		this->hands[handID] = item;
	}

	void SetAmmo(nonstd::optional<Container::Item> item) noexcept {
		++numChanges;
		this->ammo = item;
	}

	size_t GetNumChanges() const noexcept {
		return numChanges;
	}

private:
	Armor armor;
	nonstd::optional<Container::Item> hands[2], ammo;
	size_t numChanges = 0;
};

struct MagicEquipment
{
	nonstd::optional<SpellWrap> hands[2];

	friend bool operator==(const MagicEquipment &lhs, const MagicEquipment &rhs) noexcept {
		for (int32_t i = 0; i <= 1; ++i)
		{
			if (lhs.hands[i] != rhs.hands[i])
				return false;
		}
		return true;
	}
	friend bool operator!=(const MagicEquipment &lhs, const MagicEquipment &rhs) noexcept {
		return !(lhs == rhs);
	}
};

struct PlayerInfo
{
	std::string name = "";
	bool isPaused = false;
	Packet::LookData look = {};
	size_t numLookChanges = 0;
	clock_t lastLookSend = 0;
	uint32_t furniture = 0;
	Components::Container inventory = {};
	Components::SpellList spellList = {};
	nonstd::optional<ActorValueArray> actorValues = {};
	clock_t lastAvSend = 0;
	size_t numAvChanges = 0;
	Equipment equipment = {};
	MagicEquipment magicEquipment = {};
	decltype(Components::MagicTarget().GetActiveEffects()) activeEffects = {};
	bool isNpc = false;
	uint16_t hostID = (uint16_t)~0;
	uint16_t combatTarget = ~0;
	bool isWerewolf = false;
};

struct ObjectInfo
{
	uint32_t locationID = 0;
	NiPoint3 pos, rot;
	std::string name;
	Object::Type type = Object::Type::Static;
	uint32_t teleportTarget = 0;
	bool isOpen = false;
	bool isDisabled = false;
	uint32_t itemTypeID;
	uint32_t itemsCount = 0;
	Components::Container::Items allItems;
	uint16_t hostPlayerID = (uint16_t)~0;
	uint8_t lockLevel = 0;
	bool isDestroyed = false;
	Components::KeywordList keywords;
	bool isHostable = true;
};

struct Text3DInfo
{
	std::string text;
	NiPoint3 pos = { 0,0,1'000'000'000 };
};

struct Player::Impl 
{
	Impl() {
		this->isStreamedIn.resize(MAX_PLAYERS, 0);
	}

	template <class W>
	class WrapComparator
	{
	public:
		bool operator()(const W &lhs, const W &rhs) const noexcept {
			return lhs.GetIdentifier() < rhs.GetIdentifier();
		}
	};

	RakNet::RakNetGUID guid = RakNet::UNASSIGNED_RAKNET_GUID;
	uint16_t id = ~0;
	std::string name;
	bool isNpc = false;
	uint32_t baseNpc = 0;
	Packet::MovementData movement, movementInStreamer;
	SoulSize soulSize = SoulSize::Grand;
	uint32_t refID = 0;
	uint32_t lastAnimID = 0;
	size_t numAnims = 0;
	std::set<uint16_t> streamedIn, streamedInNpcs;
	std::vector<uint8_t> isStreamedIn;
	std::vector<uint8_t> isStreamedInByFid;
	std::set<uint32_t> streamedInText3Ds;
	std::set<Components::Keyword> knownKeywords;
	size_t numAddAlchemyIngr = 0;

	std::chrono::time_point<std::chrono::system_clock> lastRecipeUpdate, lastTeleportUse, lastAVChange[ActorValueArray::NUM_AVS];
	clock_t lastEffectsStep = 0;

	std::array<std::shared_ptr<PlayerInfo>, MAX_PLAYERS> plInfo;
	std::map<uint32_t, ObjectInfo> objectsInfo;
	std::map<uint32_t, Text3DInfo> text3DsInfo;

	struct KnownItemType
	{
		ItemTypeInfo info;
		size_t numChanges = 0;
	};

	std::map<void *, KnownItemType> itemTypesInfo;
	std::map<SpellWrap, SpellInfo, WrapComparator<SpellWrap>> spellsInfo;
	std::map<EffectWrap, EffectInfo, WrapComparator<EffectWrap>> effectsInfo;
	std::map<std::string, nonstd::optional<uint32_t>> animsInfo;
	std::map<uint32_t, RecipeInfo> recipesInfo;

	Equipment equipment;
	MagicEquipment magicEquipment;

	bool loading = false;
	bool paused = false;
	Packet::LookData look;
	size_t numLookChanges = 0;
	bool isSpawned = false;
	clock_t lastLoadScreen = 0;
	uint32_t location = 0;
	uint32_t virtualWorld = 0;
	bool isWaitingForNewLook = false;
	uint32_t furniture = 0;
	bool silent = false;
	uint32_t dialogID = ~0;
	ActorValueArray actorValues;
	nonstd::optional<uint16_t> killerID = nonstd::nullopt;
	std::map<ItemTypeWrap, std::list<ItemTypeWrap>> weapsPoison;
	bool movementSync = true;
	bool isWerewolf = false;
	uint16_t combatTarget = ~0;
	RakNet::RakNetGUID foreHostGUID = RakNet::UNASSIGNED_RAKNET_GUID;
	size_t numAvChanges = 0;
	uint16_t lastHitTarget = ~0;

	bool dataSearch = false;
	std::set<DataSearch::Opcode> enabledOpcodes;
	std::map<std::string, uint32_t> allowedMagicHits;
	RakNet::RakNetGUID lastAttackTarget = RakNet::UNASSIGNED_RAKNET_GUID;
	nonstd::optional<ItemTypeWrap> lastAttackWeap;

	Components::Container craftIngredients;
	clock_t lastPush = 0;
	nonstd::optional<ItemTypeWrap> lastSoulGem;
	nonstd::optional<ItemTypeWrap> lastAddedCraftIngr;
	
	clock_t lastMagickaCheck = 0;

	enum {
		MAX_HITS = 25,
	};

	static std::set<SpellWrap> customEnchs;
};
std::set<SpellWrap> Player::Impl::customEnchs;

// Helper funcs:
void Player::WriteNonPlayerContainer(RakNet::BitStream &bsOut, const Container &container) noexcept
{
	this->UpdateItemTypes(container);
	auto items = container.GetItems();
	bsOut.Write((uint32_t)items.size());
	for (auto it = items.begin(); it != items.end(); ++it)
	{
		const auto itemTypeWrapID = IDAssigner<ItemTypeWrap>::GetID(it->first.itemType);
		bsOut.Write(itemTypeWrapID);
		bsOut.Write(it->second);
	}
};
// ...

static uint64_t unusedUniqueID = 0;

Player::Player(RakNet::RakNetGUID guid, uint16_t id, const std::string &name, bool isNpc) noexcept : pImpl(new Impl), uniqueID(++unusedUniqueID)
{
	pImpl->guid = guid;
	pImpl->id = id;
	pImpl->name = name;
	pImpl->isNpc = isNpc;
}

Player::~Player() noexcept
{
	delete pImpl;
}

std::unique_ptr<Player> Player::CreateDummy() noexcept
{
	auto p = new Player(RakNet::UNASSIGNED_RAKNET_GUID, ~0, "Dummy");
	return std::unique_ptr<Player>(p);
}

uint16_t Player::GetID() const noexcept
{
	return pImpl->id;
}

RakNet::RakNetGUID Player::GetGUID() const noexcept
{
	return pImpl->guid;
}

const std::string &Player::GetName() const noexcept
{
	return pImpl->name;
}

void Player::SetName(const std::string &name) noexcept
{
	pImpl->name = name;
}

bool Player::IsSpawned() const noexcept
{
	return pImpl->isSpawned;
}

uint32_t Player::GetLocation() const noexcept
{
	return pImpl->location;
}

void Player::SetMovement(const Packet::MovementData &mov) noexcept
{
	const auto chunkSize = Player::GetStreamDistance() / 2;

	if (g.plManager != nullptr)
	{
		const Chunk chunk = { int64_t(mov.pos.x / chunkSize), int64_t(mov.pos.y / chunkSize) };
		const Chunk lastChunk = { int64_t(pImpl->movement.pos.x / chunkSize), int64_t(pImpl->movement.pos.y / chunkSize) };

		if (lastChunk != chunk)
		{
			gPlayersByChunk[lastChunk].erase(this->GetID());
			gPlayersByChunk[chunk].insert(this->GetID());

			this->UpdateStreamedPlayersList();
		}
	}

	pImpl->movement = mov;
	if ((pImpl->movement.pos - pImpl->movementInStreamer.pos).Length() > 512)
	{
		pImpl->movementInStreamer = mov;
		Streamer::GetSingleton().SetPos(this->GetID(), mov.pos);
	}
}

Packet::MovementData Player::GetMovement() const noexcept
{
	return pImpl->movement;
}

void Player::SetLocation(uint32_t location) noexcept
{
	if (this->pImpl->location != location)
	{
		this->pImpl->location = location;
		this->UpdateStreamedPlayersList();
		//this->SendClientMessage("Change location to " + std::to_string(location), false);
		Streamer::GetSingleton().SetLocation(this->GetID(), location);
	}
}

uint32_t Player::GetVirtualWorld() const noexcept
{
	return pImpl->virtualWorld;
}

void Player::SetVirtualWorld(uint32_t w) noexcept
{
	if (this->pImpl->virtualWorld != w)
	{
		this->pImpl->virtualWorld = w;
		this->UpdateStreamedPlayersList();
		Streamer::GetSingleton().SetVirtualWorld(this->GetID(), w);
	}
}

bool Player::IsWaitingForNewLook() const noexcept
{
	return this->pImpl->isWaitingForNewLook;
}

void Player::SetWaitingForNewLook(bool v) noexcept
{
	this->pImpl->isWaitingForNewLook = v;
}

uint32_t Player::GetCurrentFurniture() const noexcept
{
	return this->pImpl->furniture;
}

void Player::SetSoulSize(SoulSize s) noexcept
{
	this->pImpl->soulSize = s;
}

SoulSize Player::GetSoulSize() const noexcept
{
	return this->pImpl->soulSize;
}

void Player::SetRefID(uint32_t v) noexcept
{
	this->pImpl->refID = v;
}

uint32_t Player::GetRefID() const noexcept
{
	return this->pImpl->refID;
}

void Player::SetCurrentFurniture(uint32_t furniture) noexcept
{
	pImpl->furniture = furniture;
	if (furniture != 0)
	{
		auto obj = g.objManager->LookupObject(furniture);
		if (obj != nullptr)
			this->UpdateItemTypes(*obj);
		else
			pImpl->furniture = 0;
	}
}

void Player::PickUpObject(Object *target) noexcept
{
	if (target != nullptr && target->GetType() == Object::Type::Item)
	{
		target->RemoveAllItems(this);
		g.objManager->DeleteObject(target->GetID());
	}
	else
		log(Log::Warning, "Attempt to pick up inappropriate object");
}

Object *Player::DropItem(ItemTypeWrap itemType, uint32_t count_) noexcept
{
	for (uint32_t count = count_; count != 0; --count)
		if (this->RemoveItem(itemType, count))
		{
			const auto movement = this->GetLastInjectedMovement();
			if (movement != nullptr)
			{
				auto pos = movement->pos;
				auto angle = (float)movement->angleZ;
				pos += {std::sin(angle) * 64, std::sin(angle) * 64, 80.f};
				const auto obj = g.objManager->NewObject(0, itemType.GetExistingGameFormID(), this->GetLocation(), pos, { 0,0,angle });
				obj->SetVirtualWorld(this->GetVirtualWorld());
				obj->SetType(Object::Type::Item);
				obj->AddItem(itemType, count);
				this->GetPlayerInfo(this->GetID()).inventory.RemoveItem(itemType, count);
				return obj;
			}
			break;
		}
	return nullptr;
}

void Player::ChangeContainer(ItemTypeWrap itemType, uint32_t count, bool isAdd, Object *currentContainer) noexcept
{
	static auto activateReach = g.cfg->GetValue("Gameplay.activateReach", 256);
	if (count > 0)
	{
		if (currentContainer != nullptr)
		{
			if (currentContainer->GetType() == Object::Type::Container && (currentContainer->GetPos() - this->GetMovement().pos).Length() < activateReach && this->IsStreamedIn(currentContainer))
			{
				if (currentContainer->GetLockLevel() == 0)
				{
					if (isAdd)
					{
						for (uint32_t change = count; change != 0; --change)
							if (this->RemoveItem(itemType, change))
							{
								(g.gamemode->OnPlayerChangeContainer(this->GetID(), currentContainer->GetID(), itemType.GetIdentifier(), change, isAdd));
								currentContainer->AddItem(itemType, change);

								this->GetPlayerInfo(this->GetID()).inventory = Container(this->GetItems());
								pImpl->objectsInfo[currentContainer->GetID()].allItems = currentContainer->GetItems();
								break;
							}
					}
					else
					{
						for (uint32_t change = count; change != 0; --change)
							if (currentContainer->RemoveItem(itemType, change))
							{
								(g.gamemode->OnPlayerChangeContainer(this->GetID(), currentContainer->GetID(), itemType.GetIdentifier(), change, isAdd));
								this->AddItem(itemType, change);

								this->GetPlayerInfo(this->GetID()).inventory = Container(this->GetItems());
								pImpl->objectsInfo[currentContainer->GetID()].allItems = currentContainer->GetItems();
								break;
							}
					}
				}
			}
		}
	}
}

void Player::TrapSoul(Player *target) noexcept
{
	if (!target->IsStreamedIn(this) || !this->IsStreamedIn(target))
		return;

	const auto soul = target->GetSoulSize();

	const auto items = this->GetItems();
	for (auto &val : items)
	{
		const auto item = val.first;
		const auto count = val.second;

		if (item.itemType.GetClass() == "SoulGem")
		{
			if (item.itemType.GetSoulSize() == SoulSize::None && item.itemType.GetCapacity() >= soul)
			{
				luabridge::LuaRef itemTypeRef(g.gamemode->GetState()), nil(g.gamemode->GetState());
				itemTypeRef = item.itemType;
				nil = luabridge::Nil();
				auto result = ItemTypeWrap::Clone(itemTypeRef, nil);

				if (result.isNil())
				{
					log(Log::Warning, "TrapSoul() failed to create soul gem");
					return;
				}

				auto newSoulGem = result.cast<ItemTypeWrap>();
				newSoulGem.SetSoulSize(soul);
				if (this->RemoveItem(item, 1))
					this->AddItem(newSoulGem, 1);
				return;
			}
		}
	}
}

void Player::UseItem(ItemTypeWrap itemType) noexcept
{
	if (this->RemoveItem(itemType, 1))
	{
		this->GetPlayerInfo(this->GetID()).inventory.RemoveItem(itemType, 1);

		if (itemType.GetClass() == "Armor" 
			|| itemType.GetClass() == "Weapon" 
			|| itemType.GetClass() == "Misc"
			|| itemType.GetClass() == "Ammo")
		{
			this->AddItem(itemType, 1);
			log(Log::Warning, this->GetName(), "[", this->GetID(), "]", " bad UseItem() on ", itemType.GetClass().data());
			return;
		}

		if (itemType.GetClass() == "Potion" || itemType.GetClass() == "Ingredient")
		{
			if (itemType.GetSubclass() != "Poison")
			{
				this->CastItem(itemType, this);
				return g.gamemode->OnPlayerEatItem(this->GetID(), itemType.GetIdentifier());
			}
			else
			{
				auto weap = this->GetEquippedWeapon(0);
				if (weap != nonstd::nullopt)
				{
					pImpl->weapsPoison[weap->itemType].push_back(itemType);
				}
			}
		}

		return g.gamemode->OnPlayerUseItem(this->GetID(), itemType.GetIdentifier());
	}
}

void Player::AddCraftIngredient(ItemTypeWrap itemType, uint32_t N) noexcept
{
	if (this->RemoveItem(itemType, N))
	{
		this->GetPlayerInfo(this->GetID()).inventory.RemoveItem(itemType, N);

		const auto furn = g.objManager->LookupObject(this->GetCurrentFurniture());
		if (furn == nullptr)
		{
			log(Log::Warning, "Attempt to inject AddCraftIngredient() without workbench (", this->GetName().data(), "[", this->GetID(), "])");
			return;
		}
		if (furn->IsEnchantingWorkbench())
		{
			if (itemType.HasEnchantment())
			{
				if (furn->IsEnchantingWorkbench())
				{
					// Learn enchantment
					this->AddSpell(itemType.GetEnchantment().cast<SpellWrap>());
				}
				return;
			}

			if (itemType.GetClass() == "SoulGem"
				&& itemType.GetSoulSize() != 0
				&& itemType.GetCapacity() != 0)
			{
				pImpl->lastSoulGem = itemType;
				return;
			}
		}

		if (itemType.GetClass() == "Ingredient")
			pImpl->numAddAlchemyIngr++;

		pImpl->craftIngredients.AddItem(itemType, N);
		pImpl->lastPush = clock();
		{
			if (itemType.GetClass() != pImpl->craftIngredients.GetItems().begin()->first.itemType.GetClass()
				&& /*furn->IsAlchemyWorkbench()*/ false)
			{
				log(Log::Warning, "Bad craft ingredients");
				this->ClearCraftIngredients();
			}
		}
	}

	pImpl->lastAddedCraftIngr = itemType;
}

nonstd::optional<Container::Item> Player::CraftItem(const ItemTypeWrap &itemType) noexcept
{
	nonstd::optional<Container::Item> result;

	if (pImpl->craftIngredients.GetNumItems() != 0)
	{

		auto recipeList = RecipeWrap::GetAllRecipes();
		for (auto &r : recipeList)
		{
			if (r.GetProduce() == itemType && r.GetContainer().GetItems() == pImpl->craftIngredients.GetItems())
			{
				result = Container::Item{ r.GetProduce() };
				this->AddItem(r.GetProduce(), r.GetProduceCount());
				this->GetPlayerInfo(this->GetID()).inventory.AddItem(r.GetProduce(), r.GetProduceCount());
				//g.gamemode->SendChatMessageToAll("OnItemCraft");
				g.gamemode->OnPlayerCreateItem(this->GetID(), itemType.GetIdentifier(), r.GetProduceCount());
				this->ClearCraftIngredients();
				log(Log::Debug, this->GetName(), " crafts an item");
				return result;
			}
			else
			{
			}
		}
		log(Log::Warning, "Craft recipe not found");
		/*log(Log::Debug, "My crafting container:");
		auto i = pImpl->craftIngredients.GetItems();
		for (auto entry : i)
			log(Log::Debug, entry.first.itemType.GetIdentifier().c_str(), " ", entry.second);*/
	}
	else
		log(Log::Warning, "Attempt to craft item without ingredients");
	return result;
}

nonstd::optional<Container::Item> Player::CraftPotion(bool isPoison) noexcept
{
	nonstd::optional<Container::Item> result;

	const auto furn = g.objManager->LookupObject(this->GetCurrentFurniture());
	if (furn == nullptr)
	{
		log(Log::Warning, "Attempt to craft item without workbench");
		return result;
	}

	if (pImpl->numAddAlchemyIngr < 2)
	{
		this->ClearCraftIngredients();
		log(Log::Warning, "CheatEngine detected in alchemy");
	}

	if (pImpl->craftIngredients.GetNumItems() != 0)
	{
		const auto cl = pImpl->craftIngredients.GetItems().begin()->first.itemType.GetClass();
		if (cl == "Ingredient")
		{
			if (furn->IsAlchemyWorkbench())
			{
				std::map<std::string, uint32_t> numEntries;
				std::map<std::string, float> baseDur, baseMag;
				std::set<ItemTypeWrap> craftIngredientsSet;
				auto items = pImpl->craftIngredients.GetItems();
				for (auto &entry : items)
				{
					craftIngredientsSet.insert(entry.first.itemType);
				}
				for (auto itemType : craftIngredientsSet)
				{
					for (uint32_t n = 1; n <= itemType.GetNumEffects(); ++n)
					{
						const auto ident = itemType.GetNthEffectIdentifier(n);
						++numEntries[ident];

						const float dur = itemType.GetNthEffectDuration(n);
						if (std::abs(baseDur[ident]) < std::abs(dur))
							baseDur[ident] = dur;
						const float mag = itemType.GetNthEffectMagnitude(n);
						if (std::abs(baseMag[ident]) < std::abs(mag))
							baseMag[ident] = mag;
					}
				}

				struct EffectItem
				{
					std::string effectIdent;
					float mag;
					float dur;
					float area;
				};

				std::vector<EffectItem> potionEffects;
				for (auto &pair : numEntries)
				{
					const auto count = pair.second;
					const auto &effectIdent = pair.first;
					if (count > 1)
					{
						potionEffects.push_back({
							effectIdent,
							baseMag[effectIdent],
							baseDur[effectIdent],
							0.0f
						});
					}
				}

				const bool failed = potionEffects.empty();
				if (!failed)
				{
					for (auto & ei : potionEffects)
					{
						/*Magnitude = Base_Mag *
							4 * (Game setting fAlchemyIngredientInitMult)
							(1.5 ^ (Alchemy_Skill / 100)) *   (1.5 is the game setting fAlchemySkillFactor)
							(1 + Fortify_Alchemy / 100) *
							(1 + Alchemist_Perk / 100)  *
							(1 + Physician_Perk / 100)  *
							(1 + Benefactor_Perk / 100 + Poisoner_Perk / 100)
							(1 + Seeker of Shadows / 100))*/
						float fAlchemyIngredientInitMult = 4;
						float fAlchemySkillFactor = 1.5;
						float Fortify_Alchemy = 0;
						float Alchemist_Perk = 0;
						float Physician_Perk = 0;
						float Benefactor_Perk = 0;
						float Poisoner_Perk = 0;
						float SeekerofShadows = 0;

						ei.mag = ei.mag * fAlchemyIngredientInitMult * (std::pow(fAlchemySkillFactor, (this->GetAVData("Alchemy").GetAfterBuffs() / 100))) *
							(1 + Fortify_Alchemy / 100) *
							(1 + Alchemist_Perk / 100)  *
							(1 + Physician_Perk / 100)  *
							(1 + Benefactor_Perk / 100 + Poisoner_Perk / 100) *
							(1 + SeekerofShadows / 100);
					}

					static int64_t i = 0;
					const auto potionIdent = "DefaultPotion" + std::to_string(i++);

					enum {
						DefaultPotionID = 0x0005661F,
						PoisonID = 0x00073F31
					};

					luabridge::LuaRef nil(g.gamemode->GetState());
					auto potionRef = ItemTypeWrap::Create(potionIdent, isPoison ? "Potion.Poison" : "Potion.Potion", isPoison ? PoisonID : DefaultPotionID, 0.5, 0, nil, nil);

					if (!potionRef.isNil())
					{
						auto potion = potionRef.cast<ItemTypeWrap>();

						if (potionEffects.size() > 4)
						{
							log(Log::Warning, "Bad alchemy effects count ", potionEffects.size());
							potionEffects.resize(2);
						}
						for (const auto &ei : potionEffects)
						{
							luabridge::LuaRef effectRef(g.gamemode->GetState());
							effectRef = EffectWrap::LookupByIdentifier(ei.effectIdent);
							potion.AddEffect(effectRef, ei.mag, ei.dur, ei.area);
						}
						this->AddItem(potion, 1);
						pImpl->numAddAlchemyIngr -= 2;
						g.gamemode->OnPlayerCreateItem(this->GetID(), potion.GetIdentifier(), 1);
						this->ClearCraftIngredients(); // was commented for long time; uncommented to fix CheatEngine potions in 1.0.35+
					}
					else
						log(Log::Warning, "Unable to craft Potion");
				}
			}
			else
				log(Log::Warning, "Bad alchemy workbench");
		}
	}

	return result;
}

void Player::EnchantItemUnsafe(ItemTypeWrap itemType, SpellWrap baseEnch, ItemTypeWrap soulGem) noexcept
{
	luabridge::LuaRef baseEnchRef(g.gamemode->GetState()), nil(g.gamemode->GetState());
	baseEnchRef = baseEnch;
	nil = luabridge::Nil();
	auto enchR1 = SpellWrap::Clone(baseEnchRef, nil, false);

	if (enchR1.isNil())
	{
		log(Log::Warning, "Unable to clone enchantmnet");
		return;
	}

	SpellWrap ench = enchR1;


	if (itemType.GetClass() == "Weapon")
	{
		for (uint32_t n = 1; n <= baseEnch.GetNumEffects(); ++n)
		{
			float skillMultiplier = 1 + (this->GetAVData("Enchanting").GetAfterBuffs() / 100);
			float enchanterPerkModifier = 0;
			float specificEnchantingPerkModifier = 0;
			float elementalDestructionPerkModifier = 0;
			float baseEnchMag = baseEnch.GetNthEffectMagnitude(n);
			float enchanterPerk = 0;
			float potionEffect = 1;
			/*float maxMag = baseEnchMag * skillMultiplier * (1 + enchanterPerkModifier) * (1 + specificEnchantingPerkModifier)
				* (1 + elementalDestructionPerkModifier)
				+ baseEnch.GetNthEffectMagnitude(n) * skillMultiplier * (1 + enchanterPerk) * (potionEffect);*/
			float maxMag = baseEnchMag * skillMultiplier * (1 + enchanterPerkModifier) * (1 + specificEnchantingPerkModifier)
				* (1 + elementalDestructionPerkModifier)
				* (1 + enchanterPerk) * (potionEffect);
			ench.AddEffect(baseEnch.GetNthEffect(n), maxMag, baseEnch.GetNthEffectDuration(n), baseEnch.GetNthEffectArea(n));
		}
	}
	else if (itemType.GetClass() == "Armor")
	{
		for (uint32_t n = 1; n <= baseEnch.GetNumEffects(); ++n)
		{
			float baseMagnitude = baseEnch.GetNthEffectMagnitude(n);
			double soulMultiplier;
			switch (soulGem.GetSoulSize())
			{
			case SoulSize::None:
				soulMultiplier = 0;
				break;
			case SoulSize::Petty:
				soulMultiplier = 1.0 / 12;
				break;
			case SoulSize::Lesser:
				soulMultiplier = 1.0 / 6;
				break;
			case SoulSize::Common:
				soulMultiplier = 1.0 / 3;
				break;
			case SoulSize::Greater:
				soulMultiplier = 2.0 / 3;
				break;
			case SoulSize::Grand:
				soulMultiplier = 1.0;
				break;
			default:
				soulMultiplier = 0.0;
				break;
			}
			float skillMultiplier = 1 + (this->GetAVData("Enchanting").GetAfterBuffs() / 100);
			float enchanterPerk = 0;
			float specificPerkModifier = 0;
			float netMagnitude = baseMagnitude * (float)soulMultiplier * skillMultiplier * (1 + enchanterPerk) * (1 + specificPerkModifier);
			ench.AddEffect(baseEnch.GetNthEffect(n), netMagnitude, baseEnch.GetNthEffectDuration(n), baseEnch.GetNthEffectArea(n));
		}
	}
	else
		return;

	Impl::customEnchs.insert(ench);

	luabridge::LuaRef itemTypeR(g.gamemode->GetState()), nil1(g.gamemode->GetState());
	itemTypeR = itemType;
	nil1 = luabridge::Nil();
	auto itemTypeCloneR = ItemTypeWrap::Clone(itemTypeR, nil1); 
	if (itemTypeCloneR.isNil())
	{
		log(Log::Warning, "Unable to clone enchantable item");
		return;
	}

	ItemTypeWrap clone = itemTypeCloneR;
	luabridge::LuaRef enchR(g.gamemode->GetState());
	enchR = ench;
	clone.SetEnchantment(enchR);

	if (this->RemoveItem(itemType, 1))
	{
		this->AddItem(clone, 1);
		g.gamemode->OnPlayerCreateItem(this->GetID(), itemType.GetIdentifier(), 1);
	}
}

nonstd::optional<ItemTypeWrap> Player::GetLastEnchSoulGem() const noexcept
{
	return pImpl->lastSoulGem;
}

void Player::SetLoading(bool loading) noexcept
{
	pImpl->loading = loading;
	if (loading)
	{
		pImpl->isSpawned = false;
		pImpl->lastLoadScreen = clock();
		//this->EndDeath();
	}
}

bool Player::IsLoading() const noexcept
{
	return pImpl->loading;
}

float Player::GetSecondsFromLastLoading() const noexcept
{
	if (pImpl->lastLoadScreen == 0)
		return std::numeric_limits<float>::infinity();
	return float(clock() - pImpl->lastLoadScreen) / CLOCKS_PER_SEC;
}

void Player::SetPaused(bool paused) noexcept
{
	pImpl->paused = paused;
}

bool Player::IsPaused() const noexcept
{
	return pImpl->paused;
}

void Player::SetLook(const Packet::LookData &look) noexcept
{
	pImpl->look = look;
	pImpl->numLookChanges++;
}

const Packet::LookData &Player::GetLook() const noexcept
{
	return pImpl->look;
}

size_t Player::GetNumLookChanges() const noexcept
{
	return pImpl->numLookChanges;
}

void Player::InjectMovement(const Packet::MovementData &movement, bool broadcastMyMovement) noexcept
{
	// Global:
	static clock_t lastStreamingStep = 0;
	if((clock() - lastStreamingStep) / (float)CLOCKS_PER_SEC >= 0.5)
	{
		Streamer::GetSingleton().Step();
		lastStreamingStep = clock();
	}

	if (std::abs(movement.pos.x) <= 1.0 && std::abs(movement.pos.y) <= 1.0 && std::abs(movement.pos.z) <= 1.0)
	{
		log(Log::Warning, "Bad movement injected by ", this->GetName().data(), "[", this->GetID(), "]");
		return;
	}

	if (this->IsDying())
	{
		this->UpdatePlayer(this); //-V678
		return;
	}

	auto furnID = this->GetCurrentFurniture();
	if (furnID != 0)
	{
		auto furn = g.objManager->LookupObject(furnID);
		if (furn != nullptr)
		{
			//switch (furn->GetType())
			{
			//default:
				if ((furn->GetType() != Object::Type::Furniture || (furn->GetPos() - movement.pos).Length() > 128) && movement.runMode != Packet::MovementData::RunMode::Standing)
				{
					this->SetCurrentFurniture(0);
				}
				//break;
			}
		}
	}

	if (pImpl->lastPush + (500 * (CLOCKS_PER_SEC / 1000.0)) < clock() && this->GetCurrentFurniture() == 0)
		this->ClearCraftIngredients();

	const auto lastMovement = this->GetMovement();
	if (movement.isInJumpState != lastMovement.isInJumpState)
	{
		if (movement.jumpStage == Packet::MovementData::JumpStage::Landed 
			&& movement.isInJumpState)
		{
			g.gamemode->OnPlayerJump(this->GetID());
		}
	}
	if (movement.jumpStage != lastMovement.jumpStage)
	{
		switch (movement.jumpStage)
		{
		case Packet::MovementData::JumpStage::Jumping:
			g.gamemode->OnPlayerJump(this->GetID());
			break;
		case Packet::MovementData::JumpStage::Falling:
			g.gamemode->OnPlayerFall(this->GetID());
			break;
		case Packet::MovementData::JumpStage::Landed:
			g.gamemode->OnPlayerOnTheGround(this->GetID());
			break;
		}
	}

	this->SetMovement(movement);
	pImpl->isSpawned = true;

	// Host npcs:
	if (this->IsNPC() == false)
	{
		for (auto id : pImpl->streamedInNpcs)
		{
			auto pl = g.plManager->GetPlayer(id);
			if (pl && pl->IsNPC())
			{
				this->HosterUpdateFor(pl);
			}
		}
	}

	if (pImpl->movement.isSprinting)
	{
		pImpl->lastAVChange[ActorValueArray::Stamina] = std::chrono::system_clock::now();
		if (this->GetAVData("Stamina").percentage <= 0)
			pImpl->movement.isSprinting = 0;
	}

	if (broadcastMyMovement)
	{
		RakNet::BitStream bsOut;
		bsOut.Write(ID_PLAYER_MOVEMENT);
		bsOut.Write(this->GetID());
		Packet::Serialize(bsOut, pImpl->movement);
		bsOut.Write((uint8_t)this->IsMovementSyncEnabled());
		bsOut.Write(this->GetLocation());
		for (auto it = pImpl->streamedIn.begin(); it != pImpl->streamedIn.end(); ++it)
		{
			auto player = g.plManager->GetPlayer(*it);
			if (player == nullptr || player == this)
				continue;
			g.networking->SendToPlayer(player, &bsOut, UNRELIABLE, HIGH_PRIORITY);
			this->UpdatePlayer(player);
		}
		this->UpdatePlayer(this);
	}
}

#ifdef max
#undef max
#endif

void Player::InjectCastConcentration(const Packet::MovementData &movement) noexcept
{
	const auto timeDiff = (clock() - pImpl->lastMagickaCheck) / (float)CLOCKS_PER_SEC;
	if (timeDiff < 0.025)
		return;
	pImpl->lastMagickaCheck = clock();

	auto getDurationMaximum = [](const SpellWrap &spell) {
		float max = 0;
		for (uint32_t n = 1; n <= spell.GetNumEffects(); ++n)
			max = std::max(max, spell.GetNthEffectDuration(n));
		return max;
	};

	for (int32_t i = 0; i <= 1; ++i)
	{
		if (movement.castStage[i] != Packet::MovementData::CastStage::None)
		{
			const auto magicka = this->GetAVData("Magicka").GetAfterBuffs();
			const auto spell = this->GetEquippedSpell(i);

			if (spell == nonstd::nullopt)
				continue;
			if (spell->GetCastingType() != "Concentration")
				continue;
			const auto costPer1s = spell->GetCost();

			const auto cost = costPer1s * timeDiff;// *getDurationMaximum(*spell);

			if (magicka < cost)
				continue;

			auto avDat = this->GetAVData("Magicka");
			avDat.DamageCurrent(cost);
			this->SetAVData("Magicka", avDat);

			if (spell->GetDelivery() == "Self")
				this->ApplySpell(*spell, this->GetID());
		}
	}
}

void Player::CastItem(ItemTypeWrap itemType, Player *reason) noexcept
{
	for (uint32_t n = 1; n <= itemType.GetNumEffects(); ++n)
	{
		try {
			this->ApplyEffect(EffectWrap(itemType.GetNthEffectIdentifier(n)),
				itemType.GetNthEffectMagnitude(n),
				itemType.GetNthEffectDuration(n),
				(reason ? reason->GetID() : ~0)
			);
		}
		catch (...) {
			log(Log::Warning, "UseItem() unknown item effect");
		}
	}
}

const Packet::MovementData *Player::GetLastInjectedMovement() const noexcept
{
	if (pImpl->movement.isEmpty())
		return nullptr;
	return &pImpl->movement;
}

void Player::InjectHitAnim(uint32_t hitAnimID) noexcept
{
	enum {
		MaxCombatAnimID = 21,
	};
	if (hitAnimID > MaxCombatAnimID)
		return;

	return this->InjectAnim(hitAnimID);
}

void Player::InjectAnim(uint32_t animID, bool sendToSelf) noexcept
{
	pImpl->numAnims++;
	pImpl->lastAnimID = animID;

	auto f = [&](Player *player) {
		RakNet::BitStream bsOut;
		bsOut.Write(ID_PLAYER_ANIM);
		bsOut.Write(this->GetID());
		const auto aeIdPtr = this->GetLastInjectedAnim();
		if (aeIdPtr != nullptr)
		{
			bsOut.Write(*aeIdPtr);

			enum {
				N = 21, // Hardcoded combat animations (Legacy)
			};

			if (*aeIdPtr != ~0 && *aeIdPtr > N)
				player->SendAnimationInfo(*aeIdPtr);
			g.networking->SendToPlayer(player, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
		}
	};

	for (auto id : pImpl->streamedIn)
	{
		auto player = g.plManager->GetPlayer(id);
		if (player != nullptr)
			f(player);
	}
	if (sendToSelf)
		f(this);
}

const uint32_t *Player::GetLastInjectedAnim() const noexcept
{
	if (this->GetNumAnims() > 0)
		return &pImpl->lastAnimID;
	else
		return nullptr;
}

size_t Player::GetNumAnims() const noexcept
{
	return pImpl->numAnims;
}

void Player::ShowDialog(uint32_t dialogID, const std::string &title, DialogStyle style, const std::string &text, int32_t defaultIndex) noexcept
{
	RakNet::BitStream bsOut;
	bsOut.Write(ID_SHOW_DIALOG);
	bsOut.Write((uint16_t)title.size());
	bsOut.Write((uint16_t)text.size());
	for (auto it = title.begin(); it != title.end(); ++it)
		bsOut.Write(*it);
	for (auto it = text.begin(); it != text.end(); ++it)
		bsOut.Write(*it);
	bsOut.Write(dialogID);
	bsOut.Write(defaultIndex);
	bsOut.Write(style);
	g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
	pImpl->dialogID = dialogID;
}

uint32_t Player::GetCurrentDialogID() const noexcept
{
	return pImpl->dialogID;
}

void Player::ReleaseDialog() noexcept
{
	pImpl->dialogID = ~0;
}

void Player::SetAVData(const std::string &avName, AVData data, const Player *reason) noexcept
{
	pImpl->numAvChanges++;
	const auto avID = ActorValueArray::GetAVID(avName);

	switch (avID)
	{
	case ActorValueArray::Health:
	case ActorValueArray::Magicka:
	case ActorValueArray::Stamina:
		break;
	case ActorValueArray::Level:
		data.percentage = 1;
		data.modifier = 0;
		if (data.base < 1)
			data.base = 1;
		data.base = (uint16_t)data.base;
		break;
	case ActorValueArray::PerkPoints:
		data.percentage = 1;
		data.modifier = 0;
		if (data.base < 0)
			data.base = 0;
		data.base = (uint16_t)data.base;
		break;
	default:
		data.percentage = 1;
		break;
	}

	const bool wasDying = this->IsDying();

	if (&pImpl->actorValues[avName] == &pImpl->actorValues[ActorValueArray::Health] && wasDying && this->IsSpawned())
	{
		if (data.percentage > 0)
		{
			log(Log::Warning, "Unable to increase Health for dying player");
			data.percentage = 0;
		}
	}

	ActorValue av;
	av.base = data.base;
	av.modifier = data.modifier;
	av.percentage = data.percentage;

	if (std::abs(pImpl->actorValues[avName].percentage - av.percentage) <= EPSILON)
	{
		pImpl->actorValues[avName] = av;
		this->SendPlayerAVs(this, this->GetPlayerInfo(this->GetID()), true);
	}
	else
		pImpl->actorValues[avName] = av;

	pImpl->lastAVChange[avID] = std::chrono::system_clock::now();

	if (!wasDying && this->IsDying())
	{
		auto id = this->GetID();
		auto guid = this->GetGUID();
		auto rid = reason ? reason->GetID() : ~0;
		g.timers->SetTimer(std::chrono::system_clock::now() + 1ms, [=] {
			auto player = g.plManager->GetPlayer(guid);
			if (player && player->GetID() == id)
			{
				if (reason != nullptr)	
					pImpl->killerID = rid;
				else
					pImpl->killerID = nonstd::nullopt;
				g.gamemode->OnPlayerDying(id, pImpl->killerID);
				player->SetCurrentFurniture(0);
			}
		});
	}
}

Player::AVData Player::GetAVData(const std::string &avName) const noexcept
{
	auto &ref = pImpl->actorValues[avName];
	return { ref.base, ref.modifier, ref.percentage };
}

size_t Player::GetNumAVChanges() const noexcept
{
	return pImpl->numAvChanges;
}

void Player::InjectAVChange(uint8_t avID, float newPercentage) noexcept
{
	uint8_t avIDRate = ActorValueArray::InvalidAV;
	uint8_t avIDRateMult = ActorValueArray::InvalidAV;
	switch (avID)
	{
	case ActorValueArray::Health:
		avIDRate = ActorValueArray::HealRate;
		avIDRateMult = ActorValueArray::HealRateMult;
		break;
	case ActorValueArray::Magicka:
		avIDRate = ActorValueArray::MagickaRate;
		avIDRateMult = ActorValueArray::MagickaRateMult;
		break;
	case ActorValueArray::Stamina:
		avIDRate = ActorValueArray::StaminaRate;
		avIDRateMult = ActorValueArray::StaminaRateMult;
		break;
	}
	switch (avID)
	{
	case ActorValueArray::Stamina:
	case ActorValueArray::Health:
	case ActorValueArray::Magicka:
	{
		if (newPercentage < 0 || newPercentage > 1 || newPercentage != newPercentage)
			return;
		auto avName = ActorValueArray::GetAVName(avID);
		const auto now = std::chrono::system_clock::now();
		const auto timePass = std::chrono::duration_cast<std::chrono::milliseconds>(now - pImpl->lastAVChange[avID]);
		const auto timePassMs = timePass.count();
		const auto rate = GetAVData(ActorValueArray::GetAVName(avIDRate)).GetAfterBuffs();
		const auto rateMult = GetAVData(ActorValueArray::GetAVName(avIDRateMult)).GetAfterBuffs();
		const float maxChange1s = rateMult / 100 * rate;
		const float maxChange = maxChange1s * (timePassMs / 1000.0f);
		float change = (newPercentage - GetAVData(avName).percentage) * (GetAVData(avName).GetAfterBuffs());
		if (change > maxChange)
			change = maxChange;

		auto data = GetAVData(avName);

		if (change > 0)
			data.RestoreCurrent(change);
		else
			data.DamageCurrent(-change);

		SetAVData(avName, data);

		auto &actorValues = this->GetPlayerInfo(this->GetID()).actorValues;
		if (actorValues != nonstd::nullopt)
			actorValues->operator[](avName).percentage = newPercentage;

		pImpl->lastAVChange[avID] = now;
		break;
	}
	default:
		log(Log::Warning, "Attempt to change AV from client (", this->GetName().data(), "[", this->GetID(), "], AV", (int32_t)avID, ")");
		break;
	}
}

void Player::UpdateStreamedPlayersList() noexcept
{
	const auto myMovement = this->GetLastInjectedMovement();
	if (myMovement == nullptr)
		return;
	const auto myPos = myMovement->pos;

	const auto streamDistance = Player::GetStreamDistance();

	auto &texts = g.t3dManager->GetText3Ds();
	for (auto it = texts.begin(); it != texts.end(); ++it)
	{
		auto &text = it->second;
		if (text == nullptr)
			continue;

		Text3DInfo inf;
		try {
			inf = pImpl->text3DsInfo.at(text->GetID());
		}
		catch (...) {
			inf = {};
		}

		if (this->IsSpawned()
			&& (text->GetPos() - myPos).Length() <= streamDistance
			&& this->GetLocation() == text->GetLocationID()
			&& (this->GetVirtualWorld() == text->GetVirtualWorld())
			&& inf.pos == text->GetPos()
			&& inf.text == text->GetText())
			this->StreamIn(&*text);
		else
			this->StreamOut(&*text);
	}

	/*Streamer::GetSingleton().Disconnect(this->GetID());
	Streamer::GetSingleton().Connect(this->GetID(), this->IsNPC());
	Streamer::GetSingleton().SetLocation(this->GetID(), this->GetLocation());
	Streamer::GetSingleton().SetPos(this->GetID(), this->GetMovement().pos);
	Streamer::GetSingleton().SetVirtualWorld(this->GetID(), this->GetVirtualWorld());*/
}

bool Player::IsDying() const noexcept {
	return pImpl->actorValues[ActorValueArray::Health].percentage <= 0;
}

void Player::EndDeath() noexcept
{
	if (this->IsDying())
	{
		g.gamemode->OnPlayerDeath(this->GetID(), pImpl->killerID);

		this->SetLoading(true);
		this->SetLoading(false);

		auto avs = {
			"Health",
			"Magicka",
			"Stamina"
		};
		for (auto av : avs)
		{
			auto dat = this->GetAVData(av);
			dat.percentage = 1.0f;
			this->SetAVData(av, dat);
		}
	}
}

void Player::LearnItemTypeEffect(ItemTypeWrap itemType, uint32_t effectIdx, bool learn) noexcept
{
	this->SendItemTypeInfo(itemType);

	RakNet::BitStream bsOut;
	bsOut.Write(ID_LEARN_EFFECT);
	bsOut.Write((uint32_t)IDAssigner<ItemTypeWrap>::GetID(itemType));
	bsOut.Write((uint32_t)effectIdx);
	bsOut.Write(learn);
	g.networking->SendToPlayer(this, &bsOut, RELIABLE, HIGH_PRIORITY);
}

bool Player::IsDataSearchStarted() const noexcept
{
	return pImpl->dataSearch;
}

void Player::InjectEquipUnequip(Container::Item item, int32_t handID, bool equip) noexcept
{
	if (equip)
		this->Equip(item, handID);
	else
		this->Unequip(item, handID);
	this->GetPlayerInfo(this->GetID()).equipment = pImpl->equipment;
}

void Player::Equip(Container::Item item, int32_t handID) noexcept
{
	if (this->GetItemCount(item) < 1)
		return;

	if (item.itemType.GetClass() == "Armor")
	{
		const auto subcl = item.itemType.GetSubclass();
		pImpl->equipment.SetArmor(subcl, item);

		if (subcl == "Shield")
		{
			pImpl->equipment.SetHand(1, nonstd::nullopt);
			pImpl->magicEquipment.hands[1] = nonstd::nullopt;

			// Unequip two-handed weapons when shield equipped
			if (pImpl->equipment.GetHand(0))
			{
				auto id = pImpl->equipment.GetHand(0)->itemType.GetSubclassID();
				if (id >= 0x09 && id <= 0x0D)
				{
					pImpl->equipment.SetHand(0, nonstd::nullopt);
				}
			}
		}
	}

	if (item.itemType.GetClass() == "Ammo")
		pImpl->equipment.SetAmmo(item);

	if (item.itemType.GetClass() == "Weapon")
	{
		if (handID < 0 || handID > 1)
			handID = this->FindHandToEquipObject(item.itemType.GetIdentifier());

		pImpl->magicEquipment.hands[handID] = nonstd::nullopt;
		pImpl->equipment.SetHand(handID, item);

		// Unequip shield on two-handed or left equip
		const auto clId = pImpl->equipment.GetHand(handID)->itemType.GetSubclassID();
		const bool twoHanded = clId >= 0x09 && clId <= 0x0D;
		if (twoHanded || handID == 1)
		{
			pImpl->equipment.SetArmor("Shield", nonstd::nullopt);
		}
	}
}

bool Player::Unequip(Container::Item item, int32_t handID) noexcept
{
	if (item.itemType.GetClass() == "Armor")
	{
		try {
			if (pImpl->equipment.GetArmor().at(item.itemType.GetSubclass()) == item)
			{
				pImpl->equipment.SetArmor(item.itemType.GetSubclass(), nonstd::nullopt);
				return true;
			}
		}
		catch (...) {
			return false;
		}
		return false;
	}

	if (item.itemType.GetClass() == "Ammo")
	{
		if (pImpl->equipment.GetAmmo() == item)
		{
			pImpl->equipment.SetAmmo(nonstd::nullopt);
			return true;
		}
	}

	if (item.itemType.GetClass() == "Weapon")
	{
		if (handID < 0 || handID > 1)
		{
			for (int32_t i = 0; i <= 1; ++i)
			{
				if (this->Unequip(item, i))
					return true;
			}
			return false;
		}
		else
		{
			if (pImpl->equipment.GetHand(handID) == item)
			{
				pImpl->equipment.SetHand(handID, nonstd::nullopt);
				return true;
			}
		}
	}

	return false;
}

void Player::InjectEquipUnequip(SpellWrap spell, int32_t handID, bool equip) noexcept
{
	if (equip)
		this->Equip(spell, handID);
	else
		this->Unequip(spell, handID);
	this->GetPlayerInfo(this->GetID()).magicEquipment = pImpl->magicEquipment;
}

void Player::Equip(SpellWrap spell, int32_t handID) noexcept
{
	if (this->HasSpell(spell) == false)
		return;

	if (spell.IsEnchantment())
		return;

	if (handID < 0 || handID > 1)
		handID = this->FindHandToEquipObject(spell.GetIdentifier());

	pImpl->equipment.SetHand(handID, nonstd::nullopt);
	pImpl->magicEquipment.hands[handID] = spell;

	if (handID == 1)
		pImpl->equipment.SetArmor("Shield", nonstd::nullopt);
}

bool Player::Unequip(SpellWrap spell, int32_t handID) noexcept
{
	if (handID < 0 || handID > 1)
	{
		for (int32_t i = 0; i <= 1; ++i)
		{
			if (this->Unequip(spell, i))
				return true;
		}
		return false;
	}
	else
	{
		if (pImpl->magicEquipment.hands[handID] == spell)
		{
			pImpl->magicEquipment.hands[handID] = nonstd::nullopt;
			return true;
		}
	}
	return false;
}

nonstd::optional<Container::Item> Player::GetEquippedWeapon(int32_t handID) const noexcept
{
	if (handID >= 0 && handID <= 1)
		return pImpl->equipment.GetHand(handID);
	else
		return nonstd::nullopt;
}

nonstd::optional<Container::Item> Player::GetEquippedArmor(const std::string &subclass) const noexcept
{
	try {
		return pImpl->equipment.GetArmor().at(subclass);
	}
	catch (...) {
		return nonstd::nullopt;
	}
}

Container Player::GetEquippedArmor() const noexcept
{
	return Equipment::ToContainer(pImpl->equipment.GetArmor());
}

nonstd::optional<Container::Item> Player::GetEquippedAmmo() const noexcept
{
	return pImpl->equipment.GetAmmo();
}

nonstd::optional<SpellWrap> Player::GetEquippedSpell(int32_t handID) const noexcept
{
	try {
		return SpellWrap(this->GetEquippedObjectIdentifier(handID));
	}
	catch (...) {
		return nonstd::nullopt;
	}
}

std::string Player::GetEquippedObjectIdentifier(int32_t handID) const noexcept
{
	if (handID >= 0 && handID <= 1)
	{
		if (pImpl->magicEquipment.hands[handID])
			return pImpl->magicEquipment.hands[handID]->GetIdentifier();
		if (pImpl->equipment.GetHand(handID))
			return pImpl->equipment.GetHand(handID)->itemType.GetIdentifier();
	}
	return "";
}

bool Player::IsEquipped(Container::Item item) const noexcept
{
	auto armor = pImpl->equipment.GetArmor();
	return pImpl->equipment.GetAmmo() == item 
		|| pImpl->equipment.GetHand(0) == item 
		|| pImpl->equipment.GetHand(1) == item 
		|| armor[item.itemType.GetSubclass()] == item;
}

bool Player::IsEquipped(ItemTypeWrap itemType) const noexcept
{
	auto armor = pImpl->equipment.GetArmor();
	return (pImpl->equipment.GetAmmo() && pImpl->equipment.GetAmmo()->itemType == itemType)
		|| (pImpl->equipment.GetHand(0) && pImpl->equipment.GetHand(0)->itemType == itemType)
		|| (pImpl->equipment.GetHand(1) && pImpl->equipment.GetHand(1)->itemType == itemType)
		|| (armor[itemType.GetSubclass()] != nonstd::nullopt && armor[itemType.GetSubclass()]->itemType == itemType);
}

bool Player::IsEquipped(SpellWrap spell) const noexcept
{
	return pImpl->magicEquipment.hands[0] == spell ||
		pImpl->magicEquipment.hands[1] == spell;
}

bool Player::IsEquipped(const std::string &equipableIdentifier) const noexcept
{
	try {
		return this->IsEquipped(ItemTypeWrap(equipableIdentifier));
	}
	catch (...) {
	}

	try {
		return this->IsEquipped(SpellWrap(equipableIdentifier));
	}
	catch (...) {
	}

	return false;
}

void Player::InjectBowShot(uint32_t power) noexcept
{
	if (power > 100)
		power = 100;
	const auto weap = this->GetEquippedWeapon(0);
	const auto ammo = this->GetEquippedAmmo();
	const auto weapSubcl = weap ? weap->itemType.GetSubclass() : "";
	if (ammo != nonstd::nullopt && weap != nonstd::nullopt
		&& (weapSubcl == "Bow" || weapSubcl == "Crossbow"))
	{
		for (auto &guid : pImpl->streamedIn)
		{
			RakNet::BitStream bsOut;
			bsOut.Write(ID_PLAYER_BOW_SHOT);
			bsOut.Write(this->GetID());
			bsOut.Write(power);
			auto pl = g.plManager->GetPlayer(guid);
			if (pl != nullptr)
				g.networking->SendToPlayer(pl, &bsOut, RELIABLE, HIGH_PRIORITY);
		}

		g.gamemode->OnPlayerBowShot(this->GetID(), (float)power);
		this->RemoveItem(ammo->itemType, 1);
		this->GetPlayerInfo(this->GetID()).inventory.RemoveItem(ammo->itemType, 1);
	}
	else
	{
		log(Log::Warning, "Bad bow shot ", weapSubcl.data());
	}
}

void Player::InjectSpellRelease(int32_t handID) noexcept
{
	auto spell = this->GetEquippedSpell(handID);
	if (spell != nonstd::nullopt)
	{
		const auto magicka = this->GetAVData("Magicka").GetCurrent();
		const auto cost = spell->GetCost();

		if (magicka < cost)
		{
			log(Log::Warning, "Low magicka (", this->GetName().data(), "[", this->GetID(), "])");
			return;
		}

		auto avDat = this->GetAVData("Magicka");
		avDat.DamageCurrent(cost);
		this->SetAVData("Magicka", avDat);

		for (auto &guid : pImpl->streamedIn)
		{
			RakNet::BitStream bsOut;
			bsOut.Write(ID_PLAYER_SPELL_RELEASE);
			bsOut.Write(this->GetID());
			bsOut.Write(handID);
			auto pl = g.plManager->GetPlayer(guid);
			if (pl != nullptr)
				g.networking->SendToPlayer(pl, &bsOut, RELIABLE, HIGH_PRIORITY);
		}

		++pImpl->allowedMagicHits[spell->GetIdentifier()];
	}
}

void Player::InjectAttack(Player *target, nonstd::optional<ItemTypeWrap> weap, nonstd::optional<SpellWrap> spell, bool isPowerHit) noexcept
{
	auto myMov = this->GetLastInjectedMovement();
	if (myMov == nullptr)
		return;
	const auto myPos = myMov->pos;

	const auto ammo = this->GetEquippedAmmo();

	const bool isShot = myMov->attackState >= 8 && myMov->attackState <= 13
		&& weap != nonstd::nullopt && (weap->GetSubclass() == "Bow" || weap->GetSubclass() == "Crossbow")
		&& ammo != nonstd::nullopt;

	if (!target || target->GetLastInjectedMovement() == nullptr)
		return;

	const auto plPos = target->GetLastInjectedMovement()->pos;
	if (target->IsNPC() && this->IsNPC())
	{
		auto targetHost = g.plManager->GetPlayer(target->GetHostGUIDRaw());
		auto thisHost = g.plManager->GetPlayer(this->GetHostGUIDRaw());
		if (!targetHost || !thisHost)
			return;
		if (!thisHost->IsStreamedIn(targetHost))
			return;
	}
	else if (!this->IsStreamedIn(target))
	{
		return;
	}

	if (!spell && !isShot && (plPos - myPos).Length() > g.cfg->GetValue("Gameplay.activateReach", 256))
		return;

	if (spell && spell->GetCastingType() == "FireAndForget")
	{
		if (pImpl->allowedMagicHits[spell->GetIdentifier()] == 0)
		{
			log(Log::Warning, "Bad spell attack (", this->GetName().data(), "[", this->GetID(), "])");
			return;
		}
		else
		{
			--pImpl->allowedMagicHits[spell->GetIdentifier()];
		}
	}

	const bool callbackResult = g.gamemode->OnPlayerHitPlayer(this->GetID(), target->GetID(),
		weap ? weap->GetIdentifier() : "",
		(ammo && isShot) ? ammo->itemType.GetIdentifier() : "",
		spell ? spell->GetIdentifier() : "");

	if (callbackResult != false)
	{
		pImpl->lastHitTarget = target->GetID();

		if (spell != nonstd::nullopt)
			return target->ApplySpell(*spell, this->GetID());

		if (weap && weap->HasEnchantment())
			target->ApplySpell(weap->GetEnchantment().cast<SpellWrap>(), this->GetID());

		float damage = 0;
		if (weap != nonstd::nullopt)
			damage = weap->GetDamage();
		else
			damage = this->GetAVData("UnarmedDamage").GetAfterBuffs();

		if (ammo && isShot)
			damage += ammo->itemType.GetDamage();

		{
			//Item Armor rating = CEILING[(base armor rating + item quality) ×(1 + 0.4 ×(skill + skill effect) / 100)] ×(1 + unison perk†) ×(1 + Matching Set) ×(1 + armor perk‡)
			//	Shield rating = CEILING[(base shield rating + item quality) ×(1 + 0.4 ×(skill + skill effect) / 100)] ×(1 + unison perk†) ×(1 + Matching Set)
			//	Displayed armor rating = SUM(item armor rating) + shield rating + armor effects
			//	Base damage reduction of 3 % per piece worn = Hidden armor rating of 25 per piece worn, including shields
			//	Damage reduction percentage = displayed armor rating × 0.12 + 3.00 × amount of pieces worn

			auto equippedArmor = target->GetEquippedArmor().GetItems();

			float displayedArmorRating = 0;

			for (auto &pair : equippedArmor)
			{
				auto &item = pair.first;
				float baseRating = item.itemType.GetArmorRating();
				auto skillAVData = target->GetAVData(item.itemType.GetSkillName());
				float skill = skillAVData.GetAfterBuffs();
				float itemArmorRating = baseRating * (1.f + 0.4f * (skill / 100.f));

				if (item.itemType.GetSkillName() != "" && skill != std::numeric_limits<float>::infinity())
					displayedArmorRating += itemArmorRating;
				else
					log(Log::Warning, "Armor skill is invalid (", item.itemType.GetIdentifier().data(), ")");
			}

			float damageReductionPercentage = (displayedArmorRating * 0.12f + 3.f * equippedArmor.size()) * 0.01f;

			damage = damage * (1.f - damageReductionPercentage);
		}

		target->DamageHealth(damage, this);
		this->UpdatePlayer(target);

		if (this->IsWerewolf())
		{
			enum {
				DefaultCombatAnim = 8
			};
			this->InjectAnim(DefaultCombatAnim);
		}

		const auto myID = this->GetID(),
			targetID = target->GetID();
		if (!isShot)
		{
			g.timers->SetTimer(std::chrono::system_clock::now() + 250ms, [=] {
				if (this == g.plManager->GetPlayer(myID) && target == g.plManager->GetPlayer(targetID))
				{
					std::ostringstream ss;
					ss << "Game.TriggerScreenBlood(" << (int32_t)damage / 2 << ")";
					target->SendCommand(Player::CommandType::CDScript, ss.str());

					std::ostringstream ss2;
					uint32_t soundID = 0;
					if (weap)
						soundID = 0x000137D3;
					ss2 << "Sound.Play(Form:" << std::to_string(soundID) << ",Form:20)";
					target->SendCommand(Player::CommandType::CDScript, ss2.str());
				}
			});
		}

		pImpl->lastAttackTarget = target->GetGUID();
		pImpl->lastAttackWeap = weap;
	}
}

void Player::InjectPoisonApply() noexcept
{
	auto target = g.plManager->GetPlayer(pImpl->lastAttackTarget);
	if (target == nullptr)
		return;

	if (target->IsStreamedIn(this) == false)
		return;

	if (pImpl->lastAttackWeap == nonstd::nullopt)
		return;
	const auto &weap = *pImpl->lastAttackWeap;

	const bool noPoison = pImpl->weapsPoison[weap].empty();
	if (noPoison)
		return;

	const auto poison = pImpl->weapsPoison[weap].front();
	pImpl->weapsPoison[weap].pop_front();

	target->CastItem(poison, this);
}

void Player::InjectLevelUp(uint8_t avID) noexcept
{
	if (avID != ActorValueArray::Health && avID != ActorValueArray::Magicka && avID != ActorValueArray::Stamina)
		return;
	const auto avName = ActorValueArray::GetAVName(avID);
	const auto levelUpBase = this->GetAVData("_fXPLevelUpBase").GetAfterBuffs();
	const auto levelUpMult = this->GetAVData("_fXPLevelUpMult").GetAfterBuffs();
	auto exp = this->GetAVData("Experience");
	auto level = this->GetAVData("Level");
	auto perkPoints = this->GetAVData("PerkPoints");
	auto av = this->GetAVData(avName);
	const float requiredExp = levelUpBase + (levelUpMult * level.base);
	if (exp.base < requiredExp)
	{
		log(Log::Warning, "Bad levelup attempt (", this->GetName().data(), "[", this->GetID(), "])");
		return;
	}
	exp.base -= requiredExp;
	level.base += 1;
	perkPoints.base += 1;
	av.base += 10.0f;
	this->SetAVData("Level", level);
	this->SetAVData("PerkPoints", perkPoints);
	this->SetAVData("Experience", exp);
	this->SetAVData(avName, av);

	for (auto avn : { "Health", "Stamina", "Magicka" })
	{
		auto avd = this->GetAVData(avn);
		avd.percentage = 1.0f;
		this->SetAVData(avn, avd);
	}
}

void Player::RunActiveEffects() noexcept
{
	if (pImpl->lastEffectsStep == 0)
		pImpl->lastEffectsStep = clock();
	const auto ms = ((double)clock() - (double)pImpl->lastEffectsStep) / CLOCKS_PER_SEC * 1000.0;
	if (ms > 4)
	{
		std::set<EffectWrap> toDispell;

		const auto &effects = this->GetActiveEffects();
		for (auto &pair : effects)
		{
			auto &effect = pair.first;
			auto &data = pair.second;
			const bool isFF = 
				effect.GetCastingType() == "FireAndForget";

			const auto stepResult = EffectWrap::Step(effect, this->GetID(), data.magnitude, isFF ? 1000 : ms, data.casterID);
			if (stepResult == EffectWrap::StepResult::ActiveEffectsChanged)
				break;

			if (isFF)
				toDispell.insert(effect);
		}
		pImpl->lastEffectsStep = clock();

		for (auto effect : toDispell)
			this->DispellEffect(effect);
	}
}

void Player::SetSilent(bool silent) noexcept
{
	if (this->IsStreamedIn(this)) //-V678
		this->UpdatePlayer(this); //-V678

	if (silent != pImpl->silent)
	{
		pImpl->silent = silent;
		RakNet::BitStream bsOut;
		bsOut.Write(ID_SILENT);
		bsOut.Write(silent);
		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
	}

	if (this->IsStreamedIn(this)) //-V678
		this->UpdatePlayer(this); //-V678
}

void Player::ShowRaceMenu() noexcept
{
	RakNet::BitStream bsOut;
	bsOut.Write(ID_SHOW_RACE_MENU);
	g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
	this->SetWaitingForNewLook(true);
}

void Player::StreamIn(Player *target) noexcept
{
	if (!target)
	{
		log(Log::Warning, "StreamIn() null argument (Player)");
		return;
	}

	auto movement = target->GetLastInjectedMovement();
	if (!movement)
	{
		log(Log::Warning, "StreamIn() unable to stream in player without movement data (", this == target, ")");
		return;
	}

	//this->HosterUpdateFor(target);

	const bool inserted = pImpl->streamedIn.insert(target->GetID()).second;
	if (inserted && target != this)
	{
		if (target->IsNPC())
			pImpl->streamedInNpcs.insert(target->GetID());
		pImpl->isStreamedIn[target->GetID()] = 1;
		this->GetPlayerInfo(target->GetID()).look = target->GetLook();

		RakNet::BitStream bsOut;
		bsOut.Write(ID_PLAYER_CREATE);
		bsOut.Write(target->GetBaseNPC());
		Packet::Serialize(bsOut, target->GetLook());
		bsOut.Write(target->GetBaseNPC());
		bsOut.Write(target->GetID());
		auto md = *movement;
		Packet::Serialize(bsOut, md);
		Packet::Serialize(bsOut, target->GetLook());
		bsOut.Write(target->GetLocation());
		auto &pName = target->GetName();
		bsOut.Write((uint16_t)pName.size());
		for (auto it = pName.begin(); it != pName.end(); ++it)
			bsOut.Write(*it);

		const auto baseNpcID = target->GetBaseNPC();
		bsOut.Write(baseNpcID);

		this->UpdateItemTypes(*target);

		g.networking->SendToPlayer(this, &bsOut, RELIABLE, HIGH_PRIORITY);

		if (target->IsNPC() == false && this->IsNPC() == false)
			log(Log::Debug, "Start streaming ", target->GetName().data(), " to ", this->GetName());
		g.gamemode->OnPlayerStreamInPlayer(this->GetID(), target->GetID());

	}
}
 
void Player::StreamOut(const Player *target) noexcept
{
	if (target == this || !target)
		return;

	if (pImpl->streamedIn.erase(target->GetID()) > 0)
	{
		if (target->IsNPC())
			pImpl->streamedInNpcs.erase(target->GetID());
		pImpl->isStreamedIn[target->GetID()] = 0;
		this->ClearPlayerInfo(target->GetID());

		if (target != this)
		{
			RakNet::BitStream bsOut;
			bsOut.Write(ID_PLAYER_DESTROY);
			bsOut.Write(target->GetID());
			g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);

			if (target->IsNPC() == false && this->IsNPC() == false)
				log(Log::Debug, "Stop streaming ", target->GetName().data(), " to ", this->GetName());
			g.gamemode->OnPlayerStreamOutPlayer(this->GetID(), target->GetID());
		}
	}
}

void Player::StreamIn(Object *target) noexcept
{
	if (!target)
	{
		log(Log::Warning, "StreamIn() null argument (Object)");
		return;
	}

	const auto fid = target->GetFastID();

	if (fid.n >= pImpl->isStreamedInByFid.size())
	{
		pImpl->isStreamedInByFid.resize(fid.n + 100, 0);
	}

	const bool needInsert = (pImpl->isStreamedInByFid[fid.n] == 0);
	if (needInsert)
	{
		pImpl->isStreamedInByFid[fid.n] = 1;

		const auto id = target->GetID();
		auto &inf = pImpl->objectsInfo[id];
		inf = {};
		inf.pos = target->GetPos();
		inf.rot = target->GetRot();
		inf.locationID = target->GetLocation();

		RakNet::BitStream bsOut;
		bsOut.Write(ID_OBJECT_CREATE);
		bsOut.Write(target->GetID());
		bsOut.Write(target->IsNative());
		bsOut.Write(target->GetBaseFormID());
		bsOut.Write(target->GetLocation());
		const auto p = target->GetPos();
		bsOut.Write(p.x);
		bsOut.Write(p.y);
		bsOut.Write(p.z);
		const auto r = target->GetRot();
		bsOut.Write(r.x);
		bsOut.Write(r.y);
		bsOut.Write(r.z);

		this->UpdateItemTypes(*target);

		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
		g.gamemode->OnPlayerStreamInObject(this->GetID(), target->GetID());

		target->SetStreamedFor(this, true);
	}
}

void Player::StreamOut(Object *target) noexcept
{
	if (!target)
	{
		log(Log::Warning, "StreamOut() null argument (Player)");
		return;
	}

	const auto fid = target->GetFastID();

	if (fid.n >= pImpl->isStreamedInByFid.size())
	{
		pImpl->isStreamedInByFid.resize(fid.n + 100, 0);
	}

	const bool needErase = (pImpl->isStreamedInByFid[fid.n] == 1);
	if (needErase)
	{
		pImpl->isStreamedInByFid[fid.n] = 0;
		target->SetStreamedFor(this, false);

		pImpl->objectsInfo.erase(target->GetID());

		RakNet::BitStream bsOut;
		bsOut.Write(ID_OBJECT_DESTROY);
		bsOut.Write(target->GetID());
		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);

		g.gamemode->OnPlayerStreamOutObject(this->GetID(), target->GetID());

		if (target->GetHostGUID() == this->GetGUID())
			target->SetHost(nullptr);
	}
}

void Player::StreamIn(const Text3D *target) noexcept
{
	if (!target)
	{
		log(Log::Warning, "StreamIn() null argument (Text3D)");
		return;
	}

	const bool inserted = pImpl->streamedInText3Ds.insert(target->GetID()).second;
	if (inserted)
	{
		RakNet::BitStream bsOut;
		bsOut.Write(ID_TEXT_CREATE);
		bsOut.Write(target->GetID());
		bsOut.Write(target->GetPos().x);
		bsOut.Write(target->GetPos().y);
		bsOut.Write(target->GetPos().z);
		auto txt = target->GetText();
		bsOut.Write((uint32_t)txt.size());
		for (auto wch : txt)
			bsOut.Write(wch);
		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
	}
}

void Player::StreamOut(Text3D *target) noexcept
{
	if (!target)
	{
		log(Log::Warning, "StreamOut() null argument (Text3D)");
		return;
	}

	if (pImpl->streamedInText3Ds.erase(target->GetID()) > 0)
	{
		pImpl->text3DsInfo.erase(target->GetID());

		RakNet::BitStream bsOut;
		bsOut.Write(ID_TEXT_DESTROY);
		bsOut.Write(target->GetID());
		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
	}

	pImpl->text3DsInfo[target->GetID()].pos = target->GetPos();
	pImpl->text3DsInfo[target->GetID()].text = target->GetText();
}


void Player::StreamOutAllObjects() noexcept
{
	for (FastID fid = { 0 }; fid.n != pImpl->isStreamedInByFid.size(); ++fid.n)
	{
		auto object = g.objManager->LookupObject(fid);
		if (!object)
			break;
		this->StreamOut(object);
	}
}

bool Player::IsStreamedIn(const Object *target) const noexcept
{
	if (target == nullptr)
	{
		log(Log::Warning, "IsStreamedIn() null argument (Object)");
		return false;
	}
	try {
		return pImpl->isStreamedInByFid.at(target->GetFastID().n);
	}
	catch (...) {
		return 0;
	}
}

bool Player::IsStreamedIn(const Player *target) const noexcept
{
	if (target == nullptr)
	{
		log(Log::Warning, "IsStreamedIn() null argument (Player)");
		return false;
	}
	return pImpl->isStreamedIn[target->GetID()] || target == this;
}

bool Player::IsStreamedIn(const Text3D *target) const noexcept
{
	if (target == nullptr)
	{
		log(Log::Warning, "IsStreamedIn() null argument (Text3D)");
		return false;
	}
	return pImpl->streamedInText3Ds.find(target->GetID()) != pImpl->streamedInText3Ds.end();
}

bool Player::HostStartAttempt(Object *obj) noexcept
{
	auto movement = this->GetLastInjectedMovement();
	if (movement)
	{
		if (obj->GetType() == Object::Type::Item
			&& (obj->GetPos() - movement->pos).Length() < 4096
			&& obj->GetVirtualWorld() == this->GetVirtualWorld()
			&& obj->GetLocation() == this->GetLocation())
		{
			if (obj->GetHostGUID() == RakNet::UNASSIGNED_RAKNET_GUID)
			{
				obj->SetHost(this);
				return true;
			}
		}
	}
	return false;
}

void Player::UseTeleportDoor(const Object *object) noexcept
{
	if (object == nullptr)
		return;

	const auto baseFormID = object->GetBaseFormID();
	switch (baseFormID)
	{
	case 0x31897:
	case 0x351EB:
	case 0x180D8:
	{
		auto target = g.objManager->LookupObject(object->GetTeleportTarget());
		if (target)
		{
			auto pos = target->GetPos();
			uint16_t a = (uint16_t)target->GetRot().z;
			this->MoveTo(target->GetLocation(), pos, a);
			this->MoveTo({}, {}, a);
			auto mov = this->GetLastInjectedMovement();
			if (mov != nullptr)
			{
				auto newMov = *mov;
				newMov.pos = pos;
				this->InjectMovement(newMov);
			}
			this->SetLocation(target->GetLocation());
		}
		break;
	}
	default:
	{
		//player->MoveTo(nonstd::nullopt, object->GetPos(), nonstd::nullopt);
		auto id = object->GetID();
		//this->SetCurrentFurniture(id);
		auto plGuid = this->GetGUID();
		g.timers->SetTimer(std::chrono::system_clock::now() + 1s, [id, plGuid] {
			auto player = g.plManager->GetPlayer(plGuid);
			auto object = g.objManager->LookupObject(id);
			if (!player || !object)
				return;
			{
				const uint32_t targetID = object->GetTeleportTarget();
				auto target = g.objManager->LookupObject(targetID);
				if (target != nullptr)
				{
					player->SetLocation(target->GetLocation());
					auto movement = player->GetLastInjectedMovement();
					if (movement != nullptr)
					{
						auto newMovement = *movement;
						newMovement.pos = target->GetPos();
						player->InjectMovement(newMovement);
					}
				}
			}
		});
	}
	}
}

void Player::EnableMovementSync(bool e) noexcept
{
	pImpl->movementSync = e;
}

bool Player::IsMovementSyncEnabled() const noexcept
{
	return pImpl->movementSync;
}

void Player::SetWerewolf(bool ww) noexcept
{
	pImpl->isWerewolf = ww;
}

bool Player::IsWerewolf() const noexcept
{
	return pImpl->isWerewolf;
}

bool Player::IsNPC() const noexcept
{
	return pImpl->isNpc;
}

void Player::SetBaseNPC(uint32_t v) noexcept
{
	if (this->IsNPC())
		pImpl->baseNpc = v;
}

uint32_t Player::GetBaseNPC() const noexcept
{
	return pImpl->baseNpc;
}

void Player::SetAggressive(bool aggressive) noexcept
{
	if (this->IsNPC())
		pImpl->combatTarget = aggressive ? 0 : ~0;
}

Player *Player::GetCombatTarget() const noexcept
{
	return g.plManager->GetPlayer(pImpl->lastHitTarget);
}

void Player::ClearCombatTarget() noexcept
{
	pImpl->combatTarget = ~0;
}

void Player::SetForegroundHost(Player *host) noexcept
{
	if (host != nullptr)
		pImpl->foreHostGUID = host->GetGUID();
	else
		pImpl->foreHostGUID = RakNet::UNASSIGNED_RAKNET_GUID;
}

void Player::SendClientMessage(const std::string &message, bool isNotification) noexcept
{
	std::shared_ptr<RakNet::BitStream> bsOut(new RakNet::BitStream);
	bsOut->Write(ID_MESSAGE);
	bsOut->Write((uint16_t)message.size());
	for (auto it = message.begin(); it != message.end(); ++it)
		bsOut->Write(*it);
	bsOut->Write((uint8_t)isNotification);

	g.networking->SendToPlayer(this, bsOut.get(), RELIABLE_ORDERED, LOW_PRIORITY);
}

void Player::SetChatBubble(const std::string &message, uint32_t ms, bool showToSelf) noexcept
{
	std::shared_ptr<RakNet::BitStream> bsOut(new RakNet::BitStream);
	bsOut->Write(ID_CHAT_BUBBLE);
	bsOut->Write((uint16_t)message.size());
	for (auto it = message.begin(); it != message.end(); ++it)
		bsOut->Write(*it);
	bsOut->Write(this->GetID());
	bsOut->Write((uint32_t)ms);

	for (auto &id : pImpl->streamedIn)
	{
		if (id != this->GetID() || showToSelf)
		{
			auto pl = g.plManager->GetPlayer(id);
			if (pl != nullptr)
				g.networking->SendToPlayer(pl, bsOut.get(), RELIABLE, LOW_PRIORITY);
		}
	}
	if(showToSelf)
		g.networking->SendToPlayer(this, bsOut.get(), RELIABLE, LOW_PRIORITY);
}

void Player::MoveTo(nonstd::optional<uint32_t> cellOrWorldSpace, nonstd::optional<NiPoint3> pos, nonstd::optional<uint16_t> angleZ) noexcept
{
	if (cellOrWorldSpace == nonstd::nullopt && pos == nonstd::nullopt && angleZ == nonstd::nullopt)
	{
		log(Log::Warning, "MoveTo() null argument");
		return;
	}

	if (this->IsNPC() == false)
	{
		this->EndDeath();

		std::shared_ptr<RakNet::BitStream> bsOut(new RakNet::BitStream);
		bsOut->Write(ID_MOVE_TO);
		if (pos == nonstd::nullopt)
			pos = NiPoint3{ std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity() };
		bsOut->Write(pos->x);
		bsOut->Write(pos->y);
		bsOut->Write(pos->z);
		if (angleZ == nonstd::nullopt)
			angleZ = ~0;
		bsOut->Write(*angleZ);
		if (cellOrWorldSpace == nonstd::nullopt)
			cellOrWorldSpace = 0;
		bsOut->Write(*cellOrWorldSpace);
		g.networking->SendToPlayer(this, bsOut.get(), RELIABLE_ORDERED, MEDIUM_PRIORITY);
	}
	else
	{
		const bool isSpawned = this->IsSpawned();
		Packet::MovementData receivedData;
		if (pos != nonstd::nullopt)
			receivedData.pos = *pos;
		if (angleZ != nonstd::nullopt)
			receivedData.angleZ = *angleZ;

		g.gamemode->OnPlayerUpdate(this->GetID());

		{
			//this->UpdateStreamedPlayersList();
			this->InjectMovement(receivedData, true);

			if (!isSpawned && this->IsSpawned())
			{
				g.gamemode->OnPlayerSpawn(this->GetID());
			}
		}
	}
}

void Player::SetWeather(uint32_t wTypeOrID) noexcept
{
	RakNet::BitStream bsOut;
	bsOut.Write(ID_WEATHER);
	bsOut.Write(wTypeOrID);
	g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
}

void Player::SetGlovalVariable(uint32_t globalID, float value) noexcept
{
	RakNet::BitStream bsOut;
	bsOut.Write(ID_GLOBAL_VARIABLE);
	bsOut.Write(globalID);
	bsOut.Write(value);
	g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
}

void Player::SendCommand(CommandType cmdType, const std::string &cmdText) noexcept
{
	RakNet::BitStream bsOut;
	bsOut.Write(ID_COMMAND);
	bsOut.Write(cmdType);
	for (auto ch : cmdText)
		bsOut.Write(ch);
	g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
}

void Player::StartDataSearch() noexcept
{
	RakNet::BitStream bsOut;
	bsOut.Write(ID_DATASEARCH_INIT);
	pImpl->dataSearch = true;
	g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
}

void Player::SetDSOpcodeEnabled(DataSearch::Opcode opcode, bool enabled) noexcept
{
	if (enabled)
		pImpl->enabledOpcodes.insert(opcode);
	else
		pImpl->enabledOpcodes.erase(opcode);
}

bool Player::IsDSOpcodeEnabled(DataSearch::Opcode opcode) const noexcept
{
	return pImpl->enabledOpcodes.count(opcode) != 0;
}

void Player::UpdatePlayer(const Player *source) noexcept {
	if (source && this->IsStreamedIn(source))
	{
		auto &inf = this->GetPlayerInfo(source->GetID());

		this->SendPlayerAVs(source, inf);

		if(this == source || rand() % 5 == 0)
		{
			this->SendPlayerName(source, inf);
			this->SendPlayerFurniture(source, inf);
			this->SendPlayerPause(source, inf);
			this->SendPlayerLook(source, inf);

			if (this == source)
			{
				if (this->SendPlayerInventory(source, inf) == false)
					this->SendPlayerEquipment(source, inf);
			}
			else
			{
				this->SendPlayerEquipment(source, inf);
			}

			if (this == source)
			{
				if (this->SendPlayerSpellList(source, inf) == false)
				{
					this->SendPlayerMagicEquipment(source, inf);
					this->SendPlayerActiveEffects(source, inf);
				}
			}
			else
			{
				this->SendPlayerMagicEquipment(source, inf);
				this->SendPlayerActiveEffects(source, inf);
			}
			this->SendPlayerHost(source, inf);
		}
	}
}

PlayerInfo &Player::GetPlayerInfo(uint16_t id) noexcept
{
	if (pImpl->plInfo[id] == nullptr)
	{
		pImpl->plInfo[id].reset(new PlayerInfo);
	}
	return *pImpl->plInfo[id];
}

void Player::ClearPlayerInfo(uint16_t id) noexcept
{
	pImpl->plInfo[id].reset();
}

void Player::SendPlayerName(const Player *player, PlayerInfo &inf) noexcept
{
	const uint16_t id = player->GetID();
	const auto &pName = player->GetName();
	if (inf.name != pName)
	{
		inf.name = pName;
		RakNet::BitStream bsOut;
		bsOut.Write(ID_PLAYER_NAME);
		bsOut.Write(id);
		bsOut.Write((uint16_t)pName.size());
		for (auto it = pName.begin(); it != pName.end(); ++it)
			bsOut.Write(*it);

		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
	}
}

void Player::SendPlayerFurniture(const Player *player, PlayerInfo &inf) noexcept
{
	const uint16_t id = player->GetID();
	const auto furniture = player->GetCurrentFurniture();
	if (inf.furniture != furniture)
	{
		inf.furniture = furniture;
		RakNet::BitStream bsOut;
		bsOut.Write(ID_PLAYER_FURNITURE);
		bsOut.Write(id);
		bsOut.Write(furniture);
		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
	}
}

void Player::SendPlayerPause(const Player *player, PlayerInfo &inf) noexcept
{
	const uint16_t id = player->GetID();
	const bool isPaused = player->IsPaused();
	if (inf.isPaused != isPaused)
	{
		inf.isPaused = isPaused;
		RakNet::BitStream bsOut;
		bsOut.Write(ID_PLAYER_PAUSE);
		bsOut.Write(id);
		bsOut.Write(isPaused);

		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
	}
}

void Player::SendPlayerLook(const Player *player, PlayerInfo &inf) noexcept
{
	const uint16_t id = player->GetID();
	if ((clock() - inf.lastLookSend) / (double)CLOCKS_PER_SEC > 0.5 || this == player)
	{
		inf.lastLookSend = clock();
		const auto &look = player->GetLook();
		if (inf.numLookChanges != player->GetNumLookChanges()
			|| inf.isWerewolf != player->IsWerewolf())
		{
			if (inf.look != look
				|| inf.isWerewolf != player->IsWerewolf())
			{
				inf.look = look;
				inf.isWerewolf = player->IsWerewolf();
				inf.numLookChanges = player->GetNumLookChanges();

				RakNet::BitStream bsOut;
				bsOut.Write(ID_PLAYER_LOOK);
				bsOut.Write(id);
				Packet::Serialize(bsOut, look);
				bsOut.Write((uint8_t)player->IsWerewolf());

				g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
			}
		}
	}
}

bool Player::SendPlayerInventory(const Player *player, PlayerInfo &inf) noexcept
{
	const uint16_t id = player->GetID();

	bool itemTypesChanged = false;
	for (auto it = pImpl->itemTypesInfo.begin(); it != pImpl->itemTypesInfo.end(); ++it)
	{
		const auto uniquePtr = it->first;
		//const auto itemType = ItemTypeWrap::LookupByUniquePtr(uniquePtr);
		const auto &info = ItemTypeWrap::LookupInfoByUniquePtr(uniquePtr);

		if (ItemTypeWrap::GetNumInfoChangesByUniquePtr(uniquePtr) != it->second.numChanges
			&& info != it->second.info)
		{
			itemTypesChanged = true;
			break;
		}
	}

	if (itemTypesChanged == false && player->GetNumInventoryChanges() == inf.inventory.GetNumInventoryChanges())
	{
		return false;
	}

	const auto size1 = inf.inventory.GetNumItems(),
		size2 = player->GetNumItems();
	const bool needSend = size1 != size2 || inf.inventory.GetItems() != player->GetItems()
		|| itemTypesChanged;
	if (needSend)
	{
		const auto toAdd = player->GetInventoryDifference(inf.inventory),
			toRemove = inf.inventory.GetInventoryDifference(*player);

		auto writePlInventory = [=](RakNet::BitStream *bsOut, bool addOrRemove, const Container::Items &items, uint8_t packetID = ID_PLAYER_INVENTORY) {
			bsOut->Write(packetID);
			bsOut->Write(id);
			bsOut->Write(addOrRemove);
			bsOut->Write((uint32_t)items.size());
			this->UpdateItemTypes(Container(items));
			for (auto it = items.begin(); it != items.end(); ++it)
			{
				const auto itemTypeWrapID = IDAssigner<ItemTypeWrap>::GetID(it->first.itemType);
				bsOut->Write(itemTypeWrapID);
				bsOut->Write(it->second);
			}
		};

		this->UpdateItemTypes(this->GetPlayerInfo(id).inventory);
		this->UpdateItemTypes(*player);

		std::unique_ptr<RakNet::BitStream> bsOutAdd,
			bsOutRemove;

		if (toAdd.GetNumItems() > 0)
		{
			bsOutAdd = std::make_unique<RakNet::BitStream>();
			writePlInventory(bsOutAdd.get(), true, toAdd.GetItems());
		}

		if (toRemove.GetNumItems() > 0)
		{
			bsOutRemove = std::make_unique<RakNet::BitStream>();
			writePlInventory(bsOutRemove.get(), false, toRemove.GetItems());
		}

		if (bsOutAdd != nullptr)
			g.networking->SendToPlayer(this, bsOutAdd.get(), RELIABLE_ORDERED, LOW_PRIORITY);
		if (bsOutRemove != nullptr)
			g.networking->SendToPlayer(this, bsOutRemove.get(), RELIABLE_ORDERED, LOW_PRIORITY);

		inf.inventory = *player;
		return true;
	}
	return false;
}

void Player::SendPlayerAVs(const Player *player, PlayerInfo &inf, bool force) noexcept
{
	const uint16_t id = player->GetID();

	auto &actorValues = player->pImpl->actorValues;

	const bool needSend = ((clock() - inf.lastAvSend) / (double)CLOCKS_PER_SEC > 0.5 || this == player || player->IsDying());

	if (needSend)
	{
		inf.lastAvSend = clock();
		if (inf.numAvChanges != player->GetNumAVChanges())
		{
			if (inf.actorValues != actorValues)
			{
				RakNet::BitStream bsOut;
				bsOut.Write(ID_PLAYER_AV);
				bsOut.Write(id);
				for (uint8_t avID = ActorValueArray::Health; avID != ActorValueArray::NUM_AVS; ++avID)
				{
					if (player != this
						&& avID != ActorValueArray::Health && avID != ActorValueArray::Magicka && avID != ActorValueArray::Stamina)
					{
						continue;
					}
					if (inf.actorValues != nonstd::nullopt)
					{
						if ((*inf.actorValues)[avID] == actorValues[avID] && !force)
						{
							continue;
						}
					}
					static_assert(sizeof(avID) == 1, "");
					bsOut.Write(avID);
					bsOut.Write(actorValues[avID].base);
					bsOut.Write(actorValues[avID].modifier);
					bsOut.Write(actorValues[avID].percentage);
				}

				g.networking->SendToPlayer(this, &bsOut, RELIABLE, LOW_PRIORITY);
				inf.actorValues = actorValues;
				inf.numAvChanges = player->GetNumAVChanges();
			}
		}
	}
}

void Player::SendPlayerEquipment(const Player *player, PlayerInfo &inf) noexcept
{
	auto &equipment = player->pImpl->equipment;

	if (inf.equipment.GetNumChanges() != equipment.GetNumChanges() && inf.equipment != equipment)
	{
		RakNet::BitStream bsOut;
		bsOut.Write(ID_PLAYER_EQUIPMENT);
		bsOut.Write(player->GetID());

		{
			auto itemTypeID = IDAssigner<ItemTypeWrap>::id_t(~0);
			if (equipment.GetAmmo() != nonstd::nullopt)
				itemTypeID = IDAssigner<ItemTypeWrap>::GetID(equipment.GetAmmo()->itemType);
			bsOut.Write(itemTypeID);
		}

		for (int32_t i = 0; i != 2; ++i)
		{
			auto itemTypeID = IDAssigner<ItemTypeWrap>::id_t(~0);
			if (equipment.GetHand(i) != nonstd::nullopt)
				itemTypeID = IDAssigner<ItemTypeWrap>::GetID(equipment.GetHand(i)->itemType);
			bsOut.Write(itemTypeID);
		}

		auto items = Equipment::ToContainer(equipment.GetArmor()).GetItems();
		for (auto &item : items)
			bsOut.Write(IDAssigner<ItemTypeWrap>::GetID(item.first.itemType)); // itemType


		this->UpdateItemTypes(*player);

		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
		inf.equipment = equipment;
	}
}

bool Player::SendPlayerSpellList(const Player *player, PlayerInfo &inf) noexcept
{
	const uint16_t id = player->GetID();

	auto spellListPtr = dynamic_cast<const SpellList *>(player);
	if (!spellListPtr)
		return false;
	auto &spellList = *spellListPtr;
	const auto &spells = spellList.GetSpells();

	if (inf.spellList.GetNumChanges() != spellList.GetNumChanges())
	{
		if (inf.spellList != spellList)
		{
			RakNet::BitStream bsOut;
			bsOut.Write(ID_PLAYER_SPELLLIST);
			bsOut.Write(id);
			for (auto &spell : spells)
				bsOut.Write(IDAssigner<SpellWrap>::GetID(spell));

			this->UpdateSpells(*player);

			g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
			inf.spellList = spellList;
			return true;
		}
	}

	return false;
}

void Player::SendPlayerMagicEquipment(const Player *player, PlayerInfo &inf) noexcept
{
	auto &magicEquipment = player->pImpl->magicEquipment;

	if (inf.magicEquipment != magicEquipment)
	{
		RakNet::BitStream bsOut;
		bsOut.Write(ID_PLAYER_MAGIC_EQUIPMENT);
		const uint16_t id = player->GetID();
		bsOut.Write(id);

		for (int32_t i = 0; i != 2; ++i)
		{
			auto spellID = IDAssigner<SpellWrap>::id_t(~0);
			if (magicEquipment.hands[i] != nonstd::nullopt)
				spellID = IDAssigner<SpellWrap>::GetID(*magicEquipment.hands[i]);
			bsOut.Write(spellID);
		}

		this->UpdateSpells(*player);

		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
		inf.magicEquipment = magicEquipment;
	}
}

void Player::SendPlayerActiveEffects(const Player *player, PlayerInfo &inf) noexcept
{
	/*const uint16_t id = player->GetID();

	auto effects = player->GetActiveEffects();

	if (pImpl->playersInfo[id].activeEffects != effects)
	{
		RakNet::BitStream bsOut;
		bsOut.Write(ID_PLAYER_ACTIVE_EFFECTS);
		bsOut.Write(id);

		bsOut.Write((uint32_t)effects.size());
		for (auto &pair : effects)
		{
			auto &effect = pair.first;
			auto &activeEffectData = pair.second;

			this->SendEffectInfo(effect);

			const auto effectID = IDAssigner<EffectWrap>::GetID(effect);
			bsOut.Write(effectID);

			bsOut.Write(activeEffectData.casterID);
			bsOut.Write(activeEffectData.magnitude);

			const auto durationMs = (int64_t)(activeEffectData.endMoment - std::chrono::system_clock::now()).count() / 1000;
			bsOut.Write(durationMs);
		}

		g.networking->SendToPlayer(this, &bsOut, UNRELIABLE, LOW_PRIORITY);
		pImpl->playersInfo[id].activeEffects = effects;
	}*/
}

void Player::SendPlayerHost(const Player *source, PlayerInfo &inf) noexcept
{
	if (source->IsNPC() == false)
		return;

	auto host = source->GetHostGUIDRaw();

	uint16_t hostID = ~0;
	if (host != RakNet::UNASSIGNED_RAKNET_GUID)
	{
		auto knownHost = g.plManager->GetPlayer(inf.hostID);
		if (knownHost != nullptr && knownHost->GetGUID() == host)
		{
			hostID = inf.hostID;
		}
		else
		{
			auto hostPl = g.plManager->GetPlayer(host);
			if (hostPl != nullptr)
				hostID = hostPl->GetID();
		}
	}

	const uint16_t target = source->pImpl->combatTarget;

	if (inf.hostID != hostID
		|| inf.combatTarget != target)
	{
		RakNet::BitStream bsOut;
		bsOut.Write(ID_PLAYER_HOST);
		bsOut.Write(source->GetID());
		bsOut.Write(hostID);
		bsOut.Write((uint8_t)source->IsNPC());
		bsOut.Write(target);
		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
		inf.hostID = hostID;
		inf.combatTarget = target;
	}
}

void Player::SendObjectState(const Object *object) noexcept
{
	const auto id = object->GetID();
	const auto &pos = object->GetPos();
	const auto &rot = object->GetRot();
	const auto locationID = object->GetLocation();
	const bool hostable = object->IsHostable();
	auto &inf = pImpl->objectsInfo[id];
	if (inf.pos != pos || inf.rot != rot || inf.locationID != locationID || inf.isHostable != hostable)
	{
		inf.pos = pos;
		inf.rot = rot;
		inf.locationID = locationID;
		inf.isHostable = hostable;
		RakNet::BitStream bsOut;
		bsOut.Write(ID_OBJECT_POS_ROT_LOCATION);
		bsOut.Write(id);
		bsOut.Write(pos.x);
		bsOut.Write(pos.y);
		bsOut.Write(pos.z);
		bsOut.Write(rot.x);
		bsOut.Write(rot.y);
		bsOut.Write(rot.z);
		bsOut.Write(locationID);
		bsOut.Write((uint8_t)hostable);
		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
	}
}

void Player::SendObjectName(const Object *object) noexcept
{
	const auto id = object->GetID();
	const auto name = object->GetName();
	if (pImpl->objectsInfo[id].name != name)
	{
		pImpl->objectsInfo[id].name = name;
		RakNet::BitStream bsOut;
		bsOut.Write(ID_OBJECT_NAME);
		bsOut.Write(id);
		bsOut.Write((uint16_t)name.size());
		for (auto it = name.begin(); it != name.end(); ++it)
			bsOut.Write(*it);
		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
	}
}

void Player::SendObjectBehavior(const Object *object) noexcept
{
	const auto id = object->GetID();
	const auto type = object->GetType();
	const auto isOpen = object->IsOpen();
	const auto teleportTarget = object->GetTeleportTarget();
	const auto isDisabled = object->IsDisabled();
	const auto itemsCount = (uint32_t)object->GetNumItems();
	const auto &allItems = object->GetItems();
	uint16_t hostPlayerID = ~0;
	{
		auto hostPlayer = g.plManager->GetPlayer(object->GetHostGUID());
		if (hostPlayer != nullptr)
			hostPlayerID = hostPlayer->GetID();
	} 
	const auto lockLevel = object->GetLockLevel();
	const auto isDestroyed = object->IsDestroyed(this);
	uint32_t itemTypeID = ~0;
	if (itemsCount > 0 && object->GetType() == Object::Type::Item)
		itemTypeID = IDAssigner<ItemTypeWrap>::GetID(object->GetItems().begin()->first.itemType);
	const auto &inf = pImpl->objectsInfo[id];
	if (inf.type != type
		|| inf.isOpen != isOpen
		|| inf.teleportTarget != teleportTarget
		|| inf.isDisabled != isDisabled
		|| inf.itemsCount != itemsCount
		|| inf.itemTypeID != itemTypeID
		|| inf.allItems != allItems
		|| inf.hostPlayerID != hostPlayerID
		|| inf.lockLevel != lockLevel
		|| inf.isDestroyed != isDestroyed)
	{
		RakNet::BitStream bsOut;
		bsOut.Write(ID_OBJECT_BEHAVIOR);
		bsOut.Write(id);
		bsOut.Write(type);
		bsOut.Write(isOpen);
		bsOut.Write(teleportTarget);
		bsOut.Write(isDisabled);
		bsOut.Write(itemsCount);
		bsOut.Write(itemTypeID);
		bsOut.Write(hostPlayerID);
		bsOut.Write(lockLevel);
		bsOut.Write(isDestroyed);

		if (type == Object::Type::Container)
		{
			const auto &items = object->GetItems();

			const auto toAdd = object->GetInventoryDifference(Container(pImpl->objectsInfo[id].allItems)),
				toRemove = Container(pImpl->objectsInfo[id].allItems).GetInventoryDifference(*object);

			WriteNonPlayerContainer(bsOut, toAdd);
			WriteNonPlayerContainer(bsOut, toRemove);
		}

		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);

		auto &info = pImpl->objectsInfo[id];
		info.type = type;
		info.isOpen = isOpen;
		info.teleportTarget = teleportTarget;
		info.isDisabled = isDisabled;
		info.itemsCount = (uint32_t)itemsCount;
		info.itemTypeID = itemTypeID;
		info.allItems = allItems;
		info.hostPlayerID = hostPlayerID;
		info.lockLevel = lockLevel;
		info.isDestroyed = isDestroyed;
	}
}

void Player::SendObjectKeywords(const Object *o) noexcept
{
	if (o->GetType() != Object::Type::Furniture)
		return;
	const auto id = o->GetID();
	const auto kwrds = o->GetKeywords();
	if (pImpl->objectsInfo[id].keywords.GetKeywords() != kwrds)
	{
		RakNet::BitStream bsOut;
		bsOut.Write(ID_OBJECT_KEYWORDS);
		bsOut.Write(id);
		bsOut.Write(o->GetNumKeywords());
		for (auto keyword : kwrds)
		{
			bsOut.Write((uint32_t)keyword.GetID());

			if (pImpl->knownKeywords.find(keyword) == pImpl->knownKeywords.end())
			{
				this->RegisterKeyword(keyword);
				return;
			}
		}
		pImpl->objectsInfo[id].keywords = { o->GetKeywords() };
		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, LOW_PRIORITY);
	}
}

void Player::SendObjectRecipes() noexcept // too fat
{
	if (pImpl->lastRecipeUpdate + (this->IsNPC() ? 10000ms : 1000ms) > std::chrono::system_clock::now())
		return;
	pImpl->lastRecipeUpdate = std::chrono::system_clock::now();

	const auto recipes = RecipeWrap::GetAllRecipes();
	for (auto &r : recipes)
		this->SendRecipeInfo(r);
}

template <class T>
std::list<EffectWrap> SerializeEffects(RakNet::BitStream &bsOut, const T &source)
{
	std::list<EffectWrap> effects;

	bsOut.Write((uint32_t)source.GetNumEffects());
	for (uint32_t n = 1; n <= source.GetNumEffects(); ++n)
	{
		const auto effect = EffectWrap(source.GetNthEffectIdentifier(n));
		const auto effectID = IDAssigner<EffectWrap>::GetID(effect);
		effects.push_back(effect);

		bsOut.Write(effectID);
		bsOut.Write(source.GetNthEffectMagnitude(n));
		bsOut.Write(source.GetNthEffectDuration(n));
		bsOut.Write(source.GetNthEffectArea(n));
	}

	return effects;
}

void Player::SendItemTypeInfo(const ItemTypeWrap &itemType) noexcept
{
	const nonstd::optional<ItemTypeInfo> info = itemType.GetInfo();

	nonstd::optional<ItemTypeInfo> knownInfo;
	try {
		knownInfo = pImpl->itemTypesInfo.at(itemType.GetUniquePtr()).info;
	}
	catch (...) {
		knownInfo = nonstd::nullopt;
	}

	if (info != knownInfo)
	{
		RakNet::BitStream bsOut;
		bsOut.Write(ID_ITEMTYPES);
		const auto itemTypeID = IDAssigner<ItemTypeWrap>::GetID(itemType);
		bsOut.Write(itemTypeID);
		bsOut.Write((uint8_t)itemType.GetClassID());
		bsOut.Write((uint8_t)itemType.GetSubclassID());
		bsOut.Write((uint32_t)itemType.GetExistingGameFormID());
		bsOut.Write((float)itemType.GetWeight());
		bsOut.Write((uint32_t)itemType.GetGoldValue());
		bsOut.Write((float)itemType.GetArmorRating());
		bsOut.Write((float)itemType.GetDamage());

		// Legacy
		const auto equipSlot = 0;
		bsOut.Write((uint32_t)equipSlot);

		const auto effects = SerializeEffects(bsOut, itemType);
		for (auto &effect : effects)
			this->SendEffectInfo(effect);

		IDAssigner<SpellWrap>::id_t enchID = ~0;
		if (info->ench != nonstd::nullopt)
		{
			enchID = IDAssigner<SpellWrap>::GetID(*info->ench);
			this->SendSpellInfo(*info->ench);
		}
		bsOut.Write(enchID);

		bsOut.Write((int32_t)itemType.GetSoulSize());
		bsOut.Write((int32_t)itemType.GetCapacity());
		bsOut.Write((float)itemType.GetHealth());

		auto *knownItemType = &pImpl->itemTypesInfo[itemType.GetUniquePtr()];

		knownItemType->info = itemType.GetInfo();
		knownItemType->numChanges = itemType.GetNumInfoChanges();
		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, HIGH_PRIORITY);
	}
}

void Player::SendSpellInfo(const SpellWrap &spell) noexcept
{
	const nonstd::optional<SpellInfo> info = SpellInfo::Get(spell);

	nonstd::optional<SpellInfo> knownInfo;
	try {
		knownInfo = pImpl->spellsInfo.at(spell);
	}
	catch (...) {
		knownInfo = nonstd::nullopt;
	}

	try {
		if (knownInfo != info)
		{
			RakNet::BitStream bsOut;
			bsOut.Write(ID_SPELL);
			bsOut.Write((uint8_t)spell.IsEnchantment());
			bsOut.Write((uint8_t)(Impl::customEnchs.count(spell) != 0));
			const auto spellID = IDAssigner<SpellWrap>::GetID(spell);
			bsOut.Write(spellID);
			bsOut.Write(spell.GetExistingGameFormID());

			const auto effects = SerializeEffects(bsOut, spell);
			for (auto &effect : effects)
				this->SendEffectInfo(effect);

			bsOut.Write((float)spell.GetCost());

			pImpl->spellsInfo[spell] = SpellInfo::Get(spell);
			g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, HIGH_PRIORITY);
		}
	}
	catch (...) {
		log(Log::Warning, "Failed to send spell info");
	}
}

void Player::SendEffectInfo(const EffectWrap &effect) noexcept
{
	const nonstd::optional<EffectInfo> info = EffectInfo::Get(effect);

	nonstd::optional<EffectInfo> knownInfo;
	try {
		knownInfo = pImpl->effectsInfo.at(effect);
	}
	catch (...) {
		knownInfo = nonstd::nullopt;
	}

	const bool stupidClient = true; // Client forgets effects (it's a bug)

	if (stupidClient || info != knownInfo)
	{
		RakNet::BitStream bsOut;
		bsOut.Write(ID_EFFECT);

		const auto effectID = IDAssigner<EffectWrap>::GetID(effect);
		bsOut.Write(effectID);
		bsOut.Write(info->existingGameFormID);
		bsOut.Write(info->archetype);
		bsOut.Write(info->castingType);
		bsOut.Write(info->delivery);

		pImpl->effectsInfo[effect] = EffectInfo::Get(effect);
		g.networking->SendToPlayer(this, &bsOut, RELIABLE_ORDERED, HIGH_PRIORITY);
	}
}
bool Player::RegisterKeyword(const Components::Keyword &keyword) noexcept
{
	if (pImpl->knownKeywords.find(keyword) == pImpl->knownKeywords.end())
	{
		const std::string str = "<RegisterKeyword> " + keyword.GetString() + ' ' + std::to_string(keyword.GetID());
		this->SendClientMessage((str), false);
		pImpl->knownKeywords.insert(keyword);
		return true;
	}
	return false;
}

void Player::SendRecipeInfo(const RecipeWrap &recipe) noexcept
{
	const auto id = recipe.GetID();
	const auto info = RecipeInfo::Get(recipe);

	if (pImpl->recipesInfo[id] != info)
	{
		std::shared_ptr<RakNet::BitStream> bsOutPtr(new RakNet::BitStream);
		auto &bsOut = *bsOutPtr;
		bsOut.Write(ID_RECIPE);
		bsOut.Write(id);

		const uint32_t produceID = IDAssigner<ItemTypeWrap>::GetID(*info.produce);
		bsOut.Write(produceID);

		this->SendItemTypeInfo(*info.produce);

		bsOut.Write(info.produceCount);

		this->RegisterKeyword(info.wbKeyword);
		{
			const auto keywordID = info.wbKeyword.GetID();
			bsOut.Write(keywordID);

			const auto cont = recipe.GetContainer();
			this->WriteNonPlayerContainer(bsOut, cont);

			g.networking->SendToPlayer(this, bsOutPtr.get(), RELIABLE_ORDERED, LOW_PRIORITY);
			//pImpl->recipesInfo[id] = info;
		}
	}
}


namespace Player_RegisterAnimation
{
	struct AnimationEvent
	{
		std::string aeName;

		friend bool operator <(const AnimationEvent &lhs, const AnimationEvent &rhs) {
			return lhs.aeName < rhs.aeName;
		}
	};
}

uint32_t Player::RegisterAnimation(const std::string &aeName) noexcept
{
	using namespace Player_RegisterAnimation;

	const AnimationEvent ae = { aeName };

	const uint32_t aeID = IDAssigner<AnimationEvent>::GetID(ae);

	if (pImpl->animsInfo[aeName] != nonstd::nullopt && *pImpl->animsInfo[aeName] == aeID)
		return aeID;
	pImpl->animsInfo[aeName] = aeID;

	const std::string str = "<RegisterAnim> " + aeName + ' ' + std::to_string(aeID);
	this->SendClientMessage((str), false);

	return aeID;
}

void Player::SendAnimationInfo(uint32_t aeID) noexcept
{
	using namespace Player_RegisterAnimation;

	auto ae = IDAssigner<AnimationEvent>::GetObj(aeID);
	if (ae != nonstd::nullopt)
	{
		const auto result = this->RegisterAnimation(ae->aeName);
		if(result != aeID)
		{
			log(Log::Warning, "SendAnimationInfo() something gone wrong");
		}
	}
	else
	{
		log(Log::Warning, "SendAnimationInfo() unknown animation ID ", aeID);
	}
}

void Player::UpdateItemTypes(const Container &itemTypesSource) noexcept
{
	auto items = itemTypesSource.GetItems();
	for (const auto &item : items)
		this->SendItemTypeInfo(item.first.itemType);
}

void Player::UpdateSpells(const SpellList &spellsSource) noexcept
{
	auto spells = spellsSource.GetSpells();
	for (const auto &spell : spells)
		this->SendSpellInfo(spell);
}

int32_t Player::FindHandToEquipObject(const std::string &equipableIdentifier) const noexcept
{
	int32_t handID = 0;
	if (pImpl->equipment.GetHand(0) != nonstd::nullopt || pImpl->magicEquipment.hands[0] != nonstd::nullopt)
	{
		if (pImpl->equipment.GetHand(1) == nonstd::nullopt && pImpl->magicEquipment.hands[1] == nonstd::nullopt)
			handID = 1;
	}

	const bool alreadyEquippedRight = this->GetEquippedObjectIdentifier(0) == equipableIdentifier;

	if (handID == 0 && alreadyEquippedRight)
		handID = 1;

	return handID;
}

void Player::HosterUpdateFor(Player *target) noexcept
{
	if (target->IsNPC() && target != this && this->IsNPC() == false)
	{
		if (target->GetHostGUID() == RakNet::UNASSIGNED_RAKNET_GUID)
		{
			target->SetHost(this);
		}
		else
		{
			auto isNumi = [](Player *pl) {
				return pl->GetName() == "Numi" || pl->GetName() == "Manuel" || pl->GetName() == "Manu";
			};
			auto isForegroundHoster = [](Player *pl, Player *npc) {
				return npc->pImpl->foreHostGUID == pl->GetGUID();
			};

			auto hoster = g.plManager->GetPlayer(target->GetHostGUID());
			if (hoster == nullptr)
			{
				log(Log::Warning, "would terminate in 1.0.1");
			}
			auto hosterMov = hoster != nullptr ? hoster->GetLastInjectedMovement() : nullptr;
			auto myMov = this->GetLastInjectedMovement();
			auto targetMov = target->GetLastInjectedMovement();
			if (myMov != nullptr && targetMov != nullptr)
			{
				if (hoster == nullptr
					|| hosterMov == nullptr
					|| hoster->IsSpawned() == false
					|| hoster->IsLoading() == true
					|| hoster->IsPaused() == true
					|| hoster->IsStreamedIn(target) == false
					//|| int64_t((hosterMov->pos - targetMov->pos).Length()) / 70 / 17.5 > int64_t((myMov->pos - targetMov->pos).Length()) / 70 / 17.5
					|| ((hosterMov->pos - targetMov->pos).Length()) > 3500 && ((myMov->pos - targetMov->pos).Length()) < 3500
					|| (NUMI_TEST && isNumi(this) && !isNumi(hoster))
					|| (isForegroundHoster(this, target) && !isForegroundHoster(hoster, target))
					)
				{
					target->SetHost(this);
					g.gamemode->OnPlayerHostPlayer(this->GetID(), target->GetID());
				}
			}
		}
	}
}

void Player::ClearCraftIngredients() noexcept
{
	pImpl->craftIngredients.RemoveAllItems();
}

float Player::GetStreamDistance() noexcept
{
	static float streamDistance = g.cfg->GetValue<float>("Gameplay.streamDistance", 0.0f);

	if (streamDistance <= 0.0f)
	{
		streamDistance = std::numeric_limits<float>::infinity();
		static bool warningWritten = false;
		if (!warningWritten)
		{
			log(Log::Warning, "stream distance is unspecified or incorrect");
			warningWritten = true;
		}
	}

	return streamDistance;
}