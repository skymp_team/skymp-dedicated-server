#include "stdafx.h"
#include "Text3DWrap.h"

#include "LocationWrap.h"

struct Text3DWrap::Impl
{
	uint32_t id = 0;
};

Text3DWrap::Text3DWrap(uint32_t id) noexcept : pImpl(new Impl)
{
	pImpl->id = id;
}

Text3DWrap::~Text3DWrap() noexcept
{
}

bool Text3DWrap::operator==(const Text3DWrap &rhs) const noexcept {
	return pImpl->id == rhs.pImpl->id;
}

uint32_t Text3DWrap::GetID() const noexcept {
	return pImpl->id;
}

std::string Text3DWrap::GetText() const noexcept 
{
	auto t3d = g.t3dManager->LookupText3D(this->GetID());
	if (t3d)
		return (t3d->GetText());
	return "";
}

float Text3DWrap::GetX() const noexcept
{
	auto t3d = g.t3dManager->LookupText3D(this->GetID());
	if (t3d)
		return t3d->GetPos().x;
	return 0;
}

float Text3DWrap::GetY() const noexcept
{
	auto t3d = g.t3dManager->LookupText3D(this->GetID());
	if (t3d)
		return t3d->GetPos().y;
	return 0;
}

float Text3DWrap::GetZ() const noexcept
{
	auto t3d = g.t3dManager->LookupText3D(this->GetID());
	if (t3d)
		return t3d->GetPos().z;
	return 0;
}

uint32_t Text3DWrap::GetVirtualWorld() const noexcept
{
	auto t3d = g.t3dManager->LookupText3D(this->GetID());
	if (t3d)
		return t3d->GetVirtualWorld();
	return 0;
}

luabridge::LuaRef Text3DWrap::GetLocation() const noexcept
{
	luabridge::LuaRef res(g.gamemode->GetState());
	res = luabridge::Nil();

	auto t3d = g.t3dManager->LookupText3D(this->GetID());
	if (t3d)
		res = LocationWrap(t3d->GetLocationID());

	return res;
}

bool Text3DWrap::IsDeleted() const noexcept
{
	return g.t3dManager->LookupText3D(this->GetID()) == nullptr;
}

void Text3DWrap::SetText(std::string str) noexcept
{
	auto t3d = g.t3dManager->LookupText3D(this->GetID());
	if (t3d)
		t3d->SetText((str));
}

void Text3DWrap::SetPos(float x, float y, float z) noexcept
{
	auto t3d = g.t3dManager->LookupText3D(this->GetID());
	if (t3d)
		t3d->SetPos(NiPoint3{ x, y, z });
}

void Text3DWrap::SetVirtualWorld(uint32_t w) noexcept
{
	auto t3d = g.t3dManager->LookupText3D(this->GetID());
	if (t3d)
		t3d->SetVirtualWorld(w);
}

void Text3DWrap::SetLocation(luabridge::LuaRef loc) noexcept
{
	auto t3d = g.t3dManager->LookupText3D(this->GetID());
	if (t3d)
	{
		if (loc.isLightUserdata() || loc.isUserdata())
		{
			const auto locID = loc.cast<LocationWrap>().GetID();
			t3d->SetLocation(locID);
		}
	}
}

void Text3DWrap::Destroy() noexcept
{
	g.t3dManager->DeleteText3D(this->GetID());
}

luabridge::LuaRef Text3DWrap::Create(std::string text, luabridge::LuaRef location, float x, float y, float z, uint32_t virtualWorldID) noexcept
{
	luabridge::LuaRef res(g.gamemode->GetState());
	res = luabridge::Nil();

	if (location.isLightUserdata() || location.isUserdata())
	{
		const uint32_t locID = location.cast<LocationWrap>().GetID();
		auto t3d = g.t3dManager->NewText3D((text), locID, { x, y, z }, virtualWorldID);

		res = Text3DWrap(t3d->GetID());
	}
	else
		log(Log::Warning, "Unable to create Text3D with emptyy location");

	return res;
}

luabridge::LuaRef Text3DWrap::LookupByID(uint32_t id) noexcept
{
	luabridge::LuaRef res(g.gamemode->GetState());
	res = luabridge::Nil();

	auto t3d = g.t3dManager->LookupText3D(id);
	if (t3d)
		res = Text3DWrap(t3d->GetID());

	return res;
}

void Text3DWrap::Register(lua_State *L)
{
	luabridge::getGlobalNamespace(L)
		.beginClass<Text3DWrap>("Text3D")
		//.addConstructor<void(*)(uint16_t)>()
		.addFunction("__eq", &Text3DWrap::operator==)
		.addFunction("GetID", &Text3DWrap::GetID)
		.addFunction("GetText", &Text3DWrap::GetText)
		.addFunction("GetX", &Text3DWrap::GetX)
		.addFunction("GetY", &Text3DWrap::GetY)
		.addFunction("GetZ", &Text3DWrap::GetZ)
		.addFunction("GetVirtualWorld", &Text3DWrap::GetVirtualWorld)
		.addFunction("GetLocation", &Text3DWrap::GetLocation)
		.addFunction("IsDeleted", &Text3DWrap::IsDeleted)
		.addFunction("SetText", &Text3DWrap::SetText)
		.addFunction("SetPos", &Text3DWrap::SetPos)
		.addFunction("Destroy", &Text3DWrap::Destroy)
		.addStaticFunction("Create", &Text3DWrap::Create)
		.addStaticFunction("LookupByID", &Text3DWrap::LookupByID)
		.endClass();
}