#pragma once

#include <lua.hpp>
#include <LuaBridge.h>

#ifdef GetCurrentDirectory
#undef GetCurrentDirectory
#endif

class GameMode : noncopyable
{
public:
	GameMode(const std::string &file);
	~GameMode() noexcept;

	// [API]
	// Global:
	static void print(const std::string &utf8text) noexcept;
	static void SendChatMessageToAll(const std::string &utf8message) noexcept;
	static uint16_t GetMaxPlayers() noexcept;
	static void SetTimer(uint32_t ms, luabridge::LuaRef callback, luabridge::LuaRef argumentsTable) noexcept;

	// 0.15.1:
	static std::string GetCurrentDirectory() noexcept;
	static std::string GetNthFileInDirectory(std::string dir, uint32_t n) noexcept;
	static std::string GetVersion() noexcept;
	static uint32_t GetTickCount() noexcept;

	// 1.0.3
	static void Terminate() noexcept;

	// 1.0.22
	static uint32_t GetPointerSize() noexcept;

	// 1.0.36
	static uint32_t GetOverloadMult() noexcept;
	static void SetOverloadMult(uint32_t newValue) noexcept;


	// [Callbacks]
	void OnServerInit() noexcept;
	void OnServerExit() noexcept;
	void OnPlayerConnect(uint16_t id) noexcept;
	void OnPlayerDisconnect(uint16_t id) noexcept;
	void OnPlayerSpawn(uint16_t id) noexcept;
	bool OnPlayerUpdate(uint16_t id) noexcept; // Deleted
	void OnPlayerDying(uint16_t id, nonstd::optional<uint16_t> killerID) noexcept;
	void OnPlayerDeath(uint16_t id, nonstd::optional<uint16_t> killerID) noexcept;
	void OnPlayerChatInput(uint16_t id, const std::string &text) noexcept;
	void OnPlayerDialogResponse(uint16_t id, uint32_t dialogID, const std::string &inputText, int32_t listItem) noexcept;
	void OnPlayerCharacterCreated(uint16_t id) noexcept;
	bool OnPlayerDropObject(uint16_t id, uint32_t objectID) noexcept; // 1.0
	bool OnPlayerActivateObject(uint16_t id, uint32_t objectID) noexcept;
	void OnPlayerActivatePlayer(uint16_t id, uint16_t targetPlayerID) noexcept;
	void OnPlayerHitObject(uint16_t id, uint32_t objectID, std::string weaponIdentifier = "", std::string ammoIdentifier = "", std::string spellIdentifier = "") noexcept;
	bool OnPlayerHitPlayer(uint16_t id, uint16_t targetID, std::string weaponIdentifier = "", std::string ammoIdentifier = "", std::string spellIdentifier = "") noexcept;
	bool OnPlayerLearnEffect(uint16_t id, std::string itemTypeIdent, uint32_t n) noexcept; // 0.12
	void OnPlayerDataSearchResult(uint16_t id, std::string resultType, luabridge::LuaRef result) noexcept; // 0.11
	void OnPlayerLearnPerk(uint16_t id, uint32_t perkID) noexcept; // 1.0
	void OnPlayerEatItem(uint16_t id, std::string itemTypeIdent) noexcept; // 1.0
	void OnPlayerStreamInPlayer(uint16_t id, uint16_t otherPlayerId) noexcept; // 1.0
	void OnPlayerStreamOutPlayer(uint16_t id, uint16_t otherPlayerId) noexcept; // 1.0
	void OnPlayerStreamInObject(uint16_t id, uint32_t objectID) noexcept; // 1.0
	void OnPlayerStreamOutObject(uint16_t id, uint32_t objectID) noexcept; // 1.0
	void OnPlayerBowShot(uint16_t id, float power) noexcept; // 1.0.2
	void OnPlayerChangeContainer(uint16_t id, uint32_t objectID, std::string itemTypeIdentifier, uint32_t count, bool isAdd) noexcept; // 1.0.10
	void OnPlayerCreateItem(uint16_t id, std::string itemTypeIdentifier, uint32_t count) noexcept; // 1.0.15
	void OnPlayerUseItem(uint16_t id, std::string itemTypeIdent) noexcept; // 1.0.15
	void OnPlayerHostPlayer(uint16_t hosterID, uint16_t targetID) noexcept; // 1.0.29
	void OnPlayerJump(uint16_t id) noexcept; // 1.0.36
	void OnPlayerFall(uint16_t id) noexcept; // 1.0.36
	void OnPlayerOnTheGround(uint16_t id) noexcept; // 1.0.36

	// ...
	lua_State *GetState() const noexcept;

private:
	struct Impl;
	Impl *const pImpl;
};
