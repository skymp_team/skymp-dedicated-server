#pragma once
#include "LocationWrap.h"

class ObjectWrap
{
public:
	ObjectWrap(uint32_t id) noexcept;

	uint32_t GetID() const noexcept;
	uint32_t GetBaseID() const noexcept;
	bool IsDeleted() const noexcept;
	bool operator==(const ObjectWrap &rhs) const noexcept;
	float GetX() const noexcept;
	float GetY() const noexcept;
	float GetZ() const noexcept;
	float GetAngleX() const noexcept;
	float GetAngleY() const noexcept;
	float GetAngleZ() const noexcept;
	luabridge::LuaRef GetLocation() const noexcept;
	luabridge::LuaRef GetVirtualWorld() const noexcept;
	std::string GetName() const noexcept;
	bool IsDisabled() const noexcept;
	bool IsDestroyed() const noexcept;
	std::string GetType() const noexcept;
	uint32_t GetItemCount(luabridge::LuaRef itemType) const noexcept;
	uint32_t GetNumInventorySlots() const noexcept;
	float GetTotalWeight() const noexcept;
	luabridge::LuaRef GetItemTypeInSlot(uint32_t slot) const noexcept;
	uint32_t GetItemCountInSlot(uint32_t slot) const noexcept;
	uint8_t GetLockLevel() const noexcept;
	bool IsHarvested() const noexcept;
	bool IsOpen() const noexcept; // 1.0
	luabridge::LuaRef GetTeleportTarget() const noexcept; // 1.0

	void SetPos(float x, float y, float z) noexcept;
	void SetAngle(float x, float y, float z) noexcept;
	bool SetLocation(luabridge::LuaRef location) noexcept;
	bool SetVirtualWorld(uint32_t virtualWorld) noexcept;
	bool Delete() noexcept;
	void SetName(const std::string &utf8name) noexcept;
	void SetDisabled(bool disabled) noexcept;
	void SetDestroyed(bool destroyed) noexcept;
	void SetDestroyedForPlayer(luabridge::LuaRef player, bool destroyed) noexcept;
	void RegisterAsDoor() noexcept;
	bool RegisterAsTeleportDoor(luabridge::LuaRef target) noexcept;
	void RegisterAsActivator() noexcept;
	void RegisterAsFurniture() noexcept;
	void RegisterAsContainer() noexcept;
	void AddItem(luabridge::LuaRef itemType, uint32_t count) noexcept;
	bool RemoveItem(luabridge::LuaRef itemType, uint32_t count) noexcept;
	void RemoveAllItems() noexcept;
	void SetLockLevel(uint8_t lockLevel) noexcept;
	void AddKeyword(const std::string &keyword) noexcept;
	void RemoveKeyword(const std::string &keyword) noexcept;
	void SetHarvested(bool harvested) noexcept;
	void SetOpen(bool open) noexcept; // 1.0
	void ResetFor(luabridge::LuaRef player) noexcept; // 1.0
	void Activate(luabridge::LuaRef player) noexcept; // 1.0.10
	void SetHostable(bool hostable) noexcept; // 1.0.14

	static luabridge::LuaRef Create(uint32_t existingReferenceID, uint32_t baseFormID, luabridge::LuaRef location, float x, float y, float z) noexcept;
	static luabridge::LuaRef CreateItem(luabridge::LuaRef itemType, uint32_t count, luabridge::LuaRef location, float x, float y, float z) noexcept;
	static luabridge::LuaRef LookupByID(uint32_t id) noexcept;

	static void Register(lua_State *state);

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;
};