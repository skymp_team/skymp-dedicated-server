#include "stdafx.h"
#include "Tests.h"

#define MY_ASSERT assert

// ���� MY_ASSERT == assert, ������ ����� ��� ������� �����
// ��������� ����� start.bat, ����� ��������� ��������� �� ������ ����� ������� �������

void Tests::RunAll() noexcept
{
	Test_Locale();

	Test_Player_Create();
	Test_Player_ShowDialog();
	Test_Player_SetLocation();
	Test_Player_SetVirtualWorld();
	Test_Player_MoveTo();
	Test_Player_SetPaused();
	Test_Player_SetAVData();
}

void Tests::Test_Locale() noexcept
{
	MY_ASSERT(StringUtil::WcharToUtf8(L"Pospelov") == "Pospelov");
	MY_ASSERT(StringUtil::Utf8ToWchar("Pospelov") == L"Pospelov");
	MY_ASSERT(StringUtil::WcharToUtf8(StringUtil::Utf8ToWchar("Pospelov")) == "Pospelov");
	MY_ASSERT(StringUtil::Utf8ToWchar(StringUtil::WcharToUtf8(L"Pospelov")) == L"Pospelov");
}

void Tests::Test_Player_Create() noexcept
{
	auto pl = Player::CreateDummy();
	MY_ASSERT(pl != nullptr);
}

void Tests::Test_Player_ShowDialog() noexcept
{
	auto pl = Player::CreateDummy();
	MY_ASSERT(pl->GetCurrentDialogID() == ~0);

	const uint32_t dialogID = 1;

	pl->ShowDialog(dialogID, "...", Player::DialogStyle::Input, "", -1);
	MY_ASSERT(pl->GetCurrentDialogID() == dialogID);

	pl->ReleaseDialog();
	MY_ASSERT(pl->GetCurrentDialogID() == ~0);
}
void Tests::Test_Player_SetName() noexcept
{
	auto pl = Player::CreateDummy();

	const std::string name = "kura";
	pl->SetName(name);
	MY_ASSERT(pl->GetName() == name);
}
void Tests::Test_Player_SetLocation() noexcept
{
	auto pl = Player::CreateDummy();

	const uint32_t locationID = 93135;

	pl->SetLocation(locationID);
	MY_ASSERT(pl->GetLocation() == locationID);
}
void Tests::Test_Player_SetVirtualWorld() noexcept
{
	auto pl = Player::CreateDummy();

	const uint32_t virtualWorldID = 1;

	pl->SetVirtualWorld(virtualWorldID);
	MY_ASSERT(pl->GetVirtualWorld() == virtualWorldID);
}
void Tests::Test_Player_MoveTo() noexcept
{
	auto pl = Player::CreateDummy();

	const uint32_t cellOrWorldSpace = 93135;

	NiPoint3 coords(0.0, 0.0, 0.0);

	uint16_t angle = 50;

	pl->MoveTo(cellOrWorldSpace, coords, angle);
	MY_ASSERT(true);
}
void Tests::Test_Player_SetPaused() noexcept
{
	auto pl = Player::CreateDummy();

	pl->SetPaused(true);
	MY_ASSERT(pl->IsPaused());
}
void Tests::Test_Player_DamageHealth() noexcept
{
	auto pl = Player::CreateDummy();
	auto healthData = pl->GetAVData("Health");
	healthData.SetCurrent(100.0f);
	float damage = 20.0f;
	healthData.DamageCurrent(damage);
	pl->SetAVData("Health", healthData);
	float health = pl->GetAVData("Health").GetCurrent();
	bool isDamageCorrect = health > 79.0f && health < 81.0f;
	MY_ASSERT(isDamageCorrect);
}
void Tests::Test_Player_SetAVData() noexcept
{
	auto pl = Player::CreateDummy();

	std::string avName = "Magicka";

	auto data = pl->GetAVData(avName);
	data.base = 100.0f;
	data.percentage = 100.0f;
	data.modifier = 0.0f;

	pl->SetAVData(avName, data);
	auto data2 = pl->GetAVData(avName);

	MY_ASSERT(data.base == data2.base);
	MY_ASSERT(data.percentage == data2.percentage);
	MY_ASSERT(data.modifier == data2.modifier);
}