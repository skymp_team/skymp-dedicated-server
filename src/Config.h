#pragma once

class IConfigIn
{
public:
	template <class T>
	T GetValue(const std::string &variableName, T defaultValue) const noexcept
	{
		try {
			std::stringstream ss;
			ss << GetValueImpl(variableName);
			T result;
			ss >> result;
			return result;
		}
		catch (...) {
			return defaultValue;
		}
	}

	virtual ~IConfigIn() = default;

protected:
	virtual std::string GetValueImpl(const std::string &variableName, const std::string &defaultValue = ""s) const = 0;
};

class Config : public IConfigIn, private noncopyable
{
public:
	explicit Config(const std::string &fileName) noexcept;
	virtual ~Config() noexcept;

	const std::string fileName;

private:
	std::string GetValueImpl(const std::string &variableName, const std::string &defaultValue = ""s) const override;

	struct Impl;
	Impl *const pImpl;
};

class ServerCfg : public Config
{
public:
	explicit ServerCfg(const std::string &fileName) noexcept : Config(fileName)
	{
	}

	virtual ~ServerCfg() = default;

	inline uint16_t GetMaxPlayers() const noexcept
	{
		return this->GetValue<uint16_t>("General.maxPlayers", 0);
	}
};