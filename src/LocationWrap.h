#pragma once

class LocationWrap
{
public:
	explicit LocationWrap(uint32_t formID) noexcept;
	~LocationWrap() noexcept;

	bool operator==(const LocationWrap &rhs) const noexcept;
	uint32_t GetID() const noexcept;

	static void Register(lua_State *state);

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;
};