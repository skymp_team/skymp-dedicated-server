#include "stdafx.h"
#include "NavMeshWrap.h"

#include "LocationWrap.h"

struct NavMeshWrap::Impl
{
	NavMesh *nav = nullptr;
};

std::map<uint32_t, NavMesh *> createdNavMesh;

NavMeshWrap::NavMeshWrap(uint32_t formID) : pImpl(new Impl)
{
	try {
		pImpl->nav = createdNavMesh.at(formID);
		return;
	}
	catch (...) {
	}

	std::stringstream ss;
	ss << "unable to create wrap for NavMesh " << std::hex << formID;
	throw std::runtime_error(ss.str());
}

NavMeshWrap::~NavMeshWrap() noexcept
{
}

bool NavMeshWrap::operator==(const NavMeshWrap &rhs) const noexcept {
	return pImpl->nav == rhs.pImpl->nav;
}

void NavMeshWrap::SetNumTriangles(uint32_t n) noexcept
{
	pImpl->nav->triangles.resize(n, { 0,0,0 });
}

uint32_t NavMeshWrap::GetNumTriangles() const noexcept
{
	return (uint32_t)pImpl->nav->triangles.size();
}

void NavMeshWrap::SetNumVertices(uint32_t n) noexcept
{
	pImpl->nav->verts.resize(n, { 0.f,0.f,0.f });
}

uint32_t NavMeshWrap::GetNumVertices() const noexcept
{
	return (uint32_t)pImpl->nav->verts.size();
}

void NavMeshWrap::SetNthVerticePos(uint32_t n, float x, float y, float z) noexcept 
{
	--n;
	if (n < this->GetNumVertices())
		pImpl->nav->verts[n] = NiPoint3(x, y, z);
}

float NavMeshWrap::GetNthVerticeX(uint32_t n) const noexcept
{
	--n;
	if (n < this->GetNumVertices())
		return pImpl->nav->verts[n].x;
	return 0;
}

float NavMeshWrap::GetNthVerticeY(uint32_t n) const noexcept
{
	--n;
	if (n < this->GetNumVertices())
		return pImpl->nav->verts[n].y;
	return 0;
}

float NavMeshWrap::GetNthVerticeZ(uint32_t n) const noexcept
{
	--n;
	if (n < this->GetNumVertices())
		return pImpl->nav->verts[n].z;
	return 0;
}

void NavMeshWrap::SetNthTriangleVertices(uint32_t n, uint16_t vert1, uint16_t vert2, uint16_t vert3) const noexcept
{
	--n;
	--vert1;
	--vert2;
	--vert3;
	if (n < this->GetNumTriangles())
	{
		pImpl->nav->triangles[n][0] = vert1;
		pImpl->nav->triangles[n][1] = vert2;
		pImpl->nav->triangles[n][2] = vert3;
	}
}

uint16_t NavMeshWrap::GetNthTriangleNthVertice(uint32_t n, uint32_t nVert) const noexcept
{
	--n;
	--nVert;
	if (n < this->GetNumTriangles() && nVert < this->GetNumVertices())
	{
		auto res = pImpl->nav->triangles[n][nVert];
		return ++res;
	}
	return 0;
}

void NavMeshWrap::SetLocation(luabridge::LuaRef loc) noexcept
{
	if (loc.isLightUserdata() || loc.isUserdata())
	{
		auto id = loc.cast<LocationWrap>().GetID();
		pImpl->nav->locationID = id;
	}
}

luabridge::LuaRef NavMeshWrap::GetLocation() const noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	
	try {
		result = LocationWrap(pImpl->nav->locationID);
	}
	catch (...) {
		result = luabridge::Nil();
	}
	
	return result;
}

luabridge::LuaRef NavMeshWrap::Create(uint32_t formID) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	if (formID == 0
		|| !(LookupByID(formID) == luabridge::Nil()))
	{
		std::stringstream ss;
		ss << "NavMesh with formID " << std::hex << formID << " already exist";
		log(Log::Warning,
			formID == 0 ? "Null NavMesh formID" : ss.str().data());
		return result;
	}

	createdNavMesh[formID] = new NavMesh;
	result = LookupByID(formID);

	return result;
}

luabridge::LuaRef NavMeshWrap::LookupByID(uint32_t formID) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());

	try {
		result = NavMeshWrap(formID);
	}
	catch (...) {
		result = luabridge::Nil();
	}
	return result;
}

const NavMesh &NavMeshWrap::GetData() const noexcept
{
	return *pImpl->nav;
}

void NavMeshWrap::SetLocation(uint32_t locID) noexcept
{
	pImpl->nav->locationID = locID;
}

uint32_t NavMeshWrap::GetID() const noexcept
{
	return pImpl->nav->formID;
}

void NavMeshWrap::Register(lua_State *L)
{
	luabridge::getGlobalNamespace(L)
		.beginClass<NavMeshWrap>("NavMesh")
		.addFunction("__eq", &NavMeshWrap::operator==)
		.addFunction("SetNthVerticePos", &NavMeshWrap::SetNthVerticePos)
		.addFunction("GetNthVerticeX", &NavMeshWrap::GetNthVerticeX)
		.addFunction("GetNthVerticeY", &NavMeshWrap::GetNthVerticeY)
		.addFunction("GetNthVerticeZ", &NavMeshWrap::GetNthVerticeZ)
		.addFunction("SetNthTriangleVertices", &NavMeshWrap::SetNthTriangleVertices)
		.addFunction("GetNthTriangleNthVertice", &NavMeshWrap::GetNthTriangleNthVertice)
		.addStaticFunction("Create", &NavMeshWrap::Create)
		.addStaticFunction("LookupByID", &NavMeshWrap::LookupByID)
		.endClass();
}