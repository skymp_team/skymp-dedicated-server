#include "stdafx.h"
#include "ItemTypeWrap.h"
#include "IDAssigner.h"
#include "MagicBase.h"

class ItemClass
{
public:
	enum {
		Misc =			0x00,
		Weapon =		0x01,
		Armor =			0x02,
		Ammo =			0x03,
		Ingredient =	0x04,
		Potion =		0x05,
		SoulGem =		0x06,
		Key =			0x07,
	};

	ItemClass() noexcept {
		Init();
	}

	ItemClass(uint8_t classID) noexcept {
		Init();
		this->classID = classID;
	}

	ItemClass(const std::string &className) noexcept {
		Init();
		try {
			this->classID = this->idByName->at(className);
		}
		catch (...) {
			this->classID = Misc;
			log(Log::Error, "Unknown item class ", className.data());
		};
	}

	uint8_t GetID() const noexcept {
		return this->classID;
	}

	std::string GetName() const noexcept {
		try {
			return nameByID->at(this->classID);
		}
		catch (...) {
			return ItemClass(Misc).GetName();
		}
	}

private:
	void Init() {
		static std::map<std::string, uint8_t> _idByName;
		static std::map<uint8_t, std::string> _nameByID;
		if (_idByName.empty())
		{
			_idByName = {
				{ "Misc",			Misc },
				{ "Weapon",			Weapon },
				{ "Armor",			Armor },
				{ "Ammo",			Ammo },
				{ "Ingredient",		Ingredient },
				{ "Potion",			Potion },
				{ "SoulGem",		SoulGem },
				{ "Key",			Key }
			};
			_nameByID.clear();
			for (auto pair : _idByName)
				_nameByID.insert({ pair.second, pair.first });
		}
		this->idByName = &_idByName;
		this->nameByID = &_nameByID;
	}

	uint8_t classID = Misc;

	std::map<std::string, uint8_t> *idByName;
	std::map<uint8_t, std::string> *nameByID;
};

class ItemSubclass
{
public:
	enum : uint8_t {
		Misc =			0x01,
		Gold =			0x02,
		Lockpick =		0x03,
		Torch =			0x04, // Is it unused ? 

		Sword =			0x05,
		WarAxe =		0x06,
		Mace =			0x07,
		Dagger =		0x08,
		Greatsword =	0x09,
		Battleaxe =		0x0A,
		Warhammer =		0x0B,
		Bow =			0x0C,
		Crossbow =		0x0D,

		Armor =			0x0E,
		Boots =			0x0F,
		Gauntlets =		0x10,
		Helmet =		0x11,
		Amulet =		0x12,
		Ring =			0x13,
		Shield =		0x14,

		Potion =		0x15,
		Food =			0x16,
		Poison =		0x17,

		END = 0x18,
	};
	
	ItemSubclass() noexcept {
		Init();
		this->subclassID = 0;
	}

	ItemSubclass(uint8_t subclassID) noexcept {
		Init();
		this->subclassID = subclassID;
	}

	ItemSubclass(std::string name) noexcept {
		Init();
		try {
			this->subclassID = this->idByName->at(name);
		}
		catch (...) {
			this->subclassID = 0;
			//log(Log::Error, "Unknown item subclass ", name.data());
		};
	}

	uint8_t GetID() const noexcept {
		return this->subclassID;
	}

	std::string GetName() const noexcept {
		try {
			return nameByID->at(this->subclassID);
		}
		catch (...) {
			return "";
		}
	}

	ItemClass GetClass() const noexcept {
		auto id = this->GetID();
		if (id >= Misc && id <= Torch)
			return ItemClass::Misc;
		if (id >= Sword && id <= Crossbow)
			return ItemClass::Weapon;
		if (id >= Armor && id <= Shield)
			return ItemClass::Armor;
		if (id >= Potion && id <= Poison)
			return ItemClass::Potion;
		return {};
	}

private:
	void Init() {
		static std::map<std::string, uint8_t> _idByName;
		static std::vector<std::string> _nameByID;
		if (_idByName.empty())
		{
			_idByName = {
				{ "Misc",			Misc },
				{ "Gold",			Gold },
				{ "Lockpick",		Lockpick },
				{ "Torch",			Torch },

				{ "Sword",			Sword },
				{ "WarAxe",			WarAxe },
				{ "Mace",			Mace },
				{ "Dagger",			Dagger },
				{ "Greatsword",		Greatsword},
				{ "Battleaxe",		Battleaxe},
				{ "Warhammer",		Warhammer },
				{ "Bow",			Bow },
				{ "Crossbow",		Crossbow },

				{ "Armor", 			Armor },
				{ "Boots", 			Boots },
				{ "Gauntlets", 		Gauntlets },
				{ "Helmet", 		Helmet },
				{ "Amulet", 		Amulet }, // 0.15.13
				{ "Ring", 			Ring }, // 0.15.13
				{ "Shield", 		Shield },

				{ "Potion",			Potion },
				{ "Food",			Food },
				{ "Poison",			Poison }
			};
			_nameByID.push_back("");
			for (uint8_t id = 0; id != ItemSubclass::END; ++id)
			{
				for (auto pair : _idByName)
				{
					if (pair.second == id)
					{
						_nameByID.push_back(pair.first);
						break;
					}
				}
			}
		}
		this->idByName = &_idByName;
		this->nameByID = &_nameByID;
	}

	uint8_t subclassID;

	std::map<std::string, uint8_t> *idByName;
	std::vector<std::string> *nameByID;
};

struct ItemType : public Components::MagicBase
{
	std::string identifier;
	ItemClass itemClass;
	ItemSubclass subclass;
	std::string skillName;
	/*uint32_t existingGameFormID;
	float weight;
	uint32_t goldValue;
	float damage;
	float armorRating;
	nonstd::optional<SpellWrap> ench;
	SoulSize soulSize = SoulSize::None, 
		capacity = SoulSize::None;
	float health = 1.0;*/
	ItemTypeInfo info;
	size_t numInfoChanges;
};

std::map<std::string, ItemType *> createdItemTypes;

struct ItemTypeWrap::Impl
{
	ItemType *itemType = nullptr;

	static uint32_t &NumChanges() {
		static uint32_t v;
		return v;
	}
};

ItemTypeWrap::ItemTypeWrap(const void *uniquePtr) : pImpl(new Impl)
{
	pImpl->itemType = (ItemType *)uniquePtr;
}

ItemTypeWrap::ItemTypeWrap(std::string name) : pImpl(new Impl)
{
	try {
		pImpl->itemType = createdItemTypes.at(name);
		return;
	}
	catch (...) {
	}
	throw std::runtime_error("unable to create wrap for ItemType " + name);
}

ItemTypeWrap::~ItemTypeWrap() noexcept
{
}

bool ItemTypeWrap::operator==(const ItemTypeWrap &rhs) const noexcept {
	return this->pImpl->itemType == rhs.pImpl->itemType;
}

std::string ItemTypeWrap::GetIdentifier() const noexcept {
	return pImpl->itemType->identifier;
}

std::string ItemTypeWrap::GetClass() const noexcept {
	return pImpl->itemType->itemClass.GetName();
}

uint8_t ItemTypeWrap::GetClassID() const noexcept {
	return pImpl->itemType->itemClass.GetID();
}

std::string ItemTypeWrap::GetSubclass() const noexcept {
	return pImpl->itemType->subclass.GetName();
}

uint8_t ItemTypeWrap::GetSubclassID() const noexcept {
	return pImpl->itemType->subclass.GetID();
}

uint32_t ItemTypeWrap::GetExistingGameFormID() const noexcept {
	return pImpl->itemType->info.existingItemID;
}

float ItemTypeWrap::GetWeight() const noexcept {
	return pImpl->itemType->info.weight;
}

uint32_t ItemTypeWrap::GetGoldValue() const noexcept {
	return pImpl->itemType->info.goldValue;
}

float ItemTypeWrap::GetDamage() const noexcept {
	return pImpl->itemType->info.damage;
}

float ItemTypeWrap::GetArmorRating() const noexcept {
	return pImpl->itemType->info.armorRating;
}

std::string ItemTypeWrap::GetSkillName() const noexcept {
	return pImpl->itemType->skillName;
}

luabridge::LuaRef ItemTypeWrap::GetEnchantment() const noexcept 
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();
	if (pImpl->itemType->info.ench != nonstd::nullopt)
		result = *pImpl->itemType->info.ench;
	return result;
}

float ItemTypeWrap::GetHealth() const noexcept
{
	return pImpl->itemType->info.health;
}

void ItemTypeWrap::SetWeight(float w) noexcept {
	if (this->GetClass() == "Ammo")
		return;
	pImpl->itemType->info.weight = w;
	pImpl->itemType->numInfoChanges++;
}

void ItemTypeWrap::SetGoldValue(uint32_t v) noexcept {
	pImpl->itemType->info.goldValue = v;
	pImpl->itemType->numInfoChanges++;
}

void ItemTypeWrap::SetDamage(float v) noexcept {
	if (this->GetClass() != "Weapon" && this->GetClass() != "Ammo")
		return;
	pImpl->itemType->info.damage = v;
	pImpl->itemType->numInfoChanges++;
}

void ItemTypeWrap::SetArmorRating(float v) noexcept {
	if (this->GetClass() != "Armor")
		return;
	pImpl->itemType->info.armorRating = v;
	pImpl->itemType->numInfoChanges++;
}

void ItemTypeWrap::SetEnchantment(luabridge::LuaRef ench) noexcept {
	if (this->GetClass() != "Armor" && this->GetClass() != "Weapon")
		return;

	if (ench.isLightUserdata() || ench.isUserdata())
	{
		pImpl->itemType->info.ench = ench.cast<SpellWrap>();
		if (this->pImpl->itemType->info.ench->IsEnchantment() == false)
		{
			ench = luabridge::Nil();
			this->SetEnchantment(ench);
			log(Log::Warning, "Attempt to call SetEnchantment() with Spell argument");
		}
	}
	else
		pImpl->itemType->info.ench = nonstd::nullopt;
	pImpl->itemType->numInfoChanges++;
}

void ItemTypeWrap::SetHealth(float hp) noexcept
{
	hp = (int16_t)hp;
	if (hp < 0)
		hp = 0;
	if (hp > 6)
		hp = 6;
	pImpl->itemType->info.health = hp;
	pImpl->itemType->numInfoChanges++;
}

luabridge::LuaRef ItemTypeWrap::GetNthEffect(uint32_t n) const noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	--n;
	if (n < pImpl->itemType->GetNumEffects())
		result = (EffectWrap)pImpl->itemType->GetEffects()[n].effect;

	return result;
}

std::string ItemTypeWrap::GetNthEffectIdentifier(uint32_t n) const noexcept
{
	--n;
	if (n < pImpl->itemType->GetNumEffects())
		return pImpl->itemType->GetEffects()[n].effect.GetIdentifier();

	return "";
}

float ItemTypeWrap::GetNthEffectMagnitude(uint32_t n) const noexcept
{
	--n;
	if (n < pImpl->itemType->GetNumEffects())
		return pImpl->itemType->GetEffects()[n].magnitude;
	return 0;
}

float ItemTypeWrap::GetNthEffectDuration(uint32_t n) const noexcept
{
	--n;
	if (n < pImpl->itemType->GetNumEffects())
		return pImpl->itemType->GetEffects()[n].durationSec;
	return 0;
}

float ItemTypeWrap::GetNthEffectArea(uint32_t n) const noexcept
{
	--n;
	if (n < pImpl->itemType->GetNumEffects())
		return pImpl->itemType->GetEffects()[n].area;
	return 0;
}

uint32_t ItemTypeWrap::GetNumEffects() const noexcept
{
	return pImpl->itemType->GetNumEffects();
}


void ItemTypeWrap::AddEffect(luabridge::LuaRef effectLuaRef, float magnitude, float durationSec, float area) noexcept
{
	if (effectLuaRef.isLightUserdata() || effectLuaRef.isUserdata())
	{
		if (this->GetClass() == "Ingredient" && this->GetNumEffects() >= 4)
		{
			log(Log::Warning, "Ingredient must not have more than 4 effects");
			return;
		}
		if (this->GetClass() != "Ingredient" && this->GetClass() != "Potion")
		{
			log(Log::Warning, "AddEffect() called on non-alchemy item");
			return;
		}
		const EffectWrap effect = effectLuaRef;
		pImpl->itemType->AddEffect(effect, magnitude, durationSec, area);
		pImpl->itemType->info.effects.push_back({
			effect.GetIdentifier(), magnitude, durationSec, area
		});
		pImpl->itemType->numInfoChanges++;
	}
}

int32_t ItemTypeWrap::GetSoulSize() const noexcept
{
	return pImpl->itemType->info.soulSize;
}

int32_t ItemTypeWrap::GetCapacity() const noexcept
{
	return pImpl->itemType->info.capacity;
}

void ItemTypeWrap::SetSoulSize(int32_t s) noexcept
{
	if (this->GetClass() != "SoulGem")
		return;
	pImpl->itemType->info.soulSize = (SoulSize)s;
	pImpl->itemType->numInfoChanges++;
}

void ItemTypeWrap::SetCapacity(int32_t cap) noexcept
{
	if (this->GetClass() != "SoulGem")
		return;
	pImpl->itemType->info.capacity = (SoulSize)cap;
	pImpl->itemType->numInfoChanges++;
}

void *ItemTypeWrap::GetUniquePtr() const noexcept
{
	return pImpl->itemType;
}

const ItemTypeInfo &ItemTypeWrap::GetInfo() const noexcept
{
	pImpl->itemType->info.classID = pImpl->itemType->itemClass.GetID();
	pImpl->itemType->info.subclassID = pImpl->itemType->subclass.GetID();
	return pImpl->itemType->info;
}

size_t ItemTypeWrap::GetNumInfoChanges() const noexcept
{
	return pImpl->itemType->numInfoChanges;
}

ItemTypeWrap ItemTypeWrap::LookupByUniquePtr(void *uniquePtr) noexcept
{
	auto itemType = (const ItemType *)uniquePtr;
	try {
		return ItemTypeWrap(itemType);
	}
	catch (const std::exception &e) {
		log(Log::Fatal, e.what());
		std::exit(-1);
		return ItemTypeWrap({});
	}
}

const ItemTypeInfo &ItemTypeWrap::LookupInfoByUniquePtr(void *uniquePtr) noexcept
{
	auto itemType = (const ItemType *)uniquePtr;
	return itemType->info;
}

const size_t ItemTypeWrap::GetNumInfoChangesByUniquePtr(void *uniquePtr) noexcept
{
	auto itemType = (const ItemType *)uniquePtr;
	return itemType->numInfoChanges;
}

std::string ItemTypeWrap::GetClassAndSubclassByID(uint8_t cl, uint8_t subCl) noexcept
{
	return ItemClass(cl).GetName() + "." + (ItemSubclass(subCl)).GetName();
}

uint32_t ItemTypeWrap::GetGlobalNumChanges() noexcept
{
	return Impl::NumChanges();
}

nonstd::optional<ItemTypeWrap> gGold = nonstd::nullopt, gLockpick = nonstd::nullopt;

luabridge::LuaRef ItemTypeWrap::Create(std::string ident,
	std::string classAndSubclass,
	uint32_t existingGameFormID,
	float weight,
	uint32_t goldValue,
	luabridge::LuaRef customArg1,
	luabridge::LuaRef skillName) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	if (ident.empty()
		|| !(SpellWrap::LookupByIdentifier(ident) == luabridge::Nil())
		|| !(ItemTypeWrap::LookupByIdentifier(ident) == luabridge::Nil()))
	{
		log(Log::Warning, 
			ident.empty() ? "Empty identifier" : "Entity already exist");
		return result;
	}

	const char del = '.';
	auto delPos = classAndSubclass.find(del);

	const std::string class_ =
		delPos != std::string::npos ?
		std::string{ classAndSubclass.begin(), classAndSubclass.begin() + delPos } :
		classAndSubclass;
	const auto cl = ItemClass(class_);

	const std::string subclass_ =
		delPos != std::string::npos ?
		std::string{ classAndSubclass.begin() + class_.size() + sizeof del, classAndSubclass.end() } :
		"";
	const auto subcl = ItemSubclass(subclass_);

	if (subcl.GetID() == ItemSubclass::Gold
		&& gGold != nonstd::nullopt)
	{
		log(Log::Warning, "Item type with subclass Gold already exist");
		return result;
	}

	if (subcl.GetID() == ItemSubclass::Lockpick
		&& gLockpick != nonstd::nullopt)
	{
		log(Log::Warning, "Item type with subclass Lockpick already exist");
		return result;
	}

	if (cl.GetName() == class_)
	{
		auto noSubclass = [](std::string cl) {
			return cl == "Ammo" || cl == "Ingredient" || cl == "SoulGem" || cl == "Key";
		};

		if (subcl.GetClass().GetID() == cl.GetID()
			|| (subcl.GetID() == 0 && noSubclass(class_)))
		{
			auto itemType = createdItemTypes.insert({ ident, new ItemType }).first->second;
			itemType->identifier = ident;
			itemType->itemClass = class_;
			itemType->subclass = subcl;
			itemType->info.existingItemID = existingGameFormID;
			itemType->info.weight = weight;
			itemType->info.goldValue = goldValue;
			itemType->info.armorRating = 0;
			itemType->info.damage = 0;

			nonstd::optional<ItemTypeWrap> wrap;
			try {
				wrap = ItemTypeWrap(ident);
				IDAssigner<ItemTypeWrap>::GenerateID(*wrap);
			}
			catch (...) {
			}
			if (wrap != nonstd::nullopt)
				result = *wrap;
			else
				log(Log::Error, "something went really wrong with ItemTypeWrap");

			switch (itemType->itemClass.GetID())
			{
			case ItemClass::Misc:
				if (itemType->subclass.GetID() == 0)
					itemType->subclass = ItemSubclass::Misc;
				switch (itemType->subclass.GetID())
				{
				case ItemSubclass::Gold:
					gGold = wrap;
					break;
				case ItemSubclass::Lockpick:
					gLockpick = wrap;
					break;
				}
				break;
			case ItemClass::Armor:
				itemType->info.armorRating = customArg1.isNumber() ? (float)customArg1 : 0;
				if (skillName.isString())
				{
					itemType->skillName = skillName.cast<std::string>();
				}
				break;
			case ItemClass::Weapon:
				itemType->info.damage = customArg1.isNumber() ? (float)customArg1 : 0;
				if (itemType->info.damage < 1.0f)
				{
					itemType->info.damage = 1.0f;
					log(Log::Warning, "Minimum Weapon damage is 1.0");
				}
				if (itemType->subclass.GetID() == 0)
					itemType->subclass = ItemSubclass::Sword;
				if (skillName.isString())
				{
					itemType->skillName = skillName.cast<std::string>();
				}
				break;
			case ItemClass::Ammo:
				itemType->info.weight = 0.0f;
				itemType->info.damage = customArg1.isNumber() ? (float)customArg1 : 0;
				if (itemType->info.damage < 1.0f)
				{
					itemType->info.damage = 1.0f;
					log(Log::Warning, "Minimum Ammo damage is 1.0");
				}
			case ItemClass::Ingredient:
			case ItemClass::Potion:
				break;
			}
		}
		else
			log(Log::Warning, classAndSubclass.data(), " subclass not found");
	}
	else
		log(Log::Warning, "Unknown ItemType class ", class_.data());

	return result;
}

luabridge::LuaRef ItemTypeWrap::LookupByIdentifier(std::string name) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());

	try {
		auto itemType = createdItemTypes.at(name);
		result = ItemTypeWrap(itemType->identifier);
	}
	catch (...) {
		result = luabridge::Nil();
	}

	return result;
}

luabridge::LuaRef ItemTypeWrap::Clone(luabridge::LuaRef srcRef, luabridge::LuaRef optionalNewItemTypeIdent) noexcept
{
	luabridge::LuaRef result(g.gamemode->GetState());
	result = luabridge::Nil();

	if (srcRef.isLightUserdata() == false && srcRef.isUserdata() == false)
	{
		log(Log::Warning, srcRef.isNil() ? "Clone() source must not be nil" : "Clone() unable to clone from non-ItemType Lua reference");
		return result;
	}

	const ItemTypeWrap src = srcRef;

	std::string newIdent;

	if (optionalNewItemTypeIdent.isString())
		newIdent = (optionalNewItemTypeIdent).cast<std::string>();

	if (newIdent == "")
	{
		do {
			static std::map<ItemTypeWrap, uint32_t> numClones;
			newIdent = src.GetIdentifier() + std::to_string(numClones[src]);
			++numClones[src];
		} while (LookupByIdentifier(newIdent).isNil() == false);
	}

	if (LookupByIdentifier(newIdent).isNil() == false)
	{
		log(Log::Warning, "Clone() identifier already used");
		return result;
	}

	auto newItemType = new ItemType(*src.pImpl->itemType);
	newItemType->identifier = newIdent;
	createdItemTypes.insert({ newIdent, newItemType }).first->second;
	auto wrap = ItemTypeWrap(newIdent);
	result = wrap;
	IDAssigner<ItemTypeWrap>::GenerateID(wrap);

	return result;
}

void ItemTypeWrap::Register(lua_State *L)
{
	luabridge::getGlobalNamespace(L)
		.beginClass<ItemTypeWrap>("ItemType")
		.addFunction("__eq", &ItemTypeWrap::operator==)
		.addFunction("GetIdentifier", &ItemTypeWrap::GetIdentifier)
		.addFunction("GetClass", &ItemTypeWrap::GetClass)
		.addFunction("GetSubclass", &ItemTypeWrap::GetSubclass)
		.addFunction("GetWeight", &ItemTypeWrap::GetWeight)
		.addFunction("GetGoldValue", &ItemTypeWrap::GetGoldValue)
		.addFunction("GetDamage", &ItemTypeWrap::GetDamage)
		.addFunction("GetArmorRating", &ItemTypeWrap::GetArmorRating)
		.addFunction("GetEnchantment", &ItemTypeWrap::GetEnchantment)
		.addFunction("GetHealth", &ItemTypeWrap::GetHealth)
		.addFunction("GetBaseID", &ItemTypeWrap::GetBaseID)
		.addFunction("GetSkillName", &ItemTypeWrap::GetSkillName)

		.addFunction("HasEnchantment", &ItemTypeWrap::HasEnchantment)

		.addFunction("SetWeight", &ItemTypeWrap::SetWeight)
		.addFunction("SetGoldValue", &ItemTypeWrap::SetGoldValue)
		.addFunction("SetDamage", &ItemTypeWrap::SetDamage)
		.addFunction("SetArmorRating", &ItemTypeWrap::SetArmorRating)
		.addFunction("SetEnchantment", &ItemTypeWrap::SetEnchantment)
		.addFunction("SetHealth", &ItemTypeWrap::SetHealth)

		.addFunction("GetNthEffect", &ItemTypeWrap::GetNthEffect)
		.addFunction("GetNthEffectIdentifier", &ItemTypeWrap::GetNthEffectIdentifier)
		.addFunction("GetNthEffectMagnitude", &ItemTypeWrap::GetNthEffectMagnitude)
		.addFunction("GetNthEffectDuration", &ItemTypeWrap::GetNthEffectDuration)
		.addFunction("GetNthEffectArea", &ItemTypeWrap::GetNthEffectArea)
		.addFunction("GetNumEffects", &ItemTypeWrap::GetNumEffects)
		.addFunction("AddEffect", &ItemTypeWrap::AddEffect)

		.addFunction("GetSoulSize", &ItemTypeWrap::GetSoulSize)
		.addFunction("SetSoulSize", &ItemTypeWrap::SetSoulSize)
		.addFunction("GetCapacity", &ItemTypeWrap::GetCapacity)
		.addFunction("SetCapacity", &ItemTypeWrap::SetCapacity)

		.addStaticFunction("Create", &ItemTypeWrap::Create)
		.addStaticFunction("LookupByIdentifier", &ItemTypeWrap::LookupByIdentifier)
		.addStaticFunction("Clone", &ItemTypeWrap::Clone)
		.endClass();
}