#pragma once
#include "ItemTypeWrap.h"
#include "Container.h"
#include "KeywordList.h"

class RecipeWrap
{
public:
	explicit RecipeWrap(uint32_t id);
	bool operator==(const RecipeWrap &rhs) const noexcept;
	bool operator!=(const RecipeWrap &rhs) const noexcept {
		return !(*this == rhs);
	}

	uint32_t GetID() const noexcept;
	std::string GetWorkbenchKeyword() const noexcept;
	ItemTypeWrap GetProduce() const noexcept;
	uint32_t GetProduceCount() const noexcept;
	
	static luabridge::LuaRef Create(std::string workbenchKeyword, luabridge::LuaRef result, uint32_t resultCount) noexcept;
	static luabridge::LuaRef LookupByID(uint32_t id) noexcept;

	// Non-Api:
	Components::Container GetContainer() const noexcept;
	static std::list<RecipeWrap> GetAllRecipes() noexcept;
	static size_t GetNumRecipes() noexcept;

	void AddItem(luabridge::LuaRef itemType, uint32_t count) noexcept;
	bool RemoveItem(luabridge::LuaRef itemType, uint32_t count) noexcept;
	void RemoveAllItems() noexcept;

	static void Register(lua_State *state);

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;

	friend bool operator <(const RecipeWrap &lhs, const RecipeWrap &rhs) {
		return lhs.GetID() < rhs.GetID();
	}
};

struct RecipeInfo
{
	nonstd::optional<ItemTypeWrap> produce;
	uint32_t produceCount;
	Components::Keyword wbKeyword;
	Components::Container components;

	RecipeInfo() {
		this->produceCount = 0;
		this->wbKeyword = (Components::Keyword)"";
	}

	static RecipeInfo Get(const RecipeWrap &src)
	{
		RecipeInfo r;
		r.produce = src.GetProduce();
		r.produceCount = src.GetProduceCount();
		r.wbKeyword = src.GetWorkbenchKeyword();
		r.components = src.GetContainer();
		return r;
	}

	friend bool operator==(const RecipeInfo &lhs, const RecipeInfo &rhs) {
		return lhs.produce == rhs.produce && lhs.produceCount == rhs.produceCount && lhs.wbKeyword == rhs.wbKeyword && lhs.components.GetItems() == rhs.components.GetItems();
	}

	friend bool operator!=(const RecipeInfo &lhs, const RecipeInfo &rhs) {
		return !(lhs == rhs);
	}
};