#include "stdafx.h"
#include "PerkWrap.h"

struct Perk
{
	uint32_t formID;
	uint32_t requiredPerkFormID;
	uint32_t requiredSkillLevel;
	std::string skillName;
	bool playable = false;
};

std::map<uint32_t, Perk *> createdPerks;

struct PerkWrap::Impl
{
	Perk *p = nullptr;
};

PerkWrap::PerkWrap(uint32_t formID) : pImpl(new Impl)
{
	try {
		pImpl->p = createdPerks.at(formID);
		return;
	}
	catch (...) {
	}
	throw std::runtime_error("unable to create wrap for Perk " + std::to_string(formID));
}

bool PerkWrap::operator==(const PerkWrap &rhs) const noexcept 
{
	return this->pImpl->p == rhs.pImpl->p;
}

uint32_t PerkWrap::GetID() const noexcept
{
	return pImpl->p->formID;
}

std::string PerkWrap::GetSkill() const noexcept
{
	return pImpl->p->skillName;
}

float PerkWrap::GetSkillLevel() const noexcept
{
	return (float)pImpl->p->requiredSkillLevel;
}

luabridge::LuaRef PerkWrap::GetRequiredPerk() const noexcept
{
	luabridge::LuaRef res(g.gamemode->GetState());
	res = luabridge::Nil();

	if (pImpl->p->requiredPerkFormID != 0)
	{
		try {
			res = PerkWrap(pImpl->p->requiredPerkFormID);
		}
		catch (...) {
		}
	}

	return res;
}

void PerkWrap::SetPlayable(bool v) noexcept
{
	pImpl->p->playable = v;
}

bool PerkWrap::IsPlayable() const noexcept
{
	return pImpl->p->playable;
}

luabridge::LuaRef PerkWrap::Create(uint32_t existingPerkID, std::string skillName, float requiredSkillLevel, luabridge::LuaRef optionalRequiredPerk) noexcept
{
	luabridge::LuaRef res(g.gamemode->GetState());

	if (createdPerks.count(existingPerkID) != 0)
		log(Log::Warning, "Perk with id ", existingPerkID, " already exist");
	else
	{
		auto perk = new Perk;
		perk->formID = existingPerkID;
		perk->requiredSkillLevel = (uint32_t)requiredSkillLevel;
		perk->skillName = skillName;
		createdPerks[existingPerkID] = perk;
		if (optionalRequiredPerk.isNil() == false)
		{
			try {
				perk->requiredPerkFormID = optionalRequiredPerk.cast<PerkWrap>().GetID();
			}
			catch (...) {
				log(Log::Warning, "Bad required perk for perk with id ", existingPerkID);
				perk->requiredPerkFormID = 0;
			}
		}
		res = PerkWrap(perk->formID);
	}

	return res;
}

luabridge::LuaRef PerkWrap::LookupByID(uint32_t id) noexcept
{
	luabridge::LuaRef res(g.gamemode->GetState());
	try {
		res = PerkWrap(id);
	}
	catch (...) {
		res = luabridge::Nil();
	}
	return res;
}

void PerkWrap::Register(lua_State *state)
{
	luabridge::getGlobalNamespace(state)
		.beginClass<PerkWrap>("Perk")
		.addFunction("__eq", &PerkWrap::operator==)
		.addFunction("GetID", &PerkWrap::GetID)
		.addFunction("GetSkill", &PerkWrap::GetSkill)
		.addFunction("GetSkillLevel", &PerkWrap::GetSkillLevel)
		.addFunction("GetRequiredPerk", &PerkWrap::GetRequiredPerk)
		.addFunction("SetPlayable", &PerkWrap::SetPlayable)
		.addFunction("IsPlayable", &PerkWrap::IsPlayable)
		.addStaticFunction("Create", &PerkWrap::Create)
		.addStaticFunction("LookupByID", &PerkWrap::LookupByID)
		.endClass();
}