#pragma once

class Text3D;

using Text3Ds = std::map<uint32_t, std::shared_ptr<Text3D>>;

class Text3DManager : noncopyable
{
public:
	Text3DManager() noexcept;
	~Text3DManager() noexcept;

	Text3D *NewText3D(std::string text, uint32_t locationID, NiPoint3 pos, uint32_t virtualWorldID) noexcept;
	void DeleteText3D(uint32_t id) noexcept;
	Text3D *LookupText3D(uint32_t id) const noexcept;
	const Text3Ds &GetText3Ds() const noexcept;

private:
	uint32_t GenerateID() const noexcept;

	struct Impl;
	Impl *const pImpl;
};

class Text3D
{
	friend class Text3DManager;
public:
	~Text3D();

	uint32_t GetID() const noexcept;
	std::string GetText() const noexcept;
	uint32_t GetLocationID() const noexcept;
	NiPoint3 GetPos() const noexcept;
	uint32_t GetVirtualWorld() const noexcept;

	void SetText(std::string str) noexcept;
	void SetLocation(uint32_t id) noexcept;
	void SetPos(NiPoint3 pos) noexcept;
	void SetVirtualWorld(uint32_t worldID) noexcept;

private:
	Text3D(std::string text, uint32_t locationID, NiPoint3 pos, uint32_t virtualWorldID) noexcept;

	struct Impl;
	Impl *const pImpl;
};