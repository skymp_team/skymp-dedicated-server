#pragma once
#include "SpellWrap.h"

enum SoulSize : int32_t
{
	None,
	Petty,
	Lesser,
	Common,
	Greater,
	Grand,

	FRONT = None,
	BACK = Grand,
};

struct ItemTypeInfo;

class ItemTypeWrap
{
private:
	explicit ItemTypeWrap(const void *uniquePtr); // unsafe
public:
	explicit ItemTypeWrap(std::string identifier);
	~ItemTypeWrap() noexcept;

	bool operator==(const  ItemTypeWrap &rhs) const noexcept;
	bool operator!=(const  ItemTypeWrap &rhs) const noexcept {
		return !(*this == rhs);
	}
	std::string GetIdentifier() const noexcept;
	std::string GetClass() const noexcept;
	uint8_t GetClassID() const noexcept;
	std::string GetSubclass() const noexcept;
	uint8_t GetSubclassID() const noexcept;
	uint32_t GetExistingGameFormID() const noexcept;
	uint32_t GetBaseID() const noexcept { return this->GetExistingGameFormID(); }; // 1.0
	float GetWeight() const noexcept;
	uint32_t GetGoldValue() const noexcept;
	float GetDamage() const noexcept;
	float GetArmorRating() const noexcept;
	std::string GetSkillName() const noexcept; // 1.0
	luabridge::LuaRef GetEnchantment() const noexcept;
	float GetHealth() const noexcept;

	bool HasEnchantment() const noexcept {
		return this->GetEnchantment().isNil() == false;
	}

	void SetWeight(float weight) noexcept;
	void SetGoldValue(uint32_t goldValue) noexcept;
	void SetDamage(float damage) noexcept;
	void SetArmorRating(float armorRating) noexcept;
	void SetEnchantment(luabridge::LuaRef ench) noexcept;
	void SetHealth(float hp) noexcept;

	// Alchemy items

	luabridge::LuaRef GetNthEffect(uint32_t n) const noexcept;
	std::string GetNthEffectIdentifier(uint32_t n) const noexcept; // 1.0.9
	float GetNthEffectMagnitude(uint32_t n) const noexcept;
	float GetNthEffectDuration(uint32_t n) const noexcept;
	float GetNthEffectArea(uint32_t n) const noexcept;
	uint32_t GetNumEffects() const noexcept;

	void AddEffect(luabridge::LuaRef effect, float magnitude, float durationSec, float area) noexcept;

	// Soul Gem

	int32_t GetSoulSize() const noexcept;
	int32_t GetCapacity() const noexcept;
	void SetSoulSize(int32_t) noexcept;
	void SetCapacity(int32_t) noexcept;

	// Non-Api:
	void *GetUniquePtr() const noexcept;
	const ItemTypeInfo &GetInfo() const noexcept;
	size_t GetNumInfoChanges() const noexcept;
	static ItemTypeWrap LookupByUniquePtr(void *uniquePtr) noexcept; // unsafe
	static const ItemTypeInfo &LookupInfoByUniquePtr(void *uniquePtr) noexcept; // unsafe
	static const size_t GetNumInfoChangesByUniquePtr(void *uniquePtr) noexcept; // unsafe
	static std::string GetClassAndSubclassByID(uint8_t cl, uint8_t subCl) noexcept;
	static uint32_t GetGlobalNumChanges() noexcept;

	static luabridge::LuaRef Create(std::string identifier, std::string classAndSubclass, uint32_t existingGameFormID, float weight, uint32_t goldValue, luabridge::LuaRef customArg1, luabridge::LuaRef skillName) noexcept;
	static luabridge::LuaRef LookupByIdentifier(std::string identifier) noexcept; 
	static luabridge::LuaRef Clone(luabridge::LuaRef otherItemType, luabridge::LuaRef optionalNewItemTypeIdent) noexcept;

	static void Register(lua_State *state);

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;

	friend bool operator <(const ItemTypeWrap &lhs, const ItemTypeWrap &rhs) {
		return lhs.GetIdentifier() < rhs.GetIdentifier();
	}
};

struct ItemTypeInfo
{
	uint8_t classID = 0;
	uint8_t subclassID = 0;
	uint32_t existingItemID = 0;
	float weight = 0;
	uint32_t goldValue = 0;
	float armorRating = 0;
	float damage = 0;
	nonstd::optional<SpellWrap> ench;
	SoulSize soulSize = SoulSize::None, capacity = SoulSize::None;
	float health = 1.0;

	// Alchemy items
	using EffectItemInfo = SpellInfo::EffectItemInfo;
	std::vector<EffectItemInfo> effects;

	friend bool operator==(const ItemTypeInfo &lhs, const ItemTypeInfo &rhs) noexcept {
		return lhs.classID == rhs.classID
			&& lhs.subclassID == rhs.subclassID
			&& lhs.existingItemID == rhs.existingItemID
			&& lhs.weight == rhs.weight
			&& lhs.goldValue == rhs.goldValue
			&& lhs.armorRating == rhs.armorRating
			&& lhs.damage == rhs.damage
			&& lhs.effects == rhs.effects
			&& lhs.ench == rhs.ench
			&& lhs.soulSize == rhs.soulSize
			&& lhs.capacity == rhs.capacity
			&& lhs.health == rhs.health;
	}

	friend bool operator!=(const ItemTypeInfo &lhs, const ItemTypeInfo &rhs) noexcept {
		return !(lhs == rhs);
	}
};