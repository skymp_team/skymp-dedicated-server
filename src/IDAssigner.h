#pragma once

template <class T>
class IDAssigner : noncopyable
{
public:

	enum {
		Offset = 100000,
	};

	using id_t = uint32_t;

	static id_t GetID(const T &some) noexcept {
		auto ida = GetSingletone();
		try {
			return ida->IDByObj.at(some);
		}
		catch (...) {
			return GenerateID(some);
		}
	}

	// Unsafe to call twice on single T instance
	static id_t GenerateID(const T &some) noexcept {
		auto ida = GetSingletone();
		static id_t id = Offset;
		ida->objByID.push_back(some);
		ida->IDByObj.insert({ some, id });
		return id++;
	}

	static nonstd::optional<T> GetObj(id_t id) noexcept {
		try {
			return GetSingletone()->objByID.at(id - Offset);
		}
		catch (...) {
			return nonstd::nullopt;
		}
	}

	static size_t Size() noexcept {
		return GetSingletone()->objByID.size();
	}

private:
	IDAssigner() = default;

	static IDAssigner<T> *GetSingletone() noexcept {
		static IDAssigner<T> ida;
		return &ida;
	}

	std::vector<T> objByID;
	std::map<T, id_t> IDByObj;
};