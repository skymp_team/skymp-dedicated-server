#pragma once

#if defined _WIN32 || _WIN64
#pragma comment(lib, "Ws2_32.lib")
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif

#if defined _WIN32 || _WIN64
#define _CRT_SECURE_NO_WARNINGS
#endif

#define VETUKH_TEST false
#define VETUKH_LIMIT 8
#define MAX_PLAYERS (65536 / 4) // with npc
#define NUMI_TEST false

#include <stdio.h>

#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <iostream>
#include <fstream>
#include <memory>
#include <chrono>
#include <thread>
#include <mutex>
#include <locale>
#include <iosfwd>
#include <iomanip>
#include <regex>

#include <nonstd_optional.hpp>

class noncopyable
{
public:
	noncopyable() = default;
	~noncopyable() = default;

private:
	noncopyable(const noncopyable&) = delete;
	noncopyable& operator=(const noncopyable&) = delete;
};

using namespace std::literals::chrono_literals;
using namespace std::literals::string_literals;

constexpr struct
{
	const char *defaultLogFile = "server_log.txt";
	size_t maxServerPassword = 24;
} constant;

#include "Config.h"
#include "Log.h"
#include "Timers.h"
#include "Networking.h"
#include "GameMode.h"
#include "Player.h"
#include "Object.h"
#include "Tests.h"
#include "Pathing.h"
#include "Text3D.h"

struct GodObject
{
	std::string version;

	std::unique_ptr<ServerCfg> cfg;
	std::unique_ptr<Networking> networking;
	std::unique_ptr<GameMode> gamemode;
	std::unique_ptr<PlayerManager> plManager;
	std::unique_ptr<Timers> timers;
	std::unique_ptr<ObjectManager> objManager;
	std::unique_ptr<Tests> tests;
	std::unique_ptr<Pathing> pathing;
	std::unique_ptr<Text3DManager> t3dManager;
};

extern GodObject g;

std::string GetFileNameByPath(const std::string &path);

#include "NiPoint3.h"
#include "StringUtil.h"

#if defined _WIN32 || _WIN64
int setenv(const char *name, const char *value, int overwrite);
#else
#include <stdlib.h>
#endif

std::string current_directory();
void read_directory(const std::string& name, std::vector<std::string>& v);

template<class ... Ts>
inline void printfUnix(Ts... as)
{
#if defined _WIN32 || _WIN64
	// ...
#else
	printf(as...);
#endif
}
